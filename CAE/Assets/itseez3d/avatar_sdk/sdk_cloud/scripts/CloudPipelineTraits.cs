﻿using System.Collections.Generic;
using ItSeez3D.AvatarSdk.Core;

namespace ItSeez3D.AvatarSdk.Cloud.PipelineTraits
{
	public class CloudTraitsKeeper : IPipelineTraitsKeeper
	{
		public PipelineType DefaultPipeline
		{
			get
			{
				return PipelineType.FACE;
			}
		}

		public Dictionary<PipelineType, PipelineTypeTraits> GetTraits()
		{
			var result = new Dictionary<PipelineType, PipelineTypeTraits>();
			result.Add(PipelineType.BUST_2_0, new Bust2Traits());
			result.Add(PipelineType.HEAD_2_0, new Head2Traits());
			result.Add(PipelineType.FACE, new FaceTraits());
			result.Add(PipelineType.HEAD, new HeadTraits());
			result.Add(PipelineType.STYLED_FACE, new StyledFaceTraits());
			result.Add(PipelineType.UMA_MALE, new UmaMaleTraits());
			result.Add(PipelineType.UMA_FEMALE, new UmaFemaleTraits());
			return result;
		}
	}

	public class Head2Traits : Head2AbstractTraits
	{
		public sealed override string PipelineSubtypeName { get { return "head/mobile"; } }
		public sealed override string DisplayName { get { return "Head 2.0"; } }
		public sealed override PipelineType Type { get { return PipelineType.HEAD_2_0; } }
	}

	public class Bust2Traits : Head2AbstractTraits
	{
		public sealed override string PipelineSubtypeName { get { return "bust/mobile"; } }
		public sealed override string DisplayName { get { return "Bust 2.0"; } }
		public sealed override PipelineType Type { get { return PipelineType.BUST_2_0; } }
	}

	public class FaceTraits : PipelineTypeTraits
	{
		public sealed override string PipelineTypeName { get { return "animated_face"; } }
		public sealed override string PipelineSubtypeName { get { return "base/legacy"; } }
		public sealed override bool HaircutsSupported { get { return true; } }
		public sealed override string DisplayName { get { return "Animated Face"; } }
		public sealed override PipelineType Type { get { return PipelineType.FACE; } }
	}

	public class HeadTraits : PipelineTypeTraits
	{
		public override string PipelineTypeName { get { return "head_1.2"; } }
		public override string PipelineSubtypeName { get { return "base/legacy"; } }
		public override bool HaircutsSupported { get { return false; } }
		public override string DisplayName { get { return "Head 1.2"; } }
		public override PipelineType Type { get { return PipelineType.HEAD; } }
	}

	public class StyledFaceTraits : PipelineTypeTraits
	{
		public sealed override string PipelineTypeName { get { return "animated_face"; } }
		public sealed override string PipelineSubtypeName { get { return "indie/legacy_styled"; } }
		public sealed override bool HaircutsSupported { get { return true; } }
		public sealed override string DisplayName { get { return "Styled Face"; } }
		public sealed override PipelineType Type { get { return PipelineType.STYLED_FACE; } }
	}

	public class UmaMaleTraits : PipelineTypeTraits
	{
		public sealed override string PipelineTypeName { get { return "head_2.0"; } }
		public sealed override string PipelineSubtypeName { get { return "uma2/male"; } }
		public sealed override bool HaircutsSupported { get { return false; } }
		public sealed override string DisplayName { get { return "UMA Male"; } }
		public sealed override PipelineType Type { get { return PipelineType.UMA_MALE; } }
	}

	public class UmaFemaleTraits : PipelineTypeTraits
	{
		public sealed override string PipelineTypeName { get { return "head_2.0"; } }
		public sealed override string PipelineSubtypeName { get { return "uma2/female"; } }
		public sealed override bool HaircutsSupported { get { return false; } }
		public sealed override string DisplayName { get { return "UMA Female"; } }
		public sealed override PipelineType Type { get { return PipelineType.UMA_FEMALE; } }
	}
}
