﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMSocketIO;

public class FMWebSocketNetwork_debug : MonoBehaviour {

    public Text BodyPartsActived;
    public GameObject Brain;
    public GameObject Artery;
    public GameObject Vein;
    public GameObject Digestive_System;
    public GameObject Eyes;
    public GameObject Heart;
    public GameObject Kidneys;
    public GameObject Skeleton;
    public GameObject Muscle;
    public GameObject Lungs;
    public GameObject Skin;
    public GameObject Dress;
    public Toggle ToggleBrain;
    public Toggle ToggleArtery;
    public Toggle ToggleVein;
    public Toggle ToggleDisgestive_System;
    public Toggle ToggleEyes;
    public Toggle ToggleHeart;
    public Toggle ToggleKidneys;
    public Toggle ToggleSkeleton;
    public Toggle ToggleMuscle;
    public Toggle ToggleLungs;
    public Toggle ToggleSkin;
    public Toggle ToggleDress;

    public void Action_SendStringAll(string _string)
    {
        FMSocketIOManager.instance.SendToAll(_string);
    }
    public void Action_SendStringServer(string _string)
    {
        FMSocketIOManager.instance.SendToServer(_string);
    }

    public void Action_SendStringOthers(string _string)
    {
        FMSocketIOManager.instance.SendToOthers(_string);
    }

    public void Action_SendByteAll()
    {
        FMSocketIOManager.instance.SendToAll(new byte[3]);
    }
    public void Action_SendByteServer()
    {
        FMSocketIOManager.instance.SendToServer(new byte[4]);
    }
    public void Action_SendByteOthers()
    {
        FMSocketIOManager.instance.SendToOthers(new byte[5]);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Receive string to active the body part patient
    /// </summary>
    /// <param name="_string">The body part</param>
    public void Action_OnReceivedData(string _string)
    {
        if (BodyPartsActived.text.Contains(_string))
        {
            BodyPartsActived.text = BodyPartsActived.text.Replace(_string + ", ", string.Empty);
        }
        else
        {
            BodyPartsActived.text +=  _string + ", ";
        }
        
        // To do : // faire une méthode plus efficience (une boucle du Content?)
        Brain.SetActive(BodyPartsActived.text.Contains("Brain"));
        Artery.SetActive(BodyPartsActived.text.Contains("Artery"));
        Vein.SetActive(BodyPartsActived.text.Contains("Vein"));
        Digestive_System.SetActive(BodyPartsActived.text.Contains("Digestive_System"));
        Eyes.SetActive(BodyPartsActived.text.Contains("Eyes"));
        Heart.SetActive(BodyPartsActived.text.Contains("Heart"));
        Kidneys.SetActive(BodyPartsActived.text.Contains("Kidneys"));
        Skeleton.SetActive(BodyPartsActived.text.Contains("Skeleton"));
        Muscle.SetActive(BodyPartsActived.text.Contains("Muscle"));
        Lungs.SetActive(BodyPartsActived.text.Contains("Lungs"));
        Skin.SetActive(BodyPartsActived.text.Contains("Skin"));
        Dress.SetActive(BodyPartsActived.text.Contains("Dress"));

        // To do : // faire une méthode plus efficience (une boucle du Content?)
        ToggleBrain.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Brain"));
        ToggleArtery.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Artery"));
        ToggleVein.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Vein"));
        ToggleDisgestive_System.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Digestive_System"));
        ToggleEyes.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Eyes"));
        ToggleHeart.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Heart"));
        ToggleKidneys.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Kidneys"));
        ToggleSkeleton.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Skeleton"));
        ToggleMuscle.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Muscle"));
        ToggleLungs.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Lungs"));
        ToggleSkin.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Skin"));
        ToggleDress.SetIsOnWithoutNotify(BodyPartsActived.text.Contains("Dress"));
}

    public void Action_OnReceivedData(byte[] _byte)
    {
        //BodyPartsActived.text = "received(byte): " + _byte.Length;
    }
}
