﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000032 System.Exception System.Linq.Error::NoMatch()
extern void Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14 ();
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000039 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000046 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000047 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000049 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004A TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000004D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000050 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000051 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000052 TSource System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
// 0x00000053 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000054 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 ();
// 0x00000055 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000056 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000057 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000058 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000059 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000005A System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000005B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005D System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000005E System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005F System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000060 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000062 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000063 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000064 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000065 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000067 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000068 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000069 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000006D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000070 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000071 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000072 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000073 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000074 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000075 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000076 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000077 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000078 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007B System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007C System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000007D System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000080 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000081 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000082 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000083 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000084 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000085 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000086 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x00000087 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000088 TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000089 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000008A System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000008B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000008C System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008D System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x0000008E System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000008F System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x00000090 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x00000091 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x00000092 TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000093 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000094 System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000095 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000096 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000097 System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x00000098 System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x00000099 System.Boolean System.Linq.Enumerable_<IntersectIterator>d__74`1::MoveNext()
// 0x0000009A System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::<>m__Finally1()
// 0x0000009B TSource System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009C System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x0000009D System.Object System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x0000009E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009F System.Collections.IEnumerator System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A0 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x000000A1 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x000000A2 System.Boolean System.Linq.Enumerable_<ReverseIterator>d__79`1::MoveNext()
// 0x000000A3 TSource System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A4 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x000000A5 System.Object System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x000000A6 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A7 System.Collections.IEnumerator System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A8 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000A9 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000AA System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x000000AB System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x000000AC TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000AD System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000AE System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000AF System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000B0 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B1 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000B2 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000B3 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000B4 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x000000B5 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000B6 System.Void System.Linq.Set`1::Resize()
// 0x000000B7 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000B8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000B9 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000BA System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BB System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000BC System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000BD System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000C0 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000C1 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000C2 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000C3 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C4 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C5 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000C6 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000C7 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000C8 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000C9 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000CA System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000CB System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000CC System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000CD System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000CE TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000CF System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000D0 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D2 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000D3 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000D5 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000D7 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000D8 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000D9 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000DA System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000DB System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000DC System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000DD System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DE System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DF System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000E1 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000E5 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000E8 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000E9 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000EA System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000EB System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000EC System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000ED System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000EE System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000EF System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000F0 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000F1 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000F2 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000F3 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[243] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NoMatch_m96B9371C94C28A7C23CC8B8D25CC7B50734E1B14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[243] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	114,
	14,
	114,
	31,
	23,
	23,
	23,
	23,
	23,
	114,
	114,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	931,
	27,
	37,
	211,
	211,
	3,
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	94,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[63] = 
{
	{ 0x02000008, { 99, 4 } },
	{ 0x02000009, { 103, 9 } },
	{ 0x0200000A, { 114, 7 } },
	{ 0x0200000B, { 123, 10 } },
	{ 0x0200000C, { 135, 11 } },
	{ 0x0200000D, { 149, 9 } },
	{ 0x0200000E, { 161, 12 } },
	{ 0x0200000F, { 176, 1 } },
	{ 0x02000010, { 177, 2 } },
	{ 0x02000011, { 179, 8 } },
	{ 0x02000012, { 187, 12 } },
	{ 0x02000013, { 199, 12 } },
	{ 0x02000014, { 211, 6 } },
	{ 0x02000015, { 217, 6 } },
	{ 0x02000017, { 223, 8 } },
	{ 0x02000019, { 231, 3 } },
	{ 0x0200001A, { 236, 5 } },
	{ 0x0200001B, { 241, 7 } },
	{ 0x0200001C, { 248, 3 } },
	{ 0x0200001D, { 251, 7 } },
	{ 0x0200001E, { 258, 4 } },
	{ 0x0200001F, { 262, 34 } },
	{ 0x02000021, { 296, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 5 } },
	{ 0x06000036, { 25, 5 } },
	{ 0x06000037, { 30, 1 } },
	{ 0x06000038, { 31, 2 } },
	{ 0x06000039, { 33, 2 } },
	{ 0x0600003A, { 35, 2 } },
	{ 0x0600003B, { 37, 1 } },
	{ 0x0600003C, { 38, 1 } },
	{ 0x0600003D, { 39, 2 } },
	{ 0x0600003E, { 41, 1 } },
	{ 0x0600003F, { 42, 2 } },
	{ 0x06000040, { 44, 1 } },
	{ 0x06000041, { 45, 2 } },
	{ 0x06000042, { 47, 3 } },
	{ 0x06000043, { 50, 2 } },
	{ 0x06000044, { 52, 2 } },
	{ 0x06000045, { 54, 2 } },
	{ 0x06000046, { 56, 4 } },
	{ 0x06000047, { 60, 4 } },
	{ 0x06000048, { 64, 3 } },
	{ 0x06000049, { 67, 4 } },
	{ 0x0600004A, { 71, 3 } },
	{ 0x0600004B, { 74, 3 } },
	{ 0x0600004C, { 77, 3 } },
	{ 0x0600004D, { 80, 1 } },
	{ 0x0600004E, { 81, 3 } },
	{ 0x0600004F, { 84, 2 } },
	{ 0x06000050, { 86, 2 } },
	{ 0x06000051, { 88, 5 } },
	{ 0x06000052, { 93, 3 } },
	{ 0x06000053, { 96, 3 } },
	{ 0x06000064, { 112, 2 } },
	{ 0x06000069, { 121, 2 } },
	{ 0x0600006E, { 133, 2 } },
	{ 0x06000074, { 146, 3 } },
	{ 0x06000079, { 158, 3 } },
	{ 0x0600007E, { 173, 3 } },
	{ 0x060000BB, { 234, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[298] = 
{
	{ (Il2CppRGCTXDataType)2, 53443 },
	{ (Il2CppRGCTXDataType)3, 52203 },
	{ (Il2CppRGCTXDataType)2, 53444 },
	{ (Il2CppRGCTXDataType)2, 53445 },
	{ (Il2CppRGCTXDataType)3, 52204 },
	{ (Il2CppRGCTXDataType)2, 53446 },
	{ (Il2CppRGCTXDataType)2, 53447 },
	{ (Il2CppRGCTXDataType)3, 52205 },
	{ (Il2CppRGCTXDataType)2, 53448 },
	{ (Il2CppRGCTXDataType)3, 52206 },
	{ (Il2CppRGCTXDataType)2, 53449 },
	{ (Il2CppRGCTXDataType)3, 52207 },
	{ (Il2CppRGCTXDataType)2, 53450 },
	{ (Il2CppRGCTXDataType)2, 53451 },
	{ (Il2CppRGCTXDataType)3, 52208 },
	{ (Il2CppRGCTXDataType)2, 53452 },
	{ (Il2CppRGCTXDataType)2, 53453 },
	{ (Il2CppRGCTXDataType)3, 52209 },
	{ (Il2CppRGCTXDataType)2, 53454 },
	{ (Il2CppRGCTXDataType)3, 52210 },
	{ (Il2CppRGCTXDataType)2, 53455 },
	{ (Il2CppRGCTXDataType)3, 52211 },
	{ (Il2CppRGCTXDataType)3, 52212 },
	{ (Il2CppRGCTXDataType)2, 42100 },
	{ (Il2CppRGCTXDataType)3, 52213 },
	{ (Il2CppRGCTXDataType)2, 53456 },
	{ (Il2CppRGCTXDataType)3, 52214 },
	{ (Il2CppRGCTXDataType)3, 52215 },
	{ (Il2CppRGCTXDataType)2, 42107 },
	{ (Il2CppRGCTXDataType)3, 52216 },
	{ (Il2CppRGCTXDataType)3, 52217 },
	{ (Il2CppRGCTXDataType)2, 53457 },
	{ (Il2CppRGCTXDataType)3, 52218 },
	{ (Il2CppRGCTXDataType)2, 53458 },
	{ (Il2CppRGCTXDataType)3, 52219 },
	{ (Il2CppRGCTXDataType)2, 53459 },
	{ (Il2CppRGCTXDataType)3, 52220 },
	{ (Il2CppRGCTXDataType)3, 52221 },
	{ (Il2CppRGCTXDataType)3, 52222 },
	{ (Il2CppRGCTXDataType)2, 53460 },
	{ (Il2CppRGCTXDataType)3, 52223 },
	{ (Il2CppRGCTXDataType)3, 52224 },
	{ (Il2CppRGCTXDataType)2, 53461 },
	{ (Il2CppRGCTXDataType)3, 52225 },
	{ (Il2CppRGCTXDataType)3, 52226 },
	{ (Il2CppRGCTXDataType)2, 53462 },
	{ (Il2CppRGCTXDataType)3, 52227 },
	{ (Il2CppRGCTXDataType)2, 53463 },
	{ (Il2CppRGCTXDataType)3, 52228 },
	{ (Il2CppRGCTXDataType)3, 52229 },
	{ (Il2CppRGCTXDataType)2, 42145 },
	{ (Il2CppRGCTXDataType)3, 52230 },
	{ (Il2CppRGCTXDataType)2, 42146 },
	{ (Il2CppRGCTXDataType)3, 52231 },
	{ (Il2CppRGCTXDataType)2, 53464 },
	{ (Il2CppRGCTXDataType)3, 52232 },
	{ (Il2CppRGCTXDataType)2, 53465 },
	{ (Il2CppRGCTXDataType)2, 53466 },
	{ (Il2CppRGCTXDataType)2, 42150 },
	{ (Il2CppRGCTXDataType)2, 53467 },
	{ (Il2CppRGCTXDataType)2, 53468 },
	{ (Il2CppRGCTXDataType)2, 53469 },
	{ (Il2CppRGCTXDataType)2, 42152 },
	{ (Il2CppRGCTXDataType)2, 53470 },
	{ (Il2CppRGCTXDataType)2, 42154 },
	{ (Il2CppRGCTXDataType)2, 53471 },
	{ (Il2CppRGCTXDataType)3, 52233 },
	{ (Il2CppRGCTXDataType)2, 53472 },
	{ (Il2CppRGCTXDataType)2, 53473 },
	{ (Il2CppRGCTXDataType)2, 42157 },
	{ (Il2CppRGCTXDataType)2, 53474 },
	{ (Il2CppRGCTXDataType)2, 42159 },
	{ (Il2CppRGCTXDataType)2, 53475 },
	{ (Il2CppRGCTXDataType)3, 52234 },
	{ (Il2CppRGCTXDataType)2, 42162 },
	{ (Il2CppRGCTXDataType)2, 53476 },
	{ (Il2CppRGCTXDataType)3, 52235 },
	{ (Il2CppRGCTXDataType)2, 53477 },
	{ (Il2CppRGCTXDataType)2, 42165 },
	{ (Il2CppRGCTXDataType)2, 53478 },
	{ (Il2CppRGCTXDataType)2, 42167 },
	{ (Il2CppRGCTXDataType)2, 42169 },
	{ (Il2CppRGCTXDataType)2, 53479 },
	{ (Il2CppRGCTXDataType)3, 52236 },
	{ (Il2CppRGCTXDataType)2, 53480 },
	{ (Il2CppRGCTXDataType)2, 42172 },
	{ (Il2CppRGCTXDataType)2, 53481 },
	{ (Il2CppRGCTXDataType)3, 52237 },
	{ (Il2CppRGCTXDataType)3, 52238 },
	{ (Il2CppRGCTXDataType)2, 53482 },
	{ (Il2CppRGCTXDataType)2, 42176 },
	{ (Il2CppRGCTXDataType)2, 53483 },
	{ (Il2CppRGCTXDataType)2, 42178 },
	{ (Il2CppRGCTXDataType)2, 42179 },
	{ (Il2CppRGCTXDataType)2, 53484 },
	{ (Il2CppRGCTXDataType)3, 52239 },
	{ (Il2CppRGCTXDataType)2, 42182 },
	{ (Il2CppRGCTXDataType)2, 53485 },
	{ (Il2CppRGCTXDataType)3, 52240 },
	{ (Il2CppRGCTXDataType)3, 52241 },
	{ (Il2CppRGCTXDataType)3, 52242 },
	{ (Il2CppRGCTXDataType)2, 42188 },
	{ (Il2CppRGCTXDataType)3, 52243 },
	{ (Il2CppRGCTXDataType)3, 52244 },
	{ (Il2CppRGCTXDataType)2, 42200 },
	{ (Il2CppRGCTXDataType)2, 53486 },
	{ (Il2CppRGCTXDataType)3, 52245 },
	{ (Il2CppRGCTXDataType)3, 52246 },
	{ (Il2CppRGCTXDataType)2, 42202 },
	{ (Il2CppRGCTXDataType)2, 53250 },
	{ (Il2CppRGCTXDataType)3, 52247 },
	{ (Il2CppRGCTXDataType)3, 52248 },
	{ (Il2CppRGCTXDataType)2, 53487 },
	{ (Il2CppRGCTXDataType)3, 52249 },
	{ (Il2CppRGCTXDataType)3, 52250 },
	{ (Il2CppRGCTXDataType)2, 42212 },
	{ (Il2CppRGCTXDataType)2, 53488 },
	{ (Il2CppRGCTXDataType)3, 52251 },
	{ (Il2CppRGCTXDataType)3, 52252 },
	{ (Il2CppRGCTXDataType)3, 51277 },
	{ (Il2CppRGCTXDataType)3, 52253 },
	{ (Il2CppRGCTXDataType)2, 53489 },
	{ (Il2CppRGCTXDataType)3, 52254 },
	{ (Il2CppRGCTXDataType)3, 52255 },
	{ (Il2CppRGCTXDataType)2, 42224 },
	{ (Il2CppRGCTXDataType)2, 53490 },
	{ (Il2CppRGCTXDataType)3, 52256 },
	{ (Il2CppRGCTXDataType)3, 52257 },
	{ (Il2CppRGCTXDataType)3, 52258 },
	{ (Il2CppRGCTXDataType)3, 52259 },
	{ (Il2CppRGCTXDataType)3, 52260 },
	{ (Il2CppRGCTXDataType)3, 51283 },
	{ (Il2CppRGCTXDataType)3, 52261 },
	{ (Il2CppRGCTXDataType)2, 53491 },
	{ (Il2CppRGCTXDataType)3, 52262 },
	{ (Il2CppRGCTXDataType)3, 52263 },
	{ (Il2CppRGCTXDataType)2, 42237 },
	{ (Il2CppRGCTXDataType)2, 53492 },
	{ (Il2CppRGCTXDataType)3, 52264 },
	{ (Il2CppRGCTXDataType)3, 52265 },
	{ (Il2CppRGCTXDataType)2, 42239 },
	{ (Il2CppRGCTXDataType)2, 53493 },
	{ (Il2CppRGCTXDataType)3, 52266 },
	{ (Il2CppRGCTXDataType)3, 52267 },
	{ (Il2CppRGCTXDataType)2, 53494 },
	{ (Il2CppRGCTXDataType)3, 52268 },
	{ (Il2CppRGCTXDataType)3, 52269 },
	{ (Il2CppRGCTXDataType)2, 53495 },
	{ (Il2CppRGCTXDataType)3, 52270 },
	{ (Il2CppRGCTXDataType)3, 52271 },
	{ (Il2CppRGCTXDataType)2, 42254 },
	{ (Il2CppRGCTXDataType)2, 53496 },
	{ (Il2CppRGCTXDataType)3, 52272 },
	{ (Il2CppRGCTXDataType)3, 52273 },
	{ (Il2CppRGCTXDataType)3, 52274 },
	{ (Il2CppRGCTXDataType)3, 51294 },
	{ (Il2CppRGCTXDataType)2, 53497 },
	{ (Il2CppRGCTXDataType)3, 52275 },
	{ (Il2CppRGCTXDataType)3, 52276 },
	{ (Il2CppRGCTXDataType)2, 53498 },
	{ (Il2CppRGCTXDataType)3, 52277 },
	{ (Il2CppRGCTXDataType)3, 52278 },
	{ (Il2CppRGCTXDataType)2, 42270 },
	{ (Il2CppRGCTXDataType)2, 53499 },
	{ (Il2CppRGCTXDataType)3, 52279 },
	{ (Il2CppRGCTXDataType)3, 52280 },
	{ (Il2CppRGCTXDataType)3, 52281 },
	{ (Il2CppRGCTXDataType)3, 52282 },
	{ (Il2CppRGCTXDataType)3, 52283 },
	{ (Il2CppRGCTXDataType)3, 52284 },
	{ (Il2CppRGCTXDataType)3, 51300 },
	{ (Il2CppRGCTXDataType)2, 53500 },
	{ (Il2CppRGCTXDataType)3, 52285 },
	{ (Il2CppRGCTXDataType)3, 52286 },
	{ (Il2CppRGCTXDataType)2, 53501 },
	{ (Il2CppRGCTXDataType)3, 52287 },
	{ (Il2CppRGCTXDataType)3, 52288 },
	{ (Il2CppRGCTXDataType)3, 52289 },
	{ (Il2CppRGCTXDataType)3, 52290 },
	{ (Il2CppRGCTXDataType)3, 52291 },
	{ (Il2CppRGCTXDataType)2, 42304 },
	{ (Il2CppRGCTXDataType)2, 42299 },
	{ (Il2CppRGCTXDataType)3, 52292 },
	{ (Il2CppRGCTXDataType)2, 42298 },
	{ (Il2CppRGCTXDataType)2, 53502 },
	{ (Il2CppRGCTXDataType)3, 52293 },
	{ (Il2CppRGCTXDataType)3, 52294 },
	{ (Il2CppRGCTXDataType)3, 52295 },
	{ (Il2CppRGCTXDataType)3, 52296 },
	{ (Il2CppRGCTXDataType)2, 53503 },
	{ (Il2CppRGCTXDataType)3, 52297 },
	{ (Il2CppRGCTXDataType)2, 42317 },
	{ (Il2CppRGCTXDataType)2, 42309 },
	{ (Il2CppRGCTXDataType)3, 52298 },
	{ (Il2CppRGCTXDataType)3, 52299 },
	{ (Il2CppRGCTXDataType)2, 42308 },
	{ (Il2CppRGCTXDataType)2, 53504 },
	{ (Il2CppRGCTXDataType)3, 52300 },
	{ (Il2CppRGCTXDataType)3, 52301 },
	{ (Il2CppRGCTXDataType)3, 52302 },
	{ (Il2CppRGCTXDataType)2, 53505 },
	{ (Il2CppRGCTXDataType)3, 52303 },
	{ (Il2CppRGCTXDataType)2, 42330 },
	{ (Il2CppRGCTXDataType)2, 42322 },
	{ (Il2CppRGCTXDataType)3, 52304 },
	{ (Il2CppRGCTXDataType)3, 52305 },
	{ (Il2CppRGCTXDataType)3, 52306 },
	{ (Il2CppRGCTXDataType)2, 42321 },
	{ (Il2CppRGCTXDataType)2, 53506 },
	{ (Il2CppRGCTXDataType)3, 52307 },
	{ (Il2CppRGCTXDataType)3, 52308 },
	{ (Il2CppRGCTXDataType)2, 53507 },
	{ (Il2CppRGCTXDataType)3, 52309 },
	{ (Il2CppRGCTXDataType)2, 42334 },
	{ (Il2CppRGCTXDataType)2, 53508 },
	{ (Il2CppRGCTXDataType)3, 52310 },
	{ (Il2CppRGCTXDataType)3, 52311 },
	{ (Il2CppRGCTXDataType)3, 52312 },
	{ (Il2CppRGCTXDataType)2, 42344 },
	{ (Il2CppRGCTXDataType)3, 52313 },
	{ (Il2CppRGCTXDataType)2, 53509 },
	{ (Il2CppRGCTXDataType)3, 52314 },
	{ (Il2CppRGCTXDataType)3, 52315 },
	{ (Il2CppRGCTXDataType)3, 52316 },
	{ (Il2CppRGCTXDataType)2, 53510 },
	{ (Il2CppRGCTXDataType)2, 53511 },
	{ (Il2CppRGCTXDataType)3, 52317 },
	{ (Il2CppRGCTXDataType)3, 52318 },
	{ (Il2CppRGCTXDataType)2, 42360 },
	{ (Il2CppRGCTXDataType)3, 52319 },
	{ (Il2CppRGCTXDataType)2, 42361 },
	{ (Il2CppRGCTXDataType)2, 53512 },
	{ (Il2CppRGCTXDataType)3, 52320 },
	{ (Il2CppRGCTXDataType)3, 52321 },
	{ (Il2CppRGCTXDataType)2, 53513 },
	{ (Il2CppRGCTXDataType)3, 52322 },
	{ (Il2CppRGCTXDataType)2, 53514 },
	{ (Il2CppRGCTXDataType)3, 52323 },
	{ (Il2CppRGCTXDataType)3, 52324 },
	{ (Il2CppRGCTXDataType)3, 52325 },
	{ (Il2CppRGCTXDataType)2, 42380 },
	{ (Il2CppRGCTXDataType)3, 52326 },
	{ (Il2CppRGCTXDataType)2, 42388 },
	{ (Il2CppRGCTXDataType)3, 52327 },
	{ (Il2CppRGCTXDataType)2, 53515 },
	{ (Il2CppRGCTXDataType)2, 53516 },
	{ (Il2CppRGCTXDataType)3, 52328 },
	{ (Il2CppRGCTXDataType)3, 52329 },
	{ (Il2CppRGCTXDataType)3, 52330 },
	{ (Il2CppRGCTXDataType)3, 52331 },
	{ (Il2CppRGCTXDataType)3, 52332 },
	{ (Il2CppRGCTXDataType)3, 52333 },
	{ (Il2CppRGCTXDataType)2, 42404 },
	{ (Il2CppRGCTXDataType)2, 53517 },
	{ (Il2CppRGCTXDataType)3, 52334 },
	{ (Il2CppRGCTXDataType)3, 52335 },
	{ (Il2CppRGCTXDataType)2, 42408 },
	{ (Il2CppRGCTXDataType)3, 52336 },
	{ (Il2CppRGCTXDataType)2, 53518 },
	{ (Il2CppRGCTXDataType)2, 42418 },
	{ (Il2CppRGCTXDataType)2, 42416 },
	{ (Il2CppRGCTXDataType)2, 53519 },
	{ (Il2CppRGCTXDataType)3, 52337 },
	{ (Il2CppRGCTXDataType)2, 53520 },
	{ (Il2CppRGCTXDataType)3, 52338 },
	{ (Il2CppRGCTXDataType)3, 52339 },
	{ (Il2CppRGCTXDataType)2, 42425 },
	{ (Il2CppRGCTXDataType)3, 52340 },
	{ (Il2CppRGCTXDataType)2, 42425 },
	{ (Il2CppRGCTXDataType)3, 52341 },
	{ (Il2CppRGCTXDataType)2, 42442 },
	{ (Il2CppRGCTXDataType)3, 52342 },
	{ (Il2CppRGCTXDataType)3, 52343 },
	{ (Il2CppRGCTXDataType)3, 52344 },
	{ (Il2CppRGCTXDataType)2, 53521 },
	{ (Il2CppRGCTXDataType)3, 52345 },
	{ (Il2CppRGCTXDataType)3, 52346 },
	{ (Il2CppRGCTXDataType)3, 52347 },
	{ (Il2CppRGCTXDataType)2, 42422 },
	{ (Il2CppRGCTXDataType)3, 52348 },
	{ (Il2CppRGCTXDataType)3, 52349 },
	{ (Il2CppRGCTXDataType)2, 42427 },
	{ (Il2CppRGCTXDataType)3, 52350 },
	{ (Il2CppRGCTXDataType)1, 53522 },
	{ (Il2CppRGCTXDataType)2, 42426 },
	{ (Il2CppRGCTXDataType)3, 52351 },
	{ (Il2CppRGCTXDataType)1, 42426 },
	{ (Il2CppRGCTXDataType)1, 42422 },
	{ (Il2CppRGCTXDataType)2, 53521 },
	{ (Il2CppRGCTXDataType)2, 42426 },
	{ (Il2CppRGCTXDataType)2, 42424 },
	{ (Il2CppRGCTXDataType)2, 42428 },
	{ (Il2CppRGCTXDataType)3, 52352 },
	{ (Il2CppRGCTXDataType)3, 52353 },
	{ (Il2CppRGCTXDataType)3, 52354 },
	{ (Il2CppRGCTXDataType)2, 42423 },
	{ (Il2CppRGCTXDataType)3, 52355 },
	{ (Il2CppRGCTXDataType)2, 42438 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	243,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	63,
	s_rgctxIndices,
	298,
	s_rgctxValues,
	NULL,
};
