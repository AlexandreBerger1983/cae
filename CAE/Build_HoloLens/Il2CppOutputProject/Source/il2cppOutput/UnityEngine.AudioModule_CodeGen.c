﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.AudioConfiguration UnityEngine.AudioSettings::GetConfiguration()
extern void AudioSettings_GetConfiguration_mC8A1E2FE7BA6707FE3741F0050D5A9E0D550B0D2 ();
// 0x00000002 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern void AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7 ();
// 0x00000003 System.Void UnityEngine.AudioSettings::GetConfiguration_Injected(UnityEngine.AudioConfiguration&)
extern void AudioSettings_GetConfiguration_Injected_m3774F16D453E14B702A07734497182451999B32B ();
// 0x00000004 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern void AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A ();
// 0x00000005 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern void AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49 ();
// 0x00000006 System.IAsyncResult UnityEngine.AudioSettings_AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827 ();
// 0x00000007 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern void AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830 ();
// 0x00000008 System.Void UnityEngine.AudioClip::.ctor()
extern void AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D ();
// 0x00000009 System.Boolean UnityEngine.AudioClip::GetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128 ();
// 0x0000000A UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
extern void AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0 ();
// 0x0000000B System.String UnityEngine.AudioClip::GetName()
extern void AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257 ();
// 0x0000000C System.Void UnityEngine.AudioClip::CreateUserSound(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD ();
// 0x0000000D System.Single UnityEngine.AudioClip::get_length()
extern void AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317 ();
// 0x0000000E System.Int32 UnityEngine.AudioClip::get_samples()
extern void AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0 ();
// 0x0000000F System.Int32 UnityEngine.AudioClip::get_channels()
extern void AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC ();
// 0x00000010 System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern void AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0 ();
// 0x00000011 UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.AudioClip_PCMReaderCallback,UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995 ();
// 0x00000012 System.Void UnityEngine.AudioClip::add_m_PCMReaderCallback(UnityEngine.AudioClip_PCMReaderCallback)
extern void AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62 ();
// 0x00000013 System.Void UnityEngine.AudioClip::remove_m_PCMReaderCallback(UnityEngine.AudioClip_PCMReaderCallback)
extern void AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7 ();
// 0x00000014 System.Void UnityEngine.AudioClip::add_m_PCMSetPositionCallback(UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5 ();
// 0x00000015 System.Void UnityEngine.AudioClip::remove_m_PCMSetPositionCallback(UnityEngine.AudioClip_PCMSetPositionCallback)
extern void AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2 ();
// 0x00000016 System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern void AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9 ();
// 0x00000017 System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern void AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F ();
// 0x00000018 System.Void UnityEngine.AudioClip_PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern void PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6 ();
// 0x00000019 System.Void UnityEngine.AudioClip_PCMReaderCallback::Invoke(System.Single[])
extern void PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6 ();
// 0x0000001A System.IAsyncResult UnityEngine.AudioClip_PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F ();
// 0x0000001B System.Void UnityEngine.AudioClip_PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern void PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B ();
// 0x0000001C System.Void UnityEngine.AudioClip_PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern void PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F ();
// 0x0000001D System.Void UnityEngine.AudioClip_PCMSetPositionCallback::Invoke(System.Int32)
extern void PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C ();
// 0x0000001E System.IAsyncResult UnityEngine.AudioClip_PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093 ();
// 0x0000001F System.Void UnityEngine.AudioClip_PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern void PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C ();
// 0x00000020 System.Void UnityEngine.AudioSource::SetPitch(UnityEngine.AudioSource,System.Single)
extern void AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B ();
// 0x00000021 System.Void UnityEngine.AudioSource::PlayHelper(UnityEngine.AudioSource,System.UInt64)
extern void AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC ();
// 0x00000022 System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioSource,UnityEngine.AudioClip,System.Single)
extern void AudioSource_PlayOneShotHelper_mDAEDF5E0C56C665DE62CDF26E7B98149B4A71704 ();
// 0x00000023 System.Void UnityEngine.AudioSource::Stop(System.Boolean)
extern void AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8 ();
// 0x00000024 System.Single UnityEngine.AudioSource::get_volume()
extern void AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0 ();
// 0x00000025 System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern void AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06 ();
// 0x00000026 System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern void AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0 ();
// 0x00000027 UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern void AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643 ();
// 0x00000028 System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B ();
// 0x00000029 System.Void UnityEngine.AudioSource::Play()
extern void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 ();
// 0x0000002A System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern void AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD ();
// 0x0000002B System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern void AudioSource_PlayOneShot_mA65B809A4480039CD8337ABF45C0E57E137EED41 ();
// 0x0000002C System.Void UnityEngine.AudioSource::Stop()
extern void AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200 ();
// 0x0000002D System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern void AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4 ();
// 0x0000002E System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern void AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128 ();
// 0x0000002F System.Single UnityEngine.AudioLowPassFilter::get_cutoffFrequency()
extern void AudioLowPassFilter_get_cutoffFrequency_mC6A24AC159FC3BF1B52F19B24E04AF0712682433 ();
// 0x00000030 System.Void UnityEngine.AudioLowPassFilter::set_cutoffFrequency(System.Single)
extern void AudioLowPassFilter_set_cutoffFrequency_m8F9D885ADC292AF38492FE53A44CDB31D6BD0AF6 ();
// 0x00000031 System.Single UnityEngine.AudioHighPassFilter::get_cutoffFrequency()
extern void AudioHighPassFilter_get_cutoffFrequency_m0733A348725BAAD6E72E4EB47C9900E4496FB917 ();
// 0x00000032 System.Void UnityEngine.AudioHighPassFilter::set_cutoffFrequency(System.Single)
extern void AudioHighPassFilter_set_cutoffFrequency_m6D4DFA0033853F8C1E7B9B49153E12BFD1E4D35A ();
// 0x00000033 System.Int32 UnityEngine.Microphone::GetMicrophoneDeviceIDFromName(System.String)
extern void Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E ();
// 0x00000034 UnityEngine.AudioClip UnityEngine.Microphone::StartRecord(System.Int32,System.Boolean,System.Single,System.Int32)
extern void Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8 ();
// 0x00000035 System.Void UnityEngine.Microphone::EndRecord(System.Int32)
extern void Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733 ();
// 0x00000036 System.Boolean UnityEngine.Microphone::IsRecording(System.Int32)
extern void Microphone_IsRecording_m70F5B087E69C0077AA9238931776EDE123FCF122 ();
// 0x00000037 System.Int32 UnityEngine.Microphone::GetRecordPosition(System.Int32)
extern void Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED ();
// 0x00000038 System.Void UnityEngine.Microphone::GetDeviceCaps(System.Int32,System.Int32&,System.Int32&)
extern void Microphone_GetDeviceCaps_m4592CD4A6371544156A042579626B9351DCD7E93 ();
// 0x00000039 UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern void Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047 ();
// 0x0000003A System.Void UnityEngine.Microphone::End(System.String)
extern void Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F ();
// 0x0000003B System.Boolean UnityEngine.Microphone::IsRecording(System.String)
extern void Microphone_IsRecording_m2E3373AD36865B7261BBA5E9140271A08E0FA004 ();
// 0x0000003C System.Int32 UnityEngine.Microphone::GetPosition(System.String)
extern void Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF ();
// 0x0000003D System.Void UnityEngine.Microphone::GetDeviceCaps(System.String,System.Int32&,System.Int32&)
extern void Microphone_GetDeviceCaps_mF079FFC698AE94F132D4E0AD072498F6937BAF6B ();
// 0x0000003E UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern void AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk ();
// 0x0000003F System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern void AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk ();
// 0x00000040 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern void AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk ();
// 0x00000041 System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern void AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk ();
// 0x00000042 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::Finalize()
extern void AudioSampleProvider_Finalize_mACCDEE1F5F0F602DC35AE68875EB8DB16830544C ();
// 0x00000043 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::Dispose()
extern void AudioSampleProvider_Dispose_mA11D514354EDA94E8B6AA83B726DF85169C8E29A ();
// 0x00000044 System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider::get_id()
extern void AudioSampleProvider_get_id_m9E16910C6A2335F87E6B257D3059A8BBC8D7253A ();
// 0x00000045 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::set_id(System.UInt32)
extern void AudioSampleProvider_set_id_mB98E8435407CAD305BBEAA91B18CBC12562DFAF9 ();
// 0x00000046 UnityEngine.Object UnityEngine.Experimental.Audio.AudioSampleProvider::get_owner()
extern void AudioSampleProvider_get_owner_m822057CE02CF8664D7BF569CDB41D8EB154251FE ();
// 0x00000047 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::set_owner(UnityEngine.Object)
extern void AudioSampleProvider_set_owner_m21FC4073346BFBE33E2B1567ECEC8ECD373B3069 ();
// 0x00000048 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832 ();
// 0x00000049 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4 ();
// 0x0000004A System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InternalRemove(System.UInt32)
extern void AudioSampleProvider_InternalRemove_m3CD8E2D57A6DC5522C71436A854926C346CEBF11 ();
// 0x0000004B System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InternalSetScriptingPtr(System.UInt32,UnityEngine.Experimental.Audio.AudioSampleProvider)
extern void AudioSampleProvider_InternalSetScriptingPtr_m018841DA2C6D92E2322F1D041E4C88A054991AF9 ();
// 0x0000004C System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::.ctor(System.Object,System.IntPtr)
extern void ConsumeSampleFramesNativeFunction__ctor_m10D2C92ADC2CC0F2731CC790036CF84E7E6E97F6 ();
// 0x0000004D System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::Invoke(System.UInt32,System.IntPtr,System.UInt32)
extern void ConsumeSampleFramesNativeFunction_Invoke_mA644FB7343047BEC754D81AC2AFABDC300DF5D74 ();
// 0x0000004E System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::BeginInvoke(System.UInt32,System.IntPtr,System.UInt32,System.AsyncCallback,System.Object)
extern void ConsumeSampleFramesNativeFunction_BeginInvoke_mCFD45D186107B1FD87B1A492904CBCF49DDA28CB ();
// 0x0000004F System.UInt32 UnityEngine.Experimental.Audio.AudioSampleProvider_ConsumeSampleFramesNativeFunction::EndInvoke(System.IAsyncResult)
extern void ConsumeSampleFramesNativeFunction_EndInvoke_m4F91AA6FC2DFEF1495E3BA8D7BF1098C9E043711 ();
// 0x00000050 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::.ctor(System.Object,System.IntPtr)
extern void SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5 ();
// 0x00000051 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32)
extern void SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD ();
// 0x00000052 System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object)
extern void SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92 ();
// 0x00000053 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::EndInvoke(System.IAsyncResult)
extern void SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28 ();
// 0x00000054 System.String UnityEngine.WebCamDevice::get_name()
extern void WebCamDevice_get_name_m4D7362BB29DC20B7C8EF47759A09D54DEE8031F7_AdjustorThunk ();
// 0x00000055 System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern void WebCamDevice_get_isFrontFacing_mD55FF74A2CE25897AD77EAB5935B6A76AD929D38_AdjustorThunk ();
// 0x00000056 System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern void WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61 ();
// 0x00000057 System.Void UnityEngine.WebCamTexture::.ctor()
extern void WebCamTexture__ctor_mA132E1976B248264D5AD01A1D45254FCF070D241 ();
// 0x00000058 System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern void WebCamTexture_Internal_CreateWebCamTexture_mE80CEFDA08815EEF5581C8385462887A62D84BB5 ();
// 0x00000059 System.Void UnityEngine.WebCamTexture::Play()
extern void WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436 ();
// 0x0000005A System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern void WebCamTexture_INTERNAL_CALL_Play_mF95EBF45A6EE05B6FCA20EAC623542A046013801 ();
// 0x0000005B System.Void UnityEngine.WebCamTexture::Stop()
extern void WebCamTexture_Stop_m4E3BD56B6481E2A0D53707119CFCB6074941B447 ();
// 0x0000005C System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern void WebCamTexture_INTERNAL_CALL_Stop_mC6D348860AD31662050913A41646C8261511E621 ();
// 0x0000005D System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern void WebCamTexture_get_isPlaying_m0BF473554164B2C30475CCD8C6BC1B93E9B52E67 ();
// 0x0000005E System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
extern void WebCamTexture_set_requestedFPS_mEEA829DEFFB545A53D3DB35B319EAB13E758E2F5 ();
// 0x0000005F UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern void WebCamTexture_get_devices_mF5D7FA78E9C67ADCBF592220A10F4B6678F1A920 ();
// 0x00000060 System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
extern void WebCamTexture_get_videoRotationAngle_m02878E5708942CE6149A57E6E10C453358D2B2A9 ();
// 0x00000061 System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
extern void WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD ();
static Il2CppMethodPointer s_methodPointers[97] = 
{
	AudioSettings_GetConfiguration_mC8A1E2FE7BA6707FE3741F0050D5A9E0D550B0D2,
	AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7,
	AudioSettings_GetConfiguration_Injected_m3774F16D453E14B702A07734497182451999B32B,
	AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A,
	AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49,
	AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827,
	AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830,
	AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D,
	AudioClip_GetData_mEF59EDCD10F83C280DB82E14FF938FF574BBD128,
	AudioClip_Construct_Internal_mEEA2165F0467FA67FA05361A85434DF5068E79B0,
	AudioClip_GetName_mD69F28B33950F21E36A2F3A659DD89725796D257,
	AudioClip_CreateUserSound_m56B1F9909AB1F6CABCA8C526657F99A8BC0E73BD,
	AudioClip_get_length_mFF1E21363B1860453451C4DA1C1459E9B9504317,
	AudioClip_get_samples_m7AD532D9288680102A452D2949107BDA88268CA0,
	AudioClip_get_channels_m2CF01E121CEBBF3B69EC7EEE7EC28172AB6078EC,
	AudioClip_GetData_m8150E67D6068CAA88BE4155CB5924B2359272EE0,
	AudioClip_Create_m7D20A6B52ACA39506B505EF162AF7EF81B173995,
	AudioClip_add_m_PCMReaderCallback_mE70789B25C74C552769FB2DAB96FC416A8508C62,
	AudioClip_remove_m_PCMReaderCallback_m0AEFEC9C086DEBABE06F1294AFE0B4031D805FE7,
	AudioClip_add_m_PCMSetPositionCallback_mC0EBAE94510712D3E1481ED8B3FB7956169754F5,
	AudioClip_remove_m_PCMSetPositionCallback_m21FB4ECFD0CE6C754C5D3BBFDCE9AD21E6D074A2,
	AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9,
	AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F,
	PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6,
	PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6,
	PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F,
	PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B,
	PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F,
	PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C,
	PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093,
	PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C,
	AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B,
	AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC,
	AudioSource_PlayOneShotHelper_mDAEDF5E0C56C665DE62CDF26E7B98149B4A71704,
	AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8,
	AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0,
	AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06,
	AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0,
	AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643,
	AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B,
	AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164,
	AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD,
	AudioSource_PlayOneShot_mA65B809A4480039CD8337ABF45C0E57E137EED41,
	AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200,
	AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4,
	AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128,
	AudioLowPassFilter_get_cutoffFrequency_mC6A24AC159FC3BF1B52F19B24E04AF0712682433,
	AudioLowPassFilter_set_cutoffFrequency_m8F9D885ADC292AF38492FE53A44CDB31D6BD0AF6,
	AudioHighPassFilter_get_cutoffFrequency_m0733A348725BAAD6E72E4EB47C9900E4496FB917,
	AudioHighPassFilter_set_cutoffFrequency_m6D4DFA0033853F8C1E7B9B49153E12BFD1E4D35A,
	Microphone_GetMicrophoneDeviceIDFromName_m08C8735264E4D5FEAED224C8E7957CF5916D9A5E,
	Microphone_StartRecord_m88F707BD42E1494928AA8D2E915062826E9021E8,
	Microphone_EndRecord_m6DDB48A30B1A34A6E996A1A1FC194DA184B46733,
	Microphone_IsRecording_m70F5B087E69C0077AA9238931776EDE123FCF122,
	Microphone_GetRecordPosition_m39EF5B7BA939CBE97875B9DA5265D8036F5D13ED,
	Microphone_GetDeviceCaps_m4592CD4A6371544156A042579626B9351DCD7E93,
	Microphone_Start_mF756A7EBA3E62EF0D138A220482B725D16E96047,
	Microphone_End_m2E3D0E4890AE014AF687987F6160CA3D5ACDC29F,
	Microphone_IsRecording_m2E3373AD36865B7261BBA5E9140271A08E0FA004,
	Microphone_GetPosition_m1C177D77958EB1BBADE1EEBB721428059B14A7FF,
	Microphone_GetDeviceCaps_mF079FFC698AE94F132D4E0AD072498F6937BAF6B,
	AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk,
	AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk,
	AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk,
	AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk,
	AudioSampleProvider_Finalize_mACCDEE1F5F0F602DC35AE68875EB8DB16830544C,
	AudioSampleProvider_Dispose_mA11D514354EDA94E8B6AA83B726DF85169C8E29A,
	AudioSampleProvider_get_id_m9E16910C6A2335F87E6B257D3059A8BBC8D7253A,
	AudioSampleProvider_set_id_mB98E8435407CAD305BBEAA91B18CBC12562DFAF9,
	AudioSampleProvider_get_owner_m822057CE02CF8664D7BF569CDB41D8EB154251FE,
	AudioSampleProvider_set_owner_m21FC4073346BFBE33E2B1567ECEC8ECD373B3069,
	AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832,
	AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4,
	AudioSampleProvider_InternalRemove_m3CD8E2D57A6DC5522C71436A854926C346CEBF11,
	AudioSampleProvider_InternalSetScriptingPtr_m018841DA2C6D92E2322F1D041E4C88A054991AF9,
	ConsumeSampleFramesNativeFunction__ctor_m10D2C92ADC2CC0F2731CC790036CF84E7E6E97F6,
	ConsumeSampleFramesNativeFunction_Invoke_mA644FB7343047BEC754D81AC2AFABDC300DF5D74,
	ConsumeSampleFramesNativeFunction_BeginInvoke_mCFD45D186107B1FD87B1A492904CBCF49DDA28CB,
	ConsumeSampleFramesNativeFunction_EndInvoke_m4F91AA6FC2DFEF1495E3BA8D7BF1098C9E043711,
	SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5,
	SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD,
	SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92,
	SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28,
	WebCamDevice_get_name_m4D7362BB29DC20B7C8EF47759A09D54DEE8031F7_AdjustorThunk,
	WebCamDevice_get_isFrontFacing_mD55FF74A2CE25897AD77EAB5935B6A76AD929D38_AdjustorThunk,
	WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61,
	WebCamTexture__ctor_mA132E1976B248264D5AD01A1D45254FCF070D241,
	WebCamTexture_Internal_CreateWebCamTexture_mE80CEFDA08815EEF5581C8385462887A62D84BB5,
	WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436,
	WebCamTexture_INTERNAL_CALL_Play_mF95EBF45A6EE05B6FCA20EAC623542A046013801,
	WebCamTexture_Stop_m4E3BD56B6481E2A0D53707119CFCB6074941B447,
	WebCamTexture_INTERNAL_CALL_Stop_mC6D348860AD31662050913A41646C8261511E621,
	WebCamTexture_get_isPlaying_m0BF473554164B2C30475CCD8C6BC1B93E9B52E67,
	WebCamTexture_set_requestedFPS_mEEA829DEFFB545A53D3DB35B319EAB13E758E2F5,
	WebCamTexture_get_devices_mF5D7FA78E9C67ADCBF592220A10F4B6678F1A920,
	WebCamTexture_get_videoRotationAngle_m02878E5708942CE6149A57E6E10C453358D2B2A9,
	WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD,
};
static const int32_t s_InvokerIndices[97] = 
{
	1912,
	856,
	17,
	132,
	31,
	1913,
	26,
	23,
	1914,
	4,
	14,
	1915,
	725,
	10,
	10,
	147,
	1916,
	26,
	26,
	26,
	26,
	26,
	32,
	132,
	26,
	218,
	26,
	132,
	32,
	587,
	26,
	1627,
	166,
	1917,
	31,
	725,
	335,
	335,
	14,
	26,
	23,
	26,
	949,
	23,
	31,
	31,
	725,
	335,
	725,
	335,
	94,
	1918,
	178,
	46,
	21,
	904,
	1919,
	168,
	109,
	94,
	457,
	1371,
	1920,
	1371,
	1921,
	23,
	23,
	10,
	32,
	14,
	26,
	32,
	32,
	178,
	574,
	132,
	1922,
	1923,
	121,
	132,
	138,
	148,
	26,
	14,
	89,
	206,
	23,
	1924,
	23,
	168,
	23,
	168,
	89,
	335,
	4,
	10,
	89,
};
extern const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule = 
{
	"UnityEngine.AudioModule.dll",
	97,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
