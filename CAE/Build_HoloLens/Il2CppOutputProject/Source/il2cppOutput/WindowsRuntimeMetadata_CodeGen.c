﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Windows.Foundation.AsyncActionCompletedHandler::.ctor(System.Object,System.IntPtr)
extern void AsyncActionCompletedHandler__ctor_m2C0D2BD025FC8CDC21086373277D42874552060D ();
// 0x00000002 System.Void Windows.Foundation.AsyncActionCompletedHandler::Invoke(Windows.Foundation.IAsyncAction,Windows.Foundation.AsyncStatus)
extern void AsyncActionCompletedHandler_Invoke_m90ED997977A5C94AE5F5CB92A629FE2EDFA3D466 ();
// 0x00000003 System.Void Windows.Foundation.AsyncOperationCompletedHandler`1::.ctor(System.Object,System.IntPtr)
// 0x00000004 System.Void Windows.Foundation.AsyncOperationCompletedHandler`1::Invoke(Windows.Foundation.IAsyncOperation`1<TResult>,Windows.Foundation.AsyncStatus)
// 0x00000005 System.Void Windows.Foundation.AsyncOperationProgressHandler`2::.ctor(System.Object,System.IntPtr)
// 0x00000006 System.Void Windows.Foundation.AsyncOperationProgressHandler`2::Invoke(Windows.Foundation.IAsyncOperationWithProgress`2<TResult,TProgress>,TProgress)
// 0x00000007 System.Void Windows.Foundation.AsyncOperationWithProgressCompletedHandler`2::.ctor(System.Object,System.IntPtr)
// 0x00000008 System.Void Windows.Foundation.AsyncOperationWithProgressCompletedHandler`2::Invoke(Windows.Foundation.IAsyncOperationWithProgress`2<TResult,TProgress>,Windows.Foundation.AsyncStatus)
// 0x00000009 Windows.Foundation.Collections.IIterator`1<T> Windows.Foundation.Collections.IIterable`1::First()
// 0x0000000A T Windows.Foundation.Collections.IIterator`1::get_Current()
// 0x0000000B System.Boolean Windows.Foundation.Collections.IIterator`1::get_HasCurrent()
// 0x0000000C System.Boolean Windows.Foundation.Collections.IIterator`1::MoveNext()
// 0x0000000D System.UInt32 Windows.Foundation.Collections.IIterator`1::GetMany(T[])
// 0x0000000E K Windows.Foundation.Collections.IKeyValuePair`2::get_Key()
// 0x0000000F V Windows.Foundation.Collections.IKeyValuePair`2::get_Value()
// 0x00000010 V Windows.Foundation.Collections.IMapView`2::Lookup(K)
// 0x00000011 System.UInt32 Windows.Foundation.Collections.IMapView`2::get_Size()
// 0x00000012 System.Boolean Windows.Foundation.Collections.IMapView`2::HasKey(K)
// 0x00000013 System.Void Windows.Foundation.Collections.IMapView`2::Split(System.Collections.Generic.IReadOnlyDictionary`2<K,V>&,System.Collections.Generic.IReadOnlyDictionary`2<K,V>&)
// 0x00000014 V Windows.Foundation.Collections.IMap`2::Lookup(K)
// 0x00000015 System.UInt32 Windows.Foundation.Collections.IMap`2::get_Size()
// 0x00000016 System.Boolean Windows.Foundation.Collections.IMap`2::HasKey(K)
// 0x00000017 System.Collections.Generic.IReadOnlyDictionary`2<K,V> Windows.Foundation.Collections.IMap`2::GetView()
// 0x00000018 System.Boolean Windows.Foundation.Collections.IMap`2::Insert(K,V)
// 0x00000019 System.Void Windows.Foundation.Collections.IMap`2::Remove(K)
// 0x0000001A System.Void Windows.Foundation.Collections.IMap`2::Clear()
// 0x0000001B T Windows.Foundation.Collections.IVectorView`1::GetAt(System.UInt32)
// 0x0000001C System.UInt32 Windows.Foundation.Collections.IVectorView`1::get_Size()
// 0x0000001D System.Boolean Windows.Foundation.Collections.IVectorView`1::IndexOf(T,System.UInt32&)
// 0x0000001E System.UInt32 Windows.Foundation.Collections.IVectorView`1::GetMany(System.UInt32,T[])
// 0x0000001F T Windows.Foundation.Collections.IVector`1::GetAt(System.UInt32)
// 0x00000020 System.UInt32 Windows.Foundation.Collections.IVector`1::get_Size()
// 0x00000021 System.Collections.Generic.IReadOnlyList`1<T> Windows.Foundation.Collections.IVector`1::GetView()
// 0x00000022 System.Boolean Windows.Foundation.Collections.IVector`1::IndexOf(T,System.UInt32&)
// 0x00000023 System.Void Windows.Foundation.Collections.IVector`1::SetAt(System.UInt32,T)
// 0x00000024 System.Void Windows.Foundation.Collections.IVector`1::InsertAt(System.UInt32,T)
// 0x00000025 System.Void Windows.Foundation.Collections.IVector`1::RemoveAt(System.UInt32)
// 0x00000026 System.Void Windows.Foundation.Collections.IVector`1::Append(T)
// 0x00000027 System.Void Windows.Foundation.Collections.IVector`1::RemoveAtEnd()
// 0x00000028 System.Void Windows.Foundation.Collections.IVector`1::Clear()
// 0x00000029 System.UInt32 Windows.Foundation.Collections.IVector`1::GetMany(System.UInt32,T[])
// 0x0000002A System.Void Windows.Foundation.Collections.IVector`1::ReplaceAll(T[])
// 0x0000002B System.Void Windows.Foundation.EventHandler`1::.ctor(System.Object,System.IntPtr)
// 0x0000002C System.Void Windows.Foundation.EventHandler`1::Invoke(System.Object,T)
// 0x0000002D System.Void Windows.Foundation.IAsyncAction::put_Completed(Windows.Foundation.AsyncActionCompletedHandler)
extern void IAsyncAction_put_Completed_m0632776C609159F4E6D0D5F6DECCE64CC355B779 ();
// 0x0000002E Windows.Foundation.AsyncActionCompletedHandler Windows.Foundation.IAsyncAction::get_Completed()
extern void IAsyncAction_get_Completed_mEF447B408CEF9BD7882E596CF87C4355CC17E71B ();
// 0x0000002F System.Void Windows.Foundation.IAsyncAction::GetResults()
extern void IAsyncAction_GetResults_m192BCF4CC6EE9E2E66722E3D2BFBBAEE124D7AD7 ();
// 0x00000030 System.UInt32 Windows.Foundation.IAsyncInfo::get_Id()
extern void IAsyncInfo_get_Id_mFB9E7E5D042A091EEA5ADBBB479329CE1248351B ();
// 0x00000031 Windows.Foundation.AsyncStatus Windows.Foundation.IAsyncInfo::get_Status()
extern void IAsyncInfo_get_Status_m3D1D43B45DDBE38620B5178B88307B0759171F7A ();
// 0x00000032 System.Exception Windows.Foundation.IAsyncInfo::get_ErrorCode()
extern void IAsyncInfo_get_ErrorCode_m9A8D01260F4211B8E794B3FC73D212F3F8CAB584 ();
// 0x00000033 System.Void Windows.Foundation.IAsyncInfo::Cancel()
extern void IAsyncInfo_Cancel_m71EA200C07A5E2B5A9BAE4BC2AB00EFF1D3AB5A8 ();
// 0x00000034 System.Void Windows.Foundation.IAsyncInfo::Close()
extern void IAsyncInfo_Close_m197A13662E7E9A888B0E3FFAFDBA15410AA850E7 ();
// 0x00000035 System.Void Windows.Foundation.IAsyncOperationWithProgress`2::put_Progress(Windows.Foundation.AsyncOperationProgressHandler`2<TResult,TProgress>)
// 0x00000036 Windows.Foundation.AsyncOperationProgressHandler`2<TResult,TProgress> Windows.Foundation.IAsyncOperationWithProgress`2::get_Progress()
// 0x00000037 System.Void Windows.Foundation.IAsyncOperationWithProgress`2::put_Completed(Windows.Foundation.AsyncOperationWithProgressCompletedHandler`2<TResult,TProgress>)
// 0x00000038 Windows.Foundation.AsyncOperationWithProgressCompletedHandler`2<TResult,TProgress> Windows.Foundation.IAsyncOperationWithProgress`2::get_Completed()
// 0x00000039 TResult Windows.Foundation.IAsyncOperationWithProgress`2::GetResults()
// 0x0000003A System.Void Windows.Foundation.IAsyncOperation`1::put_Completed(Windows.Foundation.AsyncOperationCompletedHandler`1<TResult>)
// 0x0000003B Windows.Foundation.AsyncOperationCompletedHandler`1<TResult> Windows.Foundation.IAsyncOperation`1::get_Completed()
// 0x0000003C TResult Windows.Foundation.IAsyncOperation`1::GetResults()
// 0x0000003D System.Void Windows.Foundation.IClosable::Close()
extern void IClosable_Close_m9A054CE065D4C97FAF595A8F92B3CB3463C5BCD6 ();
// 0x0000003E Windows.Foundation.PropertyType Windows.Foundation.IPropertyValue::get_Type()
extern void IPropertyValue_get_Type_mB7A711F071E38F3BAEC1862D436AC4AC6DC07FAB ();
// 0x0000003F System.Boolean Windows.Foundation.IPropertyValue::get_IsNumericScalar()
extern void IPropertyValue_get_IsNumericScalar_m1C1F22011064BCF30C5B7E675AC389AF33EC8E5F ();
// 0x00000040 System.Byte Windows.Foundation.IPropertyValue::GetUInt8()
extern void IPropertyValue_GetUInt8_m6E0FA81F823A7980FDBE76ECAFF20E57EB6666F6 ();
// 0x00000041 System.Int16 Windows.Foundation.IPropertyValue::GetInt16()
extern void IPropertyValue_GetInt16_m33B5D0C3CF38BC633A0085AE70DB332CCBE396DF ();
// 0x00000042 System.UInt16 Windows.Foundation.IPropertyValue::GetUInt16()
extern void IPropertyValue_GetUInt16_mEE71D2389CCEFB98E952C00FB8D6BEC588335A57 ();
// 0x00000043 System.Int32 Windows.Foundation.IPropertyValue::GetInt32()
extern void IPropertyValue_GetInt32_m9D0D781ED2D0EFCFD725E9126184AB423EA45C2B ();
// 0x00000044 System.UInt32 Windows.Foundation.IPropertyValue::GetUInt32()
extern void IPropertyValue_GetUInt32_m9186A8BA0E36E15EF0AC2BA14D050DED85699C6E ();
// 0x00000045 System.Int64 Windows.Foundation.IPropertyValue::GetInt64()
extern void IPropertyValue_GetInt64_mFBA4443B87A23C4162616CE67978E15CC32A158E ();
// 0x00000046 System.UInt64 Windows.Foundation.IPropertyValue::GetUInt64()
extern void IPropertyValue_GetUInt64_m01C60E6EA36BD050254AEFE6AF99D1D4977CD15F ();
// 0x00000047 System.Single Windows.Foundation.IPropertyValue::GetSingle()
extern void IPropertyValue_GetSingle_m7DA893EAE56B29828034B018B0844208FF80CCDD ();
// 0x00000048 System.Double Windows.Foundation.IPropertyValue::GetDouble()
extern void IPropertyValue_GetDouble_m34EF323F40F8971CD414DFB38CB6719F071BC869 ();
// 0x00000049 System.Char Windows.Foundation.IPropertyValue::GetChar16()
extern void IPropertyValue_GetChar16_mA1539B0EDFFFD74BBCDB28C1FAC9487811D48386 ();
// 0x0000004A System.Boolean Windows.Foundation.IPropertyValue::GetBoolean()
extern void IPropertyValue_GetBoolean_m0F0CE0E65CD9E983BCC7DBA628CF546547DAC005 ();
// 0x0000004B System.String Windows.Foundation.IPropertyValue::GetString()
extern void IPropertyValue_GetString_m03CC616B8EFD87A5023AB25FC20F934D2F09E169 ();
// 0x0000004C System.Guid Windows.Foundation.IPropertyValue::GetGuid()
extern void IPropertyValue_GetGuid_mE05F5B0248EA98B227F69B2E02D9B10A7B946FC2 ();
// 0x0000004D System.DateTimeOffset Windows.Foundation.IPropertyValue::GetDateTime()
extern void IPropertyValue_GetDateTime_m509277C336754537642EEC6D3A68750DDFABA56E ();
// 0x0000004E System.TimeSpan Windows.Foundation.IPropertyValue::GetTimeSpan()
extern void IPropertyValue_GetTimeSpan_mE29774C717C9B4EC735C20A8CED6796CDF4E91E5 ();
// 0x0000004F Windows.Foundation.Point Windows.Foundation.IPropertyValue::GetPoint()
extern void IPropertyValue_GetPoint_m9E32A5CC948BB3572CD3FF6CA2F5B78F26596E73 ();
// 0x00000050 Windows.Foundation.Size Windows.Foundation.IPropertyValue::GetSize()
extern void IPropertyValue_GetSize_m7212C32E319B8F552ED909C1287C6786B706A659 ();
// 0x00000051 Windows.Foundation.Rect Windows.Foundation.IPropertyValue::GetRect()
extern void IPropertyValue_GetRect_mFB23B562FCD514351684F706405000203ADFD829 ();
// 0x00000052 System.Void Windows.Foundation.IPropertyValue::GetUInt8Array(System.Byte[]&)
extern void IPropertyValue_GetUInt8Array_m7348AAFD3D46CD731DD94146E5E60CFBD1F10553 ();
// 0x00000053 System.Void Windows.Foundation.IPropertyValue::GetInt16Array(System.Int16[]&)
extern void IPropertyValue_GetInt16Array_m9E714AF77215627B9C8119AF3FA6508C50D206FE ();
// 0x00000054 System.Void Windows.Foundation.IPropertyValue::GetUInt16Array(System.UInt16[]&)
extern void IPropertyValue_GetUInt16Array_m4D5B5FB0A7FC0A22E80141B159783C2A0B0FBD0B ();
// 0x00000055 System.Void Windows.Foundation.IPropertyValue::GetInt32Array(System.Int32[]&)
extern void IPropertyValue_GetInt32Array_m799E55885F6A13610A5051F1C2917A0234B9AC03 ();
// 0x00000056 System.Void Windows.Foundation.IPropertyValue::GetUInt32Array(System.UInt32[]&)
extern void IPropertyValue_GetUInt32Array_m335BBA978CD7ACD928B58B3D525C68E2B4DE99FF ();
// 0x00000057 System.Void Windows.Foundation.IPropertyValue::GetInt64Array(System.Int64[]&)
extern void IPropertyValue_GetInt64Array_m2A3152938A9BBA08D7357BC63F400F8C4FD7C76B ();
// 0x00000058 System.Void Windows.Foundation.IPropertyValue::GetUInt64Array(System.UInt64[]&)
extern void IPropertyValue_GetUInt64Array_m9B9E15BA794B247E403386503A1F31881DA86BF7 ();
// 0x00000059 System.Void Windows.Foundation.IPropertyValue::GetSingleArray(System.Single[]&)
extern void IPropertyValue_GetSingleArray_m641609788D53A6254B05EC12B73EC7BACB56D33F ();
// 0x0000005A System.Void Windows.Foundation.IPropertyValue::GetDoubleArray(System.Double[]&)
extern void IPropertyValue_GetDoubleArray_m796ECF392FE47A8F5AF69F16B1B6694666B13D6B ();
// 0x0000005B System.Void Windows.Foundation.IPropertyValue::GetChar16Array(System.Char[]&)
extern void IPropertyValue_GetChar16Array_mA72D84594CACE711C869C39FC47BCE332FC8A618 ();
// 0x0000005C System.Void Windows.Foundation.IPropertyValue::GetBooleanArray(System.Boolean[]&)
extern void IPropertyValue_GetBooleanArray_m32F829C6CA7AE67D42E0D58D9AB28A14AF056517 ();
// 0x0000005D System.Void Windows.Foundation.IPropertyValue::GetStringArray(System.String[]&)
extern void IPropertyValue_GetStringArray_mC5D40C9E740A5FB497BDB39B81487A5DDB3129B6 ();
// 0x0000005E System.Void Windows.Foundation.IPropertyValue::GetInspectableArray(System.Object[]&)
extern void IPropertyValue_GetInspectableArray_m259D5ED0D3880F47B2AE4C4AC1AA310FAC2662D3 ();
// 0x0000005F System.Void Windows.Foundation.IPropertyValue::GetGuidArray(System.Guid[]&)
extern void IPropertyValue_GetGuidArray_m0147631B0DD882717A53BA395C30E070B164BBF6 ();
// 0x00000060 System.Void Windows.Foundation.IPropertyValue::GetDateTimeArray(System.DateTimeOffset[]&)
extern void IPropertyValue_GetDateTimeArray_m6034A0D68EC7E2BD093A76C45BC15F8A2C4A9399 ();
// 0x00000061 System.Void Windows.Foundation.IPropertyValue::GetTimeSpanArray(System.TimeSpan[]&)
extern void IPropertyValue_GetTimeSpanArray_m9DA46A07FE5FDC63435A13AB6D0ED4BFC27D6937 ();
// 0x00000062 System.Void Windows.Foundation.IPropertyValue::GetPointArray(Windows.Foundation.Point[]&)
extern void IPropertyValue_GetPointArray_mAA9D45EF50AD3D78B725BD20460FFE87205C4DB3 ();
// 0x00000063 System.Void Windows.Foundation.IPropertyValue::GetSizeArray(Windows.Foundation.Size[]&)
extern void IPropertyValue_GetSizeArray_m82A3EF5E089F7CD94188D15910F78F8DD7445FFB ();
// 0x00000064 System.Void Windows.Foundation.IPropertyValue::GetRectArray(Windows.Foundation.Rect[]&)
extern void IPropertyValue_GetRectArray_m6D1A32BFF9FA7E6C3DBB9200322BDE50CF1196F3 ();
// 0x00000065 T[] Windows.Foundation.IReferenceArray`1::get_Value()
// 0x00000066 T Windows.Foundation.IReference`1::get_Value()
// 0x00000067 System.String Windows.Foundation.IStringable::ToString()
extern void IStringable_ToString_m52082689C4261A2691DF8177D839F717B13705FF ();
// 0x00000068 System.Void Windows.Foundation.Metadata.ActivatableAttribute::.ctor(System.UInt32,System.String)
extern void ActivatableAttribute__ctor_m3A1923F31F174AE97F2B0101314EC0BA758F8AA5 ();
// 0x00000069 System.Void Windows.Foundation.Metadata.ActivatableAttribute::.ctor(System.Type,System.UInt32,System.String)
extern void ActivatableAttribute__ctor_m9BA03D4599F108D6D0310A55020FADDB76723B8D ();
// 0x0000006A System.Void Windows.Foundation.Metadata.AllowMultipleAttribute::.ctor()
extern void AllowMultipleAttribute__ctor_m7571A1E76CC239AD97D314F09A83F54D16427607 ();
// 0x0000006B System.Void Windows.Foundation.Metadata.ApiContractAttribute::.ctor()
extern void ApiContractAttribute__ctor_mAB0018EB0A4ADE3034A9A6323B64DDB5ACB046F5 ();
// 0x0000006C System.Boolean Windows.Foundation.Metadata.ApiInformation::IsMethodPresent(System.String,System.String)
extern void ApiInformation_IsMethodPresent_m2888CC939FEFAF5B4C739A57F9EE638C79426234 ();
// 0x0000006D System.Boolean Windows.Foundation.Metadata.ApiInformation::IsPropertyPresent(System.String,System.String)
extern void ApiInformation_IsPropertyPresent_mA520CD1EF1AEA15A7BD2CAFF7625CF2766C6E6FD ();
// 0x0000006E System.Boolean Windows.Foundation.Metadata.ApiInformation::IsApiContractPresent(System.String,System.UInt16)
extern void ApiInformation_IsApiContractPresent_m88AAAF0DC1DD55B3C0B031C0977AB571BDCBEC62 ();
// 0x0000006F System.Void Windows.Foundation.Metadata.ApiInformation::Finalize()
extern void ApiInformation_Finalize_m187D52BE403B1F03BDCEB8434FC7577BD372BAB9 ();
// 0x00000070 System.Void Windows.Foundation.Metadata.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern void AttributeUsageAttribute__ctor_m62442B771738CEF57966B0F060F569C03590A15E ();
// 0x00000071 System.Void Windows.Foundation.Metadata.ContractVersionAttribute::.ctor(System.UInt32)
extern void ContractVersionAttribute__ctor_m2A857BC1776E7DF83A2C74C4060327725BA4F620 ();
// 0x00000072 System.Void Windows.Foundation.Metadata.ContractVersionAttribute::.ctor(System.Type,System.UInt32)
extern void ContractVersionAttribute__ctor_mED07DE385B3C7757FC25EDBBEF863B7E10E7BC0B ();
// 0x00000073 System.Void Windows.Foundation.Metadata.ContractVersionAttribute::.ctor(System.String,System.UInt32)
extern void ContractVersionAttribute__ctor_mECE02615A3BF74717E6D6DDD9130F70C6D31894E ();
// 0x00000074 System.Void Windows.Foundation.Metadata.DefaultAttribute::.ctor()
extern void DefaultAttribute__ctor_mCA1A5A887CCC96CCA65D8C684E040F7DE53C847B ();
// 0x00000075 System.Void Windows.Foundation.Metadata.DualApiPartitionAttribute::.ctor()
extern void DualApiPartitionAttribute__ctor_m43302517DA7AE55C651A0A882BE92207C62760AD ();
// 0x00000076 System.Void Windows.Foundation.Metadata.ExclusiveToAttribute::.ctor(System.Type)
extern void ExclusiveToAttribute__ctor_mC32704F33DF1024E6E7736E549567FBA7B0706F6 ();
// 0x00000077 System.Void Windows.Foundation.Metadata.GCPressureAttribute::.ctor()
extern void GCPressureAttribute__ctor_mE6205B6DC20F2B69D30BD72D6E07F029BFE8CCE6 ();
// 0x00000078 System.Void Windows.Foundation.Metadata.GuidAttribute::.ctor(System.UInt32,System.UInt16,System.UInt16,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
extern void GuidAttribute__ctor_m6968CB3415E091AB6558B45DDE116F6927C8CF02 ();
// 0x00000079 System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped0_IsTypePresent()
// 0x0000007A System.Boolean Windows.Foundation.Metadata.IApiInformationStatics::IsMethodPresent(System.String,System.String)
// 0x0000007B System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped1_IsMethodPresent()
// 0x0000007C System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped2_IsEventPresent()
// 0x0000007D System.Boolean Windows.Foundation.Metadata.IApiInformationStatics::IsPropertyPresent(System.String,System.String)
// 0x0000007E System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped3_IsReadOnlyPropertyPresent()
// 0x0000007F System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped4_IsWriteablePropertyPresent()
// 0x00000080 System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped5_IsEnumNamedValuePresent()
// 0x00000081 System.Boolean Windows.Foundation.Metadata.IApiInformationStatics::IsApiContractPresent(System.String,System.UInt16)
// 0x00000082 System.Void Windows.Foundation.Metadata.IApiInformationStatics::U24__Stripped6_IsApiContractPresent()
// 0x00000083 System.Void Windows.Foundation.Metadata.LengthIsAttribute::.ctor(System.Int32)
extern void LengthIsAttribute__ctor_m9513073617452E5C0931CF3C18D78517BCA4D42C ();
// 0x00000084 System.Void Windows.Foundation.Metadata.MarshalingBehaviorAttribute::.ctor(Windows.Foundation.Metadata.MarshalingType)
extern void MarshalingBehaviorAttribute__ctor_m99A98FB82335CB64E7EBC43C32D9BB4AF0323276 ();
// 0x00000085 System.Void Windows.Foundation.Metadata.MuseAttribute::.ctor()
extern void MuseAttribute__ctor_mE71C75AD616E4F47A57A6BF05C30CEA19849F1B6 ();
// 0x00000086 System.Void Windows.Foundation.Metadata.OverloadAttribute::.ctor(System.String)
extern void OverloadAttribute__ctor_mDA2F3A0F74EA36ECA9C97C54EABBFA7FD849031C ();
// 0x00000087 System.Void Windows.Foundation.Metadata.RemoteAsyncAttribute::.ctor()
extern void RemoteAsyncAttribute__ctor_mFAF2329392512D820529928F96B626AA0B6251AC ();
// 0x00000088 System.Void Windows.Foundation.Metadata.StaticAttribute::.ctor(System.Type,System.UInt32,System.String)
extern void StaticAttribute__ctor_m47D2C33E947357F085280F1DF6B797425634255C ();
// 0x00000089 System.Void Windows.Foundation.Metadata.ThreadingAttribute::.ctor(Windows.Foundation.Metadata.ThreadingModel)
extern void ThreadingAttribute__ctor_m1F74D113D96D9DE9E4F582F497D7D8E35B9868EF ();
// 0x0000008A System.Void Windows.Foundation.Metadata.WebHostHiddenAttribute::.ctor()
extern void WebHostHiddenAttribute__ctor_m41D4E691C0F41F6372D72FF38C1FB846080A1F9C ();
// 0x0000008B System.Void Windows.Foundation.TypedEventHandler`2::.ctor(System.Object,System.IntPtr)
// 0x0000008C System.Void Windows.Foundation.TypedEventHandler`2::Invoke(TSender,TResult)
// 0x0000008D System.String Windows.ApplicationModel.Resources.IResourceLoader::GetString(System.String)
// 0x0000008E System.Void Windows.ApplicationModel.Resources.IResourceLoader2::U24__Stripped0_GetStringForUri()
// 0x0000008F System.Void Windows.ApplicationModel.Resources.IResourceLoaderFactory::U24__Stripped0_CreateResourceLoaderByName()
// 0x00000090 System.Void Windows.ApplicationModel.Resources.IResourceLoaderStatics::U24__Stripped0_GetStringForReference()
// 0x00000091 System.Void Windows.ApplicationModel.Resources.IResourceLoaderStatics2::U24__Stripped0_GetForCurrentView()
// 0x00000092 System.Void Windows.ApplicationModel.Resources.IResourceLoaderStatics2::U24__Stripped1_GetForCurrentView()
// 0x00000093 Windows.ApplicationModel.Resources.ResourceLoader Windows.ApplicationModel.Resources.IResourceLoaderStatics2::GetForViewIndependentUse()
// 0x00000094 System.Void Windows.ApplicationModel.Resources.IResourceLoaderStatics2::U24__Stripped2_GetForViewIndependentUse()
// 0x00000095 System.Void Windows.ApplicationModel.Resources.IResourceLoaderStatics3::U24__Stripped0_GetForUIContext()
// 0x00000096 System.String Windows.ApplicationModel.Resources.ResourceLoader::GetString(System.String)
extern void ResourceLoader_GetString_m8586C7138823D162F671D6958F3F245C17E6B628 ();
// 0x00000097 Windows.ApplicationModel.Resources.ResourceLoader Windows.ApplicationModel.Resources.ResourceLoader::GetForViewIndependentUse()
extern void ResourceLoader_GetForViewIndependentUse_m3B47FB65B1ED8348C6B7BAA0059004AFEE2E40BD ();
// 0x00000098 System.Void Windows.ApplicationModel.Resources.ResourceLoader::Finalize()
extern void ResourceLoader_Finalize_mB71BCB445F26E1739A1473D35C40BDE5ACC24826 ();
// 0x00000099 Windows.Foundation.IMemoryBufferReference Windows.Foundation.IMemoryBuffer::CreateReference()
extern void IMemoryBuffer_CreateReference_m84973892E6A7C4A897AC07ACFCEF8C915DA93AEC ();
// 0x0000009A System.Void Windows.Foundation.IMemoryBufferReference::U24__Stripped0_get_Capacity()
// 0x0000009B System.Void Windows.Foundation.IMemoryBufferReference::U24__Stripped1_add_Closed()
// 0x0000009C System.Void Windows.Foundation.IMemoryBufferReference::U24__Stripped2_remove_Closed()
// 0x0000009D System.String Windows.Foundation.IUriEscapeStatics::UnescapeComponent(System.String)
// 0x0000009E System.String Windows.Foundation.IUriEscapeStatics::EscapeComponent(System.String)
// 0x0000009F System.String Windows.Foundation.IUriRuntimeClass::get_AbsoluteUri()
// 0x000000A0 System.String Windows.Foundation.IUriRuntimeClass::get_DisplayUri()
// 0x000000A1 System.String Windows.Foundation.IUriRuntimeClass::get_Domain()
// 0x000000A2 System.String Windows.Foundation.IUriRuntimeClass::get_Extension()
// 0x000000A3 System.String Windows.Foundation.IUriRuntimeClass::get_Fragment()
// 0x000000A4 System.String Windows.Foundation.IUriRuntimeClass::get_Host()
// 0x000000A5 System.String Windows.Foundation.IUriRuntimeClass::get_Password()
// 0x000000A6 System.String Windows.Foundation.IUriRuntimeClass::get_Path()
// 0x000000A7 System.String Windows.Foundation.IUriRuntimeClass::get_Query()
// 0x000000A8 Windows.Foundation.WwwFormUrlDecoder Windows.Foundation.IUriRuntimeClass::get_QueryParsed()
// 0x000000A9 System.String Windows.Foundation.IUriRuntimeClass::get_RawUri()
extern void IUriRuntimeClass_get_RawUri_m78B5E0C829EFB5D1A66092F860F6386C77D0523A ();
// 0x000000AA System.String Windows.Foundation.IUriRuntimeClass::get_SchemeName()
// 0x000000AB System.String Windows.Foundation.IUriRuntimeClass::get_UserName()
// 0x000000AC System.Int32 Windows.Foundation.IUriRuntimeClass::get_Port()
// 0x000000AD System.Boolean Windows.Foundation.IUriRuntimeClass::get_Suspicious()
// 0x000000AE System.Boolean Windows.Foundation.IUriRuntimeClass::Equals(System.Uri)
// 0x000000AF System.Uri Windows.Foundation.IUriRuntimeClass::CombineUri(System.String)
// 0x000000B0 System.Uri Windows.Foundation.IUriRuntimeClassFactory::CreateUri(System.String)
// 0x000000B1 System.Uri Windows.Foundation.IUriRuntimeClassFactory::CreateWithRelativeUri(System.String,System.String)
// 0x000000B2 System.String Windows.Foundation.IUriRuntimeClassWithAbsoluteCanonicalUri::get_AbsoluteCanonicalUri()
// 0x000000B3 System.String Windows.Foundation.IUriRuntimeClassWithAbsoluteCanonicalUri::get_DisplayIri()
// 0x000000B4 System.Void Windows.Foundation.IWwwFormUrlDecoderEntry::U24__Stripped0_get_Name()
// 0x000000B5 System.Void Windows.Foundation.IWwwFormUrlDecoderEntry::U24__Stripped1_get_Value()
// 0x000000B6 System.Void Windows.Foundation.IWwwFormUrlDecoderRuntimeClass::U24__Stripped0_GetFirstValueByName()
// 0x000000B7 System.Void Windows.Foundation.IWwwFormUrlDecoderRuntimeClassFactory::U24__Stripped0_CreateWwwFormUrlDecoder()
// 0x000000B8 System.Void Windows.Foundation.Uri::.ctor(System.String)
extern void Uri__ctor_m55DB673B115A2FB75273586B8D47620759C730CC ();
// 0x000000B9 System.Void Windows.Foundation.Uri::.ctor(System.String,System.String)
extern void Uri__ctor_m97F32BC3726B386B85AFD0382E1B39C2D20E527A ();
// 0x000000BA System.String Windows.Foundation.Uri::get_AbsoluteUri()
extern void Uri_get_AbsoluteUri_m7BFA42C7D52CB5800FD74FED9E447E5C0D9897C7 ();
// 0x000000BB System.String Windows.Foundation.Uri::get_DisplayUri()
extern void Uri_get_DisplayUri_m80EE99B856781C6225F8983FCD716AF25616BFCC ();
// 0x000000BC System.String Windows.Foundation.Uri::get_Domain()
extern void Uri_get_Domain_m0C91AFCC6748C3DD043B6D8E4E99396E6A7AC877 ();
// 0x000000BD System.String Windows.Foundation.Uri::get_Extension()
extern void Uri_get_Extension_m4BCA3E2A6A7A381C5F25A4CDF661CB2F0EF0AC3B ();
// 0x000000BE System.String Windows.Foundation.Uri::get_Fragment()
extern void Uri_get_Fragment_m926F742201AA560AD7A28FA636926874FC844254 ();
// 0x000000BF System.String Windows.Foundation.Uri::get_Host()
extern void Uri_get_Host_m2BE4A95EAE0A43240B536D466001368ABED44A68 ();
// 0x000000C0 System.String Windows.Foundation.Uri::get_Password()
extern void Uri_get_Password_m03AD38D3E8F7CE159F7A177C0D3126935CC80830 ();
// 0x000000C1 System.String Windows.Foundation.Uri::get_Path()
extern void Uri_get_Path_m0D9CC251E59E3CC913AF8DA0CB92DF184D56F2F3 ();
// 0x000000C2 System.String Windows.Foundation.Uri::get_Query()
extern void Uri_get_Query_m284C7FF8237C6145BF4E830345DEB3614A30DCF5 ();
// 0x000000C3 Windows.Foundation.WwwFormUrlDecoder Windows.Foundation.Uri::get_QueryParsed()
extern void Uri_get_QueryParsed_mA6306FF2AC4431DFF2407A0C186E37F180865FE3 ();
// 0x000000C4 System.String Windows.Foundation.Uri::get_RawUri()
extern void Uri_get_RawUri_m00849E771F72BBD94E6AB42A8EE4610A7BC36DB7 ();
// 0x000000C5 System.String Windows.Foundation.Uri::get_SchemeName()
extern void Uri_get_SchemeName_m5FDC03980E9E649D028F58777F084CA76177D7E2 ();
// 0x000000C6 System.String Windows.Foundation.Uri::get_UserName()
extern void Uri_get_UserName_m938E239ED50710F66056A097110C7865F279BA37 ();
// 0x000000C7 System.Int32 Windows.Foundation.Uri::get_Port()
extern void Uri_get_Port_mC55B2793356B7129B7351A94EE7146C7C16418EB ();
// 0x000000C8 System.Boolean Windows.Foundation.Uri::get_Suspicious()
extern void Uri_get_Suspicious_m8B40D4095C71B12C28F131EB9BAFA2EA8FE2213E ();
// 0x000000C9 System.Boolean Windows.Foundation.Uri::Equals(System.Uri)
extern void Uri_Equals_m518D9A5B0A697F3FAEA3650F3AA32930E669746D ();
// 0x000000CA System.Uri Windows.Foundation.Uri::CombineUri(System.String)
extern void Uri_CombineUri_m7A362D69A2B4B4C5A0482F39506A78938F6AEAE7 ();
// 0x000000CB System.String Windows.Foundation.Uri::get_AbsoluteCanonicalUri()
extern void Uri_get_AbsoluteCanonicalUri_m3037ADFF5D31E5BC7209485DF4819AEC3021C824 ();
// 0x000000CC System.String Windows.Foundation.Uri::get_DisplayIri()
extern void Uri_get_DisplayIri_mBE1BDB9380A1E01D8DCD532D81B8CBFF55FB1FC6 ();
// 0x000000CD System.String Windows.Foundation.Uri::ToString()
extern void Uri_ToString_m803AE2936135C60D584961D0857717E88A51DD55 ();
// 0x000000CE System.String Windows.Foundation.Uri::UnescapeComponent(System.String)
extern void Uri_UnescapeComponent_m8CD7B535D394D7E41432D0F1CFA9287EF3ACA3B9 ();
// 0x000000CF System.String Windows.Foundation.Uri::EscapeComponent(System.String)
extern void Uri_EscapeComponent_mAFDB9963E2EA253255FF86DF1270959E357EFA1D ();
// 0x000000D0 System.Void Windows.Foundation.Uri::Finalize()
extern void Uri_Finalize_mAD57CE06963D886EB6DD0082F6B90C5BDDE7E8AC ();
// 0x000000D1 Windows.Foundation.Collections.IIterator`1<Windows.Foundation.IWwwFormUrlDecoderEntry> Windows.Foundation.WwwFormUrlDecoder::First()
extern void WwwFormUrlDecoder_First_m8D0D362AE3137612020821CBCF8C498CB51EFC79 ();
// 0x000000D2 Windows.Foundation.IWwwFormUrlDecoderEntry Windows.Foundation.WwwFormUrlDecoder::GetAt(System.UInt32)
extern void WwwFormUrlDecoder_GetAt_m43ED945C60EA065ECDC8649F80A96D777C5C302A ();
// 0x000000D3 System.UInt32 Windows.Foundation.WwwFormUrlDecoder::get_Size()
extern void WwwFormUrlDecoder_get_Size_m1D1C2B7B59C49A9693CDF960647A9BC1C4F7321C ();
// 0x000000D4 System.Boolean Windows.Foundation.WwwFormUrlDecoder::IndexOf(Windows.Foundation.IWwwFormUrlDecoderEntry,System.UInt32&)
extern void WwwFormUrlDecoder_IndexOf_mD77DF2F9EE659B35E43C982FC73544A09237405F ();
// 0x000000D5 System.UInt32 Windows.Foundation.WwwFormUrlDecoder::GetMany(System.UInt32,Windows.Foundation.IWwwFormUrlDecoderEntry[])
extern void WwwFormUrlDecoder_GetMany_mCE9F986BD2DAD9CFDDF1176DC33293FC9164A779 ();
// 0x000000D6 Windows.Foundation.IWwwFormUrlDecoderEntry Windows.Foundation.WwwFormUrlDecoder::get_Item(System.Int32)
extern void WwwFormUrlDecoder_get_Item_m068A456764193EA4D524C0CBD03DDA621FEA53DD ();
// 0x000000D7 System.Int32 Windows.Foundation.WwwFormUrlDecoder::get_Count()
extern void WwwFormUrlDecoder_get_Count_mC189E36F85384C614621138AA1BCAA1FEB5CB154 ();
// 0x000000D8 System.Collections.Generic.IEnumerator`1<Windows.Foundation.IWwwFormUrlDecoderEntry> Windows.Foundation.WwwFormUrlDecoder::GetEnumerator()
extern void WwwFormUrlDecoder_GetEnumerator_m551609AA9EE4214DECC647BA95C41E2C475E80E3 ();
// 0x000000D9 System.Collections.IEnumerator Windows.Foundation.WwwFormUrlDecoder::GetEnumerator()
extern void WwwFormUrlDecoder_GetEnumerator_m4EA94748942773CC0823E901EE63843E5FC218BD ();
// 0x000000DA System.Void Windows.Foundation.WwwFormUrlDecoder::Finalize()
extern void WwwFormUrlDecoder_Finalize_mB1B110B2B5BA22503CF62734D1B140820825FF70 ();
// 0x000000DB Windows.Graphics.Holographic.HolographicViewConfiguration Windows.Graphics.Holographic.HolographicDisplay::TryGetViewConfiguration(Windows.Graphics.Holographic.HolographicViewConfigurationKind)
extern void HolographicDisplay_TryGetViewConfiguration_mAD05250269F4ED3FCA2F303DA511D4ADEAEFFDB7 ();
// 0x000000DC Windows.Graphics.Holographic.HolographicDisplay Windows.Graphics.Holographic.HolographicDisplay::GetDefault()
extern void HolographicDisplay_GetDefault_m7102C2AFE1F07195DF9F8D964249E6AAB8BF84DF ();
// 0x000000DD System.Void Windows.Graphics.Holographic.HolographicDisplay::Finalize()
extern void HolographicDisplay_Finalize_m49CDF9B7C36B2036C803D6F05D15FDCFFF2A7293 ();
// 0x000000DE System.Void Windows.Graphics.Holographic.HolographicFrame::Finalize()
extern void HolographicFrame_Finalize_m7ACBA8A0BF51FE70349182520250F40D651B1CF7 ();
// 0x000000DF System.Void Windows.Graphics.Holographic.HolographicViewConfiguration::put_IsEnabled(System.Boolean)
extern void HolographicViewConfiguration_put_IsEnabled_m3476B0065A7EC22D37A44C6BA77B0920495C6C2B ();
// 0x000000E0 System.Void Windows.Graphics.Holographic.HolographicViewConfiguration::Finalize()
extern void HolographicViewConfiguration_Finalize_mF2D783CDE2BB8841593DCCBAF197C0D05D03785A ();
// 0x000000E1 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped0_get_DisplayName()
// 0x000000E2 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped1_get_MaxViewportSize()
// 0x000000E3 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped2_get_IsStereo()
// 0x000000E4 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped3_get_IsOpaque()
// 0x000000E5 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped4_get_AdapterId()
// 0x000000E6 System.Void Windows.Graphics.Holographic.IHolographicDisplay::U24__Stripped5_get_SpatialLocator()
// 0x000000E7 System.Void Windows.Graphics.Holographic.IHolographicDisplay2::U24__Stripped0_get_RefreshRate()
// 0x000000E8 Windows.Graphics.Holographic.HolographicViewConfiguration Windows.Graphics.Holographic.IHolographicDisplay3::TryGetViewConfiguration(Windows.Graphics.Holographic.HolographicViewConfigurationKind)
// 0x000000E9 Windows.Graphics.Holographic.HolographicDisplay Windows.Graphics.Holographic.IHolographicDisplayStatics::GetDefault()
// 0x000000EA System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped0_get_AddedCameras()
// 0x000000EB System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped1_get_RemovedCameras()
// 0x000000EC System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped2_GetRenderingParameters()
// 0x000000ED System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped3_get_Duration()
// 0x000000EE System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped4_get_CurrentPrediction()
// 0x000000EF System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped5_UpdateCurrentPrediction()
// 0x000000F0 System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped6_PresentUsingCurrentPrediction()
// 0x000000F1 System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped7_PresentUsingCurrentPrediction()
// 0x000000F2 System.Void Windows.Graphics.Holographic.IHolographicFrame::U24__Stripped8_WaitForFrameToFinish()
// 0x000000F3 System.Void Windows.Graphics.Holographic.IHolographicFrame2::U24__Stripped0_GetQuadLayerUpdateParameters()
// 0x000000F4 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped0_get_NativeRenderTargetSize()
// 0x000000F5 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped1_get_RenderTargetSize()
// 0x000000F6 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped2_RequestRenderTargetSize()
// 0x000000F7 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped3_get_SupportedPixelFormats()
// 0x000000F8 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped4_get_PixelFormat()
// 0x000000F9 System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped5_put_PixelFormat()
// 0x000000FA System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped6_get_IsStereo()
// 0x000000FB System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped7_get_RefreshRate()
// 0x000000FC System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped8_get_Kind()
// 0x000000FD System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped9_get_Display()
// 0x000000FE System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::U24__Stripped10_get_IsEnabled()
// 0x000000FF System.Void Windows.Graphics.Holographic.IHolographicViewConfiguration::put_IsEnabled(System.Boolean)
// 0x00000100 Windows.Foundation.IMemoryBufferReference Windows.Graphics.Imaging.BitmapBuffer::CreateReference()
extern void BitmapBuffer_CreateReference_m8602FD8674A59E46848584D5E06313028890F2DF ();
// 0x00000101 System.Void Windows.Graphics.Imaging.BitmapBuffer::Close()
extern void BitmapBuffer_Close_m07248A632436DC7D1FC8A2735E8269BB93700E18 ();
// 0x00000102 System.Void Windows.Graphics.Imaging.BitmapBuffer::Dispose()
extern void BitmapBuffer_Dispose_mD47A3B0BDD57D1FD65D763BE12B998A19EB08127 ();
// 0x00000103 System.Void Windows.Graphics.Imaging.BitmapBuffer::Finalize()
extern void BitmapBuffer_Finalize_m9F24A6995B7A934134869982215FEEF4B1A7D905 ();
// 0x00000104 System.Void Windows.Graphics.Imaging.IBitmapBuffer::U24__Stripped0_GetPlaneCount()
// 0x00000105 System.Void Windows.Graphics.Imaging.IBitmapBuffer::U24__Stripped1_GetPlaneDescription()
// 0x00000106 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped0_get_BitmapPixelFormat()
// 0x00000107 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped1_get_BitmapAlphaMode()
// 0x00000108 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped2_get_PixelWidth()
// 0x00000109 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped3_get_PixelHeight()
// 0x0000010A System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped4_get_IsReadOnly()
// 0x0000010B System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped5_put_DpiX()
// 0x0000010C System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped6_get_DpiX()
// 0x0000010D System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped7_put_DpiY()
// 0x0000010E System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped8_get_DpiY()
// 0x0000010F Windows.Graphics.Imaging.BitmapBuffer Windows.Graphics.Imaging.ISoftwareBitmap::LockBuffer(Windows.Graphics.Imaging.BitmapBufferAccessMode)
// 0x00000110 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped9_CopyTo()
// 0x00000111 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped10_CopyFromBuffer()
// 0x00000112 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped11_CopyToBuffer()
// 0x00000113 System.Void Windows.Graphics.Imaging.ISoftwareBitmap::U24__Stripped12_GetReadOnlyView()
// 0x00000114 System.Void Windows.Graphics.Imaging.ISoftwareBitmapFactory::U24__Stripped0_Create()
// 0x00000115 System.Void Windows.Graphics.Imaging.ISoftwareBitmapFactory::U24__Stripped1_CreateWithAlpha()
// 0x00000116 Windows.Graphics.Imaging.SoftwareBitmap Windows.Graphics.Imaging.ISoftwareBitmapStatics::Copy(Windows.Graphics.Imaging.SoftwareBitmap)
// 0x00000117 System.Void Windows.Graphics.Imaging.ISoftwareBitmapStatics::U24__Stripped0_Convert()
// 0x00000118 Windows.Graphics.Imaging.SoftwareBitmap Windows.Graphics.Imaging.ISoftwareBitmapStatics::Convert(Windows.Graphics.Imaging.SoftwareBitmap,Windows.Graphics.Imaging.BitmapPixelFormat,Windows.Graphics.Imaging.BitmapAlphaMode)
// 0x00000119 System.Void Windows.Graphics.Imaging.ISoftwareBitmapStatics::U24__Stripped1_CreateCopyFromBuffer()
// 0x0000011A System.Void Windows.Graphics.Imaging.ISoftwareBitmapStatics::U24__Stripped2_CreateCopyFromBuffer()
// 0x0000011B System.Void Windows.Graphics.Imaging.ISoftwareBitmapStatics::U24__Stripped3_CreateCopyFromSurfaceAsync()
// 0x0000011C System.Void Windows.Graphics.Imaging.ISoftwareBitmapStatics::U24__Stripped4_CreateCopyFromSurfaceAsync()
// 0x0000011D Windows.Graphics.Imaging.BitmapBuffer Windows.Graphics.Imaging.SoftwareBitmap::LockBuffer(Windows.Graphics.Imaging.BitmapBufferAccessMode)
extern void SoftwareBitmap_LockBuffer_m23B648E747F72030D6E3985E7D6CED3FC6624EB9 ();
// 0x0000011E System.Void Windows.Graphics.Imaging.SoftwareBitmap::Close()
extern void SoftwareBitmap_Close_m432AC22AEF3EF755E948ADA362D8EE123C3ED912 ();
// 0x0000011F Windows.Graphics.Imaging.SoftwareBitmap Windows.Graphics.Imaging.SoftwareBitmap::Copy(Windows.Graphics.Imaging.SoftwareBitmap)
extern void SoftwareBitmap_Copy_m88F3148D49144E0921AF558BC45FA5C1CA93BB4D ();
// 0x00000120 Windows.Graphics.Imaging.SoftwareBitmap Windows.Graphics.Imaging.SoftwareBitmap::Convert(Windows.Graphics.Imaging.SoftwareBitmap,Windows.Graphics.Imaging.BitmapPixelFormat,Windows.Graphics.Imaging.BitmapAlphaMode)
extern void SoftwareBitmap_Convert_m69A3A26001D01FDB674DEFB713B18D2E7FCFFFEA ();
// 0x00000121 System.Void Windows.Graphics.Imaging.SoftwareBitmap::Dispose()
extern void SoftwareBitmap_Dispose_m43281CA2937F3586428C3D24CE2967E5B7469B89 ();
// 0x00000122 System.Void Windows.Graphics.Imaging.SoftwareBitmap::Finalize()
extern void SoftwareBitmap_Finalize_m8D21C0287E6BB30BB6AC4D77770C1275196C5721 ();
// 0x00000123 System.Void Windows.Media.Capture.Frames.IMediaFrameFormat::U24__Stripped0_get_MajorType()
// 0x00000124 System.String Windows.Media.Capture.Frames.IMediaFrameFormat::get_Subtype()
// 0x00000125 Windows.Media.MediaProperties.MediaRatio Windows.Media.Capture.Frames.IMediaFrameFormat::get_FrameRate()
// 0x00000126 System.Void Windows.Media.Capture.Frames.IMediaFrameFormat::U24__Stripped1_get_Properties()
// 0x00000127 Windows.Media.Capture.Frames.VideoMediaFrameFormat Windows.Media.Capture.Frames.IMediaFrameFormat::get_VideoFormat()
// 0x00000128 System.Void Windows.Media.Capture.Frames.IMediaFrameFormat2::U24__Stripped0_get_AudioEncodingProperties()
// 0x00000129 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.Media.Capture.Frames.IMediaFrameReader::add_FrameArrived(Windows.Foundation.TypedEventHandler`2<Windows.Media.Capture.Frames.MediaFrameReader,Windows.Media.Capture.Frames.MediaFrameArrivedEventArgs>)
// 0x0000012A System.Void Windows.Media.Capture.Frames.IMediaFrameReader::remove_FrameArrived(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
// 0x0000012B Windows.Media.Capture.Frames.MediaFrameReference Windows.Media.Capture.Frames.IMediaFrameReader::TryAcquireLatestFrame()
// 0x0000012C Windows.Foundation.IAsyncOperation`1<Windows.Media.Capture.Frames.MediaFrameReaderStartStatus> Windows.Media.Capture.Frames.IMediaFrameReader::StartAsync()
// 0x0000012D Windows.Foundation.IAsyncAction Windows.Media.Capture.Frames.IMediaFrameReader::StopAsync()
// 0x0000012E System.Void Windows.Media.Capture.Frames.IMediaFrameReader2::U24__Stripped0_put_AcquisitionMode()
// 0x0000012F System.Void Windows.Media.Capture.Frames.IMediaFrameReader2::U24__Stripped1_get_AcquisitionMode()
// 0x00000130 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped0_get_SourceKind()
// 0x00000131 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped1_get_Format()
// 0x00000132 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped2_get_SystemRelativeTime()
// 0x00000133 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped3_get_Duration()
// 0x00000134 System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Object> Windows.Media.Capture.Frames.IMediaFrameReference::get_Properties()
// 0x00000135 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped4_get_BufferMediaFrame()
// 0x00000136 Windows.Media.Capture.Frames.VideoMediaFrame Windows.Media.Capture.Frames.IMediaFrameReference::get_VideoMediaFrame()
// 0x00000137 System.Void Windows.Media.Capture.Frames.IMediaFrameReference::U24__Stripped5_get_CoordinateSystem()
// 0x00000138 System.Void Windows.Media.Capture.Frames.IMediaFrameReference2::U24__Stripped0_get_AudioMediaFrame()
// 0x00000139 Windows.Media.Capture.Frames.MediaFrameSourceInfo Windows.Media.Capture.Frames.IMediaFrameSource::get_Info()
// 0x0000013A System.Void Windows.Media.Capture.Frames.IMediaFrameSource::U24__Stripped0_get_Controller()
// 0x0000013B System.Collections.Generic.IReadOnlyList`1<Windows.Media.Capture.Frames.MediaFrameFormat> Windows.Media.Capture.Frames.IMediaFrameSource::get_SupportedFormats()
// 0x0000013C System.Void Windows.Media.Capture.Frames.IMediaFrameSource::U24__Stripped1_get_CurrentFormat()
// 0x0000013D Windows.Foundation.IAsyncAction Windows.Media.Capture.Frames.IMediaFrameSource::SetFormatAsync(Windows.Media.Capture.Frames.MediaFrameFormat)
// 0x0000013E System.Void Windows.Media.Capture.Frames.IMediaFrameSource::U24__Stripped2_add_FormatChanged()
// 0x0000013F System.Void Windows.Media.Capture.Frames.IMediaFrameSource::U24__Stripped3_remove_FormatChanged()
// 0x00000140 System.Void Windows.Media.Capture.Frames.IMediaFrameSource::U24__Stripped4_TryGetCameraIntrinsics()
// 0x00000141 System.String Windows.Media.Capture.Frames.IMediaFrameSourceGroup::get_Id()
// 0x00000142 System.String Windows.Media.Capture.Frames.IMediaFrameSourceGroup::get_DisplayName()
// 0x00000143 System.Void Windows.Media.Capture.Frames.IMediaFrameSourceGroup::U24__Stripped0_get_SourceInfos()
// 0x00000144 Windows.Foundation.IAsyncOperation`1<System.Collections.Generic.IReadOnlyList`1<Windows.Media.Capture.Frames.MediaFrameSourceGroup>> Windows.Media.Capture.Frames.IMediaFrameSourceGroupStatics::FindAllAsync()
// 0x00000145 System.Void Windows.Media.Capture.Frames.IMediaFrameSourceGroupStatics::U24__Stripped0_FromIdAsync()
// 0x00000146 System.Void Windows.Media.Capture.Frames.IMediaFrameSourceGroupStatics::U24__Stripped1_GetDeviceSelector()
// 0x00000147 System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped0_get_Id()
// 0x00000148 Windows.Media.Capture.MediaStreamType Windows.Media.Capture.Frames.IMediaFrameSourceInfo::get_MediaStreamType()
// 0x00000149 System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped1_get_SourceKind()
// 0x0000014A System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped2_get_SourceGroup()
// 0x0000014B System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped3_get_DeviceInformation()
// 0x0000014C System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped4_get_Properties()
// 0x0000014D System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo::U24__Stripped5_get_CoordinateSystem()
// 0x0000014E System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo2::U24__Stripped0_get_ProfileId()
// 0x0000014F System.Void Windows.Media.Capture.Frames.IMediaFrameSourceInfo2::U24__Stripped1_get_VideoProfileMediaDescription()
// 0x00000150 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped0_get_FrameReference()
// 0x00000151 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped1_get_VideoFormat()
// 0x00000152 Windows.Graphics.Imaging.SoftwareBitmap Windows.Media.Capture.Frames.IVideoMediaFrame::get_SoftwareBitmap()
// 0x00000153 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped2_get_Direct3DSurface()
// 0x00000154 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped3_get_CameraIntrinsics()
// 0x00000155 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped4_get_InfraredMediaFrame()
// 0x00000156 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped5_get_DepthMediaFrame()
// 0x00000157 System.Void Windows.Media.Capture.Frames.IVideoMediaFrame::U24__Stripped6_GetVideoFrame()
// 0x00000158 System.Void Windows.Media.Capture.Frames.IVideoMediaFrameFormat::U24__Stripped0_get_MediaFrameFormat()
// 0x00000159 System.Void Windows.Media.Capture.Frames.IVideoMediaFrameFormat::U24__Stripped1_get_DepthFormat()
// 0x0000015A System.UInt32 Windows.Media.Capture.Frames.IVideoMediaFrameFormat::get_Width()
// 0x0000015B System.UInt32 Windows.Media.Capture.Frames.IVideoMediaFrameFormat::get_Height()
// 0x0000015C System.Void Windows.Media.Capture.Frames.MediaFrameArrivedEventArgs::Finalize()
extern void MediaFrameArrivedEventArgs_Finalize_mEBCFE091DD50F69085ED3D2F794F4EAE13E898AF ();
// 0x0000015D System.String Windows.Media.Capture.Frames.MediaFrameFormat::get_Subtype()
extern void MediaFrameFormat_get_Subtype_m8FD725CDFDF2A3009B6BC5988824F9C0F42C2688 ();
// 0x0000015E Windows.Media.MediaProperties.MediaRatio Windows.Media.Capture.Frames.MediaFrameFormat::get_FrameRate()
extern void MediaFrameFormat_get_FrameRate_m401C16BADABF96DCDCC89362DC57388F6B9F1B7A ();
// 0x0000015F Windows.Media.Capture.Frames.VideoMediaFrameFormat Windows.Media.Capture.Frames.MediaFrameFormat::get_VideoFormat()
extern void MediaFrameFormat_get_VideoFormat_m0D438D1A7E225A6B9EBED8BA0B99ACF7093FBE5C ();
// 0x00000160 System.Void Windows.Media.Capture.Frames.MediaFrameFormat::Finalize()
extern void MediaFrameFormat_Finalize_m879627BDD2CEF9F5555B3B55ADAD0A888C07093A ();
// 0x00000161 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.Media.Capture.Frames.MediaFrameReader::add_FrameArrived(Windows.Foundation.TypedEventHandler`2<Windows.Media.Capture.Frames.MediaFrameReader,Windows.Media.Capture.Frames.MediaFrameArrivedEventArgs>)
extern void MediaFrameReader_add_FrameArrived_m70D86584FF72C43B88380FA65B7E800979AC3EC3 ();
// 0x00000162 System.Void Windows.Media.Capture.Frames.MediaFrameReader::remove_FrameArrived(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
extern void MediaFrameReader_remove_FrameArrived_m6A9A144AF1321C2AB4EBACFE83295B33904D1CCE ();
// 0x00000163 Windows.Media.Capture.Frames.MediaFrameReference Windows.Media.Capture.Frames.MediaFrameReader::TryAcquireLatestFrame()
extern void MediaFrameReader_TryAcquireLatestFrame_mFD595E6B034B121167C809E350FA18ABF433B16B ();
// 0x00000164 Windows.Foundation.IAsyncOperation`1<Windows.Media.Capture.Frames.MediaFrameReaderStartStatus> Windows.Media.Capture.Frames.MediaFrameReader::StartAsync()
extern void MediaFrameReader_StartAsync_mD00B94B2ADE8C9151FCCC4130DD5BE713D6EEB5F ();
// 0x00000165 Windows.Foundation.IAsyncAction Windows.Media.Capture.Frames.MediaFrameReader::StopAsync()
extern void MediaFrameReader_StopAsync_mAB7D7C521B8F61001CE184AEA5A26FAA6CAD6219 ();
// 0x00000166 System.Void Windows.Media.Capture.Frames.MediaFrameReader::Close()
extern void MediaFrameReader_Close_mED99DD83A468375FE9C3FB51BBCC243E2455CC9E ();
// 0x00000167 System.Void Windows.Media.Capture.Frames.MediaFrameReader::Dispose()
extern void MediaFrameReader_Dispose_m7916FAE70AC08476419D471BC29DBB934688AF87 ();
// 0x00000168 System.Void Windows.Media.Capture.Frames.MediaFrameReader::Finalize()
extern void MediaFrameReader_Finalize_m26F437FF525495E71637A3E18B954C968E22454F ();
// 0x00000169 System.Collections.Generic.IReadOnlyDictionary`2<System.Guid,System.Object> Windows.Media.Capture.Frames.MediaFrameReference::get_Properties()
extern void MediaFrameReference_get_Properties_mA05E567CAA3515FA16644C41CE2DB077D0B0D493 ();
// 0x0000016A Windows.Media.Capture.Frames.VideoMediaFrame Windows.Media.Capture.Frames.MediaFrameReference::get_VideoMediaFrame()
extern void MediaFrameReference_get_VideoMediaFrame_mED7947FBE13B3BC0E8FB71663B36E04413FD64DB ();
// 0x0000016B System.Void Windows.Media.Capture.Frames.MediaFrameReference::Close()
extern void MediaFrameReference_Close_m0DAE598E79427AE1ABA6B09187EC0DA391BC73CA ();
// 0x0000016C System.Void Windows.Media.Capture.Frames.MediaFrameReference::Dispose()
extern void MediaFrameReference_Dispose_m19D18EDFD49FFE58A2A6D29DFA7CE07B5404BE61 ();
// 0x0000016D System.Void Windows.Media.Capture.Frames.MediaFrameReference::Finalize()
extern void MediaFrameReference_Finalize_mE998A8EB857A16D64F389A132039B0532A3F1984 ();
// 0x0000016E Windows.Media.Capture.Frames.MediaFrameSourceInfo Windows.Media.Capture.Frames.MediaFrameSource::get_Info()
extern void MediaFrameSource_get_Info_m280F33DDF494CA64FF56C55C1D08878CABBD43F3 ();
// 0x0000016F System.Collections.Generic.IReadOnlyList`1<Windows.Media.Capture.Frames.MediaFrameFormat> Windows.Media.Capture.Frames.MediaFrameSource::get_SupportedFormats()
extern void MediaFrameSource_get_SupportedFormats_m8FCA5D492E1F2A7E2D0C265B293289BE5DDC3B95 ();
// 0x00000170 Windows.Foundation.IAsyncAction Windows.Media.Capture.Frames.MediaFrameSource::SetFormatAsync(Windows.Media.Capture.Frames.MediaFrameFormat)
extern void MediaFrameSource_SetFormatAsync_m164D8239DBB2D928594CD6B096B3C71C23B97B0B ();
// 0x00000171 System.Void Windows.Media.Capture.Frames.MediaFrameSource::Finalize()
extern void MediaFrameSource_Finalize_mFEF856DC91B46ED1BC71A17A7097D701FE3AA7AE ();
// 0x00000172 System.String Windows.Media.Capture.Frames.MediaFrameSourceGroup::get_Id()
extern void MediaFrameSourceGroup_get_Id_m33E3573E15198AC724E421C40F73515B65A2B106 ();
// 0x00000173 System.String Windows.Media.Capture.Frames.MediaFrameSourceGroup::get_DisplayName()
extern void MediaFrameSourceGroup_get_DisplayName_m0479A785405125C6D378F43BB20C485208DD25E7 ();
// 0x00000174 Windows.Foundation.IAsyncOperation`1<System.Collections.Generic.IReadOnlyList`1<Windows.Media.Capture.Frames.MediaFrameSourceGroup>> Windows.Media.Capture.Frames.MediaFrameSourceGroup::FindAllAsync()
extern void MediaFrameSourceGroup_FindAllAsync_mE587BFF743EF9A3499A6BD642B89CB2F8E9D072D ();
// 0x00000175 System.Void Windows.Media.Capture.Frames.MediaFrameSourceGroup::Finalize()
extern void MediaFrameSourceGroup_Finalize_m26E8F4D99F38C52AFAA6C7B63A68EB85B5B30970 ();
// 0x00000176 Windows.Media.Capture.MediaStreamType Windows.Media.Capture.Frames.MediaFrameSourceInfo::get_MediaStreamType()
extern void MediaFrameSourceInfo_get_MediaStreamType_mCF9BBAD9E4A8367B4E6B0256846C10274B723550 ();
// 0x00000177 System.Void Windows.Media.Capture.Frames.MediaFrameSourceInfo::Finalize()
extern void MediaFrameSourceInfo_Finalize_mFD58ADCC85714EEB7443E54CA01EF4DEF88408D8 ();
// 0x00000178 Windows.Graphics.Imaging.SoftwareBitmap Windows.Media.Capture.Frames.VideoMediaFrame::get_SoftwareBitmap()
extern void VideoMediaFrame_get_SoftwareBitmap_m30CB8C4238A58FF5193755B33D4C9B85FBED7D94 ();
// 0x00000179 System.Void Windows.Media.Capture.Frames.VideoMediaFrame::Finalize()
extern void VideoMediaFrame_Finalize_m022A89FF6DB77A8C4F36BBE3178E084BB913C09F ();
// 0x0000017A System.UInt32 Windows.Media.Capture.Frames.VideoMediaFrameFormat::get_Width()
extern void VideoMediaFrameFormat_get_Width_m0BEA3FD8264C7BC5C64788CB8036F05CF551E9AF ();
// 0x0000017B System.UInt32 Windows.Media.Capture.Frames.VideoMediaFrameFormat::get_Height()
extern void VideoMediaFrameFormat_get_Height_m53CEF98901AF365A5C5883CEADC6B39DF9043EF1 ();
// 0x0000017C System.Void Windows.Media.Capture.Frames.VideoMediaFrameFormat::Finalize()
extern void VideoMediaFrameFormat_Finalize_m0CCD39E36A6314230EECC633160BC2AB4633EB89 ();
// 0x0000017D System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped0_InitializeAsync()
// 0x0000017E Windows.Foundation.IAsyncAction Windows.Media.Capture.IMediaCapture::InitializeAsync(Windows.Media.Capture.MediaCaptureInitializationSettings)
// 0x0000017F System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped1_StartRecordToStorageFileAsync()
// 0x00000180 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped2_StartRecordToStreamAsync()
// 0x00000181 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped3_StartRecordToCustomSinkAsync()
// 0x00000182 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped4_StartRecordToCustomSinkAsync()
// 0x00000183 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped5_StopRecordAsync()
// 0x00000184 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped6_CapturePhotoToStorageFileAsync()
// 0x00000185 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped7_CapturePhotoToStreamAsync()
// 0x00000186 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped8_AddEffectAsync()
// 0x00000187 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped9_ClearEffectsAsync()
// 0x00000188 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped10_SetEncoderProperty()
// 0x00000189 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped11_GetEncoderProperty()
// 0x0000018A System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped12_add_Failed()
// 0x0000018B System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped13_remove_Failed()
// 0x0000018C System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped14_add_RecordLimitationExceeded()
// 0x0000018D System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped15_remove_RecordLimitationExceeded()
// 0x0000018E System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped16_get_MediaCaptureSettings()
// 0x0000018F System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped17_get_AudioDeviceController()
// 0x00000190 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped18_get_VideoDeviceController()
// 0x00000191 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped19_SetPreviewMirroring()
// 0x00000192 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped20_GetPreviewMirroring()
// 0x00000193 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped21_SetPreviewRotation()
// 0x00000194 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped22_GetPreviewRotation()
// 0x00000195 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped23_SetRecordRotation()
// 0x00000196 System.Void Windows.Media.Capture.IMediaCapture::U24__Stripped24_GetRecordRotation()
// 0x00000197 System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped0_PrepareLowLagRecordToStorageFileAsync()
// 0x00000198 System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped1_PrepareLowLagRecordToStreamAsync()
// 0x00000199 System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped2_PrepareLowLagRecordToCustomSinkAsync()
// 0x0000019A System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped3_PrepareLowLagRecordToCustomSinkAsync()
// 0x0000019B System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped4_PrepareLowLagPhotoCaptureAsync()
// 0x0000019C System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped5_PrepareLowLagPhotoSequenceCaptureAsync()
// 0x0000019D System.Void Windows.Media.Capture.IMediaCapture2::U24__Stripped6_SetEncodingPropertiesAsync()
// 0x0000019E System.Void Windows.Media.Capture.IMediaCapture3::U24__Stripped0_PrepareVariablePhotoSequenceCaptureAsync()
// 0x0000019F System.Void Windows.Media.Capture.IMediaCapture3::U24__Stripped1_add_FocusChanged()
// 0x000001A0 System.Void Windows.Media.Capture.IMediaCapture3::U24__Stripped2_remove_FocusChanged()
// 0x000001A1 System.Void Windows.Media.Capture.IMediaCapture3::U24__Stripped3_add_PhotoConfirmationCaptured()
// 0x000001A2 System.Void Windows.Media.Capture.IMediaCapture3::U24__Stripped4_remove_PhotoConfirmationCaptured()
// 0x000001A3 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped0_AddAudioEffectAsync()
// 0x000001A4 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped1_AddVideoEffectAsync()
// 0x000001A5 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped2_PauseRecordAsync()
// 0x000001A6 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped3_ResumeRecordAsync()
// 0x000001A7 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped4_add_CameraStreamStateChanged()
// 0x000001A8 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped5_remove_CameraStreamStateChanged()
// 0x000001A9 System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped6_get_CameraStreamState()
// 0x000001AA System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped7_GetPreviewFrameAsync()
// 0x000001AB System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped8_GetPreviewFrameAsync()
// 0x000001AC System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped9_add_ThermalStatusChanged()
// 0x000001AD System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped10_remove_ThermalStatusChanged()
// 0x000001AE System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped11_get_ThermalStatus()
// 0x000001AF System.Void Windows.Media.Capture.IMediaCapture4::U24__Stripped12_PrepareAdvancedPhotoCaptureAsync()
// 0x000001B0 System.Void Windows.Media.Capture.IMediaCapture5::U24__Stripped0_RemoveEffectAsync()
// 0x000001B1 System.Void Windows.Media.Capture.IMediaCapture5::U24__Stripped1_PauseRecordWithResultAsync()
// 0x000001B2 System.Void Windows.Media.Capture.IMediaCapture5::U24__Stripped2_StopRecordWithResultAsync()
// 0x000001B3 System.Collections.Generic.IReadOnlyDictionary`2<System.String,Windows.Media.Capture.Frames.MediaFrameSource> Windows.Media.Capture.IMediaCapture5::get_FrameSources()
// 0x000001B4 System.Void Windows.Media.Capture.IMediaCapture5::U24__Stripped3_CreateFrameReaderAsync()
// 0x000001B5 Windows.Foundation.IAsyncOperation`1<Windows.Media.Capture.Frames.MediaFrameReader> Windows.Media.Capture.IMediaCapture5::CreateFrameReaderAsync(Windows.Media.Capture.Frames.MediaFrameSource,System.String)
// 0x000001B6 System.Void Windows.Media.Capture.IMediaCapture5::U24__Stripped4_CreateFrameReaderAsync()
// 0x000001B7 System.Void Windows.Media.Capture.IMediaCapture6::U24__Stripped0_add_CaptureDeviceExclusiveControlStatusChanged()
// 0x000001B8 System.Void Windows.Media.Capture.IMediaCapture6::U24__Stripped1_remove_CaptureDeviceExclusiveControlStatusChanged()
// 0x000001B9 System.Void Windows.Media.Capture.IMediaCapture6::U24__Stripped2_CreateMultiSourceFrameReaderAsync()
// 0x000001BA System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped0_put_AudioDeviceId()
// 0x000001BB System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped1_get_AudioDeviceId()
// 0x000001BC System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped2_put_VideoDeviceId()
// 0x000001BD System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped3_get_VideoDeviceId()
// 0x000001BE System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::put_StreamingCaptureMode(Windows.Media.Capture.StreamingCaptureMode)
// 0x000001BF System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped4_get_StreamingCaptureMode()
// 0x000001C0 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped5_put_PhotoCaptureSource()
// 0x000001C1 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings::U24__Stripped6_get_PhotoCaptureSource()
// 0x000001C2 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings2::U24__Stripped0_put_MediaCategory()
// 0x000001C3 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings2::U24__Stripped1_get_MediaCategory()
// 0x000001C4 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings2::U24__Stripped2_put_AudioProcessing()
// 0x000001C5 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings2::U24__Stripped3_get_AudioProcessing()
// 0x000001C6 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings3::U24__Stripped0_put_AudioSource()
// 0x000001C7 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings3::U24__Stripped1_get_AudioSource()
// 0x000001C8 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings3::U24__Stripped2_put_VideoSource()
// 0x000001C9 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings3::U24__Stripped3_get_VideoSource()
// 0x000001CA System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped0_get_VideoProfile()
// 0x000001CB System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped1_put_VideoProfile()
// 0x000001CC System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped2_get_PreviewMediaDescription()
// 0x000001CD System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped3_put_PreviewMediaDescription()
// 0x000001CE System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped4_get_RecordMediaDescription()
// 0x000001CF System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped5_put_RecordMediaDescription()
// 0x000001D0 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped6_get_PhotoMediaDescription()
// 0x000001D1 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings4::U24__Stripped7_put_PhotoMediaDescription()
// 0x000001D2 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::U24__Stripped0_get_SourceGroup()
// 0x000001D3 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::put_SourceGroup(Windows.Media.Capture.Frames.MediaFrameSourceGroup)
// 0x000001D4 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::U24__Stripped1_get_SharingMode()
// 0x000001D5 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::put_SharingMode(Windows.Media.Capture.MediaCaptureSharingMode)
// 0x000001D6 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::U24__Stripped2_get_MemoryPreference()
// 0x000001D7 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings5::put_MemoryPreference(Windows.Media.Capture.MediaCaptureMemoryPreference)
// 0x000001D8 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings6::U24__Stripped0_get_AlwaysPlaySystemShutterSound()
// 0x000001D9 System.Void Windows.Media.Capture.IMediaCaptureInitializationSettings6::U24__Stripped1_put_AlwaysPlaySystemShutterSound()
// 0x000001DA System.Void Windows.Media.Capture.IMediaCaptureStatics::U24__Stripped0_IsVideoProfileSupported()
// 0x000001DB System.Void Windows.Media.Capture.IMediaCaptureStatics::U24__Stripped1_FindAllVideoProfiles()
// 0x000001DC System.Void Windows.Media.Capture.IMediaCaptureStatics::U24__Stripped2_FindConcurrentProfiles()
// 0x000001DD System.Void Windows.Media.Capture.IMediaCaptureStatics::U24__Stripped3_FindKnownVideoProfiles()
// 0x000001DE System.Void Windows.Media.Capture.IMediaCaptureVideoPreview::U24__Stripped0_StartPreviewAsync()
// 0x000001DF System.Void Windows.Media.Capture.IMediaCaptureVideoPreview::U24__Stripped1_StartPreviewToCustomSinkAsync()
// 0x000001E0 System.Void Windows.Media.Capture.IMediaCaptureVideoPreview::U24__Stripped2_StartPreviewToCustomSinkAsync()
// 0x000001E1 System.Void Windows.Media.Capture.IMediaCaptureVideoPreview::U24__Stripped3_StopPreviewAsync()
// 0x000001E2 System.Void Windows.Media.Capture.MediaCapture::.ctor()
extern void MediaCapture__ctor_mEBE907D602FB9D309E9F6CF73D975BA01FE4F342 ();
// 0x000001E3 Windows.Foundation.IAsyncAction Windows.Media.Capture.MediaCapture::InitializeAsync(Windows.Media.Capture.MediaCaptureInitializationSettings)
extern void MediaCapture_InitializeAsync_mF5A49F3E4CE77A412A186DC00DD0F8CFFDA5A7CD ();
// 0x000001E4 System.Void Windows.Media.Capture.MediaCapture::Close()
extern void MediaCapture_Close_mDC19E5968E0CDDE863F44F592CA2EC8083FC401E ();
// 0x000001E5 System.Collections.Generic.IReadOnlyDictionary`2<System.String,Windows.Media.Capture.Frames.MediaFrameSource> Windows.Media.Capture.MediaCapture::get_FrameSources()
extern void MediaCapture_get_FrameSources_m0145231D3841EBDADCCF9C5097F348E4551EE12C ();
// 0x000001E6 Windows.Foundation.IAsyncOperation`1<Windows.Media.Capture.Frames.MediaFrameReader> Windows.Media.Capture.MediaCapture::CreateFrameReaderAsync(Windows.Media.Capture.Frames.MediaFrameSource,System.String)
extern void MediaCapture_CreateFrameReaderAsync_mB7301A500EF6BDF979650BC851E47393F90C4C87 ();
// 0x000001E7 System.Void Windows.Media.Capture.MediaCapture::Dispose()
extern void MediaCapture_Dispose_m450038C19982448178B7D45682DE531ED42A3CC1 ();
// 0x000001E8 System.Void Windows.Media.Capture.MediaCapture::Finalize()
extern void MediaCapture_Finalize_m398CBD076F24219044BFE613E4DE0EE45EC81AD2 ();
// 0x000001E9 System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::.ctor()
extern void MediaCaptureInitializationSettings__ctor_mB5627F7AB7A8F5E146CEF4EBA6EB080C476BF94E ();
// 0x000001EA System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::put_StreamingCaptureMode(Windows.Media.Capture.StreamingCaptureMode)
extern void MediaCaptureInitializationSettings_put_StreamingCaptureMode_m5DBBF366F04B2A1C7726C02D2EB39699EA2544AE ();
// 0x000001EB System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::put_SourceGroup(Windows.Media.Capture.Frames.MediaFrameSourceGroup)
extern void MediaCaptureInitializationSettings_put_SourceGroup_mABDD33585ED37B912B52F8442C44A865D195A46C ();
// 0x000001EC System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::put_SharingMode(Windows.Media.Capture.MediaCaptureSharingMode)
extern void MediaCaptureInitializationSettings_put_SharingMode_m0A9B9CFFC9F0D9682FDDEA478855AE4C7FF65FC3 ();
// 0x000001ED System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::put_MemoryPreference(Windows.Media.Capture.MediaCaptureMemoryPreference)
extern void MediaCaptureInitializationSettings_put_MemoryPreference_m5EB56AB89F176DCEF2411FA65C0FB8DB51D057F3 ();
// 0x000001EE System.Void Windows.Media.Capture.MediaCaptureInitializationSettings::Finalize()
extern void MediaCaptureInitializationSettings_Finalize_m228F1D5B00EB1C6F3CADE7A9E739227AAA4969B6 ();
// 0x000001EF System.Void Windows.Media.MediaProperties.IMediaRatio::U24__Stripped0_put_Numerator()
// 0x000001F0 System.UInt32 Windows.Media.MediaProperties.IMediaRatio::get_Numerator()
// 0x000001F1 System.Void Windows.Media.MediaProperties.IMediaRatio::U24__Stripped1_put_Denominator()
// 0x000001F2 System.UInt32 Windows.Media.MediaProperties.IMediaRatio::get_Denominator()
// 0x000001F3 System.UInt32 Windows.Media.MediaProperties.MediaRatio::get_Numerator()
extern void MediaRatio_get_Numerator_mFD3FC5319D3E5BB50D803371B0435C2CA6E1CFAB ();
// 0x000001F4 System.UInt32 Windows.Media.MediaProperties.MediaRatio::get_Denominator()
extern void MediaRatio_get_Denominator_m3B29DEE537BC7E4602EAAF81D549E4F0DC7BEF6F ();
// 0x000001F5 System.Void Windows.Media.MediaProperties.MediaRatio::Finalize()
extern void MediaRatio_Finalize_mD39CF0713FACFF3FF8179C46936C4BD1E85F467B ();
// 0x000001F6 System.DateTimeOffset Windows.Perception.IPerceptionTimestamp::get_TargetTime()
// 0x000001F7 System.Void Windows.Perception.IPerceptionTimestamp::U24__Stripped0_get_PredictionAmount()
// 0x000001F8 System.Void Windows.Perception.IPerceptionTimestamp2::U24__Stripped0_get_SystemRelativeTargetTime()
// 0x000001F9 Windows.Perception.PerceptionTimestamp Windows.Perception.IPerceptionTimestampHelperStatics::FromHistoricalTargetTime(System.DateTimeOffset)
// 0x000001FA System.Void Windows.Perception.IPerceptionTimestampHelperStatics2::U24__Stripped0_FromSystemRelativeTargetTime()
// 0x000001FB System.Boolean Windows.Perception.People.EyesPose::get_IsCalibrationValid()
extern void EyesPose_get_IsCalibrationValid_m8068F4172B56F0E3D002C6AF6FC1157D36BF5638 ();
// 0x000001FC System.Nullable`1<Windows.Perception.Spatial.SpatialRay> Windows.Perception.People.EyesPose::get_Gaze()
extern void EyesPose_get_Gaze_m5D2EF2C3C421C04A43E29B97FE3D4C6561442622 ();
// 0x000001FD Windows.Perception.PerceptionTimestamp Windows.Perception.People.EyesPose::get_UpdateTimestamp()
extern void EyesPose_get_UpdateTimestamp_m4862FFCB98CF48418A17480C41DB5EBD433C9FA4 ();
// 0x000001FE System.Boolean Windows.Perception.People.EyesPose::IsSupported()
extern void EyesPose_IsSupported_m3ADC5B187E3223E9506A0A02B7F7B811BFAD308E ();
// 0x000001FF Windows.Foundation.IAsyncOperation`1<Windows.UI.Input.GazeInputAccessStatus> Windows.Perception.People.EyesPose::RequestAccessAsync()
extern void EyesPose_RequestAccessAsync_m5BA0E88C19898B68264E3CD09614FAEC4C914B0B ();
// 0x00000200 System.Void Windows.Perception.People.EyesPose::Finalize()
extern void EyesPose_Finalize_m82FD23BAA527945E31CC73CEC3E3686FFF097544 ();
// 0x00000201 System.UInt32 Windows.Perception.People.HandMeshObserver::get_TriangleIndexCount()
extern void HandMeshObserver_get_TriangleIndexCount_m72D9A87FD7433C47F4F9F1AB0EB8F37661451466 ();
// 0x00000202 System.UInt32 Windows.Perception.People.HandMeshObserver::get_VertexCount()
extern void HandMeshObserver_get_VertexCount_m5BF946FC59A9C0E25F545DFEA8C00D6285F4AABC ();
// 0x00000203 System.Void Windows.Perception.People.HandMeshObserver::GetTriangleIndices(System.UInt16[])
extern void HandMeshObserver_GetTriangleIndices_mA0A850F3FF458E34C3B7DED4BF76FFE0B3B7F772 ();
// 0x00000204 Windows.Perception.People.HandMeshVertexState Windows.Perception.People.HandMeshObserver::GetVertexStateForPose(Windows.Perception.People.HandPose)
extern void HandMeshObserver_GetVertexStateForPose_m929A618E54D6ED72FC013D830E5DC420BA8FE391 ();
// 0x00000205 Windows.Perception.People.HandPose Windows.Perception.People.HandMeshObserver::get_NeutralPose()
extern void HandMeshObserver_get_NeutralPose_m089C4B81C9090BF049E6372A602A60491CEB7D86 ();
// 0x00000206 System.Void Windows.Perception.People.HandMeshObserver::Finalize()
extern void HandMeshObserver_Finalize_m6C9AC6214584BE4F6B8C2810BEC17D1CEB81F5C3 ();
// 0x00000207 Windows.Perception.Spatial.SpatialCoordinateSystem Windows.Perception.People.HandMeshVertexState::get_CoordinateSystem()
extern void HandMeshVertexState_get_CoordinateSystem_mD4B2BFFD6A3E44494044A9DCF231F1075556A42E ();
// 0x00000208 System.Void Windows.Perception.People.HandMeshVertexState::GetVertices(Windows.Perception.People.HandMeshVertex[])
extern void HandMeshVertexState_GetVertices_m0A39BCC22D116DE1DB2AC21F6CC93E8DAF97EA97 ();
// 0x00000209 System.Void Windows.Perception.People.HandMeshVertexState::Finalize()
extern void HandMeshVertexState_Finalize_mB9490F5513F0146413693B8548BC9DE9C4AE702D ();
// 0x0000020A System.Boolean Windows.Perception.People.HandPose::TryGetJoints(Windows.Perception.Spatial.SpatialCoordinateSystem,Windows.Perception.People.HandJointKind[],Windows.Perception.People.JointPose[])
extern void HandPose_TryGetJoints_m1CB7787BE738367E79E74D194FC7EB642901516F ();
// 0x0000020B System.Void Windows.Perception.People.HandPose::Finalize()
extern void HandPose_Finalize_m3FD1AEAED997015D914DD6FB81F2BFB1DED6569D ();
// 0x0000020C System.Boolean Windows.Perception.People.IEyesPose::get_IsCalibrationValid()
// 0x0000020D System.Nullable`1<Windows.Perception.Spatial.SpatialRay> Windows.Perception.People.IEyesPose::get_Gaze()
// 0x0000020E Windows.Perception.PerceptionTimestamp Windows.Perception.People.IEyesPose::get_UpdateTimestamp()
// 0x0000020F System.Boolean Windows.Perception.People.IEyesPoseStatics::IsSupported()
// 0x00000210 Windows.Foundation.IAsyncOperation`1<Windows.UI.Input.GazeInputAccessStatus> Windows.Perception.People.IEyesPoseStatics::RequestAccessAsync()
// 0x00000211 System.Void Windows.Perception.People.IHandMeshObserver::U24__Stripped0_get_Source()
// 0x00000212 System.UInt32 Windows.Perception.People.IHandMeshObserver::get_TriangleIndexCount()
// 0x00000213 System.UInt32 Windows.Perception.People.IHandMeshObserver::get_VertexCount()
// 0x00000214 System.Void Windows.Perception.People.IHandMeshObserver::GetTriangleIndices(System.UInt16[])
// 0x00000215 Windows.Perception.People.HandMeshVertexState Windows.Perception.People.IHandMeshObserver::GetVertexStateForPose(Windows.Perception.People.HandPose)
// 0x00000216 Windows.Perception.People.HandPose Windows.Perception.People.IHandMeshObserver::get_NeutralPose()
// 0x00000217 System.Void Windows.Perception.People.IHandMeshObserver::U24__Stripped1_get_NeutralPoseVersion()
// 0x00000218 System.Void Windows.Perception.People.IHandMeshObserver::U24__Stripped2_get_ModelId()
// 0x00000219 Windows.Perception.Spatial.SpatialCoordinateSystem Windows.Perception.People.IHandMeshVertexState::get_CoordinateSystem()
// 0x0000021A System.Void Windows.Perception.People.IHandMeshVertexState::GetVertices(Windows.Perception.People.HandMeshVertex[])
// 0x0000021B System.Void Windows.Perception.People.IHandMeshVertexState::U24__Stripped0_get_UpdateTimestamp()
// 0x0000021C System.Void Windows.Perception.People.IHandPose::U24__Stripped0_TryGetJoint()
// 0x0000021D System.Boolean Windows.Perception.People.IHandPose::TryGetJoints(Windows.Perception.Spatial.SpatialCoordinateSystem,Windows.Perception.People.HandJointKind[],Windows.Perception.People.JointPose[])
// 0x0000021E System.Void Windows.Perception.People.IHandPose::U24__Stripped1_GetRelativeJoint()
// 0x0000021F System.Void Windows.Perception.People.IHandPose::U24__Stripped2_GetRelativeJoints()
// 0x00000220 System.DateTimeOffset Windows.Perception.PerceptionTimestamp::get_TargetTime()
extern void PerceptionTimestamp_get_TargetTime_mB521E7C9592D22D6990534922606376C6AF67285 ();
// 0x00000221 System.Void Windows.Perception.PerceptionTimestamp::Finalize()
extern void PerceptionTimestamp_Finalize_m59D16C1DA87A48B375B88EE9EEB22968C7606B44 ();
// 0x00000222 Windows.Perception.PerceptionTimestamp Windows.Perception.PerceptionTimestampHelper::FromHistoricalTargetTime(System.DateTimeOffset)
extern void PerceptionTimestampHelper_FromHistoricalTargetTime_m3887862422EFE0EB81A026771F85278980FB2943 ();
// 0x00000223 System.Void Windows.Perception.PerceptionTimestampHelper::Finalize()
extern void PerceptionTimestampHelper_Finalize_mC9DAD6F7124A5896BAB8CF4E84B7F2BC0397E1D9 ();
// 0x00000224 System.Nullable`1<System.Numerics.Matrix4x4> Windows.Perception.Spatial.ISpatialCoordinateSystem::TryGetTransformTo(Windows.Perception.Spatial.SpatialCoordinateSystem)
// 0x00000225 System.Nullable`1<System.Numerics.Matrix4x4> Windows.Perception.Spatial.SpatialCoordinateSystem::TryGetTransformTo(Windows.Perception.Spatial.SpatialCoordinateSystem)
extern void SpatialCoordinateSystem_TryGetTransformTo_m9B3D98B85802722F2CC37F275351B746E6A79A25 ();
// 0x00000226 System.Void Windows.Perception.Spatial.SpatialCoordinateSystem::Finalize()
extern void SpatialCoordinateSystem_Finalize_mE29CFDA8D33B2152B88A0F2841E55AD5DA773996 ();
// 0x00000227 System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserver::U24__Stripped0_GetObservedSurfaces()
// 0x00000228 System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserver::U24__Stripped1_SetBoundingVolume()
// 0x00000229 System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserver::U24__Stripped2_SetBoundingVolumes()
// 0x0000022A System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserver::U24__Stripped3_add_ObservedSurfacesChanged()
// 0x0000022B System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserver::U24__Stripped4_remove_ObservedSurfacesChanged()
// 0x0000022C System.Void Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserverStatics::U24__Stripped0_RequestAccessAsync()
// 0x0000022D System.Boolean Windows.Perception.Spatial.Surfaces.ISpatialSurfaceObserverStatics2::IsSupported()
// 0x0000022E System.Boolean Windows.Perception.Spatial.Surfaces.SpatialSurfaceObserver::IsSupported()
extern void SpatialSurfaceObserver_IsSupported_mCB54738653BC2840AA78E5E083C5E18EF4D3C408 ();
// 0x0000022F System.Void Windows.Perception.Spatial.Surfaces.SpatialSurfaceObserver::Finalize()
extern void SpatialSurfaceObserver_Finalize_mE83A1FAE6C0110CAB5A1A76718BF6C4238AA4E8C ();
// 0x00000230 Windows.Foundation.IAsyncAction Windows.Storage.FileIO::AppendTextAsync(Windows.Storage.IStorageFile,System.String)
extern void FileIO_AppendTextAsync_m5BCC79A14E6ADD76772F6E2BE28D1BD31CC51EB5 ();
// 0x00000231 Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IBuffer> Windows.Storage.FileIO::ReadBufferAsync(Windows.Storage.IStorageFile)
extern void FileIO_ReadBufferAsync_mF4FFAFC3AF09C53DAE527D9A7C89D683DDEA4531 ();
// 0x00000232 System.Void Windows.Storage.FileIO::Finalize()
extern void FileIO_Finalize_mA051D2375BF6A19AF021B36DA1176C98BBF6F883 ();
// 0x00000233 System.Void Windows.Storage.IFileIOStatics::U24__Stripped0_ReadTextAsync()
// 0x00000234 System.Void Windows.Storage.IFileIOStatics::U24__Stripped1_ReadTextAsync()
// 0x00000235 System.Void Windows.Storage.IFileIOStatics::U24__Stripped2_WriteTextAsync()
// 0x00000236 System.Void Windows.Storage.IFileIOStatics::U24__Stripped3_WriteTextAsync()
// 0x00000237 Windows.Foundation.IAsyncAction Windows.Storage.IFileIOStatics::AppendTextAsync(Windows.Storage.IStorageFile,System.String)
// 0x00000238 System.Void Windows.Storage.IFileIOStatics::U24__Stripped4_AppendTextAsync()
// 0x00000239 System.Void Windows.Storage.IFileIOStatics::U24__Stripped5_ReadLinesAsync()
// 0x0000023A System.Void Windows.Storage.IFileIOStatics::U24__Stripped6_ReadLinesAsync()
// 0x0000023B System.Void Windows.Storage.IFileIOStatics::U24__Stripped7_WriteLinesAsync()
// 0x0000023C System.Void Windows.Storage.IFileIOStatics::U24__Stripped8_WriteLinesAsync()
// 0x0000023D System.Void Windows.Storage.IFileIOStatics::U24__Stripped9_AppendLinesAsync()
// 0x0000023E System.Void Windows.Storage.IFileIOStatics::U24__Stripped10_AppendLinesAsync()
// 0x0000023F Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IBuffer> Windows.Storage.IFileIOStatics::ReadBufferAsync(Windows.Storage.IStorageFile)
// 0x00000240 System.Void Windows.Storage.IFileIOStatics::U24__Stripped11_WriteBufferAsync()
// 0x00000241 System.Void Windows.Storage.IFileIOStatics::U24__Stripped12_WriteBytesAsync()
// 0x00000242 System.Void Windows.Storage.IKnownFoldersCameraRollStatics::U24__Stripped0_get_CameraRoll()
// 0x00000243 System.Void Windows.Storage.IKnownFoldersPlaylistsStatics::U24__Stripped0_get_Playlists()
// 0x00000244 System.Void Windows.Storage.IKnownFoldersSavedPicturesStatics::U24__Stripped0_get_SavedPictures()
// 0x00000245 Windows.Storage.StorageFolder Windows.Storage.IKnownFoldersStatics::get_MusicLibrary()
// 0x00000246 System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped0_get_PicturesLibrary()
// 0x00000247 System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped1_get_VideosLibrary()
// 0x00000248 System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped2_get_DocumentsLibrary()
// 0x00000249 System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped3_get_HomeGroup()
// 0x0000024A System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped4_get_RemovableDevices()
// 0x0000024B System.Void Windows.Storage.IKnownFoldersStatics::U24__Stripped5_get_MediaServerDevices()
// 0x0000024C System.Void Windows.Storage.IKnownFoldersStatics2::U24__Stripped0_get_Objects3D()
// 0x0000024D System.Void Windows.Storage.IKnownFoldersStatics2::U24__Stripped1_get_AppCaptures()
// 0x0000024E System.Void Windows.Storage.IKnownFoldersStatics2::U24__Stripped2_get_RecordedCalls()
// 0x0000024F System.Void Windows.Storage.IKnownFoldersStatics3::U24__Stripped0_GetFolderForUserAsync()
// 0x00000250 System.Void Windows.Storage.IStorageFile::U24__Stripped0_get_FileType()
// 0x00000251 System.Void Windows.Storage.IStorageFile::U24__Stripped1_get_ContentType()
// 0x00000252 System.Void Windows.Storage.IStorageFile::U24__Stripped2_OpenAsync()
// 0x00000253 System.Void Windows.Storage.IStorageFile::U24__Stripped3_OpenTransactedWriteAsync()
// 0x00000254 System.Void Windows.Storage.IStorageFile::U24__Stripped4_CopyAsync()
// 0x00000255 System.Void Windows.Storage.IStorageFile::U24__Stripped5_CopyAsync()
// 0x00000256 System.Void Windows.Storage.IStorageFile::U24__Stripped6_CopyAsync()
// 0x00000257 System.Void Windows.Storage.IStorageFile::U24__Stripped7_CopyAndReplaceAsync()
// 0x00000258 System.Void Windows.Storage.IStorageFile::U24__Stripped8_MoveAsync()
// 0x00000259 System.Void Windows.Storage.IStorageFile::U24__Stripped9_MoveAsync()
// 0x0000025A System.Void Windows.Storage.IStorageFile::U24__Stripped10_MoveAsync()
// 0x0000025B System.Void Windows.Storage.IStorageFile::U24__Stripped11_MoveAndReplaceAsync()
// 0x0000025C System.Void Windows.Storage.IStorageFile2::U24__Stripped0_OpenAsync()
// 0x0000025D System.Void Windows.Storage.IStorageFile2::U24__Stripped1_OpenTransactedWriteAsync()
// 0x0000025E System.Void Windows.Storage.IStorageFilePropertiesWithAvailability::U24__Stripped0_get_IsAvailable()
// 0x0000025F Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.IStorageFileStatics::GetFileFromPathAsync(System.String)
// 0x00000260 System.Void Windows.Storage.IStorageFileStatics::U24__Stripped0_GetFileFromApplicationUriAsync()
// 0x00000261 System.Void Windows.Storage.IStorageFileStatics::U24__Stripped1_CreateStreamedFileAsync()
// 0x00000262 System.Void Windows.Storage.IStorageFileStatics::U24__Stripped2_ReplaceWithStreamedFileAsync()
// 0x00000263 System.Void Windows.Storage.IStorageFileStatics::U24__Stripped3_CreateStreamedFileFromUriAsync()
// 0x00000264 System.Void Windows.Storage.IStorageFileStatics::U24__Stripped4_ReplaceWithStreamedFileFromUriAsync()
// 0x00000265 System.Void Windows.Storage.IStorageFolder::U24__Stripped0_CreateFileAsync()
// 0x00000266 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.IStorageFolder::CreateFileAsync(System.String,Windows.Storage.CreationCollisionOption)
extern void IStorageFolder_CreateFileAsync_mD20BADEBA391C026E479581424EFF60D5EE729FA ();
// 0x00000267 System.Void Windows.Storage.IStorageFolder::U24__Stripped1_CreateFolderAsync()
// 0x00000268 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFolder> Windows.Storage.IStorageFolder::CreateFolderAsync(System.String,Windows.Storage.CreationCollisionOption)
extern void IStorageFolder_CreateFolderAsync_m506538F2866DDF4CDA48BFC1EA034B56C0945F0D ();
// 0x00000269 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.IStorageFolder::GetFileAsync(System.String)
extern void IStorageFolder_GetFileAsync_m3B0FF62937AAE51707D544E9F9B5041EBB5ABE7D ();
// 0x0000026A Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFolder> Windows.Storage.IStorageFolder::GetFolderAsync(System.String)
extern void IStorageFolder_GetFolderAsync_mD37B62674AFC57B8B229BCE38C0CE6FD2F4975DF ();
// 0x0000026B System.Void Windows.Storage.IStorageFolder::U24__Stripped2_GetItemAsync()
// 0x0000026C System.Void Windows.Storage.IStorageFolder::U24__Stripped3_GetFilesAsync()
// 0x0000026D System.Void Windows.Storage.IStorageFolder::U24__Stripped4_GetFoldersAsync()
// 0x0000026E System.Void Windows.Storage.IStorageFolder::U24__Stripped5_GetItemsAsync()
// 0x0000026F System.Void Windows.Storage.IStorageFolder2::U24__Stripped0_TryGetItemAsync()
// 0x00000270 System.Void Windows.Storage.IStorageFolder3::U24__Stripped0_TryGetChangeTracker()
// 0x00000271 System.Void Windows.Storage.IStorageFolderStatics::U24__Stripped0_GetFolderFromPathAsync()
// 0x00000272 System.Void Windows.Storage.IStorageItem::U24__Stripped0_RenameAsync()
// 0x00000273 System.Void Windows.Storage.IStorageItem::U24__Stripped1_RenameAsync()
// 0x00000274 System.Void Windows.Storage.IStorageItem::U24__Stripped2_DeleteAsync()
// 0x00000275 System.Void Windows.Storage.IStorageItem::U24__Stripped3_DeleteAsync()
// 0x00000276 System.Void Windows.Storage.IStorageItem::U24__Stripped4_GetBasicPropertiesAsync()
// 0x00000277 System.String Windows.Storage.IStorageItem::get_Name()
extern void IStorageItem_get_Name_m598919A4062C6DEEDD83B371F1C7F4CBFCB0AAF8 ();
// 0x00000278 System.String Windows.Storage.IStorageItem::get_Path()
extern void IStorageItem_get_Path_m1F543E2CE313AD19AE39D0EC8C6832ECEDC29678 ();
// 0x00000279 System.Void Windows.Storage.IStorageItem::U24__Stripped5_get_Attributes()
// 0x0000027A System.Void Windows.Storage.IStorageItem::U24__Stripped6_get_DateCreated()
// 0x0000027B System.Void Windows.Storage.IStorageItem::U24__Stripped7_IsOfType()
// 0x0000027C System.Void Windows.Storage.IStorageItem2::U24__Stripped0_GetParentAsync()
// 0x0000027D System.Void Windows.Storage.IStorageItem2::U24__Stripped1_IsEqual()
// 0x0000027E System.Void Windows.Storage.IStorageItemProperties::U24__Stripped0_GetThumbnailAsync()
// 0x0000027F System.Void Windows.Storage.IStorageItemProperties::U24__Stripped1_GetThumbnailAsync()
// 0x00000280 System.Void Windows.Storage.IStorageItemProperties::U24__Stripped2_GetThumbnailAsync()
// 0x00000281 System.Void Windows.Storage.IStorageItemProperties::U24__Stripped3_get_DisplayName()
// 0x00000282 System.Void Windows.Storage.IStorageItemProperties::U24__Stripped4_get_DisplayType()
// 0x00000283 System.Void Windows.Storage.IStorageItemProperties::U24__Stripped5_get_FolderRelativeId()
// 0x00000284 System.Void Windows.Storage.IStorageItemProperties::U24__Stripped6_get_Properties()
// 0x00000285 System.Void Windows.Storage.IStorageItemProperties2::U24__Stripped0_GetScaledImageAsThumbnailAsync()
// 0x00000286 System.Void Windows.Storage.IStorageItemProperties2::U24__Stripped1_GetScaledImageAsThumbnailAsync()
// 0x00000287 System.Void Windows.Storage.IStorageItemProperties2::U24__Stripped2_GetScaledImageAsThumbnailAsync()
// 0x00000288 System.Void Windows.Storage.IStorageItemPropertiesWithProvider::U24__Stripped0_get_Provider()
// 0x00000289 Windows.Storage.StorageFolder Windows.Storage.KnownFolders::get_MusicLibrary()
extern void KnownFolders_get_MusicLibrary_m340C733773BC804A47FC8AF73032F5FC201E0398 ();
// 0x0000028A System.Void Windows.Storage.KnownFolders::Finalize()
extern void KnownFolders_Finalize_mC87485CFB0FDEF05B0ED2751DBC66E2E4DE52D4A ();
// 0x0000028B System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped0_GetIndexedStateAsync()
// 0x0000028C System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped1_CreateFileQuery()
// 0x0000028D System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped2_CreateFileQuery()
// 0x0000028E System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped3_CreateFileQueryWithOptions()
// 0x0000028F System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped4_CreateFolderQuery()
// 0x00000290 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped5_CreateFolderQuery()
// 0x00000291 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped6_CreateFolderQueryWithOptions()
// 0x00000292 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped7_CreateItemQuery()
// 0x00000293 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped8_CreateItemQueryWithOptions()
// 0x00000294 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped9_GetFilesAsync()
// 0x00000295 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped10_GetFilesAsync()
// 0x00000296 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped11_GetFoldersAsync()
// 0x00000297 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped12_GetFoldersAsync()
// 0x00000298 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped13_GetItemsAsync()
// 0x00000299 System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped14_AreQueryOptionsSupported()
// 0x0000029A System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped15_IsCommonFolderQuerySupported()
// 0x0000029B System.Void Windows.Storage.Search.IStorageFolderQueryOperations::U24__Stripped16_IsCommonFileQuerySupported()
// 0x0000029C System.String Windows.Storage.StorageFile::get_Name()
extern void StorageFile_get_Name_m943DA65F57D87F5C92DEC8ED660827392BDDA147 ();
// 0x0000029D System.String Windows.Storage.StorageFile::get_Path()
extern void StorageFile_get_Path_mCDC8594ADE45C229095A1B54E5E7D8266036B881 ();
// 0x0000029E Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> Windows.Storage.StorageFile::OpenReadAsync()
extern void StorageFile_OpenReadAsync_m387A1257B972674675539257DF1AEE173AB1642E ();
// 0x0000029F Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.StorageFile::GetFileFromPathAsync(System.String)
extern void StorageFile_GetFileFromPathAsync_m9F81FC387822425FEA45811BA3ABB6E04DC45937 ();
// 0x000002A0 System.Void Windows.Storage.StorageFile::Finalize()
extern void StorageFile_Finalize_m5D4907DD76378B1B469DD96F398A2B3F16B7E9C4 ();
// 0x000002A1 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.StorageFolder::CreateFileAsync(System.String,Windows.Storage.CreationCollisionOption)
extern void StorageFolder_CreateFileAsync_m03F8C43977E395A4EE54118649C11520B553A4DE ();
// 0x000002A2 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFolder> Windows.Storage.StorageFolder::CreateFolderAsync(System.String,Windows.Storage.CreationCollisionOption)
extern void StorageFolder_CreateFolderAsync_m51031ACA6F47AC2885FD6405BFD267301890AA35 ();
// 0x000002A3 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFile> Windows.Storage.StorageFolder::GetFileAsync(System.String)
extern void StorageFolder_GetFileAsync_mD8C3D41300E9A1EA81D518285472B6C3375F40B4 ();
// 0x000002A4 Windows.Foundation.IAsyncOperation`1<Windows.Storage.StorageFolder> Windows.Storage.StorageFolder::GetFolderAsync(System.String)
extern void StorageFolder_GetFolderAsync_mB3A1D9D51C38114DE488D0642EFA267A7C584959 ();
// 0x000002A5 System.String Windows.Storage.StorageFolder::get_Name()
extern void StorageFolder_get_Name_mE9F2D7E1A765E3E7DF35E2FDCC1E5D65EB58646E ();
// 0x000002A6 System.String Windows.Storage.StorageFolder::get_Path()
extern void StorageFolder_get_Path_m554AF6FC4C2D85267A933F07812930C901B0ECE4 ();
// 0x000002A7 System.Void Windows.Storage.StorageFolder::Finalize()
extern void StorageFolder_Finalize_m7CA25B3B42C216FA0EC858383E2D11739A183B9F ();
// 0x000002A8 System.Void Windows.Storage.Streams.DataReader::.ctor(Windows.Storage.Streams.IInputStream)
extern void DataReader__ctor_mA815BC225A32DC3C5E461B4187EF8F0DA357F98B ();
// 0x000002A9 System.Void Windows.Storage.Streams.DataReader::ReadBytes(System.Byte[])
extern void DataReader_ReadBytes_mBC023AA8541AC227458AB11A35289C327EB76A42 ();
// 0x000002AA Windows.Storage.Streams.DataReaderLoadOperation Windows.Storage.Streams.DataReader::LoadAsync(System.UInt32)
extern void DataReader_LoadAsync_m79F9DC31F4227ED504BDBE0C52EB94FE30D51F9A ();
// 0x000002AB System.Void Windows.Storage.Streams.DataReader::Close()
extern void DataReader_Close_mBBC1BF39F9E428BDCE279FF6599CE1792F225A5E ();
// 0x000002AC Windows.Storage.Streams.DataReader Windows.Storage.Streams.DataReader::FromBuffer(Windows.Storage.Streams.IBuffer)
extern void DataReader_FromBuffer_m7534BFED3BE632F61AB605A1CB8E15FF50068386 ();
// 0x000002AD System.Void Windows.Storage.Streams.DataReader::Dispose()
extern void DataReader_Dispose_m75B122AFEFAA7C360CFDAD5A35924DB6D0EE41A7 ();
// 0x000002AE System.Void Windows.Storage.Streams.DataReader::Finalize()
extern void DataReader_Finalize_mC67F3FD301147166DA6480D957AE9932D81BA3EE ();
// 0x000002AF System.Void Windows.Storage.Streams.DataReaderLoadOperation::put_Completed(Windows.Foundation.AsyncOperationCompletedHandler`1<System.UInt32>)
extern void DataReaderLoadOperation_put_Completed_m5B9121C03BC6836A54D5BDE401B8D397D2E11E51 ();
// 0x000002B0 Windows.Foundation.AsyncOperationCompletedHandler`1<System.UInt32> Windows.Storage.Streams.DataReaderLoadOperation::get_Completed()
extern void DataReaderLoadOperation_get_Completed_mC14F994FB4A5DA7FBFA416C58778F62901D4ED31 ();
// 0x000002B1 System.UInt32 Windows.Storage.Streams.DataReaderLoadOperation::GetResults()
extern void DataReaderLoadOperation_GetResults_mC54CF30022C106A3DEF5B83E5BA75482B2B0B8CA ();
// 0x000002B2 System.UInt32 Windows.Storage.Streams.DataReaderLoadOperation::get_Id()
extern void DataReaderLoadOperation_get_Id_mD9EAB7D1F0C3B3B2B221B82CBE017F2623E16A97 ();
// 0x000002B3 Windows.Foundation.AsyncStatus Windows.Storage.Streams.DataReaderLoadOperation::get_Status()
extern void DataReaderLoadOperation_get_Status_mE47D478CCBE6743E89AD0967154B86485DDCA24E ();
// 0x000002B4 System.Exception Windows.Storage.Streams.DataReaderLoadOperation::get_ErrorCode()
extern void DataReaderLoadOperation_get_ErrorCode_m216D09604C403DC996A2F5EB076B64F5401F3C35 ();
// 0x000002B5 System.Void Windows.Storage.Streams.DataReaderLoadOperation::Cancel()
extern void DataReaderLoadOperation_Cancel_mF3165597DFEB58FAF8DFA5C1EE177DA30D773385 ();
// 0x000002B6 System.Void Windows.Storage.Streams.DataReaderLoadOperation::Close()
extern void DataReaderLoadOperation_Close_mA18BBC13B38C7FE8039A7D51336B52FCDE9350FE ();
// 0x000002B7 System.Void Windows.Storage.Streams.DataReaderLoadOperation::Finalize()
extern void DataReaderLoadOperation_Finalize_mDFF40F1879C8308A006226C12C83140C28A3DFA2 ();
// 0x000002B8 System.UInt32 Windows.Storage.Streams.IBuffer::get_Capacity()
extern void IBuffer_get_Capacity_m5745CFEF40ED7190B05A09CE37B07650634038C9 ();
// 0x000002B9 System.UInt32 Windows.Storage.Streams.IBuffer::get_Length()
extern void IBuffer_get_Length_mDADF6292B118D0FD40831F9C33D7EFE7AEDB889B ();
// 0x000002BA System.Void Windows.Storage.Streams.IBuffer::put_Length(System.UInt32)
extern void IBuffer_put_Length_m60458A1E0C478452AFD6A20A5B7090AEA469A8B0 ();
// 0x000002BB System.Void Windows.Storage.Streams.IContentTypeProvider::U24__Stripped0_get_ContentType()
// 0x000002BC System.Void Windows.Storage.Streams.IDataReader::U24__Stripped0_get_UnconsumedBufferLength()
// 0x000002BD System.Void Windows.Storage.Streams.IDataReader::U24__Stripped1_get_UnicodeEncoding()
// 0x000002BE System.Void Windows.Storage.Streams.IDataReader::U24__Stripped2_put_UnicodeEncoding()
// 0x000002BF System.Void Windows.Storage.Streams.IDataReader::U24__Stripped3_get_ByteOrder()
// 0x000002C0 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped4_put_ByteOrder()
// 0x000002C1 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped5_get_InputStreamOptions()
// 0x000002C2 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped6_put_InputStreamOptions()
// 0x000002C3 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped7_ReadByte()
// 0x000002C4 System.Void Windows.Storage.Streams.IDataReader::ReadBytes(System.Byte[])
extern void IDataReader_ReadBytes_m2A817837CC3EF0E8C47136D41BDDBD831A44AEFC ();
// 0x000002C5 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped8_ReadBuffer()
// 0x000002C6 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped9_ReadBoolean()
// 0x000002C7 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped10_ReadGuid()
// 0x000002C8 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped11_ReadInt16()
// 0x000002C9 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped12_ReadInt32()
// 0x000002CA System.Void Windows.Storage.Streams.IDataReader::U24__Stripped13_ReadInt64()
// 0x000002CB System.Void Windows.Storage.Streams.IDataReader::U24__Stripped14_ReadUInt16()
// 0x000002CC System.Void Windows.Storage.Streams.IDataReader::U24__Stripped15_ReadUInt32()
// 0x000002CD System.Void Windows.Storage.Streams.IDataReader::U24__Stripped16_ReadUInt64()
// 0x000002CE System.Void Windows.Storage.Streams.IDataReader::U24__Stripped17_ReadSingle()
// 0x000002CF System.Void Windows.Storage.Streams.IDataReader::U24__Stripped18_ReadDouble()
// 0x000002D0 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped19_ReadString()
// 0x000002D1 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped20_ReadDateTime()
// 0x000002D2 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped21_ReadTimeSpan()
// 0x000002D3 Windows.Storage.Streams.DataReaderLoadOperation Windows.Storage.Streams.IDataReader::LoadAsync(System.UInt32)
extern void IDataReader_LoadAsync_m624CE64319C1FF6F17D1526083E91F4E91187CE7 ();
// 0x000002D4 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped22_DetachBuffer()
// 0x000002D5 System.Void Windows.Storage.Streams.IDataReader::U24__Stripped23_DetachStream()
// 0x000002D6 Windows.Storage.Streams.DataReader Windows.Storage.Streams.IDataReaderFactory::CreateDataReader(Windows.Storage.Streams.IInputStream)
// 0x000002D7 Windows.Storage.Streams.DataReader Windows.Storage.Streams.IDataReaderStatics::FromBuffer(Windows.Storage.Streams.IBuffer)
// 0x000002D8 Windows.Foundation.IAsyncOperationWithProgress`2<Windows.Storage.Streams.IBuffer,System.UInt32> Windows.Storage.Streams.IInputStream::ReadAsync(Windows.Storage.Streams.IBuffer,System.UInt32,Windows.Storage.Streams.InputStreamOptions)
extern void IInputStream_ReadAsync_mCCC52942D8DECE2C743C51DD683483531F495C88 ();
// 0x000002D9 System.Void Windows.Storage.Streams.IInputStreamReference::U24__Stripped0_OpenSequentialReadAsync()
// 0x000002DA Windows.Foundation.IAsyncOperationWithProgress`2<System.UInt32,System.UInt32> Windows.Storage.Streams.IOutputStream::WriteAsync(Windows.Storage.Streams.IBuffer)
extern void IOutputStream_WriteAsync_mBE75066C704E76E8AC90135CE779B83A3DD71921 ();
// 0x000002DB Windows.Foundation.IAsyncOperation`1<System.Boolean> Windows.Storage.Streams.IOutputStream::FlushAsync()
extern void IOutputStream_FlushAsync_mCD6E8AF55B37FFF946B184F22B390E2E652F9ADB ();
// 0x000002DC System.UInt64 Windows.Storage.Streams.IRandomAccessStream::get_Size()
extern void IRandomAccessStream_get_Size_mC7C3FD2E620A221E63F7F6AD1E806A68B3E76C03 ();
// 0x000002DD System.Void Windows.Storage.Streams.IRandomAccessStream::put_Size(System.UInt64)
extern void IRandomAccessStream_put_Size_m0501A392E49E4FF49F7A2B165DDEC5ABC0C69C6E ();
// 0x000002DE System.Void Windows.Storage.Streams.IRandomAccessStream::U24__Stripped0_GetInputStreamAt()
// 0x000002DF System.Void Windows.Storage.Streams.IRandomAccessStream::U24__Stripped1_GetOutputStreamAt()
// 0x000002E0 System.UInt64 Windows.Storage.Streams.IRandomAccessStream::get_Position()
extern void IRandomAccessStream_get_Position_m48DEBF691715522B04ED7539AE7F22041221F646 ();
// 0x000002E1 System.Void Windows.Storage.Streams.IRandomAccessStream::Seek(System.UInt64)
extern void IRandomAccessStream_Seek_m591EC62FCE22806BA0B5A1EFDEA9E58DC63E6D1A ();
// 0x000002E2 System.Void Windows.Storage.Streams.IRandomAccessStream::U24__Stripped2_CloneStream()
// 0x000002E3 System.Boolean Windows.Storage.Streams.IRandomAccessStream::get_CanRead()
extern void IRandomAccessStream_get_CanRead_mC556996DA2DF65C97288F7474DE32FAD8EBFF2BD ();
// 0x000002E4 System.Boolean Windows.Storage.Streams.IRandomAccessStream::get_CanWrite()
extern void IRandomAccessStream_get_CanWrite_m494C042532256B5EB0A66ADFBD50C8F3CC8E5580 ();
// 0x000002E5 Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> Windows.Storage.Streams.IRandomAccessStreamReference::OpenReadAsync()
extern void IRandomAccessStreamReference_OpenReadAsync_m41E9FEBEB9E9454B1C7FD349F974581B93FB53C3 ();
// 0x000002E6 System.Void Windows.System.ILauncherStatics::U24__Stripped0_LaunchFileAsync()
// 0x000002E7 System.Void Windows.System.ILauncherStatics::U24__Stripped1_LaunchFileAsync()
// 0x000002E8 Windows.Foundation.IAsyncOperation`1<System.Boolean> Windows.System.ILauncherStatics::LaunchUriAsync(System.Uri)
// 0x000002E9 System.Void Windows.System.ILauncherStatics::U24__Stripped2_LaunchUriAsync()
// 0x000002EA System.Void Windows.System.ILauncherStatics2::U24__Stripped0_LaunchUriForResultsAsync()
// 0x000002EB System.Void Windows.System.ILauncherStatics2::U24__Stripped1_LaunchUriForResultsAsync()
// 0x000002EC System.Void Windows.System.ILauncherStatics2::U24__Stripped2_LaunchUriAsync()
// 0x000002ED System.Void Windows.System.ILauncherStatics2::U24__Stripped3_QueryUriSupportAsync()
// 0x000002EE System.Void Windows.System.ILauncherStatics2::U24__Stripped4_QueryUriSupportAsync()
// 0x000002EF System.Void Windows.System.ILauncherStatics2::U24__Stripped5_QueryFileSupportAsync()
// 0x000002F0 System.Void Windows.System.ILauncherStatics2::U24__Stripped6_QueryFileSupportAsync()
// 0x000002F1 System.Void Windows.System.ILauncherStatics2::U24__Stripped7_FindUriSchemeHandlersAsync()
// 0x000002F2 System.Void Windows.System.ILauncherStatics2::U24__Stripped8_FindUriSchemeHandlersAsync()
// 0x000002F3 System.Void Windows.System.ILauncherStatics2::U24__Stripped9_FindFileHandlersAsync()
// 0x000002F4 System.Void Windows.System.ILauncherStatics3::U24__Stripped0_LaunchFolderAsync()
// 0x000002F5 System.Void Windows.System.ILauncherStatics3::U24__Stripped1_LaunchFolderAsync()
// 0x000002F6 System.Void Windows.System.ILauncherStatics4::U24__Stripped0_QueryAppUriSupportAsync()
// 0x000002F7 System.Void Windows.System.ILauncherStatics4::U24__Stripped1_QueryAppUriSupportAsync()
// 0x000002F8 System.Void Windows.System.ILauncherStatics4::U24__Stripped2_FindAppUriHandlersAsync()
// 0x000002F9 System.Void Windows.System.ILauncherStatics4::U24__Stripped3_LaunchUriForUserAsync()
// 0x000002FA System.Void Windows.System.ILauncherStatics4::U24__Stripped4_LaunchUriForUserAsync()
// 0x000002FB System.Void Windows.System.ILauncherStatics4::U24__Stripped5_LaunchUriForUserAsync()
// 0x000002FC System.Void Windows.System.ILauncherStatics4::U24__Stripped6_LaunchUriForResultsForUserAsync()
// 0x000002FD System.Void Windows.System.ILauncherStatics4::U24__Stripped7_LaunchUriForResultsForUserAsync()
// 0x000002FE System.Void Windows.System.ILauncherStatics5::U24__Stripped0_LaunchFolderPathAsync()
// 0x000002FF System.Void Windows.System.ILauncherStatics5::U24__Stripped1_LaunchFolderPathAsync()
// 0x00000300 System.Void Windows.System.ILauncherStatics5::U24__Stripped2_LaunchFolderPathForUserAsync()
// 0x00000301 System.Void Windows.System.ILauncherStatics5::U24__Stripped3_LaunchFolderPathForUserAsync()
// 0x00000302 System.UInt64 Windows.System.IMemoryManagerStatics::get_AppMemoryUsage()
// 0x00000303 System.UInt64 Windows.System.IMemoryManagerStatics::get_AppMemoryUsageLimit()
// 0x00000304 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped0_get_AppMemoryUsageLevel()
// 0x00000305 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped1_add_AppMemoryUsageIncreased()
// 0x00000306 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped2_remove_AppMemoryUsageIncreased()
// 0x00000307 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped3_add_AppMemoryUsageDecreased()
// 0x00000308 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped4_remove_AppMemoryUsageDecreased()
// 0x00000309 System.Void Windows.System.IMemoryManagerStatics::U24__Stripped5_add_AppMemoryUsageLimitChanging()
// 0x0000030A System.Void Windows.System.IMemoryManagerStatics::U24__Stripped6_remove_AppMemoryUsageLimitChanging()
// 0x0000030B System.Void Windows.System.IMemoryManagerStatics2::U24__Stripped0_GetAppMemoryReport()
// 0x0000030C System.Void Windows.System.IMemoryManagerStatics2::U24__Stripped1_GetProcessMemoryReport()
// 0x0000030D System.Void Windows.System.IMemoryManagerStatics3::U24__Stripped0_TrySetAppMemoryUsageLimit()
// 0x0000030E System.Void Windows.System.IMemoryManagerStatics4::U24__Stripped0_get_ExpectedAppMemoryUsageLimit()
// 0x0000030F Windows.Foundation.IAsyncOperation`1<System.Boolean> Windows.System.Launcher::LaunchUriAsync(System.Uri)
extern void Launcher_LaunchUriAsync_m895997EB22B4003E990AFFDA4704E7A4FB96D9F3 ();
// 0x00000310 System.Void Windows.System.Launcher::Finalize()
extern void Launcher_Finalize_m6C9ADD94B7E29E8001859D7F164554402A275AC1 ();
// 0x00000311 System.UInt64 Windows.System.MemoryManager::get_AppMemoryUsage()
extern void MemoryManager_get_AppMemoryUsage_m3BBDE59FC2DCB6074A92C7859CC42E3311BD8D3B ();
// 0x00000312 System.UInt64 Windows.System.MemoryManager::get_AppMemoryUsageLimit()
extern void MemoryManager_get_AppMemoryUsageLimit_m8A8BF85D26D6A48DE98963D3BDB7C87B7FD67113 ();
// 0x00000313 System.Void Windows.System.MemoryManager::Finalize()
extern void MemoryManager_Finalize_m118D60A4217AA9572B93529E7B46F6AEEFD059A7 ();
// 0x00000314 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped0_get_HasTouchpad()
// 0x00000315 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped1_get_HasThumbstick()
// 0x00000316 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped2_get_SimpleHapticsController()
// 0x00000317 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped3_get_VendorId()
// 0x00000318 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped4_get_ProductId()
// 0x00000319 System.Void Windows.UI.Input.Spatial.ISpatialInteractionController::U24__Stripped5_get_Version()
// 0x0000031A Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> Windows.UI.Input.Spatial.ISpatialInteractionController2::TryGetRenderableModelAsync()
// 0x0000031B System.Void Windows.UI.Input.Spatial.ISpatialInteractionController3::U24__Stripped0_TryGetBatteryReport()
// 0x0000031C System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped0_add_SourceDetected()
// 0x0000031D System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped1_remove_SourceDetected()
// 0x0000031E System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped2_add_SourceLost()
// 0x0000031F System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped3_remove_SourceLost()
// 0x00000320 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped4_add_SourceUpdated()
// 0x00000321 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped5_remove_SourceUpdated()
// 0x00000322 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped6_add_SourcePressed()
// 0x00000323 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped7_remove_SourcePressed()
// 0x00000324 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped8_add_SourceReleased()
// 0x00000325 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped9_remove_SourceReleased()
// 0x00000326 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped10_add_InteractionDetected()
// 0x00000327 System.Void Windows.UI.Input.Spatial.ISpatialInteractionManager::U24__Stripped11_remove_InteractionDetected()
// 0x00000328 System.Collections.Generic.IReadOnlyList`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState> Windows.UI.Input.Spatial.ISpatialInteractionManager::GetDetectedSourcesAtTimestamp(Windows.Perception.PerceptionTimestamp)
// 0x00000329 Windows.UI.Input.Spatial.SpatialInteractionManager Windows.UI.Input.Spatial.ISpatialInteractionManagerStatics::GetForCurrentView()
// 0x0000032A System.Boolean Windows.UI.Input.Spatial.ISpatialInteractionManagerStatics2::IsSourceKindSupported(Windows.UI.Input.Spatial.SpatialInteractionSourceKind)
// 0x0000032B System.UInt32 Windows.UI.Input.Spatial.ISpatialInteractionSource::get_Id()
// 0x0000032C System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource::U24__Stripped0_get_Kind()
// 0x0000032D System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource2::U24__Stripped0_get_IsPointingSupported()
// 0x0000032E System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource2::U24__Stripped1_get_IsMenuSupported()
// 0x0000032F System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource2::U24__Stripped2_get_IsGraspSupported()
// 0x00000330 Windows.UI.Input.Spatial.SpatialInteractionController Windows.UI.Input.Spatial.ISpatialInteractionSource2::get_Controller()
// 0x00000331 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource2::U24__Stripped3_TryGetStateAtTimestamp()
// 0x00000332 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource3::U24__Stripped0_get_Handedness()
// 0x00000333 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSource4::U24__Stripped0_TryCreateHandMeshObserver()
// 0x00000334 Windows.Foundation.IAsyncOperation`1<Windows.Perception.People.HandMeshObserver> Windows.UI.Input.Spatial.ISpatialInteractionSource4::TryCreateHandMeshObserverAsync()
// 0x00000335 Windows.UI.Input.Spatial.SpatialInteractionSource Windows.UI.Input.Spatial.ISpatialInteractionSourceState::get_Source()
// 0x00000336 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState::U24__Stripped0_get_Properties()
// 0x00000337 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState::U24__Stripped1_get_IsPressed()
// 0x00000338 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState::U24__Stripped2_get_Timestamp()
// 0x00000339 System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState::U24__Stripped3_TryGetPointerPose()
// 0x0000033A System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState2::U24__Stripped0_get_IsSelectPressed()
// 0x0000033B System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState2::U24__Stripped1_get_IsMenuPressed()
// 0x0000033C System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState2::U24__Stripped2_get_IsGrasped()
// 0x0000033D System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState2::U24__Stripped3_get_SelectPressedValue()
// 0x0000033E System.Void Windows.UI.Input.Spatial.ISpatialInteractionSourceState2::U24__Stripped4_get_ControllerProperties()
// 0x0000033F Windows.Perception.People.HandPose Windows.UI.Input.Spatial.ISpatialInteractionSourceState3::TryGetHandPose()
// 0x00000340 System.Void Windows.UI.Input.Spatial.ISpatialPointerPose::U24__Stripped0_get_Timestamp()
// 0x00000341 System.Void Windows.UI.Input.Spatial.ISpatialPointerPose::U24__Stripped1_get_Head()
// 0x00000342 System.Void Windows.UI.Input.Spatial.ISpatialPointerPose2::U24__Stripped0_TryGetInteractionSourcePose()
// 0x00000343 Windows.Perception.People.EyesPose Windows.UI.Input.Spatial.ISpatialPointerPose3::get_Eyes()
// 0x00000344 System.Void Windows.UI.Input.Spatial.ISpatialPointerPose3::U24__Stripped0_get_IsHeadCapturedBySystem()
// 0x00000345 Windows.UI.Input.Spatial.SpatialPointerPose Windows.UI.Input.Spatial.ISpatialPointerPoseStatics::TryGetAtTimestamp(Windows.Perception.Spatial.SpatialCoordinateSystem,Windows.Perception.PerceptionTimestamp)
// 0x00000346 Windows.Foundation.IAsyncOperation`1<Windows.Storage.Streams.IRandomAccessStreamWithContentType> Windows.UI.Input.Spatial.SpatialInteractionController::TryGetRenderableModelAsync()
extern void SpatialInteractionController_TryGetRenderableModelAsync_mC211C0D21A15F56F91E8A2B63F7618B19FF0869F ();
// 0x00000347 System.Void Windows.UI.Input.Spatial.SpatialInteractionController::Finalize()
extern void SpatialInteractionController_Finalize_m067220C2BF20B93548C714FC21E3C60AB04BF6FE ();
// 0x00000348 System.Collections.Generic.IReadOnlyList`1<Windows.UI.Input.Spatial.SpatialInteractionSourceState> Windows.UI.Input.Spatial.SpatialInteractionManager::GetDetectedSourcesAtTimestamp(Windows.Perception.PerceptionTimestamp)
extern void SpatialInteractionManager_GetDetectedSourcesAtTimestamp_m05ED57C7DCC6235ABFDC2687C1E8ECE305DCCC2A ();
// 0x00000349 System.Boolean Windows.UI.Input.Spatial.SpatialInteractionManager::IsSourceKindSupported(Windows.UI.Input.Spatial.SpatialInteractionSourceKind)
extern void SpatialInteractionManager_IsSourceKindSupported_mD85293F528AAC9D7B3976A5742E4F8BE9D951873 ();
// 0x0000034A Windows.UI.Input.Spatial.SpatialInteractionManager Windows.UI.Input.Spatial.SpatialInteractionManager::GetForCurrentView()
extern void SpatialInteractionManager_GetForCurrentView_m82E6D63B8FF621584770E431FCC715153D1BB53B ();
// 0x0000034B System.Void Windows.UI.Input.Spatial.SpatialInteractionManager::Finalize()
extern void SpatialInteractionManager_Finalize_mDEE46B05E2AAE45028F54C4CFCC07D9C3DF63BA7 ();
// 0x0000034C System.UInt32 Windows.UI.Input.Spatial.SpatialInteractionSource::get_Id()
extern void SpatialInteractionSource_get_Id_mB951FE8156B99B8019862574020299C1FBE1046E ();
// 0x0000034D Windows.UI.Input.Spatial.SpatialInteractionController Windows.UI.Input.Spatial.SpatialInteractionSource::get_Controller()
extern void SpatialInteractionSource_get_Controller_m65FC82975089D5C94D79A686DC44B012B244E47A ();
// 0x0000034E Windows.Foundation.IAsyncOperation`1<Windows.Perception.People.HandMeshObserver> Windows.UI.Input.Spatial.SpatialInteractionSource::TryCreateHandMeshObserverAsync()
extern void SpatialInteractionSource_TryCreateHandMeshObserverAsync_m579BCF683A9AF55D46D09730F4AD40670EFF1835 ();
// 0x0000034F System.Void Windows.UI.Input.Spatial.SpatialInteractionSource::Finalize()
extern void SpatialInteractionSource_Finalize_m069D38EDFC520B92CC522A14D2D290A1FC7651A6 ();
// 0x00000350 Windows.UI.Input.Spatial.SpatialInteractionSource Windows.UI.Input.Spatial.SpatialInteractionSourceState::get_Source()
extern void SpatialInteractionSourceState_get_Source_m4869A5CAE87869961CC4C7322D71008B42C3E779 ();
// 0x00000351 Windows.Perception.People.HandPose Windows.UI.Input.Spatial.SpatialInteractionSourceState::TryGetHandPose()
extern void SpatialInteractionSourceState_TryGetHandPose_m982D9241242B89C5B714CF4E42601EEF0870DA0B ();
// 0x00000352 System.Void Windows.UI.Input.Spatial.SpatialInteractionSourceState::Finalize()
extern void SpatialInteractionSourceState_Finalize_m06799F82FCF4D9CD484397B4CECCCE4DEDF13BD5 ();
// 0x00000353 Windows.Perception.People.EyesPose Windows.UI.Input.Spatial.SpatialPointerPose::get_Eyes()
extern void SpatialPointerPose_get_Eyes_mB1A9DF3E82A698A9DEA917D0481C50054A75AD69 ();
// 0x00000354 Windows.UI.Input.Spatial.SpatialPointerPose Windows.UI.Input.Spatial.SpatialPointerPose::TryGetAtTimestamp(Windows.Perception.Spatial.SpatialCoordinateSystem,Windows.Perception.PerceptionTimestamp)
extern void SpatialPointerPose_TryGetAtTimestamp_m86E035A372E4724987F5D572081513327F3435D8 ();
// 0x00000355 System.Void Windows.UI.Input.Spatial.SpatialPointerPose::Finalize()
extern void SpatialPointerPose_Finalize_m1A878B5FE34401D17122E29B1A500A3485D247A9 ();
// 0x00000356 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.UI.ViewManagement.IInputPane::add_Showing(Windows.Foundation.TypedEventHandler`2<Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs>)
// 0x00000357 System.Void Windows.UI.ViewManagement.IInputPane::remove_Showing(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
// 0x00000358 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.UI.ViewManagement.IInputPane::add_Hiding(Windows.Foundation.TypedEventHandler`2<Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs>)
// 0x00000359 System.Void Windows.UI.ViewManagement.IInputPane::remove_Hiding(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
// 0x0000035A System.Void Windows.UI.ViewManagement.IInputPane::U24__Stripped0_get_OccludedRect()
// 0x0000035B System.Boolean Windows.UI.ViewManagement.IInputPane2::TryShow()
// 0x0000035C System.Boolean Windows.UI.ViewManagement.IInputPane2::TryHide()
// 0x0000035D System.Void Windows.UI.ViewManagement.IInputPaneControl::U24__Stripped0_get_Visible()
// 0x0000035E System.Void Windows.UI.ViewManagement.IInputPaneControl::U24__Stripped1_put_Visible()
// 0x0000035F Windows.UI.ViewManagement.InputPane Windows.UI.ViewManagement.IInputPaneStatics::GetForCurrentView()
// 0x00000360 System.Void Windows.UI.ViewManagement.IInputPaneStatics2::U24__Stripped0_GetForUIContext()
// 0x00000361 System.Void Windows.UI.ViewManagement.IInputPaneVisibilityEventArgs::U24__Stripped0_get_OccludedRect()
// 0x00000362 System.Void Windows.UI.ViewManagement.IInputPaneVisibilityEventArgs::U24__Stripped1_put_EnsuredFocusedElementInView()
// 0x00000363 System.Void Windows.UI.ViewManagement.IInputPaneVisibilityEventArgs::U24__Stripped2_get_EnsuredFocusedElementInView()
// 0x00000364 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.UI.ViewManagement.InputPane::add_Showing(Windows.Foundation.TypedEventHandler`2<Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs>)
extern void InputPane_add_Showing_m01D1CAD6737FD3744B580A51BB4D9022DEEEF998 ();
// 0x00000365 System.Void Windows.UI.ViewManagement.InputPane::remove_Showing(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
extern void InputPane_remove_Showing_mD771A5466DD120825B177C98B59FCD80E045641B ();
// 0x00000366 System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken Windows.UI.ViewManagement.InputPane::add_Hiding(Windows.Foundation.TypedEventHandler`2<Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs>)
extern void InputPane_add_Hiding_m0BC33F3185CC399A61B5EA74DA7D179AFA36E16A ();
// 0x00000367 System.Void Windows.UI.ViewManagement.InputPane::remove_Hiding(System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken)
extern void InputPane_remove_Hiding_m6148A6A1D540823C34DD35AD36839AB9294E1BEB ();
// 0x00000368 System.Boolean Windows.UI.ViewManagement.InputPane::TryShow()
extern void InputPane_TryShow_mC49C9A2569A559E0EDBF0C32C391CB53128119F3 ();
// 0x00000369 System.Boolean Windows.UI.ViewManagement.InputPane::TryHide()
extern void InputPane_TryHide_m018BE2BC1BC3B0FDEAD949DB5CBDD84C2EC37E54 ();
// 0x0000036A Windows.UI.ViewManagement.InputPane Windows.UI.ViewManagement.InputPane::GetForCurrentView()
extern void InputPane_GetForCurrentView_mC70FE8BAFBC2F91A3166DD901A44DB0709538EC2 ();
// 0x0000036B System.Void Windows.UI.ViewManagement.InputPane::Finalize()
extern void InputPane_Finalize_m4780B175D6C1BA2A5F0A1CAB99438FB222A28D3A ();
// 0x0000036C System.Void Windows.UI.ViewManagement.InputPaneVisibilityEventArgs::Finalize()
extern void InputPaneVisibilityEventArgs_Finalize_m1C521FD279440FA70097D64D4AC0355414205F91 ();
// 0x0000036D Windows.UI.Xaml.Interop.IBindableIterator Windows.UI.Xaml.Interop.IBindableIterable::First()
extern void IBindableIterable_First_m35A822CD2DF5C55F51539416F98640C7123A6C63 ();
// 0x0000036E System.Object Windows.UI.Xaml.Interop.IBindableIterator::get_Current()
extern void IBindableIterator_get_Current_m1EF798DD2A9BBF29A1FDBD113D1EA7336230E7D9 ();
// 0x0000036F System.Boolean Windows.UI.Xaml.Interop.IBindableIterator::get_HasCurrent()
extern void IBindableIterator_get_HasCurrent_m8F6F66EDB43F4A356B5E1C61A46D276B5E952E70 ();
// 0x00000370 System.Boolean Windows.UI.Xaml.Interop.IBindableIterator::MoveNext()
extern void IBindableIterator_MoveNext_m6ACF76CEFB49BA5FD184348B4A3F495D0E47395A ();
// 0x00000371 System.Object Windows.UI.Xaml.Interop.IBindableVector::GetAt(System.UInt32)
extern void IBindableVector_GetAt_m860227C0726F0C40C40300F2D114C90C040DCFBD ();
// 0x00000372 System.UInt32 Windows.UI.Xaml.Interop.IBindableVector::get_Size()
extern void IBindableVector_get_Size_m1804E8861F18C40EC16058667D11382062A82771 ();
// 0x00000373 Windows.UI.Xaml.Interop.IBindableVectorView Windows.UI.Xaml.Interop.IBindableVector::GetView()
extern void IBindableVector_GetView_m4C2BFF3B150F6EB8019728B4C2C2AF5879A81B55 ();
// 0x00000374 System.Boolean Windows.UI.Xaml.Interop.IBindableVector::IndexOf(System.Object,System.UInt32&)
extern void IBindableVector_IndexOf_mF971A05290A1678F48EC52978E53A9CBA688E489 ();
// 0x00000375 System.Void Windows.UI.Xaml.Interop.IBindableVector::SetAt(System.UInt32,System.Object)
extern void IBindableVector_SetAt_m4658569CC8E08EF010A7EB35E66059F7D4BBFDB8 ();
// 0x00000376 System.Void Windows.UI.Xaml.Interop.IBindableVector::InsertAt(System.UInt32,System.Object)
extern void IBindableVector_InsertAt_mA154571EE15503B3425185CFB55419AA4ED3BD59 ();
// 0x00000377 System.Void Windows.UI.Xaml.Interop.IBindableVector::RemoveAt(System.UInt32)
extern void IBindableVector_RemoveAt_m7B14341EBAE9C5799028FC3EF0771210595F4E3C ();
// 0x00000378 System.Void Windows.UI.Xaml.Interop.IBindableVector::Append(System.Object)
extern void IBindableVector_Append_m623B1AF95642A0AE7BE231983B60CC203FDF4214 ();
// 0x00000379 System.Void Windows.UI.Xaml.Interop.IBindableVector::RemoveAtEnd()
extern void IBindableVector_RemoveAtEnd_m391C9A85A1C813017D9EBB3211CA294156F654FC ();
// 0x0000037A System.Void Windows.UI.Xaml.Interop.IBindableVector::Clear()
extern void IBindableVector_Clear_mBEA87A79EBF207B3F5701BB09506759CBC6F5ED1 ();
// 0x0000037B System.Object Windows.UI.Xaml.Interop.IBindableVectorView::GetAt(System.UInt32)
extern void IBindableVectorView_GetAt_mAA8D68A43FF339E8A336BFD94CC516C26FFB757B ();
// 0x0000037C System.UInt32 Windows.UI.Xaml.Interop.IBindableVectorView::get_Size()
extern void IBindableVectorView_get_Size_mB12143398D9A3942E36CE820551EE06F8EB5A77B ();
// 0x0000037D System.Boolean Windows.UI.Xaml.Interop.IBindableVectorView::IndexOf(System.Object,System.UInt32&)
extern void IBindableVectorView_IndexOf_m091E767F9C60C5FE8A460A65F0C3476F1C1931C0 ();
static Il2CppMethodPointer s_methodPointers[893] = 
{
	AsyncActionCompletedHandler__ctor_m2C0D2BD025FC8CDC21086373277D42874552060D,
	AsyncActionCompletedHandler_Invoke_m90ED997977A5C94AE5F5CB92A629FE2EDFA3D466,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IAsyncAction_put_Completed_m0632776C609159F4E6D0D5F6DECCE64CC355B779,
	IAsyncAction_get_Completed_mEF447B408CEF9BD7882E596CF87C4355CC17E71B,
	IAsyncAction_GetResults_m192BCF4CC6EE9E2E66722E3D2BFBBAEE124D7AD7,
	IAsyncInfo_get_Id_mFB9E7E5D042A091EEA5ADBBB479329CE1248351B,
	IAsyncInfo_get_Status_m3D1D43B45DDBE38620B5178B88307B0759171F7A,
	IAsyncInfo_get_ErrorCode_m9A8D01260F4211B8E794B3FC73D212F3F8CAB584,
	IAsyncInfo_Cancel_m71EA200C07A5E2B5A9BAE4BC2AB00EFF1D3AB5A8,
	IAsyncInfo_Close_m197A13662E7E9A888B0E3FFAFDBA15410AA850E7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IClosable_Close_m9A054CE065D4C97FAF595A8F92B3CB3463C5BCD6,
	IPropertyValue_get_Type_mB7A711F071E38F3BAEC1862D436AC4AC6DC07FAB,
	IPropertyValue_get_IsNumericScalar_m1C1F22011064BCF30C5B7E675AC389AF33EC8E5F,
	IPropertyValue_GetUInt8_m6E0FA81F823A7980FDBE76ECAFF20E57EB6666F6,
	IPropertyValue_GetInt16_m33B5D0C3CF38BC633A0085AE70DB332CCBE396DF,
	IPropertyValue_GetUInt16_mEE71D2389CCEFB98E952C00FB8D6BEC588335A57,
	IPropertyValue_GetInt32_m9D0D781ED2D0EFCFD725E9126184AB423EA45C2B,
	IPropertyValue_GetUInt32_m9186A8BA0E36E15EF0AC2BA14D050DED85699C6E,
	IPropertyValue_GetInt64_mFBA4443B87A23C4162616CE67978E15CC32A158E,
	IPropertyValue_GetUInt64_m01C60E6EA36BD050254AEFE6AF99D1D4977CD15F,
	IPropertyValue_GetSingle_m7DA893EAE56B29828034B018B0844208FF80CCDD,
	IPropertyValue_GetDouble_m34EF323F40F8971CD414DFB38CB6719F071BC869,
	IPropertyValue_GetChar16_mA1539B0EDFFFD74BBCDB28C1FAC9487811D48386,
	IPropertyValue_GetBoolean_m0F0CE0E65CD9E983BCC7DBA628CF546547DAC005,
	IPropertyValue_GetString_m03CC616B8EFD87A5023AB25FC20F934D2F09E169,
	IPropertyValue_GetGuid_mE05F5B0248EA98B227F69B2E02D9B10A7B946FC2,
	IPropertyValue_GetDateTime_m509277C336754537642EEC6D3A68750DDFABA56E,
	IPropertyValue_GetTimeSpan_mE29774C717C9B4EC735C20A8CED6796CDF4E91E5,
	IPropertyValue_GetPoint_m9E32A5CC948BB3572CD3FF6CA2F5B78F26596E73,
	IPropertyValue_GetSize_m7212C32E319B8F552ED909C1287C6786B706A659,
	IPropertyValue_GetRect_mFB23B562FCD514351684F706405000203ADFD829,
	IPropertyValue_GetUInt8Array_m7348AAFD3D46CD731DD94146E5E60CFBD1F10553,
	IPropertyValue_GetInt16Array_m9E714AF77215627B9C8119AF3FA6508C50D206FE,
	IPropertyValue_GetUInt16Array_m4D5B5FB0A7FC0A22E80141B159783C2A0B0FBD0B,
	IPropertyValue_GetInt32Array_m799E55885F6A13610A5051F1C2917A0234B9AC03,
	IPropertyValue_GetUInt32Array_m335BBA978CD7ACD928B58B3D525C68E2B4DE99FF,
	IPropertyValue_GetInt64Array_m2A3152938A9BBA08D7357BC63F400F8C4FD7C76B,
	IPropertyValue_GetUInt64Array_m9B9E15BA794B247E403386503A1F31881DA86BF7,
	IPropertyValue_GetSingleArray_m641609788D53A6254B05EC12B73EC7BACB56D33F,
	IPropertyValue_GetDoubleArray_m796ECF392FE47A8F5AF69F16B1B6694666B13D6B,
	IPropertyValue_GetChar16Array_mA72D84594CACE711C869C39FC47BCE332FC8A618,
	IPropertyValue_GetBooleanArray_m32F829C6CA7AE67D42E0D58D9AB28A14AF056517,
	IPropertyValue_GetStringArray_mC5D40C9E740A5FB497BDB39B81487A5DDB3129B6,
	IPropertyValue_GetInspectableArray_m259D5ED0D3880F47B2AE4C4AC1AA310FAC2662D3,
	IPropertyValue_GetGuidArray_m0147631B0DD882717A53BA395C30E070B164BBF6,
	IPropertyValue_GetDateTimeArray_m6034A0D68EC7E2BD093A76C45BC15F8A2C4A9399,
	IPropertyValue_GetTimeSpanArray_m9DA46A07FE5FDC63435A13AB6D0ED4BFC27D6937,
	IPropertyValue_GetPointArray_mAA9D45EF50AD3D78B725BD20460FFE87205C4DB3,
	IPropertyValue_GetSizeArray_m82A3EF5E089F7CD94188D15910F78F8DD7445FFB,
	IPropertyValue_GetRectArray_m6D1A32BFF9FA7E6C3DBB9200322BDE50CF1196F3,
	NULL,
	NULL,
	IStringable_ToString_m52082689C4261A2691DF8177D839F717B13705FF,
	ActivatableAttribute__ctor_m3A1923F31F174AE97F2B0101314EC0BA758F8AA5,
	ActivatableAttribute__ctor_m9BA03D4599F108D6D0310A55020FADDB76723B8D,
	AllowMultipleAttribute__ctor_m7571A1E76CC239AD97D314F09A83F54D16427607,
	ApiContractAttribute__ctor_mAB0018EB0A4ADE3034A9A6323B64DDB5ACB046F5,
	ApiInformation_IsMethodPresent_m2888CC939FEFAF5B4C739A57F9EE638C79426234,
	ApiInformation_IsPropertyPresent_mA520CD1EF1AEA15A7BD2CAFF7625CF2766C6E6FD,
	ApiInformation_IsApiContractPresent_m88AAAF0DC1DD55B3C0B031C0977AB571BDCBEC62,
	ApiInformation_Finalize_m187D52BE403B1F03BDCEB8434FC7577BD372BAB9,
	AttributeUsageAttribute__ctor_m62442B771738CEF57966B0F060F569C03590A15E,
	ContractVersionAttribute__ctor_m2A857BC1776E7DF83A2C74C4060327725BA4F620,
	ContractVersionAttribute__ctor_mED07DE385B3C7757FC25EDBBEF863B7E10E7BC0B,
	ContractVersionAttribute__ctor_mECE02615A3BF74717E6D6DDD9130F70C6D31894E,
	DefaultAttribute__ctor_mCA1A5A887CCC96CCA65D8C684E040F7DE53C847B,
	DualApiPartitionAttribute__ctor_m43302517DA7AE55C651A0A882BE92207C62760AD,
	ExclusiveToAttribute__ctor_mC32704F33DF1024E6E7736E549567FBA7B0706F6,
	GCPressureAttribute__ctor_mE6205B6DC20F2B69D30BD72D6E07F029BFE8CCE6,
	GuidAttribute__ctor_m6968CB3415E091AB6558B45DDE116F6927C8CF02,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LengthIsAttribute__ctor_m9513073617452E5C0931CF3C18D78517BCA4D42C,
	MarshalingBehaviorAttribute__ctor_m99A98FB82335CB64E7EBC43C32D9BB4AF0323276,
	MuseAttribute__ctor_mE71C75AD616E4F47A57A6BF05C30CEA19849F1B6,
	OverloadAttribute__ctor_mDA2F3A0F74EA36ECA9C97C54EABBFA7FD849031C,
	RemoteAsyncAttribute__ctor_mFAF2329392512D820529928F96B626AA0B6251AC,
	StaticAttribute__ctor_m47D2C33E947357F085280F1DF6B797425634255C,
	ThreadingAttribute__ctor_m1F74D113D96D9DE9E4F582F497D7D8E35B9868EF,
	WebHostHiddenAttribute__ctor_m41D4E691C0F41F6372D72FF38C1FB846080A1F9C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLoader_GetString_m8586C7138823D162F671D6958F3F245C17E6B628,
	ResourceLoader_GetForViewIndependentUse_m3B47FB65B1ED8348C6B7BAA0059004AFEE2E40BD,
	ResourceLoader_Finalize_mB71BCB445F26E1739A1473D35C40BDE5ACC24826,
	IMemoryBuffer_CreateReference_m84973892E6A7C4A897AC07ACFCEF8C915DA93AEC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IUriRuntimeClass_get_RawUri_m78B5E0C829EFB5D1A66092F860F6386C77D0523A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Uri__ctor_m55DB673B115A2FB75273586B8D47620759C730CC,
	Uri__ctor_m97F32BC3726B386B85AFD0382E1B39C2D20E527A,
	Uri_get_AbsoluteUri_m7BFA42C7D52CB5800FD74FED9E447E5C0D9897C7,
	Uri_get_DisplayUri_m80EE99B856781C6225F8983FCD716AF25616BFCC,
	Uri_get_Domain_m0C91AFCC6748C3DD043B6D8E4E99396E6A7AC877,
	Uri_get_Extension_m4BCA3E2A6A7A381C5F25A4CDF661CB2F0EF0AC3B,
	Uri_get_Fragment_m926F742201AA560AD7A28FA636926874FC844254,
	Uri_get_Host_m2BE4A95EAE0A43240B536D466001368ABED44A68,
	Uri_get_Password_m03AD38D3E8F7CE159F7A177C0D3126935CC80830,
	Uri_get_Path_m0D9CC251E59E3CC913AF8DA0CB92DF184D56F2F3,
	Uri_get_Query_m284C7FF8237C6145BF4E830345DEB3614A30DCF5,
	Uri_get_QueryParsed_mA6306FF2AC4431DFF2407A0C186E37F180865FE3,
	Uri_get_RawUri_m00849E771F72BBD94E6AB42A8EE4610A7BC36DB7,
	Uri_get_SchemeName_m5FDC03980E9E649D028F58777F084CA76177D7E2,
	Uri_get_UserName_m938E239ED50710F66056A097110C7865F279BA37,
	Uri_get_Port_mC55B2793356B7129B7351A94EE7146C7C16418EB,
	Uri_get_Suspicious_m8B40D4095C71B12C28F131EB9BAFA2EA8FE2213E,
	Uri_Equals_m518D9A5B0A697F3FAEA3650F3AA32930E669746D,
	Uri_CombineUri_m7A362D69A2B4B4C5A0482F39506A78938F6AEAE7,
	Uri_get_AbsoluteCanonicalUri_m3037ADFF5D31E5BC7209485DF4819AEC3021C824,
	Uri_get_DisplayIri_mBE1BDB9380A1E01D8DCD532D81B8CBFF55FB1FC6,
	Uri_ToString_m803AE2936135C60D584961D0857717E88A51DD55,
	Uri_UnescapeComponent_m8CD7B535D394D7E41432D0F1CFA9287EF3ACA3B9,
	Uri_EscapeComponent_mAFDB9963E2EA253255FF86DF1270959E357EFA1D,
	Uri_Finalize_mAD57CE06963D886EB6DD0082F6B90C5BDDE7E8AC,
	WwwFormUrlDecoder_First_m8D0D362AE3137612020821CBCF8C498CB51EFC79,
	WwwFormUrlDecoder_GetAt_m43ED945C60EA065ECDC8649F80A96D777C5C302A,
	WwwFormUrlDecoder_get_Size_m1D1C2B7B59C49A9693CDF960647A9BC1C4F7321C,
	WwwFormUrlDecoder_IndexOf_mD77DF2F9EE659B35E43C982FC73544A09237405F,
	WwwFormUrlDecoder_GetMany_mCE9F986BD2DAD9CFDDF1176DC33293FC9164A779,
	WwwFormUrlDecoder_get_Item_m068A456764193EA4D524C0CBD03DDA621FEA53DD,
	WwwFormUrlDecoder_get_Count_mC189E36F85384C614621138AA1BCAA1FEB5CB154,
	WwwFormUrlDecoder_GetEnumerator_m551609AA9EE4214DECC647BA95C41E2C475E80E3,
	WwwFormUrlDecoder_GetEnumerator_m4EA94748942773CC0823E901EE63843E5FC218BD,
	WwwFormUrlDecoder_Finalize_mB1B110B2B5BA22503CF62734D1B140820825FF70,
	HolographicDisplay_TryGetViewConfiguration_mAD05250269F4ED3FCA2F303DA511D4ADEAEFFDB7,
	HolographicDisplay_GetDefault_m7102C2AFE1F07195DF9F8D964249E6AAB8BF84DF,
	HolographicDisplay_Finalize_m49CDF9B7C36B2036C803D6F05D15FDCFFF2A7293,
	HolographicFrame_Finalize_m7ACBA8A0BF51FE70349182520250F40D651B1CF7,
	HolographicViewConfiguration_put_IsEnabled_m3476B0065A7EC22D37A44C6BA77B0920495C6C2B,
	HolographicViewConfiguration_Finalize_mF2D783CDE2BB8841593DCCBAF197C0D05D03785A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitmapBuffer_CreateReference_m8602FD8674A59E46848584D5E06313028890F2DF,
	BitmapBuffer_Close_m07248A632436DC7D1FC8A2735E8269BB93700E18,
	BitmapBuffer_Dispose_mD47A3B0BDD57D1FD65D763BE12B998A19EB08127,
	BitmapBuffer_Finalize_m9F24A6995B7A934134869982215FEEF4B1A7D905,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SoftwareBitmap_LockBuffer_m23B648E747F72030D6E3985E7D6CED3FC6624EB9,
	SoftwareBitmap_Close_m432AC22AEF3EF755E948ADA362D8EE123C3ED912,
	SoftwareBitmap_Copy_m88F3148D49144E0921AF558BC45FA5C1CA93BB4D,
	SoftwareBitmap_Convert_m69A3A26001D01FDB674DEFB713B18D2E7FCFFFEA,
	SoftwareBitmap_Dispose_m43281CA2937F3586428C3D24CE2967E5B7469B89,
	SoftwareBitmap_Finalize_m8D21C0287E6BB30BB6AC4D77770C1275196C5721,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MediaFrameArrivedEventArgs_Finalize_mEBCFE091DD50F69085ED3D2F794F4EAE13E898AF,
	MediaFrameFormat_get_Subtype_m8FD725CDFDF2A3009B6BC5988824F9C0F42C2688,
	MediaFrameFormat_get_FrameRate_m401C16BADABF96DCDCC89362DC57388F6B9F1B7A,
	MediaFrameFormat_get_VideoFormat_m0D438D1A7E225A6B9EBED8BA0B99ACF7093FBE5C,
	MediaFrameFormat_Finalize_m879627BDD2CEF9F5555B3B55ADAD0A888C07093A,
	MediaFrameReader_add_FrameArrived_m70D86584FF72C43B88380FA65B7E800979AC3EC3,
	MediaFrameReader_remove_FrameArrived_m6A9A144AF1321C2AB4EBACFE83295B33904D1CCE,
	MediaFrameReader_TryAcquireLatestFrame_mFD595E6B034B121167C809E350FA18ABF433B16B,
	MediaFrameReader_StartAsync_mD00B94B2ADE8C9151FCCC4130DD5BE713D6EEB5F,
	MediaFrameReader_StopAsync_mAB7D7C521B8F61001CE184AEA5A26FAA6CAD6219,
	MediaFrameReader_Close_mED99DD83A468375FE9C3FB51BBCC243E2455CC9E,
	MediaFrameReader_Dispose_m7916FAE70AC08476419D471BC29DBB934688AF87,
	MediaFrameReader_Finalize_m26F437FF525495E71637A3E18B954C968E22454F,
	MediaFrameReference_get_Properties_mA05E567CAA3515FA16644C41CE2DB077D0B0D493,
	MediaFrameReference_get_VideoMediaFrame_mED7947FBE13B3BC0E8FB71663B36E04413FD64DB,
	MediaFrameReference_Close_m0DAE598E79427AE1ABA6B09187EC0DA391BC73CA,
	MediaFrameReference_Dispose_m19D18EDFD49FFE58A2A6D29DFA7CE07B5404BE61,
	MediaFrameReference_Finalize_mE998A8EB857A16D64F389A132039B0532A3F1984,
	MediaFrameSource_get_Info_m280F33DDF494CA64FF56C55C1D08878CABBD43F3,
	MediaFrameSource_get_SupportedFormats_m8FCA5D492E1F2A7E2D0C265B293289BE5DDC3B95,
	MediaFrameSource_SetFormatAsync_m164D8239DBB2D928594CD6B096B3C71C23B97B0B,
	MediaFrameSource_Finalize_mFEF856DC91B46ED1BC71A17A7097D701FE3AA7AE,
	MediaFrameSourceGroup_get_Id_m33E3573E15198AC724E421C40F73515B65A2B106,
	MediaFrameSourceGroup_get_DisplayName_m0479A785405125C6D378F43BB20C485208DD25E7,
	MediaFrameSourceGroup_FindAllAsync_mE587BFF743EF9A3499A6BD642B89CB2F8E9D072D,
	MediaFrameSourceGroup_Finalize_m26E8F4D99F38C52AFAA6C7B63A68EB85B5B30970,
	MediaFrameSourceInfo_get_MediaStreamType_mCF9BBAD9E4A8367B4E6B0256846C10274B723550,
	MediaFrameSourceInfo_Finalize_mFD58ADCC85714EEB7443E54CA01EF4DEF88408D8,
	VideoMediaFrame_get_SoftwareBitmap_m30CB8C4238A58FF5193755B33D4C9B85FBED7D94,
	VideoMediaFrame_Finalize_m022A89FF6DB77A8C4F36BBE3178E084BB913C09F,
	VideoMediaFrameFormat_get_Width_m0BEA3FD8264C7BC5C64788CB8036F05CF551E9AF,
	VideoMediaFrameFormat_get_Height_m53CEF98901AF365A5C5883CEADC6B39DF9043EF1,
	VideoMediaFrameFormat_Finalize_m0CCD39E36A6314230EECC633160BC2AB4633EB89,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MediaCapture__ctor_mEBE907D602FB9D309E9F6CF73D975BA01FE4F342,
	MediaCapture_InitializeAsync_mF5A49F3E4CE77A412A186DC00DD0F8CFFDA5A7CD,
	MediaCapture_Close_mDC19E5968E0CDDE863F44F592CA2EC8083FC401E,
	MediaCapture_get_FrameSources_m0145231D3841EBDADCCF9C5097F348E4551EE12C,
	MediaCapture_CreateFrameReaderAsync_mB7301A500EF6BDF979650BC851E47393F90C4C87,
	MediaCapture_Dispose_m450038C19982448178B7D45682DE531ED42A3CC1,
	MediaCapture_Finalize_m398CBD076F24219044BFE613E4DE0EE45EC81AD2,
	MediaCaptureInitializationSettings__ctor_mB5627F7AB7A8F5E146CEF4EBA6EB080C476BF94E,
	MediaCaptureInitializationSettings_put_StreamingCaptureMode_m5DBBF366F04B2A1C7726C02D2EB39699EA2544AE,
	MediaCaptureInitializationSettings_put_SourceGroup_mABDD33585ED37B912B52F8442C44A865D195A46C,
	MediaCaptureInitializationSettings_put_SharingMode_m0A9B9CFFC9F0D9682FDDEA478855AE4C7FF65FC3,
	MediaCaptureInitializationSettings_put_MemoryPreference_m5EB56AB89F176DCEF2411FA65C0FB8DB51D057F3,
	MediaCaptureInitializationSettings_Finalize_m228F1D5B00EB1C6F3CADE7A9E739227AAA4969B6,
	NULL,
	NULL,
	NULL,
	NULL,
	MediaRatio_get_Numerator_mFD3FC5319D3E5BB50D803371B0435C2CA6E1CFAB,
	MediaRatio_get_Denominator_m3B29DEE537BC7E4602EAAF81D549E4F0DC7BEF6F,
	MediaRatio_Finalize_mD39CF0713FACFF3FF8179C46936C4BD1E85F467B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EyesPose_get_IsCalibrationValid_m8068F4172B56F0E3D002C6AF6FC1157D36BF5638,
	EyesPose_get_Gaze_m5D2EF2C3C421C04A43E29B97FE3D4C6561442622,
	EyesPose_get_UpdateTimestamp_m4862FFCB98CF48418A17480C41DB5EBD433C9FA4,
	EyesPose_IsSupported_m3ADC5B187E3223E9506A0A02B7F7B811BFAD308E,
	EyesPose_RequestAccessAsync_m5BA0E88C19898B68264E3CD09614FAEC4C914B0B,
	EyesPose_Finalize_m82FD23BAA527945E31CC73CEC3E3686FFF097544,
	HandMeshObserver_get_TriangleIndexCount_m72D9A87FD7433C47F4F9F1AB0EB8F37661451466,
	HandMeshObserver_get_VertexCount_m5BF946FC59A9C0E25F545DFEA8C00D6285F4AABC,
	HandMeshObserver_GetTriangleIndices_mA0A850F3FF458E34C3B7DED4BF76FFE0B3B7F772,
	HandMeshObserver_GetVertexStateForPose_m929A618E54D6ED72FC013D830E5DC420BA8FE391,
	HandMeshObserver_get_NeutralPose_m089C4B81C9090BF049E6372A602A60491CEB7D86,
	HandMeshObserver_Finalize_m6C9AC6214584BE4F6B8C2810BEC17D1CEB81F5C3,
	HandMeshVertexState_get_CoordinateSystem_mD4B2BFFD6A3E44494044A9DCF231F1075556A42E,
	HandMeshVertexState_GetVertices_m0A39BCC22D116DE1DB2AC21F6CC93E8DAF97EA97,
	HandMeshVertexState_Finalize_mB9490F5513F0146413693B8548BC9DE9C4AE702D,
	HandPose_TryGetJoints_m1CB7787BE738367E79E74D194FC7EB642901516F,
	HandPose_Finalize_m3FD1AEAED997015D914DD6FB81F2BFB1DED6569D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PerceptionTimestamp_get_TargetTime_mB521E7C9592D22D6990534922606376C6AF67285,
	PerceptionTimestamp_Finalize_m59D16C1DA87A48B375B88EE9EEB22968C7606B44,
	PerceptionTimestampHelper_FromHistoricalTargetTime_m3887862422EFE0EB81A026771F85278980FB2943,
	PerceptionTimestampHelper_Finalize_mC9DAD6F7124A5896BAB8CF4E84B7F2BC0397E1D9,
	NULL,
	SpatialCoordinateSystem_TryGetTransformTo_m9B3D98B85802722F2CC37F275351B746E6A79A25,
	SpatialCoordinateSystem_Finalize_mE29CFDA8D33B2152B88A0F2841E55AD5DA773996,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpatialSurfaceObserver_IsSupported_mCB54738653BC2840AA78E5E083C5E18EF4D3C408,
	SpatialSurfaceObserver_Finalize_mE83A1FAE6C0110CAB5A1A76718BF6C4238AA4E8C,
	FileIO_AppendTextAsync_m5BCC79A14E6ADD76772F6E2BE28D1BD31CC51EB5,
	FileIO_ReadBufferAsync_mF4FFAFC3AF09C53DAE527D9A7C89D683DDEA4531,
	FileIO_Finalize_mA051D2375BF6A19AF021B36DA1176C98BBF6F883,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IStorageFolder_CreateFileAsync_mD20BADEBA391C026E479581424EFF60D5EE729FA,
	NULL,
	IStorageFolder_CreateFolderAsync_m506538F2866DDF4CDA48BFC1EA034B56C0945F0D,
	IStorageFolder_GetFileAsync_m3B0FF62937AAE51707D544E9F9B5041EBB5ABE7D,
	IStorageFolder_GetFolderAsync_mD37B62674AFC57B8B229BCE38C0CE6FD2F4975DF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IStorageItem_get_Name_m598919A4062C6DEEDD83B371F1C7F4CBFCB0AAF8,
	IStorageItem_get_Path_m1F543E2CE313AD19AE39D0EC8C6832ECEDC29678,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	KnownFolders_get_MusicLibrary_m340C733773BC804A47FC8AF73032F5FC201E0398,
	KnownFolders_Finalize_mC87485CFB0FDEF05B0ED2751DBC66E2E4DE52D4A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StorageFile_get_Name_m943DA65F57D87F5C92DEC8ED660827392BDDA147,
	StorageFile_get_Path_mCDC8594ADE45C229095A1B54E5E7D8266036B881,
	StorageFile_OpenReadAsync_m387A1257B972674675539257DF1AEE173AB1642E,
	StorageFile_GetFileFromPathAsync_m9F81FC387822425FEA45811BA3ABB6E04DC45937,
	StorageFile_Finalize_m5D4907DD76378B1B469DD96F398A2B3F16B7E9C4,
	StorageFolder_CreateFileAsync_m03F8C43977E395A4EE54118649C11520B553A4DE,
	StorageFolder_CreateFolderAsync_m51031ACA6F47AC2885FD6405BFD267301890AA35,
	StorageFolder_GetFileAsync_mD8C3D41300E9A1EA81D518285472B6C3375F40B4,
	StorageFolder_GetFolderAsync_mB3A1D9D51C38114DE488D0642EFA267A7C584959,
	StorageFolder_get_Name_mE9F2D7E1A765E3E7DF35E2FDCC1E5D65EB58646E,
	StorageFolder_get_Path_m554AF6FC4C2D85267A933F07812930C901B0ECE4,
	StorageFolder_Finalize_m7CA25B3B42C216FA0EC858383E2D11739A183B9F,
	DataReader__ctor_mA815BC225A32DC3C5E461B4187EF8F0DA357F98B,
	DataReader_ReadBytes_mBC023AA8541AC227458AB11A35289C327EB76A42,
	DataReader_LoadAsync_m79F9DC31F4227ED504BDBE0C52EB94FE30D51F9A,
	DataReader_Close_mBBC1BF39F9E428BDCE279FF6599CE1792F225A5E,
	DataReader_FromBuffer_m7534BFED3BE632F61AB605A1CB8E15FF50068386,
	DataReader_Dispose_m75B122AFEFAA7C360CFDAD5A35924DB6D0EE41A7,
	DataReader_Finalize_mC67F3FD301147166DA6480D957AE9932D81BA3EE,
	DataReaderLoadOperation_put_Completed_m5B9121C03BC6836A54D5BDE401B8D397D2E11E51,
	DataReaderLoadOperation_get_Completed_mC14F994FB4A5DA7FBFA416C58778F62901D4ED31,
	DataReaderLoadOperation_GetResults_mC54CF30022C106A3DEF5B83E5BA75482B2B0B8CA,
	DataReaderLoadOperation_get_Id_mD9EAB7D1F0C3B3B2B221B82CBE017F2623E16A97,
	DataReaderLoadOperation_get_Status_mE47D478CCBE6743E89AD0967154B86485DDCA24E,
	DataReaderLoadOperation_get_ErrorCode_m216D09604C403DC996A2F5EB076B64F5401F3C35,
	DataReaderLoadOperation_Cancel_mF3165597DFEB58FAF8DFA5C1EE177DA30D773385,
	DataReaderLoadOperation_Close_mA18BBC13B38C7FE8039A7D51336B52FCDE9350FE,
	DataReaderLoadOperation_Finalize_mDFF40F1879C8308A006226C12C83140C28A3DFA2,
	IBuffer_get_Capacity_m5745CFEF40ED7190B05A09CE37B07650634038C9,
	IBuffer_get_Length_mDADF6292B118D0FD40831F9C33D7EFE7AEDB889B,
	IBuffer_put_Length_m60458A1E0C478452AFD6A20A5B7090AEA469A8B0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IDataReader_ReadBytes_m2A817837CC3EF0E8C47136D41BDDBD831A44AEFC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IDataReader_LoadAsync_m624CE64319C1FF6F17D1526083E91F4E91187CE7,
	NULL,
	NULL,
	NULL,
	NULL,
	IInputStream_ReadAsync_mCCC52942D8DECE2C743C51DD683483531F495C88,
	NULL,
	IOutputStream_WriteAsync_mBE75066C704E76E8AC90135CE779B83A3DD71921,
	IOutputStream_FlushAsync_mCD6E8AF55B37FFF946B184F22B390E2E652F9ADB,
	IRandomAccessStream_get_Size_mC7C3FD2E620A221E63F7F6AD1E806A68B3E76C03,
	IRandomAccessStream_put_Size_m0501A392E49E4FF49F7A2B165DDEC5ABC0C69C6E,
	NULL,
	NULL,
	IRandomAccessStream_get_Position_m48DEBF691715522B04ED7539AE7F22041221F646,
	IRandomAccessStream_Seek_m591EC62FCE22806BA0B5A1EFDEA9E58DC63E6D1A,
	NULL,
	IRandomAccessStream_get_CanRead_mC556996DA2DF65C97288F7474DE32FAD8EBFF2BD,
	IRandomAccessStream_get_CanWrite_m494C042532256B5EB0A66ADFBD50C8F3CC8E5580,
	IRandomAccessStreamReference_OpenReadAsync_m41E9FEBEB9E9454B1C7FD349F974581B93FB53C3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Launcher_LaunchUriAsync_m895997EB22B4003E990AFFDA4704E7A4FB96D9F3,
	Launcher_Finalize_m6C9ADD94B7E29E8001859D7F164554402A275AC1,
	MemoryManager_get_AppMemoryUsage_m3BBDE59FC2DCB6074A92C7859CC42E3311BD8D3B,
	MemoryManager_get_AppMemoryUsageLimit_m8A8BF85D26D6A48DE98963D3BDB7C87B7FD67113,
	MemoryManager_Finalize_m118D60A4217AA9572B93529E7B46F6AEEFD059A7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpatialInteractionController_TryGetRenderableModelAsync_mC211C0D21A15F56F91E8A2B63F7618B19FF0869F,
	SpatialInteractionController_Finalize_m067220C2BF20B93548C714FC21E3C60AB04BF6FE,
	SpatialInteractionManager_GetDetectedSourcesAtTimestamp_m05ED57C7DCC6235ABFDC2687C1E8ECE305DCCC2A,
	SpatialInteractionManager_IsSourceKindSupported_mD85293F528AAC9D7B3976A5742E4F8BE9D951873,
	SpatialInteractionManager_GetForCurrentView_m82E6D63B8FF621584770E431FCC715153D1BB53B,
	SpatialInteractionManager_Finalize_mDEE46B05E2AAE45028F54C4CFCC07D9C3DF63BA7,
	SpatialInteractionSource_get_Id_mB951FE8156B99B8019862574020299C1FBE1046E,
	SpatialInteractionSource_get_Controller_m65FC82975089D5C94D79A686DC44B012B244E47A,
	SpatialInteractionSource_TryCreateHandMeshObserverAsync_m579BCF683A9AF55D46D09730F4AD40670EFF1835,
	SpatialInteractionSource_Finalize_m069D38EDFC520B92CC522A14D2D290A1FC7651A6,
	SpatialInteractionSourceState_get_Source_m4869A5CAE87869961CC4C7322D71008B42C3E779,
	SpatialInteractionSourceState_TryGetHandPose_m982D9241242B89C5B714CF4E42601EEF0870DA0B,
	SpatialInteractionSourceState_Finalize_m06799F82FCF4D9CD484397B4CECCCE4DEDF13BD5,
	SpatialPointerPose_get_Eyes_mB1A9DF3E82A698A9DEA917D0481C50054A75AD69,
	SpatialPointerPose_TryGetAtTimestamp_m86E035A372E4724987F5D572081513327F3435D8,
	SpatialPointerPose_Finalize_m1A878B5FE34401D17122E29B1A500A3485D247A9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InputPane_add_Showing_m01D1CAD6737FD3744B580A51BB4D9022DEEEF998,
	InputPane_remove_Showing_mD771A5466DD120825B177C98B59FCD80E045641B,
	InputPane_add_Hiding_m0BC33F3185CC399A61B5EA74DA7D179AFA36E16A,
	InputPane_remove_Hiding_m6148A6A1D540823C34DD35AD36839AB9294E1BEB,
	InputPane_TryShow_mC49C9A2569A559E0EDBF0C32C391CB53128119F3,
	InputPane_TryHide_m018BE2BC1BC3B0FDEAD949DB5CBDD84C2EC37E54,
	InputPane_GetForCurrentView_mC70FE8BAFBC2F91A3166DD901A44DB0709538EC2,
	InputPane_Finalize_m4780B175D6C1BA2A5F0A1CAB99438FB222A28D3A,
	InputPaneVisibilityEventArgs_Finalize_m1C521FD279440FA70097D64D4AC0355414205F91,
	IBindableIterable_First_m35A822CD2DF5C55F51539416F98640C7123A6C63,
	IBindableIterator_get_Current_m1EF798DD2A9BBF29A1FDBD113D1EA7336230E7D9,
	IBindableIterator_get_HasCurrent_m8F6F66EDB43F4A356B5E1C61A46D276B5E952E70,
	IBindableIterator_MoveNext_m6ACF76CEFB49BA5FD184348B4A3F495D0E47395A,
	IBindableVector_GetAt_m860227C0726F0C40C40300F2D114C90C040DCFBD,
	IBindableVector_get_Size_m1804E8861F18C40EC16058667D11382062A82771,
	IBindableVector_GetView_m4C2BFF3B150F6EB8019728B4C2C2AF5879A81B55,
	IBindableVector_IndexOf_mF971A05290A1678F48EC52978E53A9CBA688E489,
	IBindableVector_SetAt_m4658569CC8E08EF010A7EB35E66059F7D4BBFDB8,
	IBindableVector_InsertAt_mA154571EE15503B3425185CFB55419AA4ED3BD59,
	IBindableVector_RemoveAt_m7B14341EBAE9C5799028FC3EF0771210595F4E3C,
	IBindableVector_Append_m623B1AF95642A0AE7BE231983B60CC203FDF4214,
	IBindableVector_RemoveAtEnd_m391C9A85A1C813017D9EBB3211CA294156F654FC,
	IBindableVector_Clear_mBEA87A79EBF207B3F5701BB09506759CBC6F5ED1,
	IBindableVectorView_GetAt_mAA8D68A43FF339E8A336BFD94CC516C26FFB757B,
	IBindableVectorView_get_Size_mB12143398D9A3942E36CE820551EE06F8EB5A77B,
	IBindableVectorView_IndexOf_m091E767F9C60C5FE8A460A65F0C3476F1C1931C0,
};
static const int32_t s_InvokerIndices[893] = 
{
	132,
	138,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	14,
	23,
	10,
	10,
	14,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	10,
	89,
	89,
	251,
	251,
	10,
	10,
	186,
	186,
	725,
	456,
	251,
	89,
	14,
	714,
	2059,
	319,
	2060,
	2061,
	2062,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	-1,
	-1,
	14,
	62,
	116,
	23,
	23,
	141,
	141,
	2063,
	23,
	32,
	32,
	138,
	138,
	23,
	23,
	26,
	23,
	415,
	23,
	90,
	23,
	23,
	90,
	23,
	23,
	23,
	2064,
	23,
	32,
	32,
	23,
	26,
	23,
	116,
	32,
	23,
	-1,
	-1,
	28,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	28,
	4,
	23,
	14,
	23,
	23,
	23,
	28,
	28,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	10,
	89,
	9,
	28,
	28,
	114,
	14,
	14,
	23,
	23,
	23,
	23,
	26,
	27,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	10,
	89,
	9,
	28,
	14,
	14,
	14,
	0,
	0,
	23,
	14,
	34,
	10,
	934,
	1108,
	34,
	10,
	14,
	14,
	23,
	34,
	4,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	34,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	34,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	54,
	23,
	23,
	23,
	23,
	34,
	23,
	0,
	207,
	23,
	23,
	23,
	14,
	14,
	23,
	14,
	23,
	2065,
	1024,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	14,
	23,
	23,
	14,
	23,
	14,
	23,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	14,
	23,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	10,
	23,
	14,
	14,
	14,
	23,
	2065,
	1024,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	28,
	23,
	14,
	14,
	4,
	23,
	10,
	23,
	14,
	23,
	10,
	10,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	32,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	14,
	114,
	23,
	23,
	23,
	32,
	26,
	32,
	32,
	23,
	23,
	10,
	23,
	10,
	10,
	10,
	23,
	2059,
	23,
	23,
	2066,
	23,
	89,
	2067,
	14,
	49,
	4,
	23,
	10,
	10,
	26,
	28,
	14,
	23,
	14,
	26,
	23,
	942,
	23,
	89,
	2067,
	14,
	89,
	14,
	23,
	10,
	10,
	26,
	28,
	14,
	23,
	23,
	14,
	26,
	23,
	23,
	942,
	23,
	23,
	2059,
	23,
	2068,
	23,
	2069,
	2069,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	49,
	23,
	1,
	0,
	23,
	23,
	23,
	23,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	58,
	23,
	58,
	28,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	4,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	0,
	23,
	58,
	58,
	28,
	28,
	14,
	14,
	23,
	26,
	26,
	34,
	23,
	0,
	23,
	23,
	26,
	14,
	10,
	10,
	10,
	14,
	23,
	23,
	23,
	10,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	34,
	23,
	23,
	28,
	28,
	54,
	23,
	28,
	14,
	186,
	213,
	23,
	23,
	186,
	213,
	23,
	89,
	89,
	14,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	186,
	186,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	0,
	23,
	164,
	164,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	30,
	10,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	14,
	23,
	114,
	14,
	23,
	28,
	46,
	4,
	23,
	10,
	14,
	14,
	23,
	14,
	14,
	23,
	14,
	1,
	23,
	2065,
	1024,
	2065,
	1024,
	23,
	89,
	89,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	2065,
	1024,
	2065,
	1024,
	89,
	89,
	4,
	23,
	23,
	14,
	14,
	89,
	89,
	34,
	10,
	14,
	934,
	62,
	62,
	32,
	26,
	23,
	23,
	34,
	10,
	934,
};
extern const Il2CppCodeGenModule g_WindowsRuntimeMetadataCodeGenModule;
const Il2CppCodeGenModule g_WindowsRuntimeMetadataCodeGenModule = 
{
	"WindowsRuntimeMetadata",
	893,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
