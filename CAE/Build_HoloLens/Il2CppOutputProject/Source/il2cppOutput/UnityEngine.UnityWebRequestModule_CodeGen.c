﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.Networking.IMultipartFormSection::get_sectionName()
// 0x00000002 System.Byte[] UnityEngine.Networking.IMultipartFormSection::get_sectionData()
// 0x00000003 System.String UnityEngine.Networking.IMultipartFormSection::get_fileName()
// 0x00000004 System.String UnityEngine.Networking.IMultipartFormSection::get_contentType()
// 0x00000005 System.Void UnityEngine.Networking.MultipartFormDataSection::.ctor(System.String,System.String,System.Text.Encoding,System.String)
extern void MultipartFormDataSection__ctor_m5850438AF3E8B39E52DFD641DE4197997DE414F7 ();
// 0x00000006 System.Void UnityEngine.Networking.MultipartFormDataSection::.ctor(System.String,System.String,System.String)
extern void MultipartFormDataSection__ctor_mD50F50A1D28689484F6FAA08BBF141CD8DF6321D ();
// 0x00000007 System.Void UnityEngine.Networking.MultipartFormDataSection::.ctor(System.String,System.String)
extern void MultipartFormDataSection__ctor_m7AA73800CFE62221B0D95526EB8CDFFAAB8C1250 ();
// 0x00000008 System.String UnityEngine.Networking.MultipartFormDataSection::get_sectionName()
extern void MultipartFormDataSection_get_sectionName_m6AC5C7D036A2AC4E52BA89A594626908D96B66BD ();
// 0x00000009 System.Byte[] UnityEngine.Networking.MultipartFormDataSection::get_sectionData()
extern void MultipartFormDataSection_get_sectionData_m6D54456191D8A29CF651FB75126A0BAFC5B6253B ();
// 0x0000000A System.String UnityEngine.Networking.MultipartFormDataSection::get_fileName()
extern void MultipartFormDataSection_get_fileName_mB11C8FD4F8A0A79C3E0196CF7817C4706186CAD7 ();
// 0x0000000B System.String UnityEngine.Networking.MultipartFormDataSection::get_contentType()
extern void MultipartFormDataSection_get_contentType_m36654A16443971B2B0DC3CBE3FBA73D5FB62EB8A ();
// 0x0000000C System.Void UnityEngine.Networking.MultipartFormFileSection::.ctor(System.String,System.Byte[],System.String,System.String)
extern void MultipartFormFileSection__ctor_m76049301EA55B6BDACB3C65F7B0AE154738EF135 ();
// 0x0000000D System.Void UnityEngine.Networking.MultipartFormFileSection::Init(System.String,System.Byte[],System.String,System.String)
extern void MultipartFormFileSection_Init_mB22A74CBD835EBA034E2D7FB67873918F78F94F4 ();
// 0x0000000E System.String UnityEngine.Networking.MultipartFormFileSection::get_sectionName()
extern void MultipartFormFileSection_get_sectionName_mABEB53BFE17BECBFBC02CCC3BC3797494C6188F6 ();
// 0x0000000F System.Byte[] UnityEngine.Networking.MultipartFormFileSection::get_sectionData()
extern void MultipartFormFileSection_get_sectionData_m671F2D103540736D6F0034795DBA63F7B09696CA ();
// 0x00000010 System.String UnityEngine.Networking.MultipartFormFileSection::get_fileName()
extern void MultipartFormFileSection_get_fileName_m1056DDD21D2AF1B9A79F92F1360141FBD4B1F49F ();
// 0x00000011 System.String UnityEngine.Networking.MultipartFormFileSection::get_contentType()
extern void MultipartFormFileSection_get_contentType_m95A6C9605B304481E65F690ACCBCD2B69889C7F2 ();
// 0x00000012 System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::.ctor()
extern void UnityWebRequestAsyncOperation__ctor_mB260FD4CE600B27EB9A2ABA0BDD20FAF8449D523 ();
// 0x00000013 System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::set_webRequest(UnityEngine.Networking.UnityWebRequest)
extern void UnityWebRequestAsyncOperation_set_webRequest_m07869D44180E2A93042A18260FA5A2BB934AC42F ();
// 0x00000014 System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String)
extern void UnityWebRequest__ctor_m8D62097CE983222DC563B0F47C9FC2C9B7A5F41F ();
// 0x00000015 System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
extern void UnityWebRequest__ctor_m3CBA159B3514D89C931002DFD333B9768A08EBFA ();
// 0x00000016 System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Networking.DownloadHandler,UnityEngine.Networking.UploadHandler)
extern void UnityWebRequest__ctor_m0D2F8F3E1202EF4256D17E91B95DB6CC673FC8D6 ();
// 0x00000017 System.String UnityEngine.Networking.UnityWebRequest::GetWebErrorString(UnityEngine.Networking.UnityWebRequest_UnityWebRequestError)
extern void UnityWebRequest_GetWebErrorString_m92A1DDF2ADFFF8AEE6B1A7FAE384743C31F9E01D ();
// 0x00000018 System.String UnityEngine.Networking.UnityWebRequest::GetHTTPStatusString(System.Int64)
extern void UnityWebRequest_GetHTTPStatusString_m370515E94B5B3C14B4A49677A31D8494262D7EDA ();
// 0x00000019 System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeCertificateHandlerOnDispose()
extern void UnityWebRequest_get_disposeCertificateHandlerOnDispose_m98EFCAC30D637479DC0DC45CFD8A15D402328F99 ();
// 0x0000001A System.Void UnityEngine.Networking.UnityWebRequest::set_disposeCertificateHandlerOnDispose(System.Boolean)
extern void UnityWebRequest_set_disposeCertificateHandlerOnDispose_m8609E1213309D1796E00860ECA9228F6454114AE ();
// 0x0000001B System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose()
extern void UnityWebRequest_get_disposeDownloadHandlerOnDispose_m3BE68E08A94D92D7076F49CB5196019E6E5E17AA ();
// 0x0000001C System.Void UnityEngine.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean)
extern void UnityWebRequest_set_disposeDownloadHandlerOnDispose_mA888301C47844E383DEC96D88CAD6CB8D9E7B9FA ();
// 0x0000001D System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose()
extern void UnityWebRequest_get_disposeUploadHandlerOnDispose_mE4A39A3A06DB4450DA49972254B4498A5F8F69DE ();
// 0x0000001E System.Void UnityEngine.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean)
extern void UnityWebRequest_set_disposeUploadHandlerOnDispose_mC176753B8AFBB40B69FAD7F1E2B2711CA5D6AA71 ();
// 0x0000001F System.IntPtr UnityEngine.Networking.UnityWebRequest::Create()
extern void UnityWebRequest_Create_m98363C34C71AA034B47FA64589711B6F0AEF6698 ();
// 0x00000020 System.Void UnityEngine.Networking.UnityWebRequest::Release()
extern void UnityWebRequest_Release_mD168D309DCE6696163B3357FA21047689D1A7D74 ();
// 0x00000021 System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy()
extern void UnityWebRequest_InternalDestroy_mF5D7484808AEAE24A43B678614D257FBF885026B ();
// 0x00000022 System.Void UnityEngine.Networking.UnityWebRequest::InternalSetDefaults()
extern void UnityWebRequest_InternalSetDefaults_m644CC3C1C737838385F0EC9523A8930E696A9309 ();
// 0x00000023 System.Void UnityEngine.Networking.UnityWebRequest::Finalize()
extern void UnityWebRequest_Finalize_mEBEE0B5A630F0D75CE9F23CDA91DB5048D92CF2C ();
// 0x00000024 System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
extern void UnityWebRequest_Dispose_m6AFA87DA329282058723E5ACE016B0B08CFE806D ();
// 0x00000025 System.Void UnityEngine.Networking.UnityWebRequest::DisposeHandlers()
extern void UnityWebRequest_DisposeHandlers_m0E54EE2A704090B2C2F1F3C90D30A47E3BF2B5C9 ();
// 0x00000026 UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::BeginWebRequest()
extern void UnityWebRequest_BeginWebRequest_m1EF3612D316F7924F6E40D63DD3B0D0118C50CC0 ();
// 0x00000027 UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
extern void UnityWebRequest_SendWebRequest_mF536CB2A0A39354A54B555B66B017816C5833EBD ();
// 0x00000028 System.Void UnityEngine.Networking.UnityWebRequest::Abort()
extern void UnityWebRequest_Abort_mF2C9BD010E5B32FF9F57C2EB4A9A0C8D0289CA7E ();
// 0x00000029 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetMethod(UnityEngine.Networking.UnityWebRequest_UnityWebRequestMethod)
extern void UnityWebRequest_SetMethod_mEE55FF0E071E784318B8C2110E3A3688BF4661CB ();
// 0x0000002A System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest_UnityWebRequestMethod)
extern void UnityWebRequest_InternalSetMethod_m636508AA8E6EF12B3255D8ED108BFF7EB1AB68C9 ();
// 0x0000002B UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCustomMethod(System.String)
extern void UnityWebRequest_SetCustomMethod_mC818FAC0FD8B91FD454C6DFBF7561EEE2D0BA4F4 ();
// 0x0000002C System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
extern void UnityWebRequest_InternalSetCustomMethod_mE9F0C84C6DCD5412AEDD76280EEC4FB82516EF16 ();
// 0x0000002D UnityEngine.Networking.UnityWebRequest_UnityWebRequestMethod UnityEngine.Networking.UnityWebRequest::GetMethod()
extern void UnityWebRequest_GetMethod_mF7CCE2E767F50DB55D0F8E02E6B56B8117558D6F ();
// 0x0000002E System.String UnityEngine.Networking.UnityWebRequest::GetCustomMethod()
extern void UnityWebRequest_GetCustomMethod_m0A7D30C190B85377439370D9DCD105E1EFB73E2F ();
// 0x0000002F System.String UnityEngine.Networking.UnityWebRequest::get_method()
extern void UnityWebRequest_get_method_m0F039F28DD8309D25824D732CE7FF7394E195855 ();
// 0x00000030 System.Void UnityEngine.Networking.UnityWebRequest::set_method(System.String)
extern void UnityWebRequest_set_method_mF2DAC86EB05D65B9BCB52056B7CBB2C1AD87EEC6 ();
// 0x00000031 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::GetError()
extern void UnityWebRequest_GetError_m55BF2299E3B195AC416CCCB46C3DBD83C075018C ();
// 0x00000032 System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern void UnityWebRequest_get_error_mC79FE2460B3F30B8F9E5385BD7D2B4C5B295D7CC ();
// 0x00000033 System.String UnityEngine.Networking.UnityWebRequest::get_url()
extern void UnityWebRequest_get_url_m030A0D89670638A2C0E86A42AA84C767314672C6 ();
// 0x00000034 System.Void UnityEngine.Networking.UnityWebRequest::set_url(System.String)
extern void UnityWebRequest_set_url_mA698FD94C447FF7C1C429D50C2EBAEEDD473007D ();
// 0x00000035 System.String UnityEngine.Networking.UnityWebRequest::GetUrl()
extern void UnityWebRequest_GetUrl_m54AFB82BFB0CFD276363E9EC03E2ED3CB89A45CB ();
// 0x00000036 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUrl(System.String)
extern void UnityWebRequest_SetUrl_mED007912E89AA114D1A3D6905586116F74C8D774 ();
// 0x00000037 System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)
extern void UnityWebRequest_InternalSetUrl_m2E2C837A6F32065CAAAF6EFA7D0237C9E206689A ();
// 0x00000038 System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
extern void UnityWebRequest_get_responseCode_m34819872549939D1EF9EA3D4010974FBEBAF0070 ();
// 0x00000039 System.Single UnityEngine.Networking.UnityWebRequest::GetUploadProgress()
extern void UnityWebRequest_GetUploadProgress_m7DE3A7666E0F80A701B804B7928B4D3F98738AB7 ();
// 0x0000003A System.Boolean UnityEngine.Networking.UnityWebRequest::IsExecuting()
extern void UnityWebRequest_IsExecuting_m01B2C236790F9C97415C79F2AFDEBC0D234EEF09 ();
// 0x0000003B System.Single UnityEngine.Networking.UnityWebRequest::get_uploadProgress()
extern void UnityWebRequest_get_uploadProgress_mA63D1BC81FCDED0715D033DB7493CDD74E732AB0 ();
// 0x0000003C System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable()
extern void UnityWebRequest_get_isModifiable_mD7583537BBC7111555FF73846D120103D2563342 ();
// 0x0000003D System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
extern void UnityWebRequest_get_isDone_mB3CC99A8DC3DB1DD44B23008688EB2DF20906FDA ();
// 0x0000003E System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
extern void UnityWebRequest_get_isNetworkError_m082AFE1A58A330AC4CBD179606B61CB39DD44588 ();
// 0x0000003F System.Boolean UnityEngine.Networking.UnityWebRequest::get_isHttpError()
extern void UnityWebRequest_get_isHttpError_m8F636B70C239EC848FACC83189DE0C22CADEC1C3 ();
// 0x00000040 System.Single UnityEngine.Networking.UnityWebRequest::GetDownloadProgress()
extern void UnityWebRequest_GetDownloadProgress_mD003AA45A293CEFEDB9ECB8E33DDC6D63C5E1578 ();
// 0x00000041 System.Single UnityEngine.Networking.UnityWebRequest::get_downloadProgress()
extern void UnityWebRequest_get_downloadProgress_mE09FED8FB4ED03B0BBC23A3A243B283D4E148DC5 ();
// 0x00000042 System.UInt64 UnityEngine.Networking.UnityWebRequest::get_uploadedBytes()
extern void UnityWebRequest_get_uploadedBytes_m7E8FC431E8A282D9A2E1DDEDD140B056E1B624A3 ();
// 0x00000043 System.UInt64 UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()
extern void UnityWebRequest_get_downloadedBytes_mD7857C8370B3274457D640782A6C76D90D6CD1AE ();
// 0x00000044 System.String UnityEngine.Networking.UnityWebRequest::GetRequestHeader(System.String)
extern void UnityWebRequest_GetRequestHeader_m386CAA2410C062B1FC1DAA107125ADC43C6AB238 ();
// 0x00000045 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
extern void UnityWebRequest_InternalSetRequestHeader_m7481D7E49B6E6078598E40B81D1A3DA9B8D2BD10 ();
// 0x00000046 System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern void UnityWebRequest_SetRequestHeader_m1B54D38BDACC2789FC8EE889EA72EDD7844D2309 ();
// 0x00000047 System.String UnityEngine.Networking.UnityWebRequest::GetResponseHeader(System.String)
extern void UnityWebRequest_GetResponseHeader_m3C6A64338B5B11F5CE2941FEE37DDAB0B67AC5C2 ();
// 0x00000048 System.String[] UnityEngine.Networking.UnityWebRequest::GetResponseHeaderKeys()
extern void UnityWebRequest_GetResponseHeaderKeys_mCA197A30EE7652C300682B5B7560BF7D86FC30AD ();
// 0x00000049 System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Networking.UnityWebRequest::GetResponseHeaders()
extern void UnityWebRequest_GetResponseHeaders_mFA5C7C10F2BA83F21826611AF204BC56E881A863 ();
// 0x0000004A UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUploadHandler(UnityEngine.Networking.UploadHandler)
extern void UnityWebRequest_SetUploadHandler_m046EF4089035441F661AED13F703024DEE030525 ();
// 0x0000004B UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::get_uploadHandler()
extern void UnityWebRequest_get_uploadHandler_mB23A35C2412258E44538F37AA540421C95E69A5C ();
// 0x0000004C System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
extern void UnityWebRequest_set_uploadHandler_m7B33656584914FB3F6FB0FF73C08F671262724A1 ();
// 0x0000004D UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetDownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void UnityWebRequest_SetDownloadHandler_mDE4E6137C34A90754C41B3A0B7B303135771EEDD ();
// 0x0000004E UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern void UnityWebRequest_get_downloadHandler_m83044026479E6B4B2739DCE9EEA8A0FAE7D9AF41 ();
// 0x0000004F System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
extern void UnityWebRequest_set_downloadHandler_mB481C2EE30F84B62396C1A837F46046F12B8FA7E ();
// 0x00000050 UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::get_certificateHandler()
extern void UnityWebRequest_get_certificateHandler_mD3C46D07991190373A7144A6732E390FFBE6DF00 ();
// 0x00000051 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetTimeoutMsec(System.Int32)
extern void UnityWebRequest_SetTimeoutMsec_m53542B77D1BA4486039178C0BEBED3005A6AF079 ();
// 0x00000052 System.Void UnityEngine.Networking.UnityWebRequest::set_timeout(System.Int32)
extern void UnityWebRequest_set_timeout_mA80432BD1798C9227EAE77554A795293072C6E1F ();
// 0x00000053 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
extern void UnityWebRequest_Get_mF4E12AA47AAF25221AD738B434B0EA8D40659B18 ();
// 0x00000054 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Delete(System.String)
extern void UnityWebRequest_Delete_m5F08DC19746922E5CCAADCAAF035AD227B061D95 ();
// 0x00000055 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.Byte[])
extern void UnityWebRequest_Put_m3B499CCF1C4FB9FBCF58B1AACC2989581EB3F26C ();
// 0x00000056 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.String)
extern void UnityWebRequest_Put_mF4F986692AF2279DDEDD8866E9611E2BA7D6A209 ();
// 0x00000057 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.String)
extern void UnityWebRequest_Post_m677069528E4C23CF91208E5A66D672568EDE17E5 ();
// 0x00000058 System.Void UnityEngine.Networking.UnityWebRequest::SetupPost(UnityEngine.Networking.UnityWebRequest,System.String)
extern void UnityWebRequest_SetupPost_mAB03D6DF06B96684D7E1A25449868CD4D1AABA57 ();
// 0x00000059 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern void UnityWebRequest_Post_mEC355EABB9732DF41AD216DB863EEB2A06AC4C87 ();
// 0x0000005A System.Void UnityEngine.Networking.UnityWebRequest::SetupPost(UnityEngine.Networking.UnityWebRequest,UnityEngine.WWWForm)
extern void UnityWebRequest_SetupPost_m31EFAEB2EC83463CD04E93B292A32DF3027FF82C ();
// 0x0000005B UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void UnityWebRequest_Post_m6043F100C5F4A37BD5C434C090A7EA4EC979A64E ();
// 0x0000005C UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern void UnityWebRequest_Post_mC469273F7673EF6B0591F24808E72C67E080F4C2 ();
// 0x0000005D System.Void UnityEngine.Networking.UnityWebRequest::SetupPost(UnityEngine.Networking.UnityWebRequest,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern void UnityWebRequest_SetupPost_m574EBD8CDAD4EE12812C16A5E3FA320ACA275169 ();
// 0x0000005E UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void UnityWebRequest_Post_mF37F372CC047102EB192E656CF8A2BA0E06449C9 ();
// 0x0000005F System.Void UnityEngine.Networking.UnityWebRequest::SetupPost(UnityEngine.Networking.UnityWebRequest,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void UnityWebRequest_SetupPost_m4A1A4086EAB42AFCA9870028EDC43CDAB94CD0DF ();
// 0x00000060 System.String UnityEngine.Networking.UnityWebRequest::EscapeURL(System.String)
extern void UnityWebRequest_EscapeURL_m95ACCD28C59C1A12E4CAF186002A31C84A7CA13F ();
// 0x00000061 System.String UnityEngine.Networking.UnityWebRequest::EscapeURL(System.String,System.Text.Encoding)
extern void UnityWebRequest_EscapeURL_m024D2743C55CB2E079B382ECFCAF23505B13A8E3 ();
// 0x00000062 System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeFormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern void UnityWebRequest_SerializeFormSections_mC8C87FA5D491AB240B084F995F8EDB42049210E2 ();
// 0x00000063 System.Byte[] UnityEngine.Networking.UnityWebRequest::GenerateBoundary()
extern void UnityWebRequest_GenerateBoundary_m93C3299F1653D0F362487B2633D6B705A8CDC63F ();
// 0x00000064 System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeSimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void UnityWebRequest_SerializeSimpleForm_m310C2D4CFB519FAF4459DB1B13BB11823A4DF96F ();
// 0x00000065 System.Void UnityEngine.WWWForm::.ctor()
extern void WWWForm__ctor_m51016B707A3BDC515538D44EB08D54402CF6F695 ();
// 0x00000066 System.Text.Encoding UnityEngine.WWWForm::get_DefaultEncoding()
extern void WWWForm_get_DefaultEncoding_m13BB72339201269AB257B275B20A5A35B233BC3F ();
// 0x00000067 System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern void WWWForm_AddField_m738A7671465A8AF5A85FC7D1164791AA2E874CC8 ();
// 0x00000068 System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern void WWWForm_AddField_m53AAD982E072132AA4D35C48A2FD96EA43EB0F7F ();
// 0x00000069 System.Void UnityEngine.WWWForm::AddField(System.String,System.Int32)
extern void WWWForm_AddField_mB26D9AEFB61E1FBEF6BC57B36A54DB059ECE9248 ();
// 0x0000006A System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String)
extern void WWWForm_AddBinaryData_mDC01404EEF71794A04210CA2696C11CB81FCF30F ();
// 0x0000006B System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String,System.String)
extern void WWWForm_AddBinaryData_m6D70BA4B0246D26B365138CBFF9EF4780A579782 ();
// 0x0000006C System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern void WWWForm_get_headers_mE1BA0494A43C8EF12C0217297411EFD6B4EC601A ();
// 0x0000006D System.Byte[] UnityEngine.WWWForm::get_data()
extern void WWWForm_get_data_m5ED2243249BE32F26F3020BB39BBEF834BE56303 ();
// 0x0000006E System.Byte UnityEngine.WWWTranscoder::Hex2Byte(System.Byte[],System.Int32)
extern void WWWTranscoder_Hex2Byte_mD417CA540CFBE045FCE32959CD3443EB9C8C7423 ();
// 0x0000006F System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern void WWWTranscoder_Byte2Hex_mA129675BFEDFED879713DAB1592772BC52FA04FB ();
// 0x00000070 System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern void WWWTranscoder_URLEncode_mD0BEAAE6DBA432657E18FA17F3BCC87A34C0973B ();
// 0x00000071 System.String UnityEngine.WWWTranscoder::DataEncode(System.String)
extern void WWWTranscoder_DataEncode_mA4FE5738930B9E578D28DA146E5B468FF4CE8880 ();
// 0x00000072 System.String UnityEngine.WWWTranscoder::DataEncode(System.String,System.Text.Encoding)
extern void WWWTranscoder_DataEncode_m991CA7AD8C98345736244A24979E440E23E5035A ();
// 0x00000073 System.Byte[] UnityEngine.WWWTranscoder::DataEncode(System.Byte[])
extern void WWWTranscoder_DataEncode_mAD3C2EBF2E04CAEBDFB8873DC7987378C88A67F4 ();
// 0x00000074 System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern void WWWTranscoder_QPEncode_m8D6CDDD2224B115D869C330D10270027C48446E7 ();
// 0x00000075 System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte[],System.Byte[],System.Boolean)
extern void WWWTranscoder_Encode_m2D65124BA0FF6E92A66B5804596B75898068CF84 ();
// 0x00000076 System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern void WWWTranscoder_ByteArrayContains_mC89ADE5434606470BB3BAF857D786138825E2D0B ();
// 0x00000077 System.Byte[] UnityEngine.WWWTranscoder::URLDecode(System.Byte[])
extern void WWWTranscoder_URLDecode_m591A567154B1B8737ECBFE065AF4FCA59217F5D8 ();
// 0x00000078 System.Boolean UnityEngine.WWWTranscoder::ByteSubArrayEquals(System.Byte[],System.Int32,System.Byte[])
extern void WWWTranscoder_ByteSubArrayEquals_m268C2A9B31CCF4D81E7BEEF843DF5D477ECA9958 ();
// 0x00000079 System.Byte[] UnityEngine.WWWTranscoder::Decode(System.Byte[],System.Byte,System.Byte[])
extern void WWWTranscoder_Decode_m2533830DAAAE6F33AA6EE85A5BF63C96F5D631D4 ();
// 0x0000007A System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern void WWWTranscoder_SevenBitClean_m6805326B108F514EF531375332C90963B9A99EA6 ();
// 0x0000007B System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern void WWWTranscoder_SevenBitClean_mC37FC90C62CF3B311A46A529C9BB6727BA81F8BD ();
// 0x0000007C System.Void UnityEngine.WWWTranscoder::.cctor()
extern void WWWTranscoder__cctor_m3436CCA2D8667A6BCF6981B6573EF048BDA49F51 ();
// 0x0000007D System.String UnityEngineInternal.WebRequestUtils::RedirectTo(System.String,System.String)
extern void WebRequestUtils_RedirectTo_m8AC7C0BFC562550118F6FF4AE218898717E922C1 ();
// 0x0000007E System.String UnityEngineInternal.WebRequestUtils::MakeInitialUrl(System.String,System.String)
extern void WebRequestUtils_MakeInitialUrl_m446CCE4EFB276BE27A9380D55B9E704D01107B83 ();
// 0x0000007F System.String UnityEngineInternal.WebRequestUtils::MakeUriString(System.Uri,System.String,System.Boolean)
extern void WebRequestUtils_MakeUriString_m5693EA04230335B9611278EFC189BD58339D01E4 ();
// 0x00000080 System.String UnityEngineInternal.WebRequestUtils::URLDecode(System.String)
extern void WebRequestUtils_URLDecode_m3F75FA29F50FB340B93815988517E9208C52EE62 ();
// 0x00000081 System.Void UnityEngineInternal.WebRequestUtils::.cctor()
extern void WebRequestUtils__cctor_m31EB3E45EC49AB6B33C7A10F79F1CD4FF2BE715A ();
// 0x00000082 System.Void UnityEngine.Networking.CertificateHandler::Release()
extern void CertificateHandler_Release_m8D680D11AF8B070587DA5C73E2AE652208BDA90A ();
// 0x00000083 System.Void UnityEngine.Networking.CertificateHandler::Finalize()
extern void CertificateHandler_Finalize_m897F6342A2C8D1AC7AA32B6B12E3C961844BF9ED ();
// 0x00000084 System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificate(System.Byte[])
extern void CertificateHandler_ValidateCertificate_m10584FA8D39D238AA435AB440279D3943273817D ();
// 0x00000085 System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificateNative(System.Byte[])
extern void CertificateHandler_ValidateCertificateNative_mE500FAB5B59229D61E85A5DC0E28A0F583679170 ();
// 0x00000086 System.Void UnityEngine.Networking.CertificateHandler::Dispose()
extern void CertificateHandler_Dispose_m9C71BAA51760FDF05AB999B6AB6E6BC71BCB8CA0 ();
// 0x00000087 System.Void UnityEngine.Networking.DownloadHandler::.ctor()
extern void DownloadHandler__ctor_m39F80F1C9B379B0D0362DF9264DE42604BDB24E0 ();
// 0x00000088 System.Void UnityEngine.Networking.DownloadHandler::Release()
extern void DownloadHandler_Release_m913DA503E4183F3323A3D0121FFC978D0F220D5D ();
// 0x00000089 System.Void UnityEngine.Networking.DownloadHandler::Finalize()
extern void DownloadHandler_Finalize_mC6CBFA6D7B38827B12D64D265D5D4FB6B57D50CA ();
// 0x0000008A System.Void UnityEngine.Networking.DownloadHandler::Dispose()
extern void DownloadHandler_Dispose_m7478E72B2DBA4B55FAA25F7A1975A13BA5891D4B ();
// 0x0000008B System.Boolean UnityEngine.Networking.DownloadHandler::get_isDone()
extern void DownloadHandler_get_isDone_mE2C08DFB69FCC1DC2F3F3509C37ECA0BEF62F8FA ();
// 0x0000008C System.Boolean UnityEngine.Networking.DownloadHandler::IsDone()
extern void DownloadHandler_IsDone_m071308DFD095AC6FA1A05DA32C0C7C1B166C2CCC ();
// 0x0000008D System.Byte[] UnityEngine.Networking.DownloadHandler::get_data()
extern void DownloadHandler_get_data_m4AE4E3764FBE186ABA89B5F3A7F91048EE5E38FB ();
// 0x0000008E System.String UnityEngine.Networking.DownloadHandler::get_text()
extern void DownloadHandler_get_text_m1D707E375899B4F4F0434B79AB8D57ADFE5424FF ();
// 0x0000008F System.Byte[] UnityEngine.Networking.DownloadHandler::GetData()
extern void DownloadHandler_GetData_m684807DC14346A128E64E455E8DD147C32125E04 ();
// 0x00000090 System.String UnityEngine.Networking.DownloadHandler::GetText()
extern void DownloadHandler_GetText_mA51553E65D6A397E07AAAC21214C817AD72550FD ();
// 0x00000091 System.Text.Encoding UnityEngine.Networking.DownloadHandler::GetTextEncoder()
extern void DownloadHandler_GetTextEncoder_m601540FD9D16122709582833632A9DEEDBF07E64 ();
// 0x00000092 System.String UnityEngine.Networking.DownloadHandler::GetContentType()
extern void DownloadHandler_GetContentType_mB1653D4D9CA539D1D622C32B52DF5C38548D30E8 ();
// 0x00000093 System.Byte[] UnityEngine.Networking.DownloadHandler::InternalGetByteArray(UnityEngine.Networking.DownloadHandler)
extern void DownloadHandler_InternalGetByteArray_mD6D13BFFBF2F56415E10FFEFDC4A68FE29D6D4FD ();
// 0x00000094 System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern void DownloadHandlerBuffer__ctor_m2134187D8FB07FBAEA2CE23BFCEB13FD94261A9A ();
// 0x00000095 System.IntPtr UnityEngine.Networking.DownloadHandlerBuffer::Create(UnityEngine.Networking.DownloadHandlerBuffer)
extern void DownloadHandlerBuffer_Create_m39E26BEA64B617123CEF559999C8352CA9FA5137 ();
// 0x00000096 System.Void UnityEngine.Networking.DownloadHandlerBuffer::InternalCreateBuffer()
extern void DownloadHandlerBuffer_InternalCreateBuffer_m661B598DF8BD7BF86374FD84C52C8AEA8FA7BEF6 ();
// 0x00000097 System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::GetData()
extern void DownloadHandlerBuffer_GetData_m5A7FFA694EA35F1CE0731803F41E50BBDB16BF14 ();
// 0x00000098 System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::InternalGetData()
extern void DownloadHandlerBuffer_InternalGetData_m9266395B691394754B68543A2FF2F19566C5ABBF ();
// 0x00000099 System.Void UnityEngine.Networking.UploadHandler::.ctor()
extern void UploadHandler__ctor_m3F76154710C5CB7099388479FA02E6555D077F6E ();
// 0x0000009A System.Void UnityEngine.Networking.UploadHandler::Release()
extern void UploadHandler_Release_m1723A22438AF0A7BE616D512E54190D9CE0EC3C4 ();
// 0x0000009B System.Void UnityEngine.Networking.UploadHandler::Finalize()
extern void UploadHandler_Finalize_m68B0CC0B647B11B53908CA4E577AEA5DBA31E4D8 ();
// 0x0000009C System.Void UnityEngine.Networking.UploadHandler::Dispose()
extern void UploadHandler_Dispose_m9BBE8D7D2BBAAC2DE84B52BADA0B79CEA6F2DAB2 ();
// 0x0000009D System.Byte[] UnityEngine.Networking.UploadHandler::get_data()
extern void UploadHandler_get_data_m88034D901424B256E7ED3AE8B8B9F4FBCCA33620 ();
// 0x0000009E System.String UnityEngine.Networking.UploadHandler::get_contentType()
extern void UploadHandler_get_contentType_mD5F0A251FFB939AB81D9AB00D260B9B18097E0DD ();
// 0x0000009F System.Void UnityEngine.Networking.UploadHandler::set_contentType(System.String)
extern void UploadHandler_set_contentType_mB90BEE88AD0FCD496BED349F4E8086AB3C76FF3E ();
// 0x000000A0 System.Byte[] UnityEngine.Networking.UploadHandler::GetData()
extern void UploadHandler_GetData_m634FE57C69CFD68E62940C223E6D52BD9FA86158 ();
// 0x000000A1 System.String UnityEngine.Networking.UploadHandler::GetContentType()
extern void UploadHandler_GetContentType_mDB11855541C0BF69CC38D9DCEE9502C131D75EDF ();
// 0x000000A2 System.Void UnityEngine.Networking.UploadHandler::SetContentType(System.String)
extern void UploadHandler_SetContentType_m71CDE15CBF56F82F32A6BFA79BD8B53A16743602 ();
// 0x000000A3 System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
extern void UploadHandlerRaw__ctor_m9F7643CA3314C8CE46DD41FBF584C268E2546935 ();
// 0x000000A4 System.IntPtr UnityEngine.Networking.UploadHandlerRaw::Create(UnityEngine.Networking.UploadHandlerRaw,System.Byte[])
extern void UploadHandlerRaw_Create_m921D80A8952FC740F358E5FD28E6D5A70622687B ();
// 0x000000A5 System.String UnityEngine.Networking.UploadHandlerRaw::InternalGetContentType()
extern void UploadHandlerRaw_InternalGetContentType_m7627E718B65F4E89084A9AB814E303ECBCCDD446 ();
// 0x000000A6 System.Void UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)
extern void UploadHandlerRaw_InternalSetContentType_mA62DF10D9DB6A5D8EA322F7E3B315EA182C3A898 ();
// 0x000000A7 System.Byte[] UnityEngine.Networking.UploadHandlerRaw::InternalGetData()
extern void UploadHandlerRaw_InternalGetData_mECBB0BBBB515C7AFD93E60425FA1843CF24629D5 ();
// 0x000000A8 System.String UnityEngine.Networking.UploadHandlerRaw::GetContentType()
extern void UploadHandlerRaw_GetContentType_mCEEA08D49C20ED607156521012A71786A7D8ED59 ();
// 0x000000A9 System.Void UnityEngine.Networking.UploadHandlerRaw::SetContentType(System.String)
extern void UploadHandlerRaw_SetContentType_mD245E302B53C361C5CBF9CE0F11E7A0DB142BB11 ();
// 0x000000AA System.Byte[] UnityEngine.Networking.UploadHandlerRaw::GetData()
extern void UploadHandlerRaw_GetData_mAE6119380528F3192119121F0F8B44E4A7CD20DC ();
static Il2CppMethodPointer s_methodPointers[170] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	MultipartFormDataSection__ctor_m5850438AF3E8B39E52DFD641DE4197997DE414F7,
	MultipartFormDataSection__ctor_mD50F50A1D28689484F6FAA08BBF141CD8DF6321D,
	MultipartFormDataSection__ctor_m7AA73800CFE62221B0D95526EB8CDFFAAB8C1250,
	MultipartFormDataSection_get_sectionName_m6AC5C7D036A2AC4E52BA89A594626908D96B66BD,
	MultipartFormDataSection_get_sectionData_m6D54456191D8A29CF651FB75126A0BAFC5B6253B,
	MultipartFormDataSection_get_fileName_mB11C8FD4F8A0A79C3E0196CF7817C4706186CAD7,
	MultipartFormDataSection_get_contentType_m36654A16443971B2B0DC3CBE3FBA73D5FB62EB8A,
	MultipartFormFileSection__ctor_m76049301EA55B6BDACB3C65F7B0AE154738EF135,
	MultipartFormFileSection_Init_mB22A74CBD835EBA034E2D7FB67873918F78F94F4,
	MultipartFormFileSection_get_sectionName_mABEB53BFE17BECBFBC02CCC3BC3797494C6188F6,
	MultipartFormFileSection_get_sectionData_m671F2D103540736D6F0034795DBA63F7B09696CA,
	MultipartFormFileSection_get_fileName_m1056DDD21D2AF1B9A79F92F1360141FBD4B1F49F,
	MultipartFormFileSection_get_contentType_m95A6C9605B304481E65F690ACCBCD2B69889C7F2,
	UnityWebRequestAsyncOperation__ctor_mB260FD4CE600B27EB9A2ABA0BDD20FAF8449D523,
	UnityWebRequestAsyncOperation_set_webRequest_m07869D44180E2A93042A18260FA5A2BB934AC42F,
	UnityWebRequest__ctor_m8D62097CE983222DC563B0F47C9FC2C9B7A5F41F,
	UnityWebRequest__ctor_m3CBA159B3514D89C931002DFD333B9768A08EBFA,
	UnityWebRequest__ctor_m0D2F8F3E1202EF4256D17E91B95DB6CC673FC8D6,
	UnityWebRequest_GetWebErrorString_m92A1DDF2ADFFF8AEE6B1A7FAE384743C31F9E01D,
	UnityWebRequest_GetHTTPStatusString_m370515E94B5B3C14B4A49677A31D8494262D7EDA,
	UnityWebRequest_get_disposeCertificateHandlerOnDispose_m98EFCAC30D637479DC0DC45CFD8A15D402328F99,
	UnityWebRequest_set_disposeCertificateHandlerOnDispose_m8609E1213309D1796E00860ECA9228F6454114AE,
	UnityWebRequest_get_disposeDownloadHandlerOnDispose_m3BE68E08A94D92D7076F49CB5196019E6E5E17AA,
	UnityWebRequest_set_disposeDownloadHandlerOnDispose_mA888301C47844E383DEC96D88CAD6CB8D9E7B9FA,
	UnityWebRequest_get_disposeUploadHandlerOnDispose_mE4A39A3A06DB4450DA49972254B4498A5F8F69DE,
	UnityWebRequest_set_disposeUploadHandlerOnDispose_mC176753B8AFBB40B69FAD7F1E2B2711CA5D6AA71,
	UnityWebRequest_Create_m98363C34C71AA034B47FA64589711B6F0AEF6698,
	UnityWebRequest_Release_mD168D309DCE6696163B3357FA21047689D1A7D74,
	UnityWebRequest_InternalDestroy_mF5D7484808AEAE24A43B678614D257FBF885026B,
	UnityWebRequest_InternalSetDefaults_m644CC3C1C737838385F0EC9523A8930E696A9309,
	UnityWebRequest_Finalize_mEBEE0B5A630F0D75CE9F23CDA91DB5048D92CF2C,
	UnityWebRequest_Dispose_m6AFA87DA329282058723E5ACE016B0B08CFE806D,
	UnityWebRequest_DisposeHandlers_m0E54EE2A704090B2C2F1F3C90D30A47E3BF2B5C9,
	UnityWebRequest_BeginWebRequest_m1EF3612D316F7924F6E40D63DD3B0D0118C50CC0,
	UnityWebRequest_SendWebRequest_mF536CB2A0A39354A54B555B66B017816C5833EBD,
	UnityWebRequest_Abort_mF2C9BD010E5B32FF9F57C2EB4A9A0C8D0289CA7E,
	UnityWebRequest_SetMethod_mEE55FF0E071E784318B8C2110E3A3688BF4661CB,
	UnityWebRequest_InternalSetMethod_m636508AA8E6EF12B3255D8ED108BFF7EB1AB68C9,
	UnityWebRequest_SetCustomMethod_mC818FAC0FD8B91FD454C6DFBF7561EEE2D0BA4F4,
	UnityWebRequest_InternalSetCustomMethod_mE9F0C84C6DCD5412AEDD76280EEC4FB82516EF16,
	UnityWebRequest_GetMethod_mF7CCE2E767F50DB55D0F8E02E6B56B8117558D6F,
	UnityWebRequest_GetCustomMethod_m0A7D30C190B85377439370D9DCD105E1EFB73E2F,
	UnityWebRequest_get_method_m0F039F28DD8309D25824D732CE7FF7394E195855,
	UnityWebRequest_set_method_mF2DAC86EB05D65B9BCB52056B7CBB2C1AD87EEC6,
	UnityWebRequest_GetError_m55BF2299E3B195AC416CCCB46C3DBD83C075018C,
	UnityWebRequest_get_error_mC79FE2460B3F30B8F9E5385BD7D2B4C5B295D7CC,
	UnityWebRequest_get_url_m030A0D89670638A2C0E86A42AA84C767314672C6,
	UnityWebRequest_set_url_mA698FD94C447FF7C1C429D50C2EBAEEDD473007D,
	UnityWebRequest_GetUrl_m54AFB82BFB0CFD276363E9EC03E2ED3CB89A45CB,
	UnityWebRequest_SetUrl_mED007912E89AA114D1A3D6905586116F74C8D774,
	UnityWebRequest_InternalSetUrl_m2E2C837A6F32065CAAAF6EFA7D0237C9E206689A,
	UnityWebRequest_get_responseCode_m34819872549939D1EF9EA3D4010974FBEBAF0070,
	UnityWebRequest_GetUploadProgress_m7DE3A7666E0F80A701B804B7928B4D3F98738AB7,
	UnityWebRequest_IsExecuting_m01B2C236790F9C97415C79F2AFDEBC0D234EEF09,
	UnityWebRequest_get_uploadProgress_mA63D1BC81FCDED0715D033DB7493CDD74E732AB0,
	UnityWebRequest_get_isModifiable_mD7583537BBC7111555FF73846D120103D2563342,
	UnityWebRequest_get_isDone_mB3CC99A8DC3DB1DD44B23008688EB2DF20906FDA,
	UnityWebRequest_get_isNetworkError_m082AFE1A58A330AC4CBD179606B61CB39DD44588,
	UnityWebRequest_get_isHttpError_m8F636B70C239EC848FACC83189DE0C22CADEC1C3,
	UnityWebRequest_GetDownloadProgress_mD003AA45A293CEFEDB9ECB8E33DDC6D63C5E1578,
	UnityWebRequest_get_downloadProgress_mE09FED8FB4ED03B0BBC23A3A243B283D4E148DC5,
	UnityWebRequest_get_uploadedBytes_m7E8FC431E8A282D9A2E1DDEDD140B056E1B624A3,
	UnityWebRequest_get_downloadedBytes_mD7857C8370B3274457D640782A6C76D90D6CD1AE,
	UnityWebRequest_GetRequestHeader_m386CAA2410C062B1FC1DAA107125ADC43C6AB238,
	UnityWebRequest_InternalSetRequestHeader_m7481D7E49B6E6078598E40B81D1A3DA9B8D2BD10,
	UnityWebRequest_SetRequestHeader_m1B54D38BDACC2789FC8EE889EA72EDD7844D2309,
	UnityWebRequest_GetResponseHeader_m3C6A64338B5B11F5CE2941FEE37DDAB0B67AC5C2,
	UnityWebRequest_GetResponseHeaderKeys_mCA197A30EE7652C300682B5B7560BF7D86FC30AD,
	UnityWebRequest_GetResponseHeaders_mFA5C7C10F2BA83F21826611AF204BC56E881A863,
	UnityWebRequest_SetUploadHandler_m046EF4089035441F661AED13F703024DEE030525,
	UnityWebRequest_get_uploadHandler_mB23A35C2412258E44538F37AA540421C95E69A5C,
	UnityWebRequest_set_uploadHandler_m7B33656584914FB3F6FB0FF73C08F671262724A1,
	UnityWebRequest_SetDownloadHandler_mDE4E6137C34A90754C41B3A0B7B303135771EEDD,
	UnityWebRequest_get_downloadHandler_m83044026479E6B4B2739DCE9EEA8A0FAE7D9AF41,
	UnityWebRequest_set_downloadHandler_mB481C2EE30F84B62396C1A837F46046F12B8FA7E,
	UnityWebRequest_get_certificateHandler_mD3C46D07991190373A7144A6732E390FFBE6DF00,
	UnityWebRequest_SetTimeoutMsec_m53542B77D1BA4486039178C0BEBED3005A6AF079,
	UnityWebRequest_set_timeout_mA80432BD1798C9227EAE77554A795293072C6E1F,
	UnityWebRequest_Get_mF4E12AA47AAF25221AD738B434B0EA8D40659B18,
	UnityWebRequest_Delete_m5F08DC19746922E5CCAADCAAF035AD227B061D95,
	UnityWebRequest_Put_m3B499CCF1C4FB9FBCF58B1AACC2989581EB3F26C,
	UnityWebRequest_Put_mF4F986692AF2279DDEDD8866E9611E2BA7D6A209,
	UnityWebRequest_Post_m677069528E4C23CF91208E5A66D672568EDE17E5,
	UnityWebRequest_SetupPost_mAB03D6DF06B96684D7E1A25449868CD4D1AABA57,
	UnityWebRequest_Post_mEC355EABB9732DF41AD216DB863EEB2A06AC4C87,
	UnityWebRequest_SetupPost_m31EFAEB2EC83463CD04E93B292A32DF3027FF82C,
	UnityWebRequest_Post_m6043F100C5F4A37BD5C434C090A7EA4EC979A64E,
	UnityWebRequest_Post_mC469273F7673EF6B0591F24808E72C67E080F4C2,
	UnityWebRequest_SetupPost_m574EBD8CDAD4EE12812C16A5E3FA320ACA275169,
	UnityWebRequest_Post_mF37F372CC047102EB192E656CF8A2BA0E06449C9,
	UnityWebRequest_SetupPost_m4A1A4086EAB42AFCA9870028EDC43CDAB94CD0DF,
	UnityWebRequest_EscapeURL_m95ACCD28C59C1A12E4CAF186002A31C84A7CA13F,
	UnityWebRequest_EscapeURL_m024D2743C55CB2E079B382ECFCAF23505B13A8E3,
	UnityWebRequest_SerializeFormSections_mC8C87FA5D491AB240B084F995F8EDB42049210E2,
	UnityWebRequest_GenerateBoundary_m93C3299F1653D0F362487B2633D6B705A8CDC63F,
	UnityWebRequest_SerializeSimpleForm_m310C2D4CFB519FAF4459DB1B13BB11823A4DF96F,
	WWWForm__ctor_m51016B707A3BDC515538D44EB08D54402CF6F695,
	WWWForm_get_DefaultEncoding_m13BB72339201269AB257B275B20A5A35B233BC3F,
	WWWForm_AddField_m738A7671465A8AF5A85FC7D1164791AA2E874CC8,
	WWWForm_AddField_m53AAD982E072132AA4D35C48A2FD96EA43EB0F7F,
	WWWForm_AddField_mB26D9AEFB61E1FBEF6BC57B36A54DB059ECE9248,
	WWWForm_AddBinaryData_mDC01404EEF71794A04210CA2696C11CB81FCF30F,
	WWWForm_AddBinaryData_m6D70BA4B0246D26B365138CBFF9EF4780A579782,
	WWWForm_get_headers_mE1BA0494A43C8EF12C0217297411EFD6B4EC601A,
	WWWForm_get_data_m5ED2243249BE32F26F3020BB39BBEF834BE56303,
	WWWTranscoder_Hex2Byte_mD417CA540CFBE045FCE32959CD3443EB9C8C7423,
	WWWTranscoder_Byte2Hex_mA129675BFEDFED879713DAB1592772BC52FA04FB,
	WWWTranscoder_URLEncode_mD0BEAAE6DBA432657E18FA17F3BCC87A34C0973B,
	WWWTranscoder_DataEncode_mA4FE5738930B9E578D28DA146E5B468FF4CE8880,
	WWWTranscoder_DataEncode_m991CA7AD8C98345736244A24979E440E23E5035A,
	WWWTranscoder_DataEncode_mAD3C2EBF2E04CAEBDFB8873DC7987378C88A67F4,
	WWWTranscoder_QPEncode_m8D6CDDD2224B115D869C330D10270027C48446E7,
	WWWTranscoder_Encode_m2D65124BA0FF6E92A66B5804596B75898068CF84,
	WWWTranscoder_ByteArrayContains_mC89ADE5434606470BB3BAF857D786138825E2D0B,
	WWWTranscoder_URLDecode_m591A567154B1B8737ECBFE065AF4FCA59217F5D8,
	WWWTranscoder_ByteSubArrayEquals_m268C2A9B31CCF4D81E7BEEF843DF5D477ECA9958,
	WWWTranscoder_Decode_m2533830DAAAE6F33AA6EE85A5BF63C96F5D631D4,
	WWWTranscoder_SevenBitClean_m6805326B108F514EF531375332C90963B9A99EA6,
	WWWTranscoder_SevenBitClean_mC37FC90C62CF3B311A46A529C9BB6727BA81F8BD,
	WWWTranscoder__cctor_m3436CCA2D8667A6BCF6981B6573EF048BDA49F51,
	WebRequestUtils_RedirectTo_m8AC7C0BFC562550118F6FF4AE218898717E922C1,
	WebRequestUtils_MakeInitialUrl_m446CCE4EFB276BE27A9380D55B9E704D01107B83,
	WebRequestUtils_MakeUriString_m5693EA04230335B9611278EFC189BD58339D01E4,
	WebRequestUtils_URLDecode_m3F75FA29F50FB340B93815988517E9208C52EE62,
	WebRequestUtils__cctor_m31EB3E45EC49AB6B33C7A10F79F1CD4FF2BE715A,
	CertificateHandler_Release_m8D680D11AF8B070587DA5C73E2AE652208BDA90A,
	CertificateHandler_Finalize_m897F6342A2C8D1AC7AA32B6B12E3C961844BF9ED,
	CertificateHandler_ValidateCertificate_m10584FA8D39D238AA435AB440279D3943273817D,
	CertificateHandler_ValidateCertificateNative_mE500FAB5B59229D61E85A5DC0E28A0F583679170,
	CertificateHandler_Dispose_m9C71BAA51760FDF05AB999B6AB6E6BC71BCB8CA0,
	DownloadHandler__ctor_m39F80F1C9B379B0D0362DF9264DE42604BDB24E0,
	DownloadHandler_Release_m913DA503E4183F3323A3D0121FFC978D0F220D5D,
	DownloadHandler_Finalize_mC6CBFA6D7B38827B12D64D265D5D4FB6B57D50CA,
	DownloadHandler_Dispose_m7478E72B2DBA4B55FAA25F7A1975A13BA5891D4B,
	DownloadHandler_get_isDone_mE2C08DFB69FCC1DC2F3F3509C37ECA0BEF62F8FA,
	DownloadHandler_IsDone_m071308DFD095AC6FA1A05DA32C0C7C1B166C2CCC,
	DownloadHandler_get_data_m4AE4E3764FBE186ABA89B5F3A7F91048EE5E38FB,
	DownloadHandler_get_text_m1D707E375899B4F4F0434B79AB8D57ADFE5424FF,
	DownloadHandler_GetData_m684807DC14346A128E64E455E8DD147C32125E04,
	DownloadHandler_GetText_mA51553E65D6A397E07AAAC21214C817AD72550FD,
	DownloadHandler_GetTextEncoder_m601540FD9D16122709582833632A9DEEDBF07E64,
	DownloadHandler_GetContentType_mB1653D4D9CA539D1D622C32B52DF5C38548D30E8,
	DownloadHandler_InternalGetByteArray_mD6D13BFFBF2F56415E10FFEFDC4A68FE29D6D4FD,
	DownloadHandlerBuffer__ctor_m2134187D8FB07FBAEA2CE23BFCEB13FD94261A9A,
	DownloadHandlerBuffer_Create_m39E26BEA64B617123CEF559999C8352CA9FA5137,
	DownloadHandlerBuffer_InternalCreateBuffer_m661B598DF8BD7BF86374FD84C52C8AEA8FA7BEF6,
	DownloadHandlerBuffer_GetData_m5A7FFA694EA35F1CE0731803F41E50BBDB16BF14,
	DownloadHandlerBuffer_InternalGetData_m9266395B691394754B68543A2FF2F19566C5ABBF,
	UploadHandler__ctor_m3F76154710C5CB7099388479FA02E6555D077F6E,
	UploadHandler_Release_m1723A22438AF0A7BE616D512E54190D9CE0EC3C4,
	UploadHandler_Finalize_m68B0CC0B647B11B53908CA4E577AEA5DBA31E4D8,
	UploadHandler_Dispose_m9BBE8D7D2BBAAC2DE84B52BADA0B79CEA6F2DAB2,
	UploadHandler_get_data_m88034D901424B256E7ED3AE8B8B9F4FBCCA33620,
	UploadHandler_get_contentType_mD5F0A251FFB939AB81D9AB00D260B9B18097E0DD,
	UploadHandler_set_contentType_mB90BEE88AD0FCD496BED349F4E8086AB3C76FF3E,
	UploadHandler_GetData_m634FE57C69CFD68E62940C223E6D52BD9FA86158,
	UploadHandler_GetContentType_mDB11855541C0BF69CC38D9DCEE9502C131D75EDF,
	UploadHandler_SetContentType_m71CDE15CBF56F82F32A6BFA79BD8B53A16743602,
	UploadHandlerRaw__ctor_m9F7643CA3314C8CE46DD41FBF584C268E2546935,
	UploadHandlerRaw_Create_m921D80A8952FC740F358E5FD28E6D5A70622687B,
	UploadHandlerRaw_InternalGetContentType_m7627E718B65F4E89084A9AB814E303ECBCCDD446,
	UploadHandlerRaw_InternalSetContentType_mA62DF10D9DB6A5D8EA322F7E3B315EA182C3A898,
	UploadHandlerRaw_InternalGetData_mECBB0BBBB515C7AFD93E60425FA1843CF24629D5,
	UploadHandlerRaw_GetContentType_mCEEA08D49C20ED607156521012A71786A7D8ED59,
	UploadHandlerRaw_SetContentType_mD245E302B53C361C5CBF9CE0F11E7A0DB142BB11,
	UploadHandlerRaw_GetData_mAE6119380528F3192119121F0F8B44E4A7CD20DC,
};
static const int32_t s_InvokerIndices[170] = 
{
	14,
	14,
	14,
	14,
	440,
	211,
	27,
	14,
	14,
	14,
	14,
	440,
	440,
	14,
	14,
	14,
	14,
	23,
	26,
	26,
	27,
	440,
	43,
	222,
	89,
	31,
	89,
	31,
	89,
	31,
	777,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	37,
	32,
	121,
	26,
	10,
	14,
	14,
	26,
	10,
	14,
	14,
	26,
	14,
	121,
	26,
	186,
	725,
	89,
	725,
	89,
	89,
	89,
	89,
	725,
	725,
	186,
	186,
	28,
	41,
	27,
	28,
	14,
	14,
	121,
	14,
	26,
	121,
	14,
	26,
	14,
	37,
	32,
	0,
	0,
	1,
	1,
	1,
	143,
	1,
	143,
	1,
	2,
	200,
	1,
	143,
	0,
	1,
	1,
	4,
	0,
	23,
	4,
	27,
	211,
	138,
	211,
	440,
	14,
	14,
	106,
	2022,
	0,
	0,
	1,
	0,
	1,
	2023,
	617,
	0,
	242,
	1265,
	141,
	109,
	3,
	1,
	1,
	219,
	0,
	3,
	23,
	23,
	9,
	9,
	23,
	23,
	23,
	23,
	23,
	89,
	89,
	14,
	14,
	14,
	14,
	14,
	14,
	0,
	23,
	24,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	14,
	14,
	26,
	14,
	14,
	26,
	26,
	2024,
	14,
	26,
	14,
	14,
	26,
	14,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestModule.dll",
	170,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
