﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture[]
struct TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B;
// UnityEngine.Touch[]
struct TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A;
// UnityEngine.WebCamTexture
struct WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73;
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1;
// WebcamManager
struct WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5;
// WebcamManager/<RestartWebcam>d__7
struct U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18;
// WebcamManager/<initAndWaitForWebCamTexture>d__29
struct U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD;
// WorldToScreenSpace
struct WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91;
// ZoomManager
struct ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF;
// _UnityEventFloat
struct _UnityEventFloat_t1CD5ADCB52C0E71B6BA30D63EBA35410F6FB078E;

IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral757D3BA1EDB250E4DB4540A97894271FD8D7F726;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_Reset_m353C8B6D1E46B7DA099E503AAA0CFD8342ECBC1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_Reset_m914092285CF2D5777DD74D018FD150F3C5699E0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_Reset_m353C8B6D1E46B7DA099E503AAA0CFD8342ECBC1E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CinitAndWaitForWebCamTextureU3Ed__29_MoveNext_m5F60D8EC89DB29F9C0F212F131CCF12C9BADBF88_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_Reset_m914092285CF2D5777DD74D018FD150F3C5699E0A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldToScreenSpace_DebugTest_mF1C9DF13586600FCD9C885D107E4BCC5CCA032BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZoomManager_GestureZoom_mFC947D24B1F247AF7E836531FD0E042A69EB0B7F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZoomManager_Start_m2E7057C34382836C748CFC31609DCC3C0DAF7F16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZoomManager_Update_mA47E100AFCF59E12AB380D2A425CA3B8D04772DF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t _UnityEventFloat__ctor_m9B4423F24CE2CCDC15D30C713A28BE29FA6051B7_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct Resolution_t350D132B8526B5211E0BF8B22782F20D55994A90 ;

struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
struct RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41;
struct TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B;
struct TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D;
struct WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A;
struct WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// WebcamManager_<RestartWebcam>d__7
struct  U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18  : public RuntimeObject
{
public:
	// System.Int32 WebcamManager_<RestartWebcam>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WebcamManager_<RestartWebcam>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WebcamManager WebcamManager_<RestartWebcam>d__7::<>4__this
	WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18, ___U3CU3E4__this_2)); }
	inline WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// WebcamManager_<initAndWaitForWebCamTexture>d__29
struct  U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD  : public RuntimeObject
{
public:
	// System.Int32 WebcamManager_<initAndWaitForWebCamTexture>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WebcamManager_<initAndWaitForWebCamTexture>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WebcamManager WebcamManager_<initAndWaitForWebCamTexture>d__29::<>4__this
	WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * ___U3CU3E4__this_2;
	// System.Int32 WebcamManager_<initAndWaitForWebCamTexture>d__29::<initFrameCount>5__2
	int32_t ___U3CinitFrameCountU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD, ___U3CU3E4__this_2)); }
	inline WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinitFrameCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD, ___U3CinitFrameCountU3E5__2_3)); }
	inline int32_t get_U3CinitFrameCountU3E5__2_3() const { return ___U3CinitFrameCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CinitFrameCountU3E5__2_3() { return &___U3CinitFrameCountU3E5__2_3; }
	inline void set_U3CinitFrameCountU3E5__2_3(int32_t value)
	{
		___U3CinitFrameCountU3E5__2_3 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:

public:
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchPhase
struct  TouchPhase_t7E9CEC3DD059E32F847242513BD6CE30866AB2A6 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_t7E9CEC3DD059E32F847242513BD6CE30866AB2A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchType
struct  TouchType_tBBD83025576FC017B10484014B5C396613A02B8E 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_tBBD83025576FC017B10484014B5C396613A02B8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.WebCamKind
struct  WebCamKind_t658D67B14A1B37DCF51FF51D4ACE4CC3ADB7C33E 
{
public:
	// System.Int32 UnityEngine.WebCamKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebCamKind_t658D67B14A1B37DCF51FF51D4ACE4CC3ADB7C33E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// _UnityEventFloat
struct  _UnityEventFloat_t1CD5ADCB52C0E71B6BA30D63EBA35410F6FB078E  : public UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6
{
public:

public:
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.Touch
struct  Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};


// UnityEngine.WebCamDevice
struct  WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 
{
public:
	// System.String UnityEngine.WebCamDevice::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.WebCamDevice::m_DepthCameraName
	String_t* ___m_DepthCameraName_1;
	// System.Int32 UnityEngine.WebCamDevice::m_Flags
	int32_t ___m_Flags_2;
	// UnityEngine.WebCamKind UnityEngine.WebCamDevice::m_Kind
	int32_t ___m_Kind_3;
	// UnityEngine.Resolution[] UnityEngine.WebCamDevice::m_Resolutions
	ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* ___m_Resolutions_4;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_DepthCameraName_1() { return static_cast<int32_t>(offsetof(WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54, ___m_DepthCameraName_1)); }
	inline String_t* get_m_DepthCameraName_1() const { return ___m_DepthCameraName_1; }
	inline String_t** get_address_of_m_DepthCameraName_1() { return &___m_DepthCameraName_1; }
	inline void set_m_DepthCameraName_1(String_t* value)
	{
		___m_DepthCameraName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DepthCameraName_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_2() { return static_cast<int32_t>(offsetof(WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54, ___m_Flags_2)); }
	inline int32_t get_m_Flags_2() const { return ___m_Flags_2; }
	inline int32_t* get_address_of_m_Flags_2() { return &___m_Flags_2; }
	inline void set_m_Flags_2(int32_t value)
	{
		___m_Flags_2 = value;
	}

	inline static int32_t get_offset_of_m_Kind_3() { return static_cast<int32_t>(offsetof(WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54, ___m_Kind_3)); }
	inline int32_t get_m_Kind_3() const { return ___m_Kind_3; }
	inline int32_t* get_address_of_m_Kind_3() { return &___m_Kind_3; }
	inline void set_m_Kind_3(int32_t value)
	{
		___m_Kind_3 = value;
	}

	inline static int32_t get_offset_of_m_Resolutions_4() { return static_cast<int32_t>(offsetof(WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54, ___m_Resolutions_4)); }
	inline ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* get_m_Resolutions_4() const { return ___m_Resolutions_4; }
	inline ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A** get_address_of_m_Resolutions_4() { return &___m_Resolutions_4; }
	inline void set_m_Resolutions_4(ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* value)
	{
		___m_Resolutions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Resolutions_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_t350D132B8526B5211E0BF8B22782F20D55994A90 * ___m_Resolutions_4;
};
// Native definition for COM marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_t350D132B8526B5211E0BF8B22782F20D55994A90 * ___m_Resolutions_4;
};

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.WebCamTexture
struct  WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20  : public Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA
{
public:

public:
};

struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// WebcamManager
struct  WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.WebCamDevice[] WebcamManager::devices
	WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* ___devices_4;
	// UnityEngine.WebCamTexture[] WebcamManager::webCams
	WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* ___webCams_5;
	// UnityEngine.Texture[] WebcamManager::textures
	TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* ___textures_6;
	// System.Int32 WebcamManager::TargetCamID
	int32_t ___TargetCamID_7;
	// System.Boolean WebcamManager::useFrontCam
	bool ___useFrontCam_8;
	// System.Boolean WebcamManager::canRestart
	bool ___canRestart_9;
	// UnityEngine.WebCamTexture WebcamManager::webCamTexture
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___webCamTexture_10;
	// UnityEngine.Texture WebcamManager::texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___texture_11;
	// UnityEngine.Material[] WebcamManager::materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___materials_12;
	// UnityEngine.GameObject[] WebcamManager::targetMeshObjects
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___targetMeshObjects_13;
	// UnityEngine.GameObject WebcamManager::BackgroundQuad
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BackgroundQuad_14;
	// System.Int32 WebcamManager::timeoutFrameCount
	int32_t ___timeoutFrameCount_15;
	// UnityEngine.Vector2 WebcamManager::requestResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___requestResolution_16;
	// UnityEngine.Vector2 WebcamManager::textureResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureResolution_17;
	// System.Boolean WebcamManager::isFlipped
	bool ___isFlipped_18;
	// System.Boolean WebcamManager::isInitWaiting
	bool ___isInitWaiting_19;
	// System.Single WebcamManager::TextureRatio
	float ___TextureRatio_20;
	// System.Single WebcamManager::ScreenRatio
	float ___ScreenRatio_21;
	// UnityEngine.Quaternion WebcamManager::baseRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___baseRotation_22;

public:
	inline static int32_t get_offset_of_devices_4() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___devices_4)); }
	inline WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* get_devices_4() const { return ___devices_4; }
	inline WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A** get_address_of_devices_4() { return &___devices_4; }
	inline void set_devices_4(WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* value)
	{
		___devices_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___devices_4), (void*)value);
	}

	inline static int32_t get_offset_of_webCams_5() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___webCams_5)); }
	inline WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* get_webCams_5() const { return ___webCams_5; }
	inline WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1** get_address_of_webCams_5() { return &___webCams_5; }
	inline void set_webCams_5(WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* value)
	{
		___webCams_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webCams_5), (void*)value);
	}

	inline static int32_t get_offset_of_textures_6() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___textures_6)); }
	inline TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* get_textures_6() const { return ___textures_6; }
	inline TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B** get_address_of_textures_6() { return &___textures_6; }
	inline void set_textures_6(TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* value)
	{
		___textures_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textures_6), (void*)value);
	}

	inline static int32_t get_offset_of_TargetCamID_7() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___TargetCamID_7)); }
	inline int32_t get_TargetCamID_7() const { return ___TargetCamID_7; }
	inline int32_t* get_address_of_TargetCamID_7() { return &___TargetCamID_7; }
	inline void set_TargetCamID_7(int32_t value)
	{
		___TargetCamID_7 = value;
	}

	inline static int32_t get_offset_of_useFrontCam_8() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___useFrontCam_8)); }
	inline bool get_useFrontCam_8() const { return ___useFrontCam_8; }
	inline bool* get_address_of_useFrontCam_8() { return &___useFrontCam_8; }
	inline void set_useFrontCam_8(bool value)
	{
		___useFrontCam_8 = value;
	}

	inline static int32_t get_offset_of_canRestart_9() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___canRestart_9)); }
	inline bool get_canRestart_9() const { return ___canRestart_9; }
	inline bool* get_address_of_canRestart_9() { return &___canRestart_9; }
	inline void set_canRestart_9(bool value)
	{
		___canRestart_9 = value;
	}

	inline static int32_t get_offset_of_webCamTexture_10() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___webCamTexture_10)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_webCamTexture_10() const { return ___webCamTexture_10; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_webCamTexture_10() { return &___webCamTexture_10; }
	inline void set_webCamTexture_10(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___webCamTexture_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webCamTexture_10), (void*)value);
	}

	inline static int32_t get_offset_of_texture_11() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___texture_11)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_texture_11() const { return ___texture_11; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_texture_11() { return &___texture_11; }
	inline void set_texture_11(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___texture_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_11), (void*)value);
	}

	inline static int32_t get_offset_of_materials_12() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___materials_12)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_materials_12() const { return ___materials_12; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_materials_12() { return &___materials_12; }
	inline void set_materials_12(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___materials_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materials_12), (void*)value);
	}

	inline static int32_t get_offset_of_targetMeshObjects_13() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___targetMeshObjects_13)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_targetMeshObjects_13() const { return ___targetMeshObjects_13; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_targetMeshObjects_13() { return &___targetMeshObjects_13; }
	inline void set_targetMeshObjects_13(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___targetMeshObjects_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetMeshObjects_13), (void*)value);
	}

	inline static int32_t get_offset_of_BackgroundQuad_14() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___BackgroundQuad_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BackgroundQuad_14() const { return ___BackgroundQuad_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BackgroundQuad_14() { return &___BackgroundQuad_14; }
	inline void set_BackgroundQuad_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BackgroundQuad_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackgroundQuad_14), (void*)value);
	}

	inline static int32_t get_offset_of_timeoutFrameCount_15() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___timeoutFrameCount_15)); }
	inline int32_t get_timeoutFrameCount_15() const { return ___timeoutFrameCount_15; }
	inline int32_t* get_address_of_timeoutFrameCount_15() { return &___timeoutFrameCount_15; }
	inline void set_timeoutFrameCount_15(int32_t value)
	{
		___timeoutFrameCount_15 = value;
	}

	inline static int32_t get_offset_of_requestResolution_16() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___requestResolution_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_requestResolution_16() const { return ___requestResolution_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_requestResolution_16() { return &___requestResolution_16; }
	inline void set_requestResolution_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___requestResolution_16 = value;
	}

	inline static int32_t get_offset_of_textureResolution_17() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___textureResolution_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_textureResolution_17() const { return ___textureResolution_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_textureResolution_17() { return &___textureResolution_17; }
	inline void set_textureResolution_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___textureResolution_17 = value;
	}

	inline static int32_t get_offset_of_isFlipped_18() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___isFlipped_18)); }
	inline bool get_isFlipped_18() const { return ___isFlipped_18; }
	inline bool* get_address_of_isFlipped_18() { return &___isFlipped_18; }
	inline void set_isFlipped_18(bool value)
	{
		___isFlipped_18 = value;
	}

	inline static int32_t get_offset_of_isInitWaiting_19() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___isInitWaiting_19)); }
	inline bool get_isInitWaiting_19() const { return ___isInitWaiting_19; }
	inline bool* get_address_of_isInitWaiting_19() { return &___isInitWaiting_19; }
	inline void set_isInitWaiting_19(bool value)
	{
		___isInitWaiting_19 = value;
	}

	inline static int32_t get_offset_of_TextureRatio_20() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___TextureRatio_20)); }
	inline float get_TextureRatio_20() const { return ___TextureRatio_20; }
	inline float* get_address_of_TextureRatio_20() { return &___TextureRatio_20; }
	inline void set_TextureRatio_20(float value)
	{
		___TextureRatio_20 = value;
	}

	inline static int32_t get_offset_of_ScreenRatio_21() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___ScreenRatio_21)); }
	inline float get_ScreenRatio_21() const { return ___ScreenRatio_21; }
	inline float* get_address_of_ScreenRatio_21() { return &___ScreenRatio_21; }
	inline void set_ScreenRatio_21(float value)
	{
		___ScreenRatio_21 = value;
	}

	inline static int32_t get_offset_of_baseRotation_22() { return static_cast<int32_t>(offsetof(WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5, ___baseRotation_22)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_baseRotation_22() const { return ___baseRotation_22; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_baseRotation_22() { return &___baseRotation_22; }
	inline void set_baseRotation_22(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___baseRotation_22 = value;
	}
};


// WorldToScreenSpace
struct  WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform WorldToScreenSpace::reference
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___reference_4;
	// UnityEngine.RectTransform[] WorldToScreenSpace::targetRect
	RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* ___targetRect_5;
	// UnityEngine.Events.UnityEvent WorldToScreenSpace::OnScreenEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnScreenEvent_6;
	// UnityEngine.Events.UnityEvent WorldToScreenSpace::OffScreenEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OffScreenEvent_7;

public:
	inline static int32_t get_offset_of_reference_4() { return static_cast<int32_t>(offsetof(WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91, ___reference_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_reference_4() const { return ___reference_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_reference_4() { return &___reference_4; }
	inline void set_reference_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___reference_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reference_4), (void*)value);
	}

	inline static int32_t get_offset_of_targetRect_5() { return static_cast<int32_t>(offsetof(WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91, ___targetRect_5)); }
	inline RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* get_targetRect_5() const { return ___targetRect_5; }
	inline RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41** get_address_of_targetRect_5() { return &___targetRect_5; }
	inline void set_targetRect_5(RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* value)
	{
		___targetRect_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetRect_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnScreenEvent_6() { return static_cast<int32_t>(offsetof(WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91, ___OnScreenEvent_6)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnScreenEvent_6() const { return ___OnScreenEvent_6; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnScreenEvent_6() { return &___OnScreenEvent_6; }
	inline void set_OnScreenEvent_6(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnScreenEvent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnScreenEvent_6), (void*)value);
	}

	inline static int32_t get_offset_of_OffScreenEvent_7() { return static_cast<int32_t>(offsetof(WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91, ___OffScreenEvent_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OffScreenEvent_7() const { return ___OffScreenEvent_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OffScreenEvent_7() { return &___OffScreenEvent_7; }
	inline void set_OffScreenEvent_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OffScreenEvent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OffScreenEvent_7), (void*)value);
	}
};


// ZoomManager
struct  ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera ZoomManager::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_4;
	// System.Single ZoomManager::miniFov
	float ___miniFov_5;
	// System.Single ZoomManager::maxFov
	float ___maxFov_6;
	// System.Single ZoomManager::fov
	float ___fov_7;
	// UnityEngine.Touch[] ZoomManager::fingers
	TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* ___fingers_8;
	// System.Single ZoomManager::NDistStart
	float ___NDistStart_9;
	// System.Single ZoomManager::NDistNow
	float ___NDistNow_10;
	// System.Single ZoomManager::NDistDelta
	float ___NDistDelta_11;
	// System.Single ZoomManager::fovStart
	float ___fovStart_12;
	// System.Boolean ZoomManager::startZoom
	bool ___startZoom_13;

public:
	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___cam_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_4() const { return ___cam_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cam_4), (void*)value);
	}

	inline static int32_t get_offset_of_miniFov_5() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___miniFov_5)); }
	inline float get_miniFov_5() const { return ___miniFov_5; }
	inline float* get_address_of_miniFov_5() { return &___miniFov_5; }
	inline void set_miniFov_5(float value)
	{
		___miniFov_5 = value;
	}

	inline static int32_t get_offset_of_maxFov_6() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___maxFov_6)); }
	inline float get_maxFov_6() const { return ___maxFov_6; }
	inline float* get_address_of_maxFov_6() { return &___maxFov_6; }
	inline void set_maxFov_6(float value)
	{
		___maxFov_6 = value;
	}

	inline static int32_t get_offset_of_fov_7() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___fov_7)); }
	inline float get_fov_7() const { return ___fov_7; }
	inline float* get_address_of_fov_7() { return &___fov_7; }
	inline void set_fov_7(float value)
	{
		___fov_7 = value;
	}

	inline static int32_t get_offset_of_fingers_8() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___fingers_8)); }
	inline TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* get_fingers_8() const { return ___fingers_8; }
	inline TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D** get_address_of_fingers_8() { return &___fingers_8; }
	inline void set_fingers_8(TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* value)
	{
		___fingers_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingers_8), (void*)value);
	}

	inline static int32_t get_offset_of_NDistStart_9() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___NDistStart_9)); }
	inline float get_NDistStart_9() const { return ___NDistStart_9; }
	inline float* get_address_of_NDistStart_9() { return &___NDistStart_9; }
	inline void set_NDistStart_9(float value)
	{
		___NDistStart_9 = value;
	}

	inline static int32_t get_offset_of_NDistNow_10() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___NDistNow_10)); }
	inline float get_NDistNow_10() const { return ___NDistNow_10; }
	inline float* get_address_of_NDistNow_10() { return &___NDistNow_10; }
	inline void set_NDistNow_10(float value)
	{
		___NDistNow_10 = value;
	}

	inline static int32_t get_offset_of_NDistDelta_11() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___NDistDelta_11)); }
	inline float get_NDistDelta_11() const { return ___NDistDelta_11; }
	inline float* get_address_of_NDistDelta_11() { return &___NDistDelta_11; }
	inline void set_NDistDelta_11(float value)
	{
		___NDistDelta_11 = value;
	}

	inline static int32_t get_offset_of_fovStart_12() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___fovStart_12)); }
	inline float get_fovStart_12() const { return ___fovStart_12; }
	inline float* get_address_of_fovStart_12() { return &___fovStart_12; }
	inline void set_fovStart_12(float value)
	{
		___fovStart_12 = value;
	}

	inline static int32_t get_offset_of_startZoom_13() { return static_cast<int32_t>(offsetof(ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF, ___startZoom_13)); }
	inline bool get_startZoom_13() const { return ___startZoom_13; }
	inline bool* get_address_of_startZoom_13() { return &___startZoom_13; }
	inline void set_startZoom_13(bool value)
	{
		___startZoom_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54  m_Items[1];

public:
	inline WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DepthCameraName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Resolutions_4), (void*)NULL);
		#endif
	}
	inline WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DepthCameraName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Resolutions_4), (void*)NULL);
		#endif
	}
};
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * m_Items[1];

public:
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Texture[]
struct TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * m_Items[1];

public:
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * m_Items[1];

public:
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * m_Items[1];

public:
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  m_Items[1];

public:
	inline Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mE03C66715289D7957CA068A675826B7EE0887BE3_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C_gshared (UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void WebcamManager::Action_StopWebcam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebcamManager_Action_StopWebcam_mDF5716C20A284683B6B55E0DA5A81F64B3454C6D (WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator WebcamManager::initAndWaitForWebCamTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WebcamManager_initAndWaitForWebCamTexture_m6BD8830FC31CBDDE173ED9645F81AFCE13E95F20 (WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* WebCamTexture_get_devices_mF5D7FA78E9C67ADCBF592220A10F4B6678F1A920 (const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture__ctor_mA132E1976B248264D5AD01A1D45254FCF070D241 (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * __this, const RuntimeMethod* method);
// System.String UnityEngine.WebCamDevice::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WebCamDevice_get_name_m4D7362BB29DC20B7C8EF47759A09D54DEE8031F7 (WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041 (float ___f0, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61 (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * __this, String_t* ___deviceName0, int32_t ___requestedWidth1, int32_t ___requestedHeight2, int32_t ___requestedFPS3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebCamDevice_get_isFrontFacing_mD55FF74A2CE25897AD77EAB5935B6A76AD929D38 (WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture_set_requestedFPS_mEEA829DEFFB545A53D3DB35B319EAB13E758E2F5 (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436 (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mE03C66715289D7957CA068A675826B7EE0887BE3_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_m171D860AF3370C46648FE8F3EE3E0E6535E1C774 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_fieldOfView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Void ZoomManager::GestureZoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_GestureZoom_mFC947D24B1F247AF7E836531FD0E042A69EB0B7F (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m497E19AA4FA22DB659F631B20FAEF65572D1B44E (const RuntimeMethod* method);
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* Input_get_touches_mD31418E8B2487DBC9641A15677B41B459859011A (const RuntimeMethod* method);
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349 (Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F (Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
inline void UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C (UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tD2F68F6DD363B64D94CB72CA4170B8DC7A1117F6 *, const RuntimeMethod*))UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebcamManager_<RestartWebcam>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__7__ctor_mAC2246AAC214D33683E28AFF4DEF61BFD4B5C0A7 (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void WebcamManager_<RestartWebcam>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__7_System_IDisposable_Dispose_m9E145E51BCB8AC47FD1E6FF92517E903F025CA08 (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean WebcamManager_<RestartWebcam>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CRestartWebcamU3Ed__7_MoveNext_m1FB1B755C59C1A02B363C8F91BC6F45879E1014A (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_004b;
			}
			case 2:
			{
				goto IL_0069;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// canRestart = false;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_canRestart_9((bool)0);
		// Action_StopWebcam();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_4 = V_1;
		NullCheck(L_4);
		WebcamManager_Action_StopWebcam_mDF5716C20A284683B6B55E0DA5A81F64B3454C6D(L_4, /*hidden argument*/NULL);
		// yield return initAndWaitForWebCamTexture();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_5 = V_1;
		NullCheck(L_5);
		RuntimeObject* L_6 = WebcamManager_initAndWaitForWebCamTexture_m6BD8830FC31CBDDE173ED9645F81AFCE13E95F20(L_5, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// canRestart = true;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_7 = V_1;
		NullCheck(L_7);
		L_7->set_canRestart_9((bool)1);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0069:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object WebcamManager_<RestartWebcam>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRestartWebcamU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA08476F2AB96473F57C75EDB4D9F90FC2F7431D3 (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void WebcamManager_<RestartWebcam>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_Reset_m353C8B6D1E46B7DA099E503AAA0CFD8342ECBC1E (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_Reset_m353C8B6D1E46B7DA099E503AAA0CFD8342ECBC1E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_Reset_m353C8B6D1E46B7DA099E503AAA0CFD8342ECBC1E_RuntimeMethod_var);
	}
}
// System.Object WebcamManager_<RestartWebcam>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRestartWebcamU3Ed__7_System_Collections_IEnumerator_get_Current_m1E31EB5C1792169B3AF03424EECD2F7283DD03A8 (U3CRestartWebcamU3Ed__7_tA6BCB019F42EAF3BE57E3A9CABC65BA672047E18 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebcamManager_<initAndWaitForWebCamTexture>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__29__ctor_m486C4215400AF2EB2C07A28949B36FF267370870 (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void WebcamManager_<initAndWaitForWebCamTexture>d__29::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__29_System_IDisposable_Dispose_m5A98C132430873536ADF2DD41CCDE69078EE4DBA (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean WebcamManager_<initAndWaitForWebCamTexture>d__29::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CinitAndWaitForWebCamTextureU3Ed__29_MoveNext_m5F60D8EC89DB29F9C0F212F131CCF12C9BADBF88 (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CinitAndWaitForWebCamTextureU3Ed__29_MoveNext_m5F60D8EC89DB29F9C0F212F131CCF12C9BADBF88_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* V_4 = NULL;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0276;
			}
			case 2:
			{
				goto IL_02d7;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// isInitWaiting = true;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_isInitWaiting_19((bool)1);
		// devices = WebCamTexture.devices;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_4 = V_1;
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_5 = WebCamTexture_get_devices_mF5D7FA78E9C67ADCBF592220A10F4B6678F1A920(/*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_devices_4(L_5);
		// webCams = new WebCamTexture[devices.Length];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_6 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_7 = V_1;
		NullCheck(L_7);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_8 = L_7->get_devices_4();
		NullCheck(L_8);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_9 = (WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1*)(WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1*)SZArrayNew(WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))));
		NullCheck(L_6);
		L_6->set_webCams_5(L_9);
		// textures = new Texture[devices.Length];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_10 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_11 = V_1;
		NullCheck(L_11);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_12 = L_11->get_devices_4();
		NullCheck(L_12);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_13 = (TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B*)(TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B*)SZArrayNew(TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))));
		NullCheck(L_10);
		L_10->set_textures_6(L_13);
		// for (int i = 0; i < devices.Length; i++)
		V_2 = 0;
		goto IL_0115;
	}

IL_0068:
	{
		// webCams[i] = new WebCamTexture();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_14 = V_1;
		NullCheck(L_14);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_15 = L_14->get_webCams_5();
		int32_t L_16 = V_2;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_17 = (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 *)il2cpp_codegen_object_new(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_mA132E1976B248264D5AD01A1D45254FCF070D241(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 *)L_17);
		// webCams[i] = new WebCamTexture(devices[i].name, Mathf.RoundToInt(requestResolution.x), Mathf.RoundToInt(requestResolution.y), 30);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_18 = V_1;
		NullCheck(L_18);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_19 = L_18->get_webCams_5();
		int32_t L_20 = V_2;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_21 = V_1;
		NullCheck(L_21);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_22 = L_21->get_devices_4();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		String_t* L_24 = WebCamDevice_get_name_m4D7362BB29DC20B7C8EF47759A09D54DEE8031F7((WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23))), /*hidden argument*/NULL);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_25 = V_1;
		NullCheck(L_25);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_26 = L_25->get_address_of_requestResolution_16();
		float L_27 = L_26->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_28 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(L_27, /*hidden argument*/NULL);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_29 = V_1;
		NullCheck(L_29);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_30 = L_29->get_address_of_requestResolution_16();
		float L_31 = L_30->get_y_1();
		int32_t L_32 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(L_31, /*hidden argument*/NULL);
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_33 = (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 *)il2cpp_codegen_object_new(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_mCDA59B88B6D7F96B76663FA98EF12B7AF2DCFD61(L_33, L_24, L_28, L_32, ((int32_t)30), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_33);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 *)L_33);
		// textures[i] = webCams[i];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_34 = V_1;
		NullCheck(L_34);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_35 = L_34->get_textures_6();
		int32_t L_36 = V_2;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_37 = V_1;
		NullCheck(L_37);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_38 = L_37->get_webCams_5();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_41);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(L_36), (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 *)L_41);
		// textures[i].wrapMode = TextureWrapMode.Repeat;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_42 = V_1;
		NullCheck(L_42);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_43 = L_42->get_textures_6();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_46, 0, /*hidden argument*/NULL);
		// if (useFrontCam)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_47 = V_1;
		NullCheck(L_47);
		bool L_48 = L_47->get_useFrontCam_8();
		if (!L_48)
		{
			goto IL_00f7;
		}
	}
	{
		// if (devices[i].isFrontFacing)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_49 = V_1;
		NullCheck(L_49);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_50 = L_49->get_devices_4();
		int32_t L_51 = V_2;
		NullCheck(L_50);
		bool L_52 = WebCamDevice_get_isFrontFacing_mD55FF74A2CE25897AD77EAB5935B6A76AD929D38((WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 *)((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_51))), /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0111;
		}
	}
	{
		// TargetCamID = i;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_53 = V_1;
		int32_t L_54 = V_2;
		NullCheck(L_53);
		L_53->set_TargetCamID_7(L_54);
		// }
		goto IL_0111;
	}

IL_00f7:
	{
		// if (!devices[i].isFrontFacing)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_55 = V_1;
		NullCheck(L_55);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_56 = L_55->get_devices_4();
		int32_t L_57 = V_2;
		NullCheck(L_56);
		bool L_58 = WebCamDevice_get_isFrontFacing_mD55FF74A2CE25897AD77EAB5935B6A76AD929D38((WebCamDevice_tA545BEDFAFD78866911F4837B8406845541B8F54 *)((L_56)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_57))), /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_0111;
		}
	}
	{
		// TargetCamID = i;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_59 = V_1;
		int32_t L_60 = V_2;
		NullCheck(L_59);
		L_59->set_TargetCamID_7(L_60);
	}

IL_0111:
	{
		// for (int i = 0; i < devices.Length; i++)
		int32_t L_61 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_0115:
	{
		// for (int i = 0; i < devices.Length; i++)
		int32_t L_62 = V_2;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_63 = V_1;
		NullCheck(L_63);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_64 = L_63->get_devices_4();
		NullCheck(L_64);
		if ((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_64)->max_length)))))))
		{
			goto IL_0068;
		}
	}
	{
		// if (TargetCamID > devices.Length - 1) TargetCamID = devices.Length - 1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_65 = V_1;
		NullCheck(L_65);
		int32_t L_66 = L_65->get_TargetCamID_7();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_67 = V_1;
		NullCheck(L_67);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_68 = L_67->get_devices_4();
		NullCheck(L_68);
		if ((((int32_t)L_66) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_68)->max_length)))), (int32_t)1)))))
		{
			goto IL_0145;
		}
	}
	{
		// if (TargetCamID > devices.Length - 1) TargetCamID = devices.Length - 1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_69 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_70 = V_1;
		NullCheck(L_70);
		WebCamDeviceU5BU5D_t1E18FBC79F0E58E248F2496291CE994A5E4AC21A* L_71 = L_70->get_devices_4();
		NullCheck(L_71);
		NullCheck(L_69);
		L_69->set_TargetCamID_7(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_71)->max_length)))), (int32_t)1)));
	}

IL_0145:
	{
		// texture = webCams[TargetCamID];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_72 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_73 = V_1;
		NullCheck(L_73);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_74 = L_73->get_webCams_5();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_75 = V_1;
		NullCheck(L_75);
		int32_t L_76 = L_75->get_TargetCamID_7();
		NullCheck(L_74);
		int32_t L_77 = L_76;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_78 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		NullCheck(L_72);
		L_72->set_texture_11(L_78);
		// webCams[TargetCamID].requestedFPS = 30;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_79 = V_1;
		NullCheck(L_79);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_80 = L_79->get_webCams_5();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_81 = V_1;
		NullCheck(L_81);
		int32_t L_82 = L_81->get_TargetCamID_7();
		NullCheck(L_80);
		int32_t L_83 = L_82;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_84 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		NullCheck(L_84);
		WebCamTexture_set_requestedFPS_mEEA829DEFFB545A53D3DB35B319EAB13E758E2F5(L_84, (30.0f), /*hidden argument*/NULL);
		// webCams[TargetCamID].Play();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_85 = V_1;
		NullCheck(L_85);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_86 = L_85->get_webCams_5();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_87 = V_1;
		NullCheck(L_87);
		int32_t L_88 = L_87->get_TargetCamID_7();
		NullCheck(L_86);
		int32_t L_89 = L_88;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_90 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		NullCheck(L_90);
		WebCamTexture_Play_mCF10A9B5EE587A066396B6378A972B31C9134436(L_90, /*hidden argument*/NULL);
		// for (int i = 0; i < materials.Length; i++)
		V_3 = 0;
		goto IL_01a3;
	}

IL_0185:
	{
		// materials[i].mainTexture = textures[TargetCamID];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_91 = V_1;
		NullCheck(L_91);
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_92 = L_91->get_materials_12();
		int32_t L_93 = V_3;
		NullCheck(L_92);
		int32_t L_94 = L_93;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_95 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_94));
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_96 = V_1;
		NullCheck(L_96);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_97 = L_96->get_textures_6();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_98 = V_1;
		NullCheck(L_98);
		int32_t L_99 = L_98->get_TargetCamID_7();
		NullCheck(L_97);
		int32_t L_100 = L_99;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_101 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_95);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_95, L_101, /*hidden argument*/NULL);
		// for (int i = 0; i < materials.Length; i++)
		int32_t L_102 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_102, (int32_t)1));
	}

IL_01a3:
	{
		// for (int i = 0; i < materials.Length; i++)
		int32_t L_103 = V_3;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_104 = V_1;
		NullCheck(L_104);
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_105 = L_104->get_materials_12();
		NullCheck(L_105);
		if ((((int32_t)L_103) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_105)->max_length)))))))
		{
			goto IL_0185;
		}
	}
	{
		// if (BackgroundQuad != null) BackgroundQuad.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_106 = V_1;
		NullCheck(L_106);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_107 = L_106->get_BackgroundQuad_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_108 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_107, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_01de;
		}
	}
	{
		// if (BackgroundQuad != null) BackgroundQuad.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_109 = V_1;
		NullCheck(L_109);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_110 = L_109->get_BackgroundQuad_14();
		NullCheck(L_110);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_111 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_110, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_111);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_112 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_111, /*hidden argument*/NULL);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_113 = V_1;
		NullCheck(L_113);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_114 = L_113->get_textures_6();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_115 = V_1;
		NullCheck(L_115);
		int32_t L_116 = L_115->get_TargetCamID_7();
		NullCheck(L_114);
		int32_t L_117 = L_116;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_118 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		NullCheck(L_112);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_112, L_118, /*hidden argument*/NULL);
	}

IL_01de:
	{
		// foreach (GameObject obj in targetMeshObjects)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_119 = V_1;
		NullCheck(L_119);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_120 = L_119->get_targetMeshObjects_13();
		V_4 = L_120;
		V_5 = 0;
		goto IL_0212;
	}

IL_01eb:
	{
		// foreach (GameObject obj in targetMeshObjects)
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_121 = V_4;
		int32_t L_122 = V_5;
		NullCheck(L_121);
		int32_t L_123 = L_122;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		// obj.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		NullCheck(L_124);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_125 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_124, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_125);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_126 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_125, /*hidden argument*/NULL);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_127 = V_1;
		NullCheck(L_127);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_128 = L_127->get_textures_6();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_129 = V_1;
		NullCheck(L_129);
		int32_t L_130 = L_129->get_TargetCamID_7();
		NullCheck(L_128);
		int32_t L_131 = L_130;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_132 = (L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		NullCheck(L_126);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_126, L_132, /*hidden argument*/NULL);
		int32_t L_133 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
	}

IL_0212:
	{
		// foreach (GameObject obj in targetMeshObjects)
		int32_t L_134 = V_5;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_135 = V_4;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_135)->max_length)))))))
		{
			goto IL_01eb;
		}
	}
	{
		// if (textures.Length > 0)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_136 = V_1;
		NullCheck(L_136);
		TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* L_137 = L_136->get_textures_6();
		NullCheck(L_137);
		if (!(((RuntimeArray*)L_137)->max_length))
		{
			goto IL_02c7;
		}
	}
	{
		// webCamTexture = webCams[TargetCamID];
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_138 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_139 = V_1;
		NullCheck(L_139);
		WebCamTextureU5BU5D_tA5B77BD4F6D628718D11003C9A67B59AF19ADCF1* L_140 = L_139->get_webCams_5();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_141 = V_1;
		NullCheck(L_141);
		int32_t L_142 = L_141->get_TargetCamID_7();
		NullCheck(L_140);
		int32_t L_143 = L_142;
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_144 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		NullCheck(L_138);
		L_138->set_webCamTexture_10(L_144);
		// int initFrameCount = 0;
		__this->set_U3CinitFrameCountU3E5__2_3(0);
		goto IL_027d;
	}

IL_0242:
	{
		// if (initFrameCount > timeoutFrameCount)
		int32_t L_145 = __this->get_U3CinitFrameCountU3E5__2_3();
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_146 = V_1;
		NullCheck(L_146);
		int32_t L_147 = L_146->get_timeoutFrameCount_15();
		if ((((int32_t)L_145) > ((int32_t)L_147)))
		{
			goto IL_028c;
		}
	}
	{
		// initFrameCount++;
		int32_t L_148 = __this->get_U3CinitFrameCountU3E5__2_3();
		V_5 = L_148;
		int32_t L_149 = V_5;
		__this->set_U3CinitFrameCountU3E5__2_3(((int32_t)il2cpp_codegen_add((int32_t)L_149, (int32_t)1)));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_150 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_150, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_150);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0276:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_027d:
	{
		// while (webCamTexture.width <= 16)
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_151 = V_1;
		NullCheck(L_151);
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_152 = L_151->get_webCamTexture_10();
		NullCheck(L_152);
		int32_t L_153 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_152);
		if ((((int32_t)L_153) <= ((int32_t)((int32_t)16))))
		{
			goto IL_0242;
		}
	}

IL_028c:
	{
		// textureResolution = new Vector2(webCamTexture.width, webCamTexture.height);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_154 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_155 = V_1;
		NullCheck(L_155);
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_156 = L_155->get_webCamTexture_10();
		NullCheck(L_156);
		int32_t L_157 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_156);
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_158 = V_1;
		NullCheck(L_158);
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_159 = L_158->get_webCamTexture_10();
		NullCheck(L_159);
		int32_t L_160 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_159);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_161;
		memset((&L_161), 0, sizeof(L_161));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_161), (((float)((float)L_157))), (((float)((float)L_160))), /*hidden argument*/NULL);
		NullCheck(L_154);
		L_154->set_textureResolution_17(L_161);
		// isFlipped = webCamTexture.videoVerticallyMirrored;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_162 = V_1;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_163 = V_1;
		NullCheck(L_163);
		WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * L_164 = L_163->get_webCamTexture_10();
		NullCheck(L_164);
		bool L_165 = WebCamTexture_get_videoVerticallyMirrored_m4E0EB16E94118818A000761778F2672B5D2DD8AD(L_164, /*hidden argument*/NULL);
		NullCheck(L_162);
		L_162->set_isFlipped_18(L_165);
		// isInitWaiting = false;
		WebcamManager_t0F6D402CB27920FF650BCECD2BEBED2E87E039E5 * L_166 = V_1;
		NullCheck(L_166);
		L_166->set_isInitWaiting_19((bool)0);
	}

IL_02c7:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_02d7:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object WebcamManager_<initAndWaitForWebCamTexture>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E2335D09A7AF258616B9A148E4FE9D16237B8C7 (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void WebcamManager_<initAndWaitForWebCamTexture>d__29::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_Reset_m914092285CF2D5777DD74D018FD150F3C5699E0A (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_Reset_m914092285CF2D5777DD74D018FD150F3C5699E0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_Reset_m914092285CF2D5777DD74D018FD150F3C5699E0A_RuntimeMethod_var);
	}
}
// System.Object WebcamManager_<initAndWaitForWebCamTexture>d__29::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CinitAndWaitForWebCamTextureU3Ed__29_System_Collections_IEnumerator_get_Current_mEFE99BE2769A43435F35C90E734B161568D9C5AB (U3CinitAndWaitForWebCamTextureU3Ed__29_t77B7500EC729D0FB1B5C0B742A71477AA8CA4BCD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldToScreenSpace::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_Start_mBC02731DDCFF751B8DDFE5CA17F72B9ECF9E2DA4 (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		// OffScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_OffScreenEvent_7();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_Update_mDB733B72CBBBA38CA1281CA4EE20F9071338AA2B (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// Vector3 SPos = Camera.main.WorldToScreenPoint(reference.position);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = __this->get_reference_4();
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// foreach(RectTransform _rect in targetRect)
		RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* L_4 = __this->get_targetRect_5();
		V_1 = L_4;
		V_2 = 0;
		goto IL_002e;
	}

IL_0021:
	{
		// foreach(RectTransform _rect in targetRect)
		RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		// _rect.position = SPos;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		// foreach(RectTransform _rect in targetRect)
		int32_t L_11 = V_2;
		RectTransformU5BU5D_tCB394094C26CC66A640A1A788BA3E9012CE22C41* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		// if (SPos.z < 0 || SPos.x < -(float)Screen.width/2f || SPos.x > (float)Screen.width *3f/ 2f || SPos.y < -(float)Screen.height/2f || SPos.y > (float)Screen.height*3f/2f)
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_0;
		float L_14 = L_13.get_z_4();
		if ((((float)L_14) < ((float)(0.0f))))
		{
			goto IL_009f;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_0;
		float L_16 = L_15.get_x_2();
		int32_t L_17 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		if ((((float)L_16) < ((float)((float)((float)((-(((float)((float)L_17)))))/(float)(2.0f))))))
		{
			goto IL_009f;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_0;
		float L_19 = L_18.get_x_2();
		int32_t L_20 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		if ((((float)L_19) > ((float)((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_20))), (float)(3.0f)))/(float)(2.0f))))))
		{
			goto IL_009f;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_0;
		float L_22 = L_21.get_y_3();
		int32_t L_23 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		if ((((float)L_22) < ((float)((float)((float)((-(((float)((float)L_23)))))/(float)(2.0f))))))
		{
			goto IL_009f;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = V_0;
		float L_25 = L_24.get_y_3();
		int32_t L_26 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		if ((!(((float)L_25) > ((float)((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_26))), (float)(3.0f)))/(float)(2.0f)))))))
		{
			goto IL_00ab;
		}
	}

IL_009f:
	{
		// OffScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_27 = __this->get_OffScreenEvent_7();
		NullCheck(L_27);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_27, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00ab:
	{
		// OnScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_28 = __this->get_OnScreenEvent_6();
		NullCheck(L_28);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_28, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::InvokeOnScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_InvokeOnScreen_m6C79CD98C8A363BC399A700F47F016B732D8D278 (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		// OnScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_OnScreenEvent_6();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::InvokeOffScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_InvokeOffScreen_mB59D5CFC7934D803540365965099259862B5F9D3 (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		// OffScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_OffScreenEvent_7();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_OnDisable_m458F5509714BB609FEFC87FC7BE03143AE829F4C (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		// OffScreenEvent.Invoke();
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_0 = __this->get_OffScreenEvent_7();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_OnEnable_m85032831839FE277D44BDB6E0D54FFA5F5829218 (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::DebugTest(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace_DebugTest_mF1C9DF13586600FCD9C885D107E4BCC5CCA032BE (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, String_t* ____text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldToScreenSpace_DebugTest_mF1C9DF13586600FCD9C885D107E4BCC5CCA032BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print("Trigger Debug: " + _text);
		String_t* L_0 = ____text0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral757D3BA1EDB250E4DB4540A97894271FD8D7F726, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m171D860AF3370C46648FE8F3EE3E0E6535E1C774(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldToScreenSpace::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldToScreenSpace__ctor_m4E801D807AC17E2E4E3D514290A1B4D6A52A7999 (WorldToScreenSpace_t61AB941BC72E9B99E7626BD530EC913A7D983B91 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_Start_m2E7057C34382836C748CFC31609DCC3C0DAF7F16 (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomManager_Start_m2E7057C34382836C748CFC31609DCC3C0DAF7F16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (cam == null) cam = Camera.main;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = __this->get_cam_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// if (cam == null) cam = Camera.main;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_2 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		__this->set_cam_4(L_2);
	}

IL_0019:
	{
		// fov = cam.fieldOfView;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = __this->get_cam_4();
		NullCheck(L_3);
		float L_4 = Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6(L_3, /*hidden argument*/NULL);
		__this->set_fov_7(L_4);
		// }
		return;
	}
}
// System.Void ZoomManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_Update_mA47E100AFCF59E12AB380D2A425CA3B8D04772DF (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomManager_Update_mA47E100AFCF59E12AB380D2A425CA3B8D04772DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GestureZoom();
		ZoomManager_GestureZoom_mFC947D24B1F247AF7E836531FD0E042A69EB0B7F(__this, /*hidden argument*/NULL);
		// fov = Mathf.Clamp(fov, miniFov, maxFov);
		float L_0 = __this->get_fov_7();
		float L_1 = __this->get_miniFov_5();
		float L_2 = __this->get_maxFov_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507(L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_fov_7(L_3);
		// cam.fieldOfView = fov;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_4 = __this->get_cam_4();
		float L_5 = __this->get_fov_7();
		NullCheck(L_4);
		Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ZoomManager::GestureZoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_GestureZoom_mFC947D24B1F247AF7E836531FD0E042A69EB0B7F (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomManager_GestureZoom_mFC947D24B1F247AF7E836531FD0E042A69EB0B7F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// if (Input.touchCount >= 2)
		int32_t L_0 = Input_get_touchCount_m497E19AA4FA22DB659F631B20FAEF65572D1B44E(/*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)2)))
		{
			goto IL_01b5;
		}
	}
	{
		// fingers = Input.touches;
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_1 = Input_get_touches_mD31418E8B2487DBC9641A15677B41B459859011A(/*hidden argument*/NULL);
		__this->set_fingers_8(L_1);
		// if (fingers[1].phase == TouchPhase.Began)
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_2 = __this->get_fingers_8();
		NullCheck(L_2);
		int32_t L_3 = Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_01bc;
		}
	}
	{
		// else if (fingers[1].phase == TouchPhase.Moved)
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_4 = __this->get_fingers_8();
		NullCheck(L_4);
		int32_t L_5 = Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_01bc;
		}
	}
	{
		// if (!startZoom)
		bool L_6 = __this->get_startZoom_13();
		if (L_6)
		{
			goto IL_00f5;
		}
	}
	{
		// startZoom = true;
		__this->set_startZoom_13((bool)1);
		// fovStart = fov;
		float L_7 = __this->get_fov_7();
		__this->set_fovStart_12(L_7);
		// Vector2 pos1 = new Vector2((float)fingers[0].position.x / (float)Screen.width, (float)fingers[0].position.y / (float)Screen.height);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_8 = __this->get_fingers_8();
		NullCheck(L_8);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		float L_10 = L_9.get_x_0();
		int32_t L_11 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_12 = __this->get_fingers_8();
		NullCheck(L_12);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		float L_14 = L_13.get_y_1();
		int32_t L_15 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_0), ((float)((float)(((float)((float)L_10)))/(float)(((float)((float)L_11))))), ((float)((float)(((float)((float)L_14)))/(float)(((float)((float)L_15))))), /*hidden argument*/NULL);
		// Vector2 pos2 = new Vector2((float)fingers[1].position.x / (float)Screen.width, (float)fingers[1].position.y / (float)Screen.height);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_16 = __this->get_fingers_8();
		NullCheck(L_16);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		float L_18 = L_17.get_x_0();
		int32_t L_19 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_20 = __this->get_fingers_8();
		NullCheck(L_20);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		float L_22 = L_21.get_y_1();
		int32_t L_23 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_1), ((float)((float)(((float)((float)L_18)))/(float)(((float)((float)L_19))))), ((float)((float)(((float)((float)L_22)))/(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		// NDistStart = Vector2.Distance(pos1, pos2);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_26 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_24, L_25, /*hidden argument*/NULL);
		__this->set_NDistStart_9(L_26);
		// }
		return;
	}

IL_00f5:
	{
		// Vector2 pos1 = new Vector2((float)fingers[0].position.x / (float)Screen.width, (float)fingers[0].position.y / (float)Screen.height);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_27 = __this->get_fingers_8();
		NullCheck(L_27);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		float L_29 = L_28.get_x_0();
		int32_t L_30 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_31 = __this->get_fingers_8();
		NullCheck(L_31);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		float L_33 = L_32.get_y_1();
		int32_t L_34 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_2), ((float)((float)(((float)((float)L_29)))/(float)(((float)((float)L_30))))), ((float)((float)(((float)((float)L_33)))/(float)(((float)((float)L_34))))), /*hidden argument*/NULL);
		// Vector2 pos2 = new Vector2((float)fingers[1].position.x / (float)Screen.width, (float)fingers[1].position.y / (float)Screen.height);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_35 = __this->get_fingers_8();
		NullCheck(L_35);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		float L_37 = L_36.get_x_0();
		int32_t L_38 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		TouchU5BU5D_t0207B72FD95EF1F56E7A6C9F0A42896B03D2BD5D* L_39 = __this->get_fingers_8();
		NullCheck(L_39);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_40 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/NULL);
		float L_41 = L_40.get_y_1();
		int32_t L_42 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_3), ((float)((float)(((float)((float)L_37)))/(float)(((float)((float)L_38))))), ((float)((float)(((float)((float)L_41)))/(float)(((float)((float)L_42))))), /*hidden argument*/NULL);
		// NDistNow = Vector2.Distance(pos1, pos2);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		float L_45 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_43, L_44, /*hidden argument*/NULL);
		__this->set_NDistNow_10(L_45);
		// NDistDelta = NDistNow - NDistStart;
		float L_46 = __this->get_NDistNow_10();
		float L_47 = __this->get_NDistStart_9();
		__this->set_NDistDelta_11(((float)il2cpp_codegen_subtract((float)L_46, (float)L_47)));
		// fov = fovStart - NDistDelta * 30f;
		float L_48 = __this->get_fovStart_12();
		float L_49 = __this->get_NDistDelta_11();
		__this->set_fov_7(((float)il2cpp_codegen_subtract((float)L_48, (float)((float)il2cpp_codegen_multiply((float)L_49, (float)(30.0f))))));
		// }
		return;
	}

IL_01b5:
	{
		// startZoom = false;
		__this->set_startZoom_13((bool)0);
	}

IL_01bc:
	{
		// }
		return;
	}
}
// System.Void ZoomManager::ZoomIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_ZoomIn_mF30F49A982C7217B5C959AC46F751833D5D91C39 (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	{
		// fov -= Time.deltaTime * 10f;
		float L_0 = __this->get_fov_7();
		float L_1 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_fov_7(((float)il2cpp_codegen_subtract((float)L_0, (float)((float)il2cpp_codegen_multiply((float)L_1, (float)(10.0f))))));
		// }
		return;
	}
}
// System.Void ZoomManager::ZoomOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager_ZoomOut_m7AD2AB998E326513BFB8184F03093963D9B4C076 (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	{
		// fov += Time.deltaTime * 10f;
		float L_0 = __this->get_fov_7();
		float L_1 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_fov_7(((float)il2cpp_codegen_add((float)L_0, (float)((float)il2cpp_codegen_multiply((float)L_1, (float)(10.0f))))));
		// }
		return;
	}
}
// System.Void ZoomManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomManager__ctor_m4A35081A42369F5AC9BDBDCFDB426F24E8B9EEA0 (ZoomManager_t04D7C455F16F44FAEED2556C5B4867F9A944E8EF * __this, const RuntimeMethod* method)
{
	{
		// public float miniFov = 10f;
		__this->set_miniFov_5((10.0f));
		// public float maxFov = 60f;
		__this->set_maxFov_6((60.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void _UnityEventFloat::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void _UnityEventFloat__ctor_m9B4423F24CE2CCDC15D30C713A28BE29FA6051B7 (_UnityEventFloat_t1CD5ADCB52C0E71B6BA30D63EBA35410F6FB078E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (_UnityEventFloat__ctor_m9B4423F24CE2CCDC15D30C713A28BE29FA6051B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C(__this, /*hidden argument*/UnityEvent_1__ctor_m029309819C0547BE76007CF58B8EB0BC5BB9765C_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
