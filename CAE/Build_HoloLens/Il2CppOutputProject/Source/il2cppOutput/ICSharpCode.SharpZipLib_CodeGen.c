﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void SharpZipBaseException__ctor_m8F36A151FD10EFE442635BB2244679104C0E5CF2 ();
// 0x00000002 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor()
extern void SharpZipBaseException__ctor_mEE9958E80315BFB20886ACCB7DF5DAD2C95A56A8 ();
// 0x00000003 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern void SharpZipBaseException__ctor_m971F830DD87B19ECF2FBE767F8813F937E2AA599 ();
// 0x00000004 System.Int64 ICSharpCode.SharpZipLib.Checksums.Adler32::get_Value()
extern void Adler32_get_Value_m16A427A694A377A95659B20DAEEEDE63511012C9 ();
// 0x00000005 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::.ctor()
extern void Adler32__ctor_mBACA5EE3E7AF140146FC9CC5683A8486CD071721 ();
// 0x00000006 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Reset()
extern void Adler32_Reset_mC980D97C390BD301FBEC388D4F39C2C9126818FE ();
// 0x00000007 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Update(System.Byte[],System.Int32,System.Int32)
extern void Adler32_Update_mBD8ED2935248F4A04C36CC87463A3B4C940C17CD ();
// 0x00000008 System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::ComputeCrc32(System.UInt32,System.Byte)
extern void Crc32_ComputeCrc32_m6431C5BB7903E72DF906A7E8DC21A7E185F1381F ();
// 0x00000009 System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern void Crc32_get_Value_mEB6DE78E1DD4475B9529C661C6B22E8FAF54FD58 ();
// 0x0000000A System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Reset()
extern void Crc32_Reset_mEE00F5C965353F4877FA72A872C4BDCC9F075D7A ();
// 0x0000000B System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern void Crc32_Update_m8CA7ACA9F8AAF87BAD859060E8AAF0B881BD0C3F ();
// 0x0000000C System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern void Crc32__ctor_mCA3D891CAAAF27B529FDBC3FC6DA460B4937CC54 ();
// 0x0000000D System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern void Crc32__cctor_m0C89DDF5655F2D7FE6096D697AF8846CCD724C72 ();
// 0x0000000E System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::Copy(System.IO.Stream,System.IO.Stream,System.Byte[])
extern void StreamUtils_Copy_m8B2E9C90C8F0E40D59D3F88F53C279A775BD1961 ();
// 0x0000000F System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassic::GenerateKeys(System.Byte[])
extern void PkzipClassic_GenerateKeys_mA0E3014C60FB014C1B6CC955336276CDA29BD4F7 ();
// 0x00000010 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassic::.ctor()
extern void PkzipClassic__ctor_m8F0621735661F9786B07CADB31821F44360A545A ();
// 0x00000011 System.Byte ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::TransformByte()
extern void PkzipClassicCryptoBase_TransformByte_m65BDE065D910453F89EB06A565981323532245EC ();
// 0x00000012 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::SetKeys(System.Byte[])
extern void PkzipClassicCryptoBase_SetKeys_m967738FEC93D2800551E6C72A079419DE4C33AC5 ();
// 0x00000013 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::UpdateKeys(System.Byte)
extern void PkzipClassicCryptoBase_UpdateKeys_m837410955967DCC14839FEC820B7A94F64474EEC ();
// 0x00000014 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::Reset()
extern void PkzipClassicCryptoBase_Reset_m6E3DCCC4E78A24722EA2450FF0BEE89C9A0D4B63 ();
// 0x00000015 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::.ctor()
extern void PkzipClassicCryptoBase__ctor_m18FFEE76F02209557A46BEF0D3A2987B429BAD9F ();
// 0x00000016 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::.ctor(System.Byte[])
extern void PkzipClassicEncryptCryptoTransform__ctor_m1DD6A37A4B93D79958426D541DB50D39A6EEBD1E ();
// 0x00000017 System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void PkzipClassicEncryptCryptoTransform_TransformFinalBlock_mB868E518CA5A17BFF5A8B246C2D64CB5A9DBD802 ();
// 0x00000018 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void PkzipClassicEncryptCryptoTransform_TransformBlock_m365A519F41971D52E109F99D6169127B85AFAD62 ();
// 0x00000019 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_InputBlockSize()
extern void PkzipClassicEncryptCryptoTransform_get_InputBlockSize_mAEF937206B50BB189DCFA595D35695BBC50C5D5F ();
// 0x0000001A System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_OutputBlockSize()
extern void PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_m3E950B4630EE51E9B1156A4AB3D1260202E69300 ();
// 0x0000001B System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanTransformMultipleBlocks()
extern void PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_m4C2D6E270A7485335428E218625AF8F69F57A20C ();
// 0x0000001C System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::Dispose()
extern void PkzipClassicEncryptCryptoTransform_Dispose_m62CEA6BDD034B46DC588DEEAC26704A5A17BC16B ();
// 0x0000001D System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::.ctor(System.Byte[])
extern void PkzipClassicDecryptCryptoTransform__ctor_m61309229BB1098237AF24C5E0D17B7476916E981 ();
// 0x0000001E System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void PkzipClassicDecryptCryptoTransform_TransformFinalBlock_mAB2F29DBAEA2CCC9649DDB8E3B7573A1564183CF ();
// 0x0000001F System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void PkzipClassicDecryptCryptoTransform_TransformBlock_m79CD1B6D5A84F58C343C39FED21736B0666168AE ();
// 0x00000020 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_InputBlockSize()
extern void PkzipClassicDecryptCryptoTransform_get_InputBlockSize_mB4E824012A26E8B1DE9C5E6051073643CD136AC7 ();
// 0x00000021 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_OutputBlockSize()
extern void PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_m57CD96B5D2D4041728C0EE3CC60469273BDEC7DA ();
// 0x00000022 System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanTransformMultipleBlocks()
extern void PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_m4952EAABE981AB4F3BA6A1C91F6EEE5E8B3A1838 ();
// 0x00000023 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::Dispose()
extern void PkzipClassicDecryptCryptoTransform_Dispose_m4029757A4955371231790DF2E68609939BDDA6E8 ();
// 0x00000024 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_BlockSize()
extern void PkzipClassicManaged_get_BlockSize_mDB8805986EDB66EB9FD4C952423DA56F3E4B1708 ();
// 0x00000025 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_BlockSize(System.Int32)
extern void PkzipClassicManaged_set_BlockSize_m140D9E6AEBA9EEC6B0A274A5440DBC21E0AAEC5E ();
// 0x00000026 System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalKeySizes()
extern void PkzipClassicManaged_get_LegalKeySizes_m478058C5D93CD270D0E9230E1CE14DBFA82F13D4 ();
// 0x00000027 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateIV()
extern void PkzipClassicManaged_GenerateIV_mBB8E77BA13AD242681625CCEC0A3DEDF66BFC722 ();
// 0x00000028 System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_Key()
extern void PkzipClassicManaged_get_Key_m856B3A80419BF4641C5BE7124D9A8C62461759CF ();
// 0x00000029 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_Key(System.Byte[])
extern void PkzipClassicManaged_set_Key_mBFA2FD2272301471ECF6A910F778391115AEA54C ();
// 0x0000002A System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateKey()
extern void PkzipClassicManaged_GenerateKey_mE3B273E8C72F300FFF712D26AD19455B4E77B8F2 ();
// 0x0000002B System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void PkzipClassicManaged_CreateEncryptor_m44228C6A99F2DFA7EED29BFC9915D9BCB224404B ();
// 0x0000002C System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void PkzipClassicManaged_CreateDecryptor_mD942306DEBF1BF2B91A2F3C2F31C2BB23665ABCA ();
// 0x0000002D System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::.ctor()
extern void PkzipClassicManaged__ctor_m0A74A32C0F8E9F76C33067B76A3C72DAA43DBB87 ();
// 0x0000002E System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::.ctor(System.String,System.Byte[],System.Int32,System.Boolean)
extern void ZipAESTransform__ctor_m2A182C1A5B7398743349BF4AEFCE5C5DDCAC31AE ();
// 0x0000002F System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void ZipAESTransform_TransformBlock_m42D466C307E6934BA535D7BA360744D9F6766CED ();
// 0x00000030 System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_PwdVerifier()
extern void ZipAESTransform_get_PwdVerifier_mEBD161CF07168ADA87DBFAF389F0FF33323D39A3 ();
// 0x00000031 System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern void ZipAESTransform_GetAuthCode_m8D0CD42A01688006A6822DFD663503F2986F6082 ();
// 0x00000032 System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void ZipAESTransform_TransformFinalBlock_m6F6357034E67183BC2F9AE07DF62EEF2234E78DC ();
// 0x00000033 System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_InputBlockSize()
extern void ZipAESTransform_get_InputBlockSize_m72A511861380B3F84C75BFA9D870A6F201EFC492 ();
// 0x00000034 System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_OutputBlockSize()
extern void ZipAESTransform_get_OutputBlockSize_mEE2473B756D74C62361081FAE874D3C5530FD86C ();
// 0x00000035 System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanTransformMultipleBlocks()
extern void ZipAESTransform_get_CanTransformMultipleBlocks_m8780A188A86CC3C37E8F38BFD0FA3A523E0D6748 ();
// 0x00000036 System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern void ZipAESTransform_Dispose_m1ADAB48AE0278A7FEF8D15C09E904C1901D17162 ();
// 0x00000037 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputStream__ctor_m7A0B8CF1B89F7AAA587E43902B00D0F0CD9E14B4 ();
// 0x00000038 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern void InflaterInputStream__ctor_mBA6C0A605EEDC63594AE34F3AB195C4FB3428362 ();
// 0x00000039 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Skip(System.Int64)
extern void InflaterInputStream_Skip_m2D1F24BE89DE39253FDAB352AB482BB6BF8F35ED ();
// 0x0000003A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::StopDecrypting()
extern void InflaterInputStream_StopDecrypting_mD8604E07265DC241FEEB7543D64307C7D7C3D919 ();
// 0x0000003B System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern void InflaterInputStream_Fill_m9025F05DD8228E9C8DAD5E3BD3A0564F54E33010 ();
// 0x0000003C System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern void InflaterInputStream_get_CanRead_m6DFB680876B5551D820013F4D053CD62290FE69D ();
// 0x0000003D System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern void InflaterInputStream_get_CanSeek_m6256711E09D964624559050DBCA206F0CFE7E908 ();
// 0x0000003E System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern void InflaterInputStream_get_CanWrite_m4EA744F64327A4D55F441DFD7926E80BF76C5012 ();
// 0x0000003F System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern void InflaterInputStream_get_Length_m8C99EF2EBB2DF8303A1901120FB4AFD1389954DA ();
// 0x00000040 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern void InflaterInputStream_get_Position_m0CD4DC13F05A44856E35E85A34B936A1FC6F1861 ();
// 0x00000041 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern void InflaterInputStream_set_Position_m7776B5CF49E242BC3236D1E59C808B7414A91DBF ();
// 0x00000042 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern void InflaterInputStream_Flush_m58B2872F55B3665E2C466732B25219C6EAE5E211 ();
// 0x00000043 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void InflaterInputStream_Seek_mA7A69C609AEAAC6E649ECF22C84881CED888D153 ();
// 0x00000044 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::SetLength(System.Int64)
extern void InflaterInputStream_SetLength_mA9FCCFC5ECE8ED62B1F0D9A31B233A1DC12B0FCF ();
// 0x00000045 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Write_m21D097DD1B5EA04A1BC4493FFC31F4BF64F660AF ();
// 0x00000046 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern void InflaterInputStream_WriteByte_mB7DB7062ED3F6C77B243664270ADA29B33DF0C51 ();
// 0x00000047 System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void InflaterInputStream_BeginWrite_mD292B6F5C1861263CB96717655DCA460FC564355 ();
// 0x00000048 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Close()
extern void InflaterInputStream_Close_mC187211EC951ABF0AE599CB63D6997CC8E594EC5 ();
// 0x00000049 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Read_m244E3D5FCF0FF553915B4E39FA1B04FF0CD98B12 ();
// 0x0000004A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater)
extern void DeflaterOutputStream__ctor_m838611ABEC2C087609493EBF1DE84543BBF7C8B0 ();
// 0x0000004B System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern void DeflaterOutputStream__ctor_m6A0C553A863CDD6D423D32D5488228E83CB95BB2 ();
// 0x0000004C System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern void DeflaterOutputStream_Finish_m483755F2DE8A192A1F0687DFF43505F117DB2DCF ();
// 0x0000004D System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanPatchEntries()
extern void DeflaterOutputStream_get_CanPatchEntries_m72631E3670E1FCA3CC59FB439373CE3CCD773A2E ();
// 0x0000004E System.String ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Password()
extern void DeflaterOutputStream_get_Password_mA2B30EB454F54FAB90AB1EDB2C8EDA720607B824 ();
// 0x0000004F System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_EncryptBlock_m2D0A41CA7D540C30AD9A03065A79281DBF2645D1 ();
// 0x00000050 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializePassword(System.String)
extern void DeflaterOutputStream_InitializePassword_m4FD5EE21BC3FA8D82BD0E938D9FA9A5C1AB88689 ();
// 0x00000051 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializeAESPassword(ICSharpCode.SharpZipLib.Zip.ZipEntry,System.String,System.Byte[]&,System.Byte[]&)
extern void DeflaterOutputStream_InitializeAESPassword_mCA745D4C3D7791404D910AE4ABFE62DA6735650E ();
// 0x00000052 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern void DeflaterOutputStream_Deflate_m6255CF7047C9F3712B99B6B98F62375ADA77E3ED ();
// 0x00000053 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern void DeflaterOutputStream_get_CanRead_mD8D1237CA7B88685DBDEA7D268E2B842C84C4575 ();
// 0x00000054 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern void DeflaterOutputStream_get_CanSeek_mE6029EA5BB162926975B6A14D7711C5A66EDD576 ();
// 0x00000055 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern void DeflaterOutputStream_get_CanWrite_mF0544F83449856CDE1F03C9D22898A63D91F8471 ();
// 0x00000056 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern void DeflaterOutputStream_get_Length_mBBF76A0901B0DDDEEF85237BF273926A973ECDCD ();
// 0x00000057 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern void DeflaterOutputStream_get_Position_m4633685728E787F044F12726EBAAB33E3C0CF797 ();
// 0x00000058 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern void DeflaterOutputStream_set_Position_mA92A01652E980FC743713D45B0D9D397DC009D23 ();
// 0x00000059 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DeflaterOutputStream_Seek_mA09D990066A3E5AC22C4F25A86748654711DCDDF ();
// 0x0000005A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::SetLength(System.Int64)
extern void DeflaterOutputStream_SetLength_m8407AE6F14037F0266E22BF9B9AE35C3F61CECB4 ();
// 0x0000005B System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern void DeflaterOutputStream_ReadByte_m7D55E2429DD72AF2131508B155FDFE1EA6BC1068 ();
// 0x0000005C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Read_m9E32EC3A3A7B1F25BD114ED425ED695ADCAFE0B3 ();
// 0x0000005D System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflaterOutputStream_BeginRead_m7461CD102A9E787F6A547A4EC4808290ED52AB02 ();
// 0x0000005E System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflaterOutputStream_BeginWrite_m9558DBD88D7BD1D6A8A782D755C29C00FA40077F ();
// 0x0000005F System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern void DeflaterOutputStream_Flush_m305DBB77E4416736AC8671E2B50E6B3931CE86F5 ();
// 0x00000060 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Close()
extern void DeflaterOutputStream_Close_mECBD690BCD5C8708EC78BFE3203C9C5CEAAE22D1 ();
// 0x00000061 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern void DeflaterOutputStream_GetAuthCodeIfAES_m65CC06A5112C1B0138DAB00522363D9CE86E6C1C ();
// 0x00000062 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern void DeflaterOutputStream_WriteByte_m3AD7D2E1A4E63CCAFD8A72DA353D83F05D3AA067 ();
// 0x00000063 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Write_m8E55308FA9336FC717D6FAE89C952BCB5F8E93D3 ();
// 0x00000064 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern void InflaterInputBuffer__ctor_m525BD6A3F651EDDE27AB329F5585BCD017103BAA ();
// 0x00000065 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern void InflaterInputBuffer_get_RawLength_m230B348BDBEE77844E76ADBAB974D261DEF23932 ();
// 0x00000066 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern void InflaterInputBuffer_get_Available_m0BE0B543527F2F91C579D4CA6FEB99109FE7A9EB ();
// 0x00000067 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern void InflaterInputBuffer_set_Available_mF43F9659FDF93A564BAFAF6D6F875A19CD7EFA8D ();
// 0x00000068 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputBuffer_SetInflaterInput_mAE734A06CB796CA37C8F2B54E830CA5C19AF048A ();
// 0x00000069 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern void InflaterInputBuffer_Fill_m7E8B6D4C01A70A3E64F8034362E18273D24DAD05 ();
// 0x0000006A System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[])
extern void InflaterInputBuffer_ReadRawBuffer_m85F7851FE8619044E8ADCF43412B6FFA265FC57E ();
// 0x0000006B System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadRawBuffer_m890C837A24835AE468DA811FE42AFB3C9FB779BD ();
// 0x0000006C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadClearTextBuffer_m0B4527F12A0E7B23BD0297666BE58BFFC2F007EA ();
// 0x0000006D System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern void InflaterInputBuffer_ReadLeByte_m3429ADFBA6BA6417F1CF21D6C5DD63CAFB59E512 ();
// 0x0000006E System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeShort()
extern void InflaterInputBuffer_ReadLeShort_m95374E29821087E737DCE1EDF8CD19CC31DB961C ();
// 0x0000006F System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeInt()
extern void InflaterInputBuffer_ReadLeInt_mB6E111FC3A6857CB4B3E65F9F9D66D304DB9E2C4 ();
// 0x00000070 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeLong()
extern void InflaterInputBuffer_ReadLeLong_mE493AA6D5BC4E742F6FAECACA1FB4052746EB73B ();
// 0x00000071 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_CryptoTransform(System.Security.Cryptography.ICryptoTransform)
extern void InflaterInputBuffer_set_CryptoTransform_m15B295ED7760B0DA8F4362C1F7D1D2BC368B19FB ();
// 0x00000072 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern void OutputWindow_Write_m5E32F25372BF2DC57607A8A614A76A4E6B2765B6 ();
// 0x00000073 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern void OutputWindow_SlowRepeat_m7250C3068C8FE843A4E1395ADF8BFD03EDD0D9D4 ();
// 0x00000074 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern void OutputWindow_Repeat_mC543AFECF22821CDD42ED4CE0542AAE34139BFB0 ();
// 0x00000075 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern void OutputWindow_CopyStored_m4A816F429A64E71535D7F2BD36BBD3E151865B40 ();
// 0x00000076 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern void OutputWindow_GetFreeSpace_mAF0D1A41321B1C930A3B016C13B91438BCBA4F72 ();
// 0x00000077 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern void OutputWindow_GetAvailable_mC9DC9C66CD82FBB7D57514B8BE814767A2C3ADC5 ();
// 0x00000078 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern void OutputWindow_CopyOutput_mD9FA38CEE42DCD4AA0C205329543768A68A37378 ();
// 0x00000079 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern void OutputWindow_Reset_m9753C61E2B3609F1426514FAE649F48723FC78B9 ();
// 0x0000007A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern void OutputWindow__ctor_m78D049C8870DE41C7D2AE530D9E8C47D40DAEC19 ();
// 0x0000007B System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern void StreamManipulator__ctor_mB563539A0C2C237DD0601F5926CE7EFB0F71EDEA ();
// 0x0000007C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern void StreamManipulator_PeekBits_mDA4F310774B17FD15E832157049A8F542ED66D51 ();
// 0x0000007D System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern void StreamManipulator_DropBits_m334C5A3695DFADD1EFE9A3912B698901A9F34B77 ();
// 0x0000007E System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern void StreamManipulator_get_AvailableBits_m6EE005776D4A342D616BE97BA52DD1AD9ABA7D27 ();
// 0x0000007F System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern void StreamManipulator_get_AvailableBytes_m973F1F85AB030771E5CA057E20A442A2B17A69AB ();
// 0x00000080 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern void StreamManipulator_SkipToByteBoundary_m894604E6C81FBDD57F43559AC5B24D7D769190C0 ();
// 0x00000081 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern void StreamManipulator_get_IsNeedingInput_m2B4FAB50AEE568B28065D4A992A52A660D3C83B3 ();
// 0x00000082 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_CopyBytes_mC805FC0E72694B3CF1CD49E4AB60926106934635 ();
// 0x00000083 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern void StreamManipulator_Reset_mB4BB692F3765647BE9E75E063D429CA089C3F59D ();
// 0x00000084 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_SetInput_m15B2CE3D9860109FFFA2613BA799B164F1D5563C ();
// 0x00000085 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern void Deflater__ctor_mE971BE9B1D73133CB84E41030C04AC295C2382A1 ();
// 0x00000086 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Reset()
extern void Deflater_Reset_m57B798FEA701F8A9E714F40CD46BC50B24B00FCA ();
// 0x00000087 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalOut()
extern void Deflater_get_TotalOut_m535406B6B823DA339D758538731E790C1FEB2105 ();
// 0x00000088 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Flush()
extern void Deflater_Flush_m0C3730DA58EAF1DF938B69AD9AC239567B3D86F3 ();
// 0x00000089 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Finish()
extern void Deflater_Finish_m793D7D6317D4351DD3EED33BECE894358FE6A844 ();
// 0x0000008A System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern void Deflater_get_IsFinished_mDCFB91947F9C4C560721F3376A3BE037BD0B3C03 ();
// 0x0000008B System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern void Deflater_get_IsNeedingInput_m2326764EEDDCC3351B1847A11CF32BB3021D18F6 ();
// 0x0000008C System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Deflater_SetInput_m8F97E03C93EFD3F1A6F3D4ED0996582A039B10DC ();
// 0x0000008D System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern void Deflater_SetLevel_mA24A6E9F5CDCD9B5913FD5175D29A52A8E247398 ();
// 0x0000008E System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetStrategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void Deflater_SetStrategy_mC86C5BA89191ACF5413BEBAF3EA69A58E08841C6 ();
// 0x0000008F System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern void Deflater_Deflate_m77264F823CAAC58A53B84C9D2C8F19E80F9EB93C ();
// 0x00000090 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.ctor()
extern void DeflaterConstants__ctor_m9438B1790FB475B367E88CB3C0310E51B509A474 ();
// 0x00000091 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
extern void DeflaterConstants__cctor_mDB4897650418D5592A8C35C79F4240E161BE2519 ();
// 0x00000092 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterEngine__ctor_mAF5FEDC2F7946B809C44F6DAF45914500AAFA269 ();
// 0x00000093 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern void DeflaterEngine_Deflate_m003B2F3399E3C11FD58923CB3576EC90C5B16B3E ();
// 0x00000094 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern void DeflaterEngine_SetInput_m5F7CF2358F0141CE198ABA217E17EC67135968F9 ();
// 0x00000095 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern void DeflaterEngine_NeedsInput_m1B66E907875D7F8A274E6E61F208BCD7B7128064 ();
// 0x00000096 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern void DeflaterEngine_Reset_m264BA96C332E2C6B1C09CDE5CD0DC7D9B447F06A ();
// 0x00000097 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern void DeflaterEngine_ResetAdler_m871358B35E8CA896F91EDA9001390AFD0F738EDE ();
// 0x00000098 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern void DeflaterEngine_get_Adler_m2ABC23912715704228AB6787EACA378D6B51DA9E ();
// 0x00000099 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void DeflaterEngine_set_Strategy_m3DD5A7D20489F4838379AA47540AD11784F3A7CF ();
// 0x0000009A System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern void DeflaterEngine_SetLevel_m9294D2E746EB603C17EA48B141F9FBB513E01942 ();
// 0x0000009B System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern void DeflaterEngine_FillWindow_mC1E21A940C497B22AFAF4C0D2FD1ADA3AE22D0B0 ();
// 0x0000009C System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern void DeflaterEngine_UpdateHash_mC78CFDD279D3D5D42020F58D9FCA3873B21D31F2 ();
// 0x0000009D System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern void DeflaterEngine_InsertString_mA91441CA28BD82A996404C6A63E8C54E825F6C14 ();
// 0x0000009E System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern void DeflaterEngine_SlideWindow_m5CC20BD3A719FDBDD771BE35B6111B683C4784D3 ();
// 0x0000009F System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern void DeflaterEngine_FindLongestMatch_m005978F9546BB28CC7E7B1FE7A42D5E42DCFF5E7 ();
// 0x000000A0 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateStored_m096821E07F37C14644438DBBEBB3C951ADB83E3C ();
// 0x000000A1 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateFast_m361266A64FC063EF60EBB99A4FE3D3D6B3592152 ();
// 0x000000A2 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateSlow_m210B7AA2677C4BFEBAA86B0F3C93846A2160AA98 ();
// 0x000000A3 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern void DeflaterHuffman__cctor_m3C01287466E7B9D500BCBE6A3FF091823E70F219 ();
// 0x000000A4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterHuffman__ctor_m1F7F8B25C569D87C8D9FA7813A3C73035BF7F8D0 ();
// 0x000000A5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern void DeflaterHuffman_Reset_mF5615EDE92439DFDD101E6A08F930F0CD60C9CBA ();
// 0x000000A6 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern void DeflaterHuffman_SendAllTrees_m6D2CDB6C89FDD90963061405F013FB2280E6CC9B ();
// 0x000000A7 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern void DeflaterHuffman_CompressBlock_mA0D6C9E932C11DD649A491B45D9E904D7A27EF07 ();
// 0x000000A8 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushStoredBlock_mF263AA576A8CC3A26A1C50ED2B61CA767DCE6D2B ();
// 0x000000A9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushBlock_m685CE0E5D13E203421971F55B33E085DD63BBDC7 ();
// 0x000000AA System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern void DeflaterHuffman_IsFull_mDF19AAC3126B1396019DDD90769CC5A1DCC65570 ();
// 0x000000AB System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern void DeflaterHuffman_TallyLit_m4433827C2643685F60EE4088D73E5EFF46F86229 ();
// 0x000000AC System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern void DeflaterHuffman_TallyDist_m9AC94F392F402FC35D47BEC532894E5A813E68DE ();
// 0x000000AD System.Int16 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern void DeflaterHuffman_BitReverse_mE1D7F1E65E4ED677F433B850E184472FA1B44D4C ();
// 0x000000AE System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern void DeflaterHuffman_Lcode_m93923F6137889A429BB0A9D0F52A3B118C4695E9 ();
// 0x000000AF System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern void DeflaterHuffman_Dcode_mCCEE4DC64DF5890AFC5C42F3B59C405EBF2AE584 ();
// 0x000000B0 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern void Tree__ctor_m65FEB84046E517386B03C5FDDA587DC8F6934A38 ();
// 0x000000B1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::Reset()
extern void Tree_Reset_m30DD681F4CB578BCD82355DC4A14A42288783341 ();
// 0x000000B2 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::WriteSymbol(System.Int32)
extern void Tree_WriteSymbol_mB6E29D2A7F41864A2BB06AB31D985A4CBAED6A67 ();
// 0x000000B3 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern void Tree_SetStaticCodes_mC3F5B09AC0AAE317230C96E37DFBD8C3088CF529 ();
// 0x000000B4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildCodes()
extern void Tree_BuildCodes_mEF630C0C5B56DF7B02C505CECDCA4FC9B694533C ();
// 0x000000B5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildTree()
extern void Tree_BuildTree_m415BF2A0444ED963B5AEB689F94E1BEB597D0FFE ();
// 0x000000B6 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::GetEncodedLength()
extern void Tree_GetEncodedLength_mBD87DBF45095B492BE15D05CF02A7FBD2086B699 ();
// 0x000000B7 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::CalcBLFreq(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree)
extern void Tree_CalcBLFreq_m15E11BB1A36A81F36F05B7A8DE8448BBC89ED0B7 ();
// 0x000000B8 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::WriteTree(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree)
extern void Tree_WriteTree_mA616BD216F0C532D6460DC2E6AFD300558F859DC ();
// 0x000000B9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildLength(System.Int32[])
extern void Tree_BuildLength_mDFF22789BC3CE893332671531F9B30181A479E6F ();
// 0x000000BA System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern void PendingBuffer__ctor_m64A8782110456673385D3DC248EA44F696EA7AB2 ();
// 0x000000BB System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern void PendingBuffer_Reset_m33A53BE87CD33E08CF59DCBA9653A2888C614321 ();
// 0x000000BC System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern void PendingBuffer_WriteShort_m89314CC8F49B8F37D966274B1C86B004A4B489F8 ();
// 0x000000BD System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_WriteBlock_mDF728CEA4EDF5337A08E3743C645D63969019212 ();
// 0x000000BE System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern void PendingBuffer_get_BitCount_m58FB62E9E330343A9F1103DA92B05C372CFDF0EF ();
// 0x000000BF System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern void PendingBuffer_AlignToByte_mAA13D99F88643D1186D7DFFE0BD45EF60E7F5D7F ();
// 0x000000C0 System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern void PendingBuffer_WriteBits_m5A8578A64EC1FCA1A22C70FFD1D5EACA835ED797 ();
// 0x000000C1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern void PendingBuffer_WriteShortMSB_m87BCD8B0B389E8B0EB44EA3996E27857505793D7 ();
// 0x000000C2 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern void PendingBuffer_get_IsFlushed_m36B004F008E08C2E18AC45F54D0022FF775EC10D ();
// 0x000000C3 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_Flush_m034A6C0BD1591036B9CEF08EA78E742A2788F385 ();
// 0x000000C4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
extern void DeflaterPending__ctor_m249F84D69E032622E112A5DD464652DDA6BA7703 ();
// 0x000000C5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern void Inflater__ctor_m29C22501C9213AB959A24D2D5FEF401F64CA75DB ();
// 0x000000C6 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern void Inflater_Reset_mC7BDBA76FF170FD49BEC5B621F428A720306672F ();
// 0x000000C7 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern void Inflater_DecodeHeader_m3F4EB87DE7B130F68B752789EDFF6446F9E3880D ();
// 0x000000C8 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern void Inflater_DecodeDict_m64A1E636D6506C177E182E9A12F26AE335D7CE23 ();
// 0x000000C9 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern void Inflater_DecodeHuffman_mDAF605F3B1D02961B74CC330F5971BFBDFA2A3D8 ();
// 0x000000CA System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern void Inflater_DecodeChksum_m551A0FF3F13BB14B10E99338A6E43ADA9521EE64 ();
// 0x000000CB System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern void Inflater_Decode_mD9C222A1D85757FBA6109972B64F1934F02B80F8 ();
// 0x000000CC System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Inflater_SetInput_m0DA71244CF1C1AB60C425AB271A6E16295B9B87E ();
// 0x000000CD System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern void Inflater_Inflate_m5DB8667224D0610AC5CE119F064C9677BCE99491 ();
// 0x000000CE System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern void Inflater_get_IsNeedingInput_mB3A7ECFB3C7F70B185B7FE07675824F3C3427B9D ();
// 0x000000CF System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern void Inflater_get_IsNeedingDictionary_mBEAAE55FB8DD2F00B3234776C9A142B88FB79296 ();
// 0x000000D0 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern void Inflater_get_IsFinished_m34F0416083458B241E26DC4EDED7EB44DC0CA2D6 ();
// 0x000000D1 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern void Inflater_get_TotalOut_m8D6F60473CA09F8635B89CAB44F9EF12427F315F ();
// 0x000000D2 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalIn()
extern void Inflater_get_TotalIn_m815E08D76ABA65AEC741690A9A2CFB5C00C35B79 ();
// 0x000000D3 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern void Inflater_get_RemainingInput_m3A538A144B21213A60BEE16A2B9DBC06CB98F365 ();
// 0x000000D4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern void Inflater__cctor_m174A596CFE4FC0BFA5134FDEDC4FBFA0ED4FB8AF ();
// 0x000000D5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor()
extern void InflaterDynHeader__ctor_mED8ED52EE32A76B4976586629CB26871EBFF5CBD ();
// 0x000000D6 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::Decode(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterDynHeader_Decode_m385C1489ED798EBE6C90E6F9635FAA23BC8DD384 ();
// 0x000000D7 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildLitLenTree()
extern void InflaterDynHeader_BuildLitLenTree_m354BF4BE41B1A836B18F0AD01BB5DBD8E29EDEDF ();
// 0x000000D8 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildDistTree()
extern void InflaterDynHeader_BuildDistTree_m22210C2D25AF902486E95AB0EB7B4CC0306AA2F0 ();
// 0x000000D9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern void InflaterDynHeader__cctor_mBD5C5EA4AFE8E51488CE11869D28F28B2662DAF0 ();
// 0x000000DA System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern void InflaterHuffmanTree__cctor_m5AE63092AA24DA0D9A327A8383A7ADA1AAADDF8A ();
// 0x000000DB System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Byte[])
extern void InflaterHuffmanTree__ctor_m11D5EEE9DDBEC930F50D1C32D8597736462F5500 ();
// 0x000000DC System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Byte[])
extern void InflaterHuffmanTree_BuildTree_m1588DED57308EFCDFA7E8C2693B1871D7C8D41BE ();
// 0x000000DD System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterHuffmanTree_GetSymbol_mD93C5D088C236F22DF0F3797A07EC93C6AFF61E8 ();
// 0x000000DE System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::get_DefaultCodePage()
extern void ZipConstants_get_DefaultCodePage_m0B3EBFFAC82693859B14D8031CDAC69C571AE9A9 ();
// 0x000000DF System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::set_DefaultCodePage(System.Int32)
extern void ZipConstants_set_DefaultCodePage_m12EFFFA731F0F8E5129D8CD0544FC9955D8013CD ();
// 0x000000E0 System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[],System.Int32)
extern void ZipConstants_ConvertToString_m6AC621A2BA16010D1F86D9BC05AE7FF554A08531 ();
// 0x000000E1 System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[])
extern void ZipConstants_ConvertToStringExt_m0BA52AA34D38CDA697400CE5AA61130A026AE350 ();
// 0x000000E2 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.String)
extern void ZipConstants_ConvertToArray_mF7060E169CE80A95E238CA438F484DFB453308F8 ();
// 0x000000E3 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.Int32,System.String)
extern void ZipConstants_ConvertToArray_mDD383A0C09CB3C5740402043B88A447444902045 ();
// 0x000000E4 System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::.cctor()
extern void ZipConstants__cctor_m79C3FD79D51FD83D6BF12BECAEED111E8C364DC7 ();
// 0x000000E5 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String)
extern void ZipEntry__ctor_m1FBF91E50F55B3F80B05F5115BBCF341EF66E516 ();
// 0x000000E6 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32)
extern void ZipEntry__ctor_m5FF62B23AB6C29F8ED3BA351C33D4EF4BCDB3448 ();
// 0x000000E7 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32,System.Int32,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry__ctor_mCE9B0653977F336A5BBBEF42901135DF1B7142FC ();
// 0x000000E8 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HasCrc()
extern void ZipEntry_get_HasCrc_m5B35C340FD7B38E6068BE5B04BA4FFD9CD6F46ED ();
// 0x000000E9 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsCrypted()
extern void ZipEntry_get_IsCrypted_mF8DFB1403914FC74B1B02C93A166472CC83DBF4D ();
// 0x000000EA System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_IsCrypted(System.Boolean)
extern void ZipEntry_set_IsCrypted_m754E1A345A841B0F09CC144D469731A3DF825C49 ();
// 0x000000EB System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CryptoCheckValue()
extern void ZipEntry_get_CryptoCheckValue_mA4AC0F78AEA8FFAF5D251471985D7BC64C15F750 ();
// 0x000000EC System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CryptoCheckValue(System.Byte)
extern void ZipEntry_set_CryptoCheckValue_mF5C0732FB7C8A07EB536D11E8F3F5E747DC7E6C8 ();
// 0x000000ED System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Flags()
extern void ZipEntry_get_Flags_m5783B86E73DF63A2B9013B6F3499C9EE286D1B42 ();
// 0x000000EE System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Flags(System.Int32)
extern void ZipEntry_set_Flags_mC2D85703534ED75330C3731BE586042D742B2DE7 ();
// 0x000000EF System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Offset()
extern void ZipEntry_get_Offset_mBF21B50E8C7EC1337903EB2D1E3F55F57FC6DB56 ();
// 0x000000F0 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Offset(System.Int64)
extern void ZipEntry_set_Offset_m3E5FA04B643FDA0DD5898C8D4ACA69CA377C94F0 ();
// 0x000000F1 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExternalFileAttributes()
extern void ZipEntry_get_ExternalFileAttributes_mB09CF894DEB1D48B735B00324FAE814C6A938151 ();
// 0x000000F2 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::HasDosAttributes(System.Int32)
extern void ZipEntry_HasDosAttributes_mD9A9651A9E64B605E55B975E61472A7749DE0FE9 ();
// 0x000000F3 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HostSystem()
extern void ZipEntry_get_HostSystem_m57BC10B87C88964D00DDF5CEC2D6C5E63A3405DB ();
// 0x000000F4 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Version()
extern void ZipEntry_get_Version_m7962573737AD1D6024E2FA9D780EF2857A918C8B ();
// 0x000000F5 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CanDecompress()
extern void ZipEntry_get_CanDecompress_m2CACDCE6FEA803E4E8F0B90005EF27FD41AAB9F2 ();
// 0x000000F6 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ForceZip64()
extern void ZipEntry_ForceZip64_m968528827ED97A648A3C2DEB5B4504515DB14C91 ();
// 0x000000F7 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsZip64Forced()
extern void ZipEntry_IsZip64Forced_mD7CDB08746CA13DDB71EF74566301287BA42CB91 ();
// 0x000000F8 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_LocalHeaderRequiresZip64()
extern void ZipEntry_get_LocalHeaderRequiresZip64_mE31175E19F78EFA6810D35F554F12EBF8D2F7183 ();
// 0x000000F9 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CentralHeaderRequiresZip64()
extern void ZipEntry_get_CentralHeaderRequiresZip64_m975B96F1AF3FA79610563666AFACAB6FC942E99E ();
// 0x000000FA System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_DosTime()
extern void ZipEntry_get_DosTime_mBB15AD296569841AEDE93599555C4429B38EF4A5 ();
// 0x000000FB System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DosTime(System.Int64)
extern void ZipEntry_set_DosTime_m51D154444E4631AB800FCC50D428DA4E430A5EF4 ();
// 0x000000FC System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DateTime(System.DateTime)
extern void ZipEntry_set_DateTime_m5CE3F03D5C42F2F722C6B123B707152780CAA030 ();
// 0x000000FD System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Name()
extern void ZipEntry_get_Name_m519B72C8F72CC545E80C14D4E846CBE5516D0F86 ();
// 0x000000FE System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Size()
extern void ZipEntry_get_Size_m30D5EDF566C26B1BBDB9185C628F7E3E88F7A192 ();
// 0x000000FF System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Size(System.Int64)
extern void ZipEntry_set_Size_m4D49CD9A9A265F7B9601090E7B59D897F706F661 ();
// 0x00000100 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressedSize()
extern void ZipEntry_get_CompressedSize_mE7A5492572E716DE60FC3676BC2B6046524D7960 ();
// 0x00000101 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressedSize(System.Int64)
extern void ZipEntry_set_CompressedSize_mE791F8902558736ACFC032D0CA3109360B115B1F ();
// 0x00000102 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Crc()
extern void ZipEntry_get_Crc_mB6D261EA47B12DA4A11C2CAB3075FE8778383E5D ();
// 0x00000103 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Crc(System.Int64)
extern void ZipEntry_set_Crc_m50134E4057A30C79DB6F2423C23EC542E0713784 ();
// 0x00000104 ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethod()
extern void ZipEntry_get_CompressionMethod_mF7793EC053E7A25619C0AE9727230326990DC8F3 ();
// 0x00000105 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressionMethod(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry_set_CompressionMethod_m1F67D10553E5A5607AFBE2CCA3BD47F3B8BB5BC5 ();
// 0x00000106 ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethodForHeader()
extern void ZipEntry_get_CompressionMethodForHeader_m94A815DC790E8B9A7C5D7457D8ABC4AB4BEDAC53 ();
// 0x00000107 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExtraData()
extern void ZipEntry_get_ExtraData_m14BEA84D9BB6E0B8EB105E29B267A0DD49813EFC ();
// 0x00000108 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExtraData(System.Byte[])
extern void ZipEntry_set_ExtraData_mB1FC8F62DF84F9B7CBC4683F47A1CD09EF1E915D ();
// 0x00000109 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESKeySize()
extern void ZipEntry_get_AESKeySize_m5342EC771286B4ECFA38BF75079927BFFC038760 ();
// 0x0000010A System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESEncryptionStrength()
extern void ZipEntry_get_AESEncryptionStrength_m59850F3045137DDC4378E1B741F6004F32A46FDB ();
// 0x0000010B System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESSaltLen()
extern void ZipEntry_get_AESSaltLen_m1393E8965C971D7CC03FCBDC88CB472B2D4F977C ();
// 0x0000010C System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESOverheadSize()
extern void ZipEntry_get_AESOverheadSize_mA07ED68E7948D384F3B0ECA0A4D156987CFFBE54 ();
// 0x0000010D System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessExtraData(System.Boolean)
extern void ZipEntry_ProcessExtraData_mE3224910D93C281207CE663A1C9D6EA611881DFD ();
// 0x0000010E System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessAESExtraData(ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern void ZipEntry_ProcessAESExtraData_mDF39206CB1DCB4DCA5EB8E465EB125AB50C5762C ();
// 0x0000010F System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Comment()
extern void ZipEntry_get_Comment_m6104FEC0A31A652A599026140B035A7C0ACDF2CB ();
// 0x00000110 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsDirectory()
extern void ZipEntry_get_IsDirectory_m5B496DDD3D2078ECD90B4F7B619C432DA7438C3A ();
// 0x00000111 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported()
extern void ZipEntry_IsCompressionMethodSupported_mD500F5DA8C1653A4F2BD0F4AAC19B0D0815F39FB ();
// 0x00000112 System.Object ICSharpCode.SharpZipLib.Zip.ZipEntry::Clone()
extern void ZipEntry_Clone_m198CB36B97D2478552E3D6732B3C839F4B51BEFC ();
// 0x00000113 System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::ToString()
extern void ZipEntry_ToString_m5E3C8AF7E5D5C83D50DF8AF183B1FF4F265CEA30 ();
// 0x00000114 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry_IsCompressionMethodSupported_m5A8428F9AFB4CF481331AA542A8CB8B6728FEFB2 ();
// 0x00000115 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ZipException__ctor_m3E92A85668B63748DB57CF33E0BBD309D001B9BF ();
// 0x00000116 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor()
extern void ZipException__ctor_mEB537EEF0EA2AEEA12E1A90E74FAC58A2106283A ();
// 0x00000117 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.String)
extern void ZipException__ctor_m1257789C8630BB0E637B2D313B620393EFDAA189 ();
// 0x00000118 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::.ctor(System.Byte[])
extern void ZipExtraData__ctor_mB00E115A0E97B8DD2DB44FED43A1EF5C346ED6CC ();
// 0x00000119 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipExtraData::GetEntryData()
extern void ZipExtraData_GetEntryData_m32462758C689977AFAB905F653366F133885DE77 ();
// 0x0000011A System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_Length()
extern void ZipExtraData_get_Length_mD73CAEB79D15EAF0C5429827E07C50EA17E34EB1 ();
// 0x0000011B System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_ValueLength()
extern void ZipExtraData_get_ValueLength_m180ADF9765CA99D00B82F18411AC47AE073C24F5 ();
// 0x0000011C System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_CurrentReadIndex()
extern void ZipExtraData_get_CurrentReadIndex_mA1360A66679C9C5CD104345F31DF3555BB7C1C07 ();
// 0x0000011D System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_UnreadCount()
extern void ZipExtraData_get_UnreadCount_m149100789C4A77713323A8B26B51FC9C29089B5C ();
// 0x0000011E System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Find(System.Int32)
extern void ZipExtraData_Find_mDB03F5C726ACA551FB0C359A0D04293F9F9B9E10 ();
// 0x0000011F System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddEntry(System.Int32,System.Byte[])
extern void ZipExtraData_AddEntry_mAB4FCCB3BDDF4875202D81E1F803ED71827D9AF7 ();
// 0x00000120 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::StartNewEntry()
extern void ZipExtraData_StartNewEntry_mAEA05553103EDAF478ECFD86F5996844F1AED47C ();
// 0x00000121 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddNewEntry(System.Int32)
extern void ZipExtraData_AddNewEntry_mA37F3B3D87792090149EBEE57B89D7F1C3FAB1BA ();
// 0x00000122 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddData(System.Byte)
extern void ZipExtraData_AddData_m77D3EBB1884D96368B94D120BB6F19696B7A7CF6 ();
// 0x00000123 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeShort(System.Int32)
extern void ZipExtraData_AddLeShort_m9C95A9766807ED6E5DB1A73EBB47A08D6EB052CD ();
// 0x00000124 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeInt(System.Int32)
extern void ZipExtraData_AddLeInt_m3E5AA03CB049742D659CACAF1BA96E595551D996 ();
// 0x00000125 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeLong(System.Int64)
extern void ZipExtraData_AddLeLong_m59AD2DFFEBD4019FD2FC20FB23131E998FFE41DC ();
// 0x00000126 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Delete(System.Int32)
extern void ZipExtraData_Delete_m66B717802890D0B7E39F81836A148443E50C45D3 ();
// 0x00000127 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadLong()
extern void ZipExtraData_ReadLong_m72829256D684F10DA52892F44A9B53CB0DB06753 ();
// 0x00000128 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadInt()
extern void ZipExtraData_ReadInt_mC80A215B4A689AD180C408FE467800705568143F ();
// 0x00000129 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShort()
extern void ZipExtraData_ReadShort_m2CB64CEA3DB460C9E88B3C473A3B76FF349CF738 ();
// 0x0000012A System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadByte()
extern void ZipExtraData_ReadByte_m47D5DE94B86E9F4D56AE61C2172C607DDA58E189 ();
// 0x0000012B System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Skip(System.Int32)
extern void ZipExtraData_Skip_m8661414EFAE706D9D7015A3E9A0034E4CE4F2E17 ();
// 0x0000012C System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadCheck(System.Int32)
extern void ZipExtraData_ReadCheck_mDAAF412ABF23D5017AAD8146A11B22E521471FB4 ();
// 0x0000012D System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShortInternal()
extern void ZipExtraData_ReadShortInternal_mD46AE749A78CC28D1F6BAF47954799001C63CACD ();
// 0x0000012E System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::SetShort(System.Int32&,System.Int32)
extern void ZipExtraData_SetShort_mA4549C1E4BDEA9AD90530A544633D790196E3EFA ();
// 0x0000012F System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Dispose()
extern void ZipExtraData_Dispose_m178E34E6C64B01B9A8226964D16C4686D94D294F ();
// 0x00000130 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::.ctor(System.IO.Stream)
extern void ZipHelperStream__ctor_m01E511518ACA8DB8C2A37FC0A62E7E566A4FBD60 ();
// 0x00000131 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanRead()
extern void ZipHelperStream_get_CanRead_mEF3C9C0EA05FC43777CBEB95CF6712185CA56913 ();
// 0x00000132 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanSeek()
extern void ZipHelperStream_get_CanSeek_m9CD01D16F947FF511751F77D799FB716572CCD51 ();
// 0x00000133 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Length()
extern void ZipHelperStream_get_Length_m8728E881725C8C49ECDC4D44FEC786942697904E ();
// 0x00000134 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Position()
extern void ZipHelperStream_get_Position_mF91154EEBC11F56F7E50843C62B853B216AA626F ();
// 0x00000135 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::set_Position(System.Int64)
extern void ZipHelperStream_set_Position_m83B6762324321E95FF7180961B863EE7999F4623 ();
// 0x00000136 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanWrite()
extern void ZipHelperStream_get_CanWrite_m03FCD2ACA3B1BE70EF10C269DFD2020D4664FE39 ();
// 0x00000137 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Flush()
extern void ZipHelperStream_Flush_m30CB88F89A7C5B47AE15BB4847A1D7CD2ADCD68E ();
// 0x00000138 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void ZipHelperStream_Seek_mDCE6D9DDAD246ADC544C834723EF15657B0F08EB ();
// 0x00000139 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::SetLength(System.Int64)
extern void ZipHelperStream_SetLength_mCEFB0E61E5E557EBF8B3F237D987C4812EC2A021 ();
// 0x0000013A System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ZipHelperStream_Read_mF5FBFEB1E90B167E6456AB7DA5B03ECE4C74B462 ();
// 0x0000013B System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ZipHelperStream_Write_m3F36559591359972B4446786095FE035FFC46A99 ();
// 0x0000013C System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Close()
extern void ZipHelperStream_Close_m90222C87FFA93CF7508F0A286CAF3325F3CEC2B0 ();
// 0x0000013D System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteZip64EndOfCentralDirectory(System.Int64,System.Int64,System.Int64)
extern void ZipHelperStream_WriteZip64EndOfCentralDirectory_m1A39566E17139E4642038BD4DAE21CDBA1801CAE ();
// 0x0000013E System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteEndOfCentralDirectory(System.Int64,System.Int64,System.Int64,System.Byte[])
extern void ZipHelperStream_WriteEndOfCentralDirectory_m5957AA2C43A8B257D63B77B6864C89033614C838 ();
// 0x0000013F System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEShort(System.Int32)
extern void ZipHelperStream_WriteLEShort_mE317DCD971DEE858CE5668FF11FD392852CFDD04 ();
// 0x00000140 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUshort(System.UInt16)
extern void ZipHelperStream_WriteLEUshort_mA1DE451DE6A87325481FC74D38023B5E5F07547F ();
// 0x00000141 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEInt(System.Int32)
extern void ZipHelperStream_WriteLEInt_m9310DE80714751BA6B9ADDC57DF7DFE63586D1D9 ();
// 0x00000142 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUint(System.UInt32)
extern void ZipHelperStream_WriteLEUint_m793623E06BC8906E9B1E69AC5B9251166A0D2459 ();
// 0x00000143 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLELong(System.Int64)
extern void ZipHelperStream_WriteLELong_m9E2CDA81328B345562168273EDE05A8D89795070 ();
// 0x00000144 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream)
extern void ZipInputStream__ctor_m88DFAA4AAC4A068D49EF74D16A055C157CFC7854 ();
// 0x00000145 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_CanDecompressEntry()
extern void ZipInputStream_get_CanDecompressEntry_m1984E72192FCBAE979CFC20AFEEEDBD933F0017C ();
// 0x00000146 ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipInputStream::GetNextEntry()
extern void ZipInputStream_GetNextEntry_mA848C115BC331C5B3A5A22AEF827C73B4A16B9E7 ();
// 0x00000147 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadDataDescriptor()
extern void ZipInputStream_ReadDataDescriptor_mB0F3D83C2BCDE84376FB3553F5F8C7CFBDA99978 ();
// 0x00000148 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CompleteCloseEntry(System.Boolean)
extern void ZipInputStream_CompleteCloseEntry_mD65397F839D1F923865CF971E21F26320F0568EB ();
// 0x00000149 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CloseEntry()
extern void ZipInputStream_CloseEntry_m30259ACA2F37EC6CF9FF17DD6ADF2A5AF1057140 ();
// 0x0000014A System.Int64 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Length()
extern void ZipInputStream_get_Length_m49B5C4B2D8451C1AA65A450A8F08566FAC64CF7D ();
// 0x0000014B System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadByte()
extern void ZipInputStream_ReadByte_m95A5488E5F2643A156E602E56EA44F874A8CF24F ();
// 0x0000014C System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotAvailable(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_ReadingNotAvailable_m71022E83E70B649475D0B61C6E3BF5F1DEABE3B1 ();
// 0x0000014D System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotSupported(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_ReadingNotSupported_m3CA5544B72E1BA3BA1AD6C738F2CF40D24C13DA8 ();
// 0x0000014E System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::InitialRead(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_InitialRead_mD3A14C9022A695B090934770CAC94B55AA6C3817 ();
// 0x0000014F System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_Read_m016044067C1A1564E50F20148F39165FD7F1FFA6 ();
// 0x00000150 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::BodyRead(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_BodyRead_m866CE6BFFBFF897F704CED9C6F7BCD436BD6363D ();
// 0x00000151 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::Close()
extern void ZipInputStream_Close_m5A0CE351D444D578F32BBC193092C701271922F6 ();
// 0x00000152 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::.ctor(System.Object,System.IntPtr)
extern void ReadDataHandler__ctor_m4E08E92133FEE6E3E72B670EF1F50C2ACA529D80 ();
// 0x00000153 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::Invoke(System.Byte[],System.Int32,System.Int32)
extern void ReadDataHandler_Invoke_m94D79659A48B33298A01ECB3137A0B465DB06273 ();
// 0x00000154 System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ReadDataHandler_BeginInvoke_m7CC8D4E4617B78573DAC466DD17201A20413D78A ();
// 0x00000155 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::EndInvoke(System.IAsyncResult)
extern void ReadDataHandler_EndInvoke_mF0C8C8F64AD42289C17BAAF1F43BACB3EC894D88 ();
// 0x00000156 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::.ctor(System.IO.Stream)
extern void ZipOutputStream__ctor_mEA0E84B4609AF3B71E4883FFA2313F3F6BC164FE ();
// 0x00000157 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::SetLevel(System.Int32)
extern void ZipOutputStream_SetLevel_m24F775142A7BACE7E2EE1DECA68E7D9EC241AFEB ();
// 0x00000158 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeShort(System.Int32)
extern void ZipOutputStream_WriteLeShort_m877E28DC5775FD07DED1B1DEE419EE472E11F9C6 ();
// 0x00000159 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeInt(System.Int32)
extern void ZipOutputStream_WriteLeInt_mE6CC1846AA1C2A0DCA3B03C3ED5DC1D2C5E2F5AD ();
// 0x0000015A System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeLong(System.Int64)
extern void ZipOutputStream_WriteLeLong_m3D23AB81EEAB33E5C49427AA01C06804AC9650F0 ();
// 0x0000015B System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::PutNextEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipOutputStream_PutNextEntry_mE0E53666BC30613697FC9ABADD4B2439EEF08A49 ();
// 0x0000015C System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CloseEntry()
extern void ZipOutputStream_CloseEntry_m0356C9ADB716C912326818E3C27ADA8D0CF7830F ();
// 0x0000015D System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteEncryptionHeader(System.Int64)
extern void ZipOutputStream_WriteEncryptionHeader_m5DE46285E9010585739001F273E0D3DFD5BAC999 ();
// 0x0000015E System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::AddExtraDataAES(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern void ZipOutputStream_AddExtraDataAES_m60DEA65AE3BE5767463CAC8932D73C708390443C ();
// 0x0000015F System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteAESHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipOutputStream_WriteAESHeader_m987074C70CCEF2ABB79595D49D9E350243E2305C ();
// 0x00000160 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ZipOutputStream_Write_m9A959CAA392EFC31829010AD152D9846406FCD61 ();
// 0x00000161 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CopyAndEncrypt(System.Byte[],System.Int32,System.Int32)
extern void ZipOutputStream_CopyAndEncrypt_m8030E1A9BCEB38060F936732D017EA8D4CA82F02 ();
// 0x00000162 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Finish()
extern void ZipOutputStream_Finish_m354372875E0EEAB9191256DD72CFF879FE0B8231 ();
static Il2CppMethodPointer s_methodPointers[354] = 
{
	SharpZipBaseException__ctor_m8F36A151FD10EFE442635BB2244679104C0E5CF2,
	SharpZipBaseException__ctor_mEE9958E80315BFB20886ACCB7DF5DAD2C95A56A8,
	SharpZipBaseException__ctor_m971F830DD87B19ECF2FBE767F8813F937E2AA599,
	Adler32_get_Value_m16A427A694A377A95659B20DAEEEDE63511012C9,
	Adler32__ctor_mBACA5EE3E7AF140146FC9CC5683A8486CD071721,
	Adler32_Reset_mC980D97C390BD301FBEC388D4F39C2C9126818FE,
	Adler32_Update_mBD8ED2935248F4A04C36CC87463A3B4C940C17CD,
	Crc32_ComputeCrc32_m6431C5BB7903E72DF906A7E8DC21A7E185F1381F,
	Crc32_get_Value_mEB6DE78E1DD4475B9529C661C6B22E8FAF54FD58,
	Crc32_Reset_mEE00F5C965353F4877FA72A872C4BDCC9F075D7A,
	Crc32_Update_m8CA7ACA9F8AAF87BAD859060E8AAF0B881BD0C3F,
	Crc32__ctor_mCA3D891CAAAF27B529FDBC3FC6DA460B4937CC54,
	Crc32__cctor_m0C89DDF5655F2D7FE6096D697AF8846CCD724C72,
	StreamUtils_Copy_m8B2E9C90C8F0E40D59D3F88F53C279A775BD1961,
	PkzipClassic_GenerateKeys_mA0E3014C60FB014C1B6CC955336276CDA29BD4F7,
	PkzipClassic__ctor_m8F0621735661F9786B07CADB31821F44360A545A,
	PkzipClassicCryptoBase_TransformByte_m65BDE065D910453F89EB06A565981323532245EC,
	PkzipClassicCryptoBase_SetKeys_m967738FEC93D2800551E6C72A079419DE4C33AC5,
	PkzipClassicCryptoBase_UpdateKeys_m837410955967DCC14839FEC820B7A94F64474EEC,
	PkzipClassicCryptoBase_Reset_m6E3DCCC4E78A24722EA2450FF0BEE89C9A0D4B63,
	PkzipClassicCryptoBase__ctor_m18FFEE76F02209557A46BEF0D3A2987B429BAD9F,
	PkzipClassicEncryptCryptoTransform__ctor_m1DD6A37A4B93D79958426D541DB50D39A6EEBD1E,
	PkzipClassicEncryptCryptoTransform_TransformFinalBlock_mB868E518CA5A17BFF5A8B246C2D64CB5A9DBD802,
	PkzipClassicEncryptCryptoTransform_TransformBlock_m365A519F41971D52E109F99D6169127B85AFAD62,
	PkzipClassicEncryptCryptoTransform_get_InputBlockSize_mAEF937206B50BB189DCFA595D35695BBC50C5D5F,
	PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_m3E950B4630EE51E9B1156A4AB3D1260202E69300,
	PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_m4C2D6E270A7485335428E218625AF8F69F57A20C,
	PkzipClassicEncryptCryptoTransform_Dispose_m62CEA6BDD034B46DC588DEEAC26704A5A17BC16B,
	PkzipClassicDecryptCryptoTransform__ctor_m61309229BB1098237AF24C5E0D17B7476916E981,
	PkzipClassicDecryptCryptoTransform_TransformFinalBlock_mAB2F29DBAEA2CCC9649DDB8E3B7573A1564183CF,
	PkzipClassicDecryptCryptoTransform_TransformBlock_m79CD1B6D5A84F58C343C39FED21736B0666168AE,
	PkzipClassicDecryptCryptoTransform_get_InputBlockSize_mB4E824012A26E8B1DE9C5E6051073643CD136AC7,
	PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_m57CD96B5D2D4041728C0EE3CC60469273BDEC7DA,
	PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_m4952EAABE981AB4F3BA6A1C91F6EEE5E8B3A1838,
	PkzipClassicDecryptCryptoTransform_Dispose_m4029757A4955371231790DF2E68609939BDDA6E8,
	PkzipClassicManaged_get_BlockSize_mDB8805986EDB66EB9FD4C952423DA56F3E4B1708,
	PkzipClassicManaged_set_BlockSize_m140D9E6AEBA9EEC6B0A274A5440DBC21E0AAEC5E,
	PkzipClassicManaged_get_LegalKeySizes_m478058C5D93CD270D0E9230E1CE14DBFA82F13D4,
	PkzipClassicManaged_GenerateIV_mBB8E77BA13AD242681625CCEC0A3DEDF66BFC722,
	PkzipClassicManaged_get_Key_m856B3A80419BF4641C5BE7124D9A8C62461759CF,
	PkzipClassicManaged_set_Key_mBFA2FD2272301471ECF6A910F778391115AEA54C,
	PkzipClassicManaged_GenerateKey_mE3B273E8C72F300FFF712D26AD19455B4E77B8F2,
	PkzipClassicManaged_CreateEncryptor_m44228C6A99F2DFA7EED29BFC9915D9BCB224404B,
	PkzipClassicManaged_CreateDecryptor_mD942306DEBF1BF2B91A2F3C2F31C2BB23665ABCA,
	PkzipClassicManaged__ctor_m0A74A32C0F8E9F76C33067B76A3C72DAA43DBB87,
	ZipAESTransform__ctor_m2A182C1A5B7398743349BF4AEFCE5C5DDCAC31AE,
	ZipAESTransform_TransformBlock_m42D466C307E6934BA535D7BA360744D9F6766CED,
	ZipAESTransform_get_PwdVerifier_mEBD161CF07168ADA87DBFAF389F0FF33323D39A3,
	ZipAESTransform_GetAuthCode_m8D0CD42A01688006A6822DFD663503F2986F6082,
	ZipAESTransform_TransformFinalBlock_m6F6357034E67183BC2F9AE07DF62EEF2234E78DC,
	ZipAESTransform_get_InputBlockSize_m72A511861380B3F84C75BFA9D870A6F201EFC492,
	ZipAESTransform_get_OutputBlockSize_mEE2473B756D74C62361081FAE874D3C5530FD86C,
	ZipAESTransform_get_CanTransformMultipleBlocks_m8780A188A86CC3C37E8F38BFD0FA3A523E0D6748,
	ZipAESTransform_Dispose_m1ADAB48AE0278A7FEF8D15C09E904C1901D17162,
	InflaterInputStream__ctor_m7A0B8CF1B89F7AAA587E43902B00D0F0CD9E14B4,
	InflaterInputStream__ctor_mBA6C0A605EEDC63594AE34F3AB195C4FB3428362,
	InflaterInputStream_Skip_m2D1F24BE89DE39253FDAB352AB482BB6BF8F35ED,
	InflaterInputStream_StopDecrypting_mD8604E07265DC241FEEB7543D64307C7D7C3D919,
	InflaterInputStream_Fill_m9025F05DD8228E9C8DAD5E3BD3A0564F54E33010,
	InflaterInputStream_get_CanRead_m6DFB680876B5551D820013F4D053CD62290FE69D,
	InflaterInputStream_get_CanSeek_m6256711E09D964624559050DBCA206F0CFE7E908,
	InflaterInputStream_get_CanWrite_m4EA744F64327A4D55F441DFD7926E80BF76C5012,
	InflaterInputStream_get_Length_m8C99EF2EBB2DF8303A1901120FB4AFD1389954DA,
	InflaterInputStream_get_Position_m0CD4DC13F05A44856E35E85A34B936A1FC6F1861,
	InflaterInputStream_set_Position_m7776B5CF49E242BC3236D1E59C808B7414A91DBF,
	InflaterInputStream_Flush_m58B2872F55B3665E2C466732B25219C6EAE5E211,
	InflaterInputStream_Seek_mA7A69C609AEAAC6E649ECF22C84881CED888D153,
	InflaterInputStream_SetLength_mA9FCCFC5ECE8ED62B1F0D9A31B233A1DC12B0FCF,
	InflaterInputStream_Write_m21D097DD1B5EA04A1BC4493FFC31F4BF64F660AF,
	InflaterInputStream_WriteByte_mB7DB7062ED3F6C77B243664270ADA29B33DF0C51,
	InflaterInputStream_BeginWrite_mD292B6F5C1861263CB96717655DCA460FC564355,
	InflaterInputStream_Close_mC187211EC951ABF0AE599CB63D6997CC8E594EC5,
	InflaterInputStream_Read_m244E3D5FCF0FF553915B4E39FA1B04FF0CD98B12,
	DeflaterOutputStream__ctor_m838611ABEC2C087609493EBF1DE84543BBF7C8B0,
	DeflaterOutputStream__ctor_m6A0C553A863CDD6D423D32D5488228E83CB95BB2,
	DeflaterOutputStream_Finish_m483755F2DE8A192A1F0687DFF43505F117DB2DCF,
	DeflaterOutputStream_get_CanPatchEntries_m72631E3670E1FCA3CC59FB439373CE3CCD773A2E,
	DeflaterOutputStream_get_Password_mA2B30EB454F54FAB90AB1EDB2C8EDA720607B824,
	DeflaterOutputStream_EncryptBlock_m2D0A41CA7D540C30AD9A03065A79281DBF2645D1,
	DeflaterOutputStream_InitializePassword_m4FD5EE21BC3FA8D82BD0E938D9FA9A5C1AB88689,
	DeflaterOutputStream_InitializeAESPassword_mCA745D4C3D7791404D910AE4ABFE62DA6735650E,
	DeflaterOutputStream_Deflate_m6255CF7047C9F3712B99B6B98F62375ADA77E3ED,
	DeflaterOutputStream_get_CanRead_mD8D1237CA7B88685DBDEA7D268E2B842C84C4575,
	DeflaterOutputStream_get_CanSeek_mE6029EA5BB162926975B6A14D7711C5A66EDD576,
	DeflaterOutputStream_get_CanWrite_mF0544F83449856CDE1F03C9D22898A63D91F8471,
	DeflaterOutputStream_get_Length_mBBF76A0901B0DDDEEF85237BF273926A973ECDCD,
	DeflaterOutputStream_get_Position_m4633685728E787F044F12726EBAAB33E3C0CF797,
	DeflaterOutputStream_set_Position_mA92A01652E980FC743713D45B0D9D397DC009D23,
	DeflaterOutputStream_Seek_mA09D990066A3E5AC22C4F25A86748654711DCDDF,
	DeflaterOutputStream_SetLength_m8407AE6F14037F0266E22BF9B9AE35C3F61CECB4,
	DeflaterOutputStream_ReadByte_m7D55E2429DD72AF2131508B155FDFE1EA6BC1068,
	DeflaterOutputStream_Read_m9E32EC3A3A7B1F25BD114ED425ED695ADCAFE0B3,
	DeflaterOutputStream_BeginRead_m7461CD102A9E787F6A547A4EC4808290ED52AB02,
	DeflaterOutputStream_BeginWrite_m9558DBD88D7BD1D6A8A782D755C29C00FA40077F,
	DeflaterOutputStream_Flush_m305DBB77E4416736AC8671E2B50E6B3931CE86F5,
	DeflaterOutputStream_Close_mECBD690BCD5C8708EC78BFE3203C9C5CEAAE22D1,
	DeflaterOutputStream_GetAuthCodeIfAES_m65CC06A5112C1B0138DAB00522363D9CE86E6C1C,
	DeflaterOutputStream_WriteByte_m3AD7D2E1A4E63CCAFD8A72DA353D83F05D3AA067,
	DeflaterOutputStream_Write_m8E55308FA9336FC717D6FAE89C952BCB5F8E93D3,
	InflaterInputBuffer__ctor_m525BD6A3F651EDDE27AB329F5585BCD017103BAA,
	InflaterInputBuffer_get_RawLength_m230B348BDBEE77844E76ADBAB974D261DEF23932,
	InflaterInputBuffer_get_Available_m0BE0B543527F2F91C579D4CA6FEB99109FE7A9EB,
	InflaterInputBuffer_set_Available_mF43F9659FDF93A564BAFAF6D6F875A19CD7EFA8D,
	InflaterInputBuffer_SetInflaterInput_mAE734A06CB796CA37C8F2B54E830CA5C19AF048A,
	InflaterInputBuffer_Fill_m7E8B6D4C01A70A3E64F8034362E18273D24DAD05,
	InflaterInputBuffer_ReadRawBuffer_m85F7851FE8619044E8ADCF43412B6FFA265FC57E,
	InflaterInputBuffer_ReadRawBuffer_m890C837A24835AE468DA811FE42AFB3C9FB779BD,
	InflaterInputBuffer_ReadClearTextBuffer_m0B4527F12A0E7B23BD0297666BE58BFFC2F007EA,
	InflaterInputBuffer_ReadLeByte_m3429ADFBA6BA6417F1CF21D6C5DD63CAFB59E512,
	InflaterInputBuffer_ReadLeShort_m95374E29821087E737DCE1EDF8CD19CC31DB961C,
	InflaterInputBuffer_ReadLeInt_mB6E111FC3A6857CB4B3E65F9F9D66D304DB9E2C4,
	InflaterInputBuffer_ReadLeLong_mE493AA6D5BC4E742F6FAECACA1FB4052746EB73B,
	InflaterInputBuffer_set_CryptoTransform_m15B295ED7760B0DA8F4362C1F7D1D2BC368B19FB,
	OutputWindow_Write_m5E32F25372BF2DC57607A8A614A76A4E6B2765B6,
	OutputWindow_SlowRepeat_m7250C3068C8FE843A4E1395ADF8BFD03EDD0D9D4,
	OutputWindow_Repeat_mC543AFECF22821CDD42ED4CE0542AAE34139BFB0,
	OutputWindow_CopyStored_m4A816F429A64E71535D7F2BD36BBD3E151865B40,
	OutputWindow_GetFreeSpace_mAF0D1A41321B1C930A3B016C13B91438BCBA4F72,
	OutputWindow_GetAvailable_mC9DC9C66CD82FBB7D57514B8BE814767A2C3ADC5,
	OutputWindow_CopyOutput_mD9FA38CEE42DCD4AA0C205329543768A68A37378,
	OutputWindow_Reset_m9753C61E2B3609F1426514FAE649F48723FC78B9,
	OutputWindow__ctor_m78D049C8870DE41C7D2AE530D9E8C47D40DAEC19,
	StreamManipulator__ctor_mB563539A0C2C237DD0601F5926CE7EFB0F71EDEA,
	StreamManipulator_PeekBits_mDA4F310774B17FD15E832157049A8F542ED66D51,
	StreamManipulator_DropBits_m334C5A3695DFADD1EFE9A3912B698901A9F34B77,
	StreamManipulator_get_AvailableBits_m6EE005776D4A342D616BE97BA52DD1AD9ABA7D27,
	StreamManipulator_get_AvailableBytes_m973F1F85AB030771E5CA057E20A442A2B17A69AB,
	StreamManipulator_SkipToByteBoundary_m894604E6C81FBDD57F43559AC5B24D7D769190C0,
	StreamManipulator_get_IsNeedingInput_m2B4FAB50AEE568B28065D4A992A52A660D3C83B3,
	StreamManipulator_CopyBytes_mC805FC0E72694B3CF1CD49E4AB60926106934635,
	StreamManipulator_Reset_mB4BB692F3765647BE9E75E063D429CA089C3F59D,
	StreamManipulator_SetInput_m15B2CE3D9860109FFFA2613BA799B164F1D5563C,
	Deflater__ctor_mE971BE9B1D73133CB84E41030C04AC295C2382A1,
	Deflater_Reset_m57B798FEA701F8A9E714F40CD46BC50B24B00FCA,
	Deflater_get_TotalOut_m535406B6B823DA339D758538731E790C1FEB2105,
	Deflater_Flush_m0C3730DA58EAF1DF938B69AD9AC239567B3D86F3,
	Deflater_Finish_m793D7D6317D4351DD3EED33BECE894358FE6A844,
	Deflater_get_IsFinished_mDCFB91947F9C4C560721F3376A3BE037BD0B3C03,
	Deflater_get_IsNeedingInput_m2326764EEDDCC3351B1847A11CF32BB3021D18F6,
	Deflater_SetInput_m8F97E03C93EFD3F1A6F3D4ED0996582A039B10DC,
	Deflater_SetLevel_mA24A6E9F5CDCD9B5913FD5175D29A52A8E247398,
	Deflater_SetStrategy_mC86C5BA89191ACF5413BEBAF3EA69A58E08841C6,
	Deflater_Deflate_m77264F823CAAC58A53B84C9D2C8F19E80F9EB93C,
	DeflaterConstants__ctor_m9438B1790FB475B367E88CB3C0310E51B509A474,
	DeflaterConstants__cctor_mDB4897650418D5592A8C35C79F4240E161BE2519,
	DeflaterEngine__ctor_mAF5FEDC2F7946B809C44F6DAF45914500AAFA269,
	DeflaterEngine_Deflate_m003B2F3399E3C11FD58923CB3576EC90C5B16B3E,
	DeflaterEngine_SetInput_m5F7CF2358F0141CE198ABA217E17EC67135968F9,
	DeflaterEngine_NeedsInput_m1B66E907875D7F8A274E6E61F208BCD7B7128064,
	DeflaterEngine_Reset_m264BA96C332E2C6B1C09CDE5CD0DC7D9B447F06A,
	DeflaterEngine_ResetAdler_m871358B35E8CA896F91EDA9001390AFD0F738EDE,
	DeflaterEngine_get_Adler_m2ABC23912715704228AB6787EACA378D6B51DA9E,
	DeflaterEngine_set_Strategy_m3DD5A7D20489F4838379AA47540AD11784F3A7CF,
	DeflaterEngine_SetLevel_m9294D2E746EB603C17EA48B141F9FBB513E01942,
	DeflaterEngine_FillWindow_mC1E21A940C497B22AFAF4C0D2FD1ADA3AE22D0B0,
	DeflaterEngine_UpdateHash_mC78CFDD279D3D5D42020F58D9FCA3873B21D31F2,
	DeflaterEngine_InsertString_mA91441CA28BD82A996404C6A63E8C54E825F6C14,
	DeflaterEngine_SlideWindow_m5CC20BD3A719FDBDD771BE35B6111B683C4784D3,
	DeflaterEngine_FindLongestMatch_m005978F9546BB28CC7E7B1FE7A42D5E42DCFF5E7,
	DeflaterEngine_DeflateStored_m096821E07F37C14644438DBBEBB3C951ADB83E3C,
	DeflaterEngine_DeflateFast_m361266A64FC063EF60EBB99A4FE3D3D6B3592152,
	DeflaterEngine_DeflateSlow_m210B7AA2677C4BFEBAA86B0F3C93846A2160AA98,
	DeflaterHuffman__cctor_m3C01287466E7B9D500BCBE6A3FF091823E70F219,
	DeflaterHuffman__ctor_m1F7F8B25C569D87C8D9FA7813A3C73035BF7F8D0,
	DeflaterHuffman_Reset_mF5615EDE92439DFDD101E6A08F930F0CD60C9CBA,
	DeflaterHuffman_SendAllTrees_m6D2CDB6C89FDD90963061405F013FB2280E6CC9B,
	DeflaterHuffman_CompressBlock_mA0D6C9E932C11DD649A491B45D9E904D7A27EF07,
	DeflaterHuffman_FlushStoredBlock_mF263AA576A8CC3A26A1C50ED2B61CA767DCE6D2B,
	DeflaterHuffman_FlushBlock_m685CE0E5D13E203421971F55B33E085DD63BBDC7,
	DeflaterHuffman_IsFull_mDF19AAC3126B1396019DDD90769CC5A1DCC65570,
	DeflaterHuffman_TallyLit_m4433827C2643685F60EE4088D73E5EFF46F86229,
	DeflaterHuffman_TallyDist_m9AC94F392F402FC35D47BEC532894E5A813E68DE,
	DeflaterHuffman_BitReverse_mE1D7F1E65E4ED677F433B850E184472FA1B44D4C,
	DeflaterHuffman_Lcode_m93923F6137889A429BB0A9D0F52A3B118C4695E9,
	DeflaterHuffman_Dcode_mCCEE4DC64DF5890AFC5C42F3B59C405EBF2AE584,
	Tree__ctor_m65FEB84046E517386B03C5FDDA587DC8F6934A38,
	Tree_Reset_m30DD681F4CB578BCD82355DC4A14A42288783341,
	Tree_WriteSymbol_mB6E29D2A7F41864A2BB06AB31D985A4CBAED6A67,
	Tree_SetStaticCodes_mC3F5B09AC0AAE317230C96E37DFBD8C3088CF529,
	Tree_BuildCodes_mEF630C0C5B56DF7B02C505CECDCA4FC9B694533C,
	Tree_BuildTree_m415BF2A0444ED963B5AEB689F94E1BEB597D0FFE,
	Tree_GetEncodedLength_mBD87DBF45095B492BE15D05CF02A7FBD2086B699,
	Tree_CalcBLFreq_m15E11BB1A36A81F36F05B7A8DE8448BBC89ED0B7,
	Tree_WriteTree_mA616BD216F0C532D6460DC2E6AFD300558F859DC,
	Tree_BuildLength_mDFF22789BC3CE893332671531F9B30181A479E6F,
	PendingBuffer__ctor_m64A8782110456673385D3DC248EA44F696EA7AB2,
	PendingBuffer_Reset_m33A53BE87CD33E08CF59DCBA9653A2888C614321,
	PendingBuffer_WriteShort_m89314CC8F49B8F37D966274B1C86B004A4B489F8,
	PendingBuffer_WriteBlock_mDF728CEA4EDF5337A08E3743C645D63969019212,
	PendingBuffer_get_BitCount_m58FB62E9E330343A9F1103DA92B05C372CFDF0EF,
	PendingBuffer_AlignToByte_mAA13D99F88643D1186D7DFFE0BD45EF60E7F5D7F,
	PendingBuffer_WriteBits_m5A8578A64EC1FCA1A22C70FFD1D5EACA835ED797,
	PendingBuffer_WriteShortMSB_m87BCD8B0B389E8B0EB44EA3996E27857505793D7,
	PendingBuffer_get_IsFlushed_m36B004F008E08C2E18AC45F54D0022FF775EC10D,
	PendingBuffer_Flush_m034A6C0BD1591036B9CEF08EA78E742A2788F385,
	DeflaterPending__ctor_m249F84D69E032622E112A5DD464652DDA6BA7703,
	Inflater__ctor_m29C22501C9213AB959A24D2D5FEF401F64CA75DB,
	Inflater_Reset_mC7BDBA76FF170FD49BEC5B621F428A720306672F,
	Inflater_DecodeHeader_m3F4EB87DE7B130F68B752789EDFF6446F9E3880D,
	Inflater_DecodeDict_m64A1E636D6506C177E182E9A12F26AE335D7CE23,
	Inflater_DecodeHuffman_mDAF605F3B1D02961B74CC330F5971BFBDFA2A3D8,
	Inflater_DecodeChksum_m551A0FF3F13BB14B10E99338A6E43ADA9521EE64,
	Inflater_Decode_mD9C222A1D85757FBA6109972B64F1934F02B80F8,
	Inflater_SetInput_m0DA71244CF1C1AB60C425AB271A6E16295B9B87E,
	Inflater_Inflate_m5DB8667224D0610AC5CE119F064C9677BCE99491,
	Inflater_get_IsNeedingInput_mB3A7ECFB3C7F70B185B7FE07675824F3C3427B9D,
	Inflater_get_IsNeedingDictionary_mBEAAE55FB8DD2F00B3234776C9A142B88FB79296,
	Inflater_get_IsFinished_m34F0416083458B241E26DC4EDED7EB44DC0CA2D6,
	Inflater_get_TotalOut_m8D6F60473CA09F8635B89CAB44F9EF12427F315F,
	Inflater_get_TotalIn_m815E08D76ABA65AEC741690A9A2CFB5C00C35B79,
	Inflater_get_RemainingInput_m3A538A144B21213A60BEE16A2B9DBC06CB98F365,
	Inflater__cctor_m174A596CFE4FC0BFA5134FDEDC4FBFA0ED4FB8AF,
	InflaterDynHeader__ctor_mED8ED52EE32A76B4976586629CB26871EBFF5CBD,
	InflaterDynHeader_Decode_m385C1489ED798EBE6C90E6F9635FAA23BC8DD384,
	InflaterDynHeader_BuildLitLenTree_m354BF4BE41B1A836B18F0AD01BB5DBD8E29EDEDF,
	InflaterDynHeader_BuildDistTree_m22210C2D25AF902486E95AB0EB7B4CC0306AA2F0,
	InflaterDynHeader__cctor_mBD5C5EA4AFE8E51488CE11869D28F28B2662DAF0,
	InflaterHuffmanTree__cctor_m5AE63092AA24DA0D9A327A8383A7ADA1AAADDF8A,
	InflaterHuffmanTree__ctor_m11D5EEE9DDBEC930F50D1C32D8597736462F5500,
	InflaterHuffmanTree_BuildTree_m1588DED57308EFCDFA7E8C2693B1871D7C8D41BE,
	InflaterHuffmanTree_GetSymbol_mD93C5D088C236F22DF0F3797A07EC93C6AFF61E8,
	ZipConstants_get_DefaultCodePage_m0B3EBFFAC82693859B14D8031CDAC69C571AE9A9,
	ZipConstants_set_DefaultCodePage_m12EFFFA731F0F8E5129D8CD0544FC9955D8013CD,
	ZipConstants_ConvertToString_m6AC621A2BA16010D1F86D9BC05AE7FF554A08531,
	ZipConstants_ConvertToStringExt_m0BA52AA34D38CDA697400CE5AA61130A026AE350,
	ZipConstants_ConvertToArray_mF7060E169CE80A95E238CA438F484DFB453308F8,
	ZipConstants_ConvertToArray_mDD383A0C09CB3C5740402043B88A447444902045,
	ZipConstants__cctor_m79C3FD79D51FD83D6BF12BECAEED111E8C364DC7,
	ZipEntry__ctor_m1FBF91E50F55B3F80B05F5115BBCF341EF66E516,
	ZipEntry__ctor_m5FF62B23AB6C29F8ED3BA351C33D4EF4BCDB3448,
	ZipEntry__ctor_mCE9B0653977F336A5BBBEF42901135DF1B7142FC,
	ZipEntry_get_HasCrc_m5B35C340FD7B38E6068BE5B04BA4FFD9CD6F46ED,
	ZipEntry_get_IsCrypted_mF8DFB1403914FC74B1B02C93A166472CC83DBF4D,
	ZipEntry_set_IsCrypted_m754E1A345A841B0F09CC144D469731A3DF825C49,
	ZipEntry_get_CryptoCheckValue_mA4AC0F78AEA8FFAF5D251471985D7BC64C15F750,
	ZipEntry_set_CryptoCheckValue_mF5C0732FB7C8A07EB536D11E8F3F5E747DC7E6C8,
	ZipEntry_get_Flags_m5783B86E73DF63A2B9013B6F3499C9EE286D1B42,
	ZipEntry_set_Flags_mC2D85703534ED75330C3731BE586042D742B2DE7,
	ZipEntry_get_Offset_mBF21B50E8C7EC1337903EB2D1E3F55F57FC6DB56,
	ZipEntry_set_Offset_m3E5FA04B643FDA0DD5898C8D4ACA69CA377C94F0,
	ZipEntry_get_ExternalFileAttributes_mB09CF894DEB1D48B735B00324FAE814C6A938151,
	ZipEntry_HasDosAttributes_mD9A9651A9E64B605E55B975E61472A7749DE0FE9,
	ZipEntry_get_HostSystem_m57BC10B87C88964D00DDF5CEC2D6C5E63A3405DB,
	ZipEntry_get_Version_m7962573737AD1D6024E2FA9D780EF2857A918C8B,
	ZipEntry_get_CanDecompress_m2CACDCE6FEA803E4E8F0B90005EF27FD41AAB9F2,
	ZipEntry_ForceZip64_m968528827ED97A648A3C2DEB5B4504515DB14C91,
	ZipEntry_IsZip64Forced_mD7CDB08746CA13DDB71EF74566301287BA42CB91,
	ZipEntry_get_LocalHeaderRequiresZip64_mE31175E19F78EFA6810D35F554F12EBF8D2F7183,
	ZipEntry_get_CentralHeaderRequiresZip64_m975B96F1AF3FA79610563666AFACAB6FC942E99E,
	ZipEntry_get_DosTime_mBB15AD296569841AEDE93599555C4429B38EF4A5,
	ZipEntry_set_DosTime_m51D154444E4631AB800FCC50D428DA4E430A5EF4,
	ZipEntry_set_DateTime_m5CE3F03D5C42F2F722C6B123B707152780CAA030,
	ZipEntry_get_Name_m519B72C8F72CC545E80C14D4E846CBE5516D0F86,
	ZipEntry_get_Size_m30D5EDF566C26B1BBDB9185C628F7E3E88F7A192,
	ZipEntry_set_Size_m4D49CD9A9A265F7B9601090E7B59D897F706F661,
	ZipEntry_get_CompressedSize_mE7A5492572E716DE60FC3676BC2B6046524D7960,
	ZipEntry_set_CompressedSize_mE791F8902558736ACFC032D0CA3109360B115B1F,
	ZipEntry_get_Crc_mB6D261EA47B12DA4A11C2CAB3075FE8778383E5D,
	ZipEntry_set_Crc_m50134E4057A30C79DB6F2423C23EC542E0713784,
	ZipEntry_get_CompressionMethod_mF7793EC053E7A25619C0AE9727230326990DC8F3,
	ZipEntry_set_CompressionMethod_m1F67D10553E5A5607AFBE2CCA3BD47F3B8BB5BC5,
	ZipEntry_get_CompressionMethodForHeader_m94A815DC790E8B9A7C5D7457D8ABC4AB4BEDAC53,
	ZipEntry_get_ExtraData_m14BEA84D9BB6E0B8EB105E29B267A0DD49813EFC,
	ZipEntry_set_ExtraData_mB1FC8F62DF84F9B7CBC4683F47A1CD09EF1E915D,
	ZipEntry_get_AESKeySize_m5342EC771286B4ECFA38BF75079927BFFC038760,
	ZipEntry_get_AESEncryptionStrength_m59850F3045137DDC4378E1B741F6004F32A46FDB,
	ZipEntry_get_AESSaltLen_m1393E8965C971D7CC03FCBDC88CB472B2D4F977C,
	ZipEntry_get_AESOverheadSize_mA07ED68E7948D384F3B0ECA0A4D156987CFFBE54,
	ZipEntry_ProcessExtraData_mE3224910D93C281207CE663A1C9D6EA611881DFD,
	ZipEntry_ProcessAESExtraData_mDF39206CB1DCB4DCA5EB8E465EB125AB50C5762C,
	ZipEntry_get_Comment_m6104FEC0A31A652A599026140B035A7C0ACDF2CB,
	ZipEntry_get_IsDirectory_m5B496DDD3D2078ECD90B4F7B619C432DA7438C3A,
	ZipEntry_IsCompressionMethodSupported_mD500F5DA8C1653A4F2BD0F4AAC19B0D0815F39FB,
	ZipEntry_Clone_m198CB36B97D2478552E3D6732B3C839F4B51BEFC,
	ZipEntry_ToString_m5E3C8AF7E5D5C83D50DF8AF183B1FF4F265CEA30,
	ZipEntry_IsCompressionMethodSupported_m5A8428F9AFB4CF481331AA542A8CB8B6728FEFB2,
	ZipException__ctor_m3E92A85668B63748DB57CF33E0BBD309D001B9BF,
	ZipException__ctor_mEB537EEF0EA2AEEA12E1A90E74FAC58A2106283A,
	ZipException__ctor_m1257789C8630BB0E637B2D313B620393EFDAA189,
	ZipExtraData__ctor_mB00E115A0E97B8DD2DB44FED43A1EF5C346ED6CC,
	ZipExtraData_GetEntryData_m32462758C689977AFAB905F653366F133885DE77,
	ZipExtraData_get_Length_mD73CAEB79D15EAF0C5429827E07C50EA17E34EB1,
	ZipExtraData_get_ValueLength_m180ADF9765CA99D00B82F18411AC47AE073C24F5,
	ZipExtraData_get_CurrentReadIndex_mA1360A66679C9C5CD104345F31DF3555BB7C1C07,
	ZipExtraData_get_UnreadCount_m149100789C4A77713323A8B26B51FC9C29089B5C,
	ZipExtraData_Find_mDB03F5C726ACA551FB0C359A0D04293F9F9B9E10,
	ZipExtraData_AddEntry_mAB4FCCB3BDDF4875202D81E1F803ED71827D9AF7,
	ZipExtraData_StartNewEntry_mAEA05553103EDAF478ECFD86F5996844F1AED47C,
	ZipExtraData_AddNewEntry_mA37F3B3D87792090149EBEE57B89D7F1C3FAB1BA,
	ZipExtraData_AddData_m77D3EBB1884D96368B94D120BB6F19696B7A7CF6,
	ZipExtraData_AddLeShort_m9C95A9766807ED6E5DB1A73EBB47A08D6EB052CD,
	ZipExtraData_AddLeInt_m3E5AA03CB049742D659CACAF1BA96E595551D996,
	ZipExtraData_AddLeLong_m59AD2DFFEBD4019FD2FC20FB23131E998FFE41DC,
	ZipExtraData_Delete_m66B717802890D0B7E39F81836A148443E50C45D3,
	ZipExtraData_ReadLong_m72829256D684F10DA52892F44A9B53CB0DB06753,
	ZipExtraData_ReadInt_mC80A215B4A689AD180C408FE467800705568143F,
	ZipExtraData_ReadShort_m2CB64CEA3DB460C9E88B3C473A3B76FF349CF738,
	ZipExtraData_ReadByte_m47D5DE94B86E9F4D56AE61C2172C607DDA58E189,
	ZipExtraData_Skip_m8661414EFAE706D9D7015A3E9A0034E4CE4F2E17,
	ZipExtraData_ReadCheck_mDAAF412ABF23D5017AAD8146A11B22E521471FB4,
	ZipExtraData_ReadShortInternal_mD46AE749A78CC28D1F6BAF47954799001C63CACD,
	ZipExtraData_SetShort_mA4549C1E4BDEA9AD90530A544633D790196E3EFA,
	ZipExtraData_Dispose_m178E34E6C64B01B9A8226964D16C4686D94D294F,
	ZipHelperStream__ctor_m01E511518ACA8DB8C2A37FC0A62E7E566A4FBD60,
	ZipHelperStream_get_CanRead_mEF3C9C0EA05FC43777CBEB95CF6712185CA56913,
	ZipHelperStream_get_CanSeek_m9CD01D16F947FF511751F77D799FB716572CCD51,
	ZipHelperStream_get_Length_m8728E881725C8C49ECDC4D44FEC786942697904E,
	ZipHelperStream_get_Position_mF91154EEBC11F56F7E50843C62B853B216AA626F,
	ZipHelperStream_set_Position_m83B6762324321E95FF7180961B863EE7999F4623,
	ZipHelperStream_get_CanWrite_m03FCD2ACA3B1BE70EF10C269DFD2020D4664FE39,
	ZipHelperStream_Flush_m30CB88F89A7C5B47AE15BB4847A1D7CD2ADCD68E,
	ZipHelperStream_Seek_mDCE6D9DDAD246ADC544C834723EF15657B0F08EB,
	ZipHelperStream_SetLength_mCEFB0E61E5E557EBF8B3F237D987C4812EC2A021,
	ZipHelperStream_Read_mF5FBFEB1E90B167E6456AB7DA5B03ECE4C74B462,
	ZipHelperStream_Write_m3F36559591359972B4446786095FE035FFC46A99,
	ZipHelperStream_Close_m90222C87FFA93CF7508F0A286CAF3325F3CEC2B0,
	ZipHelperStream_WriteZip64EndOfCentralDirectory_m1A39566E17139E4642038BD4DAE21CDBA1801CAE,
	ZipHelperStream_WriteEndOfCentralDirectory_m5957AA2C43A8B257D63B77B6864C89033614C838,
	ZipHelperStream_WriteLEShort_mE317DCD971DEE858CE5668FF11FD392852CFDD04,
	ZipHelperStream_WriteLEUshort_mA1DE451DE6A87325481FC74D38023B5E5F07547F,
	ZipHelperStream_WriteLEInt_m9310DE80714751BA6B9ADDC57DF7DFE63586D1D9,
	ZipHelperStream_WriteLEUint_m793623E06BC8906E9B1E69AC5B9251166A0D2459,
	ZipHelperStream_WriteLELong_m9E2CDA81328B345562168273EDE05A8D89795070,
	ZipInputStream__ctor_m88DFAA4AAC4A068D49EF74D16A055C157CFC7854,
	ZipInputStream_get_CanDecompressEntry_m1984E72192FCBAE979CFC20AFEEEDBD933F0017C,
	ZipInputStream_GetNextEntry_mA848C115BC331C5B3A5A22AEF827C73B4A16B9E7,
	ZipInputStream_ReadDataDescriptor_mB0F3D83C2BCDE84376FB3553F5F8C7CFBDA99978,
	ZipInputStream_CompleteCloseEntry_mD65397F839D1F923865CF971E21F26320F0568EB,
	ZipInputStream_CloseEntry_m30259ACA2F37EC6CF9FF17DD6ADF2A5AF1057140,
	ZipInputStream_get_Length_m49B5C4B2D8451C1AA65A450A8F08566FAC64CF7D,
	ZipInputStream_ReadByte_m95A5488E5F2643A156E602E56EA44F874A8CF24F,
	ZipInputStream_ReadingNotAvailable_m71022E83E70B649475D0B61C6E3BF5F1DEABE3B1,
	ZipInputStream_ReadingNotSupported_m3CA5544B72E1BA3BA1AD6C738F2CF40D24C13DA8,
	ZipInputStream_InitialRead_mD3A14C9022A695B090934770CAC94B55AA6C3817,
	ZipInputStream_Read_m016044067C1A1564E50F20148F39165FD7F1FFA6,
	ZipInputStream_BodyRead_m866CE6BFFBFF897F704CED9C6F7BCD436BD6363D,
	ZipInputStream_Close_m5A0CE351D444D578F32BBC193092C701271922F6,
	ReadDataHandler__ctor_m4E08E92133FEE6E3E72B670EF1F50C2ACA529D80,
	ReadDataHandler_Invoke_m94D79659A48B33298A01ECB3137A0B465DB06273,
	ReadDataHandler_BeginInvoke_m7CC8D4E4617B78573DAC466DD17201A20413D78A,
	ReadDataHandler_EndInvoke_mF0C8C8F64AD42289C17BAAF1F43BACB3EC894D88,
	ZipOutputStream__ctor_mEA0E84B4609AF3B71E4883FFA2313F3F6BC164FE,
	ZipOutputStream_SetLevel_m24F775142A7BACE7E2EE1DECA68E7D9EC241AFEB,
	ZipOutputStream_WriteLeShort_m877E28DC5775FD07DED1B1DEE419EE472E11F9C6,
	ZipOutputStream_WriteLeInt_mE6CC1846AA1C2A0DCA3B03C3ED5DC1D2C5E2F5AD,
	ZipOutputStream_WriteLeLong_m3D23AB81EEAB33E5C49427AA01C06804AC9650F0,
	ZipOutputStream_PutNextEntry_mE0E53666BC30613697FC9ABADD4B2439EEF08A49,
	ZipOutputStream_CloseEntry_m0356C9ADB716C912326818E3C27ADA8D0CF7830F,
	ZipOutputStream_WriteEncryptionHeader_m5DE46285E9010585739001F273E0D3DFD5BAC999,
	ZipOutputStream_AddExtraDataAES_m60DEA65AE3BE5767463CAC8932D73C708390443C,
	ZipOutputStream_WriteAESHeader_m987074C70CCEF2ABB79595D49D9E350243E2305C,
	ZipOutputStream_Write_m9A959CAA392EFC31829010AD152D9846406FCD61,
	ZipOutputStream_CopyAndEncrypt_m8030E1A9BCEB38060F936732D017EA8D4CA82F02,
	ZipOutputStream_Finish_m354372875E0EEAB9191256DD72CFF879FE0B8231,
};
static const int32_t s_InvokerIndices[354] = 
{
	120,
	23,
	26,
	186,
	23,
	23,
	35,
	301,
	186,
	23,
	35,
	23,
	3,
	200,
	0,
	23,
	89,
	26,
	31,
	23,
	23,
	26,
	54,
	122,
	10,
	10,
	89,
	23,
	26,
	54,
	122,
	10,
	10,
	89,
	23,
	10,
	32,
	14,
	23,
	14,
	26,
	23,
	114,
	114,
	23,
	746,
	122,
	14,
	14,
	54,
	10,
	10,
	89,
	23,
	27,
	110,
	981,
	23,
	23,
	89,
	89,
	89,
	186,
	186,
	213,
	23,
	737,
	213,
	35,
	31,
	729,
	23,
	503,
	27,
	110,
	23,
	89,
	14,
	35,
	26,
	577,
	23,
	89,
	89,
	89,
	186,
	186,
	213,
	737,
	213,
	10,
	503,
	729,
	729,
	23,
	23,
	23,
	31,
	35,
	138,
	10,
	10,
	32,
	26,
	23,
	121,
	503,
	503,
	10,
	10,
	10,
	186,
	26,
	32,
	38,
	137,
	502,
	10,
	10,
	503,
	23,
	23,
	23,
	37,
	32,
	10,
	10,
	23,
	89,
	503,
	23,
	35,
	140,
	23,
	186,
	23,
	23,
	89,
	89,
	35,
	32,
	32,
	503,
	23,
	3,
	26,
	657,
	35,
	89,
	23,
	23,
	10,
	32,
	32,
	23,
	23,
	10,
	23,
	30,
	657,
	657,
	657,
	3,
	26,
	23,
	32,
	23,
	1043,
	1043,
	89,
	30,
	52,
	225,
	21,
	21,
	206,
	23,
	32,
	27,
	23,
	23,
	10,
	26,
	26,
	26,
	32,
	23,
	32,
	35,
	10,
	23,
	137,
	32,
	89,
	503,
	23,
	31,
	23,
	89,
	89,
	89,
	89,
	89,
	35,
	503,
	89,
	89,
	89,
	186,
	186,
	10,
	3,
	23,
	9,
	14,
	14,
	3,
	3,
	26,
	26,
	121,
	115,
	178,
	124,
	139,
	0,
	139,
	3,
	26,
	138,
	206,
	89,
	89,
	31,
	89,
	31,
	10,
	32,
	186,
	213,
	10,
	30,
	10,
	10,
	89,
	23,
	89,
	89,
	89,
	186,
	213,
	329,
	14,
	186,
	213,
	186,
	213,
	186,
	213,
	10,
	32,
	10,
	14,
	26,
	10,
	89,
	10,
	10,
	31,
	26,
	14,
	89,
	89,
	14,
	14,
	46,
	120,
	23,
	26,
	26,
	14,
	10,
	10,
	10,
	10,
	30,
	62,
	23,
	32,
	31,
	32,
	32,
	213,
	30,
	186,
	10,
	10,
	10,
	32,
	32,
	10,
	64,
	23,
	26,
	89,
	89,
	186,
	186,
	213,
	89,
	23,
	737,
	213,
	503,
	35,
	23,
	2445,
	2446,
	32,
	602,
	32,
	32,
	213,
	26,
	89,
	14,
	23,
	31,
	23,
	186,
	10,
	503,
	503,
	503,
	503,
	503,
	23,
	132,
	503,
	729,
	121,
	26,
	32,
	32,
	32,
	213,
	26,
	23,
	213,
	143,
	26,
	35,
	35,
	23,
};
extern const Il2CppCodeGenModule g_ICSharpCode_SharpZipLibCodeGenModule;
const Il2CppCodeGenModule g_ICSharpCode_SharpZipLibCodeGenModule = 
{
	"ICSharpCode.SharpZipLib.dll",
	354,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
